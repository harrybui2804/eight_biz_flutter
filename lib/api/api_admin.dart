import 'dart:convert';
import 'package:eight_biz_flutter/api/api_url.dart';
import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/models/biz_manager.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/biz_role.dart';
import 'package:eight_biz_flutter/response/res_add_manager.dart';
import 'package:eight_biz_flutter/response/res_create_role.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_get_manager.dart';
import 'package:eight_biz_flutter/response/res_managers.dart';
import 'package:eight_biz_flutter/response/res_model.dart';
import 'package:eight_biz_flutter/response/res_role_detail.dart';
import 'package:eight_biz_flutter/response/res_roles.dart';
import 'package:eight_biz_flutter/response/res_success.dart';
import 'package:eight_biz_flutter/utils/string_utils.dart';

import 'api_client.dart';

class ApiAdmin extends ApiClient {
  
  Future<ResModel> loadListManagers(String businessId, String next) async {
    try {
      var url = next != null ? removeBaseUrl(AppConfig().apiBaseUrl, next) : ApiUrl.bizManager;
      url = url.replaceAll('{business_id}', businessId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        final list = data['results'] as List;
        return ResManagers(
          count: data['count'] as int,
          next: data['next'],
          results: list.map((obj) {
            return BizManager(
              id: obj['id'],
              role: obj['role'],
              roleId: obj['role__id'],
              isNotification: obj['is_notification'],
              isActive: obj['is_active'],
              user: UserManager(
                id: obj['user'] != null ? obj['user']['id'] : null,
                firstName: obj['user'] != null ? obj['user']['first_name'] : null,
                lastName: obj['user'] != null ? obj['user']['last_name'] : null,
                customerCode: obj['user'] != null ? obj['user']['customer_code'] : null,
                email: obj['user'] != null ? obj['user']['email'] : null,
                phone: obj['user'] != null ? obj['user']['phone'] : null,
              )
            );
          }).toList()
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('loadListManagers $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> loadManagerDetail(String businessId, String managerId) async {
    try {
      final url = ApiUrl.bizManagerDetail.replaceAll('{business_id}', businessId).replaceAll('{manager_id}', managerId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        var user = data['user'] ?? {};
        var pers = data['list_permission'] ?? [];
        return ResGetManager(
          id: managerId,
          isActive: data['is_active'],
          isNotification: data['is_notification'],
          isOwner: data['is_owner'],
          role: data['role'],
          user: UserManager(
            phone: user['phone'],
            email: user['email'],
            firstName: user['first_name'],
            lastName: user['last_name']
          ),
          permissions: (pers as List).map((per){
            return BusinessPermision.fromJson(per);
          }).toList()
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('loadManagerDetail $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> loadListRoles(String businessId) async {
    try {
      final url = ApiUrl.rolesUrl.replaceAll('{business_id}', businessId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        final list = data as List;
        return ResRoles(
          roles: list.map((role) {
            return BizRole(
              id: role['id'],
              role: role['role']
            );
          }).toList()
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> loadRoleDetail(String businessId, BizRole role) async {
    try {
      final url = ApiUrl.roleDetailUrl.replaceAll('{business_id}', businessId).replaceAll('{role_id}', role.id);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        final permissions = data['list_permissions'] as List;
        return ResRoleDetail(
          role: role,
          permissions: permissions.map((per){
            return BusinessPermision.fromJson(per);
          }).toList()
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> addRole(String businessId, String role, List<BusinessPermision> permissions) async {
    try {
      final url = ApiUrl.rolesUrl.replaceAll('{business_id}', businessId);
      final body = jsonEncode({
        "role": role,
        "list_permissions": BusinessPermision.listToJson(permissions)
      });
      final response = await request(url, Method.post, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResCreateRole(
          message: data["message"],
          roleId: data['role__id']
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> addManager(String businessId, String phone, String role, List<BusinessPermision> permissions) async {
    try {
      final url = ApiUrl.bizManager.replaceAll('{business_id}', businessId);
      final body = jsonEncode({
        "phone": phone,
        "email": null,
        "role": role,
        "list_permissions": BusinessPermision.listToJson(permissions)
      });
      final response = await request(url, Method.post, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResAddManager(
          message: data['message']
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('addManager $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> updateManager(String bizId, String roleId, String managerId, List<BusinessPermision> pers) async {
    try {
      final url = ApiUrl.bizManagerDetail.replaceAll('{business_id}', bizId).replaceAll('{manager_id}', managerId);
      final listPers = BusinessPermision.listToJson(pers);
      final body = jsonEncode({
        "role": roleId,
        "list_permissions": listPers
      });
      final response = await request(url, Method.put, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: data['message']
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('updateManager $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> deleteManager(String bizId, String managerId) async {
    try {
      final url = ApiUrl.bizManagerDetail.replaceAll('{business_id}', bizId).replaceAll('{manager_id}', managerId);
      final response = await request(url, Method.delete);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: data['message']
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('deleteManager $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }
  
}
import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:eight_biz_flutter/api/api_url.dart';
import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/models/business_address.dart';
import 'package:eight_biz_flutter/models/signup.dart';
import 'package:eight_biz_flutter/models/user_profile.dart';
import 'package:eight_biz_flutter/response/res_checkexist.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_exist_mail.dart';
import 'package:eight_biz_flutter/response/res_get_profile.dart';
import 'package:eight_biz_flutter/response/res_login.dart';
import 'package:eight_biz_flutter/response/res_model.dart';
import 'package:eight_biz_flutter/response/res_resend_code.dart';
import 'package:eight_biz_flutter/response/res_signup.dart';
import 'package:eight_biz_flutter/response/res_success.dart';
import 'package:eight_biz_flutter/response/res_update_profile.dart';
import 'package:eight_biz_flutter/response/res_upload.dart';
import 'package:eight_biz_flutter/response/res_verify_code.dart';
import 'package:eight_biz_flutter/utils/app_localizations.dart';

import 'api_client.dart';

class ApiAuth extends ApiClient {
  
  Future<ResModel> checkExistPhone(String phone, double latitude, double longitude) async {
    try {
      final body = jsonEncode({
        "phone": phone,
        "latitude": latitude,
	      "longitude": longitude
      });
      final response = await request(ApiUrl.checkExistUrl, Method.post, body: body, isAuth: false);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResCheckExist(
          isAlreadyUser: data['is_already_user'],
          message: data['message'],
          registeredId: data['registered_id']
        );
      } else {
        return ResError(
          code: data['code'],
          message: data['message'],
          data: {
            'registered_id': data['registered_id']
          }
        );
      }
    } catch (e) {
      print('login error: $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> updatePhone(String phone) async {
    try {
      final body = jsonEncode({
        "phone": phone
      });
      final response = await request(ApiUrl.updatePhoneUrl, Method.put, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: 'SUCCESS!'
        );
      } else {
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('updatePhone error: $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> checkEmailVerify() async {
    try {
      final response = await request(ApiUrl.checkEmailVerify, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        final language = data['language'] ?? 'en';
        AppConfig().setLanguage(language);
        AppConfig().setLocale(Locale(language));
        await AppLocalizations.load(Locale(language));
        if(data['is_verified_email'] != null && data['is_verified_email']) {
          return ResSuccess(
            message: ''
          );
        } else {
          return ResError(
            code: data['code'],
            message: AppLang.askVerifyEmail,
            data: data['email']
          );
        }
      } else {
        return ResError(
          code: data['code'],
          message: data['message'],
          data: data['email']
        );
      }
    } catch (e) {
      print('checkEmailVerify error: $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> checkExistMail(String phone, String email) async {
    try {
      final body = jsonEncode({
        "phone": phone,
        "email": email
      });
      final response = await request(ApiUrl.checkEmailExist, Method.post, body: body, isAuth: false);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResExistMail(
          isExist: data['message'] == true,
          registeredId: data['registered_id']
        );
      } else {
        return ResError(
          code: data['code'],
          message: data['message'],
          data: {
            'registered_id': data['registered_id']
          }
        );
      }
    } catch (e) {
      print('checkExistMail error: $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> resendPhoneCode(String phone) async {
    try {
      final body = jsonEncode({
        "phone": phone
      });
      final response = await request(ApiUrl.sendverifyUrl, Method.post, body: body, isAuth: false);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResResendCode(
          message: data['message'],
          registeredId: data['registered_id']
        );
      } else {
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('login error: $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> submitVerifyCode(String registerId, String code) async {
    try {
      final body = jsonEncode({
        "register_id": registerId,
        "code": code
      });
      final response = await request(ApiUrl.verifyPhoneUrl, Method.post, body: body, isAuth: false);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResVerifyCode(
          message: data['message'],
          registeredId: data['registered_id'],
          publicKey: data['public_key'],
          privateKey: data['private_key']
        );
      } else {
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('login error: $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> signup(ReqSignUp model) async {
    try {
      final body = jsonEncode({
        "register_id": model.registerId,
        "first_name": model.firstName,
        "last_name": model.lastName,
        "email": model.email,
        "code": (model.referralCode != null && model.referralCode.length == 0) 
          ? null
          : model.referralCode,
        "public_key": model.publicVerifyKey
      });
      final response = await request(ApiUrl.signupUrl, Method.post, body: body, isAuth: false);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSignup(
          message: data['message']
        );
      } else {
        print('signup error ${response.body}');
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('signup error: $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> login(String phone, String password) async {
    try {
      final body = jsonEncode({
        "email_or_phone": phone,
        "password": password
      });
      final response = await request(ApiUrl.loginUrl, Method.post, body: body, isAuth: false);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResLogin(
          token: data['token']
        );
      } else {
        print('login error ${response.body}');
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('login error: $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> loadProfile() async {
    try {
      final response = await request(ApiUrl.profileUrl, Method.get);
      print('loadProfile $response');
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        var add = data['address'] ?? {};
        return ResGetProfile(
          userProfile: UserProfile(
            id: data['id'],
            firstName: data['first_name'] ?? '',
            lastName: data['last_name'] ?? '',
            email: data['email'] ?? '',
            phone: data['phone'] ?? '',
            displayName: data['display_name'] ?? '',
            isActive: data['is_active'],
            customerCode: data['customer_code'],
            isStaff: data['is_staff'],
            imageUrl: data['image_url'],
            dateOfBirth: data['date_of_birth'],
            address: data['address'] != null
              ? BizAddress(
                address1: add['address_1'] ?? '',
                address2: add['address_2'] ?? '',
                city: add['city'] ?? '',
                state: add['state'] ?? '',
                postcode: add['postcode'],
                country: add['country'],
                latitude: add['latitude'],
                longitude: add['longitude']
              ) 
              : null,
            inviteLink: data['invite_link'],
            isRankUp: data['is_rank_up'],
            point: data['point'] != null ? data['point'].toDouble() : 0,
            totalSpent: data['total_spent'] != null ? data['total_spent'].toDouble() : 0,
            totalReferrals: data['total_referrals']
          )
        );
      } else {
        print('loadProfile error ${response.body}');
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('loadProfile error: $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> updateProfile(String firstName, String lastName, String dateOfBirth, String phone, BizAddress address) async {
    try {
      final body = {
        "first_name": firstName,
        "last_name": lastName,
        "date_of_birth": dateOfBirth,
        "address": {
          "address_1": address.address1,
          "address_2": "",
          "city": "",
          "state": "",
          "postcode": "",
          "country": "",
          "latitude": address.latitude,
          "longitude": address.longitude
        }
      };
      if(phone != null) {
        body['phone'] = phone;
      }
      final response = await request(ApiUrl.profileUrl, Method.put, body: jsonEncode(body));
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResUpdateProfile(
          message: data['message']
        );
      } else {
        print('updateProfile error ${response.body}');
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
      //profileUrl
    } catch (e) {
      print('updateProfile error: $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> uploadProfileImage(File file) async {
    try {
      final response = await postFileHttp(ApiUrl.profileImage, file, isPutMethod: true, fieldName: 'file');
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResUpload(
          file: file,
          message: data['message']
        );
      } else {
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('uploadMedia $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> resendVerifyEmail(String email) async {
    try {
      final url = ApiUrl.sendVerifyEmail;
      final body = jsonEncode({
        "email": email
      });
      final response = await request(url, Method.post, body: body, isAuth: false);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: data['message']
        );
      } else {
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('resendVerifyEmail $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> changeLanguage(String language) async {
    try {
      final url = ApiUrl.changeLanguage;
      final body = jsonEncode({
        'language': language
      });
      final response = await request(url, Method.put, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        AppConfig().setLanguage(language);
        AppConfig().setLocale(Locale(language));
        await AppLocalizations.load(Locale(language));
        return ResSuccess(
          message: data['message'] == 'SUCCESS' ? AppLang().success : data['message']
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('changeLanguage $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }
}
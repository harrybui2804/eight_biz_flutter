import 'dart:convert';

import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/models/broadcast.dart';
import 'package:eight_biz_flutter/response/res_broadcasts.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_model.dart';
import 'package:eight_biz_flutter/response/res_success.dart';
import 'package:eight_biz_flutter/utils/string_utils.dart';

import 'api_client.dart';
import 'api_url.dart';

class ApiBroadCast extends ApiClient {

  Future<ResModel> loadListBroadcast(String businessId, String next) async {
    try {
      var url = next != null ? removeBaseUrl(AppConfig().apiBaseUrl, next) : ApiUrl.listBroadcast;
      url = url.replaceAll('{business_id}', businessId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        final list = data['results'] as List;
        return ResBroadcasts(
          count: data['count'] as int,
          next: data['next'],
          previous: data['previous'],
          results: list.map((obj) {
            return Broadcast(
              id: obj['id'],
              title: obj['title'],
              body: obj['body'],
              description: obj['description'],
              createdAt: obj['created_at'],
              status: obj['status'],
              totalSend: obj['total_users_to_send'],
              totalReceived: obj['total_users_received'],
              totalRead: obj['total_users_read']
            );
          }).toList()
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('loadListManagers $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> addBroadcast(String bizId, String title, String bodyNotif, String description, bool status) async {
    try {
      var url = ApiUrl.addBroadcast.replaceAll('{business_id}', bizId);
      final body = jsonEncode({
        "title": title,
        "body": bodyNotif,
        "description": description,
        "status": status
      });
      final response = await request(url, Method.post, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: data['message']
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('addBroadcast $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> updateBroadcast(String bizId, String broadcastId, String title, String bodyNotif, String description, bool status) async {
    try {
      var url = ApiUrl.updateBroadcast.replaceAll('{business_id}', bizId).replaceAll('{broadcast_id}', broadcastId);
      final body = jsonEncode({
        "title": title,
        "body": bodyNotif,
        "description": description,
        "status": status
      });
      final response = await request(url, Method.put, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: data['message']
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('updateBroadcast $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> deleteBroadcast(String bizId, String broadcastId) async {
    try {
      var url = ApiUrl.updateBroadcast.replaceAll('{business_id}', bizId).replaceAll('{broadcast_id}', broadcastId);
      final response = await request(url, Method.delete);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: data['message']
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('deleteBroadcast $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }
}
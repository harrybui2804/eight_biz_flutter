import 'dart:collection';
import 'dart:io';

import 'package:eight_biz_flutter/components/error_modal.dart';
import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/screens/auth/get_started.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:eight_biz_flutter/utils/user_detail.dart';

enum Method {
  post,
  put,
  patch,
  delete,
  get
}

class ApiClient {
  final http.Client httpClient = http.Client();

  Future<http.Response> request(url, Method method, {body, isAuth = true, headers}) async {
    try {
      final _baseUrl = AppConfig().apiBaseUrl;
      final hasToken = isAuth != null ? isAuth : true;
      Map _headers = {
        'Content-Type' : 'application/json'
      };
      if(hasToken) {
        final _token = await UserDetail().getToken();
        _headers['Authorization'] = 'Bearer $_token';
      }
      var combinedMap = _headers;
      if(_headers != null && headers != null) {
        var mapList = [_headers, headers];
        _headers.addAll(headers);
        combinedMap = mapList.reduce( (map1, map2) => map1..addAll(map2) );
      }
      Map<String, String> header = HashMap.from(combinedMap);
      final _url = _baseUrl + url;
      if(method == Method.post) {
        return await httpClient.post(_url, body: body, headers: header);
      } else if(method == Method.put) {
        return await httpClient.put(_url, headers: header, body: body);
      } else if(method == Method.patch) {
        return await httpClient.patch(_url, headers: header, body: body);
      } else if(method == Method.delete) {
        return await httpClient.delete(_url, headers: header);
      }
      return await httpClient.get(_url, headers: header);
    } catch (e) {
      print('request $e');
      return http.Response('{code: \'Unhandle\', message: ${e.toString()}}', 500);
    }
  }

  Future<Response> postFileHttp(String url, File file, {bool isPutMethod = false, String fieldName = 'image'}) async {
    try {
      final apiUrl = AppConfig().apiBaseUrl + url;
      var postUri = Uri.parse(apiUrl);
      var methodName = isPutMethod ?  "PUT" : "POST";
      var request = new http.MultipartRequest(methodName, postUri);
      final _token = await UserDetail().getToken();
      final header = {
        'Authorization': 'Bearer $_token',
        'Content-Type': 'multipart/form-data'
      };
      request.headers.addAll(header);
      request.files.add(
        await http.MultipartFile.fromPath(fieldName, file.path)
      );
      final res = await request.send();
      return await http.Response.fromStream(res);
    } catch (e) {
      print('postFile ${e.toString()}');
      return http.Response('{code: \'Unhandle\', message: ${e.toString()}}', 500);
    }
  }

  void catchAuthen(code, String message) {
    if((code != null && code == 'PARA_1' && message != null && message.contains('Incorrect authentication credentials')) || code == 'AUTH_00') {
      if(AppConfig().appContext != null) {
        showDialog(
          context: AppConfig().appContext,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return ErorrModal(
              title: code,
              body: message,
              onClose: () {
                Keys.navKey.currentState.pushNamedAndRemoveUntil(GetStarterdPage.routeName, (Route<dynamic> route) => false);
              },
            );
          }
        );
      }
    }
  }
}
import 'dart:convert';
import 'dart:io';
import 'package:eight_biz_flutter/api/api_url.dart';
import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/models/biz_category.dart';
import 'package:eight_biz_flutter/models/biz_open_hour.dart';
import 'package:eight_biz_flutter/models/biz_owner.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/business_address.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/models/business_list.dart';
import 'package:eight_biz_flutter/models/media.dart';
import 'package:eight_biz_flutter/response/res_biz_detail.dart';
import 'package:eight_biz_flutter/response/res_categories.dart';
import 'package:eight_biz_flutter/response/res_create_biz.dart';
import 'package:eight_biz_flutter/response/res_delete_media.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_listbiz.dart';
import 'package:eight_biz_flutter/response/res_medias.dart';
import 'package:eight_biz_flutter/response/res_model.dart';
import 'package:eight_biz_flutter/response/res_permission.dart';
import 'package:eight_biz_flutter/response/res_success.dart';
import 'package:eight_biz_flutter/response/res_update_biz.dart';
import 'package:eight_biz_flutter/response/res_upload.dart';
import 'package:eight_biz_flutter/response/res_upload_policy.dart';
import 'package:eight_biz_flutter/utils/string_utils.dart';
import 'package:eight_biz_flutter/utils/user_detail.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'api_client.dart';

class ApiBiz extends ApiClient {
  
  Future<ResModel> loadListBiz(String next) async {
    try {
      final url = next != null ? removeBaseUrl(AppConfig().apiBaseUrl, next) : ApiUrl.listBizUrl;
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        final list = data['results'] as List;
        return ResListBiz(
          count: data['count'] as int,
          next: data['next'],
          results: list.map((obj) {
            var biz = obj['business'] ?? {};
            return BusinessList(
              isOwner: obj['is_owner'],
              isActive: obj['is_active'],
              business: Data(
                id: biz['id'],
                name: biz['name'],
                addresses: biz['addresses'],
              )
            );
          }).toList()
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('loadListBiz $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> loadCategories() async {
    try {
      final url = ApiUrl.categoriesUrl + '?lang=' + AppConfig().language;
      print(url);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        final list = data as List;
        return ResCategories(
          categories: list.map((cat) => BizCategory(
            id: cat['id'],
            name: cat['name'],
            imageUrl: cat['image_url']
          )).toList()
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('loadCategories $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> createBusiness(BusinessDetail biz) async {
    try {
      final body = jsonEncode({
        "name": biz.name,
        "category": biz.categories.map((cat) => cat.id).toList(),
        "abn": biz.abn,
        "website": biz.website ?? '',
        "description": biz.description ?? '',
        "email": biz.email,
        "contact_number": biz.contactNumber ?? '',
        "address": {
          "address_1": biz.address.address1,
          "address_2": "",
          "city": biz.address.city,
          "state": biz.address.state,
          "postcode": biz.address.postcode,
          "country": biz.address.country,
          "latitude": biz.address.latitude,
          "longitude": biz.address.longitude
        },
        "opening_hours": biz.openHour.toJsonArray(),
      });
      final response = await request(ApiUrl.createBizUrl, Method.post, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResCreateBiz(
          businessId: data['business_id'],
          message: data['message']
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> updateBusiness(BusinessDetail business, bool updateLogo, bool updateCover) async {
    try {
      final data = {
        "name": business.name,
        "category": business.categories.map((cat) => cat.id).toList(),
        "abn": business.abn,
        "website": business.website,
        "description": business.description,
        "email": business.email,
        "contact_number": business.contactNumber,
        "address": {
          "address_1": business.address != null ? business.address.address1 : '',
          "address_2": business.address != null ? business.address.address2 : '',
          "city": business.address != null ? business.address.city : '',
          "state": business.address != null ? business.address.state : '',
          "postcode": business.address != null ? business.address.postcode : '',
          "country": business.address != null ? business.address.country : '',
          "latitude": business.address != null ? business.address.latitude : 0,
          "longitude": business.address != null ? business.address.longitude : 0
        },
        "opening_hours": business.openHour.toJsonArray()
      };
      if(updateLogo) {
        data.addAll({
          'logo_image': business.logoImage
        });
      } else {
        if(business.logoImage != null && business.logoImage.length > 0) {
          data.addAll({
            'logo_image': business.logoImage
          });
        }
      }
      if(updateCover) {
        data.addAll({
          'cover_image': business.coverImage
        });
      } else {
        if(business.coverImage != null && business.coverImage.length > 0) {
          data.addAll({
            'cover_image': business.coverImage
          });
        }
      }
      final body = jsonEncode(data);
      final apiUrl = ApiUrl.bizDetailUrl.replaceAll('{business_id}', business.id);
      final response = await request(apiUrl, Method.put, body: body);
      final res = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResUpdateBiz(
          business: business,
          message: res['message'],
          updateCover: updateCover,
          updateLogo: updateLogo
        );
      } else {
        catchAuthen(res['code'], res['message']);
        return ResError(
          code: res['code'],
          message: res['message']
        );
      }
    } catch (e) {
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> loadBizDetail(String businessId) async {
    try {
      final url = ApiUrl.bizDetailUrl.replaceAll('{business_id}', businessId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResBizDetail(
          business: BusinessDetail(
            id: data['id'],
            addresses: data['addresses'],
            address: data['address'] != null ? BizAddress(
              id: data['address']['id'],
              address1: data['address']['address_1'],
              address2: data['address']['address_2'],
              city: data['address']['city'],
              country: data['address']['country'],
              state: data['address']['state'],
              createdAt: data['address']['created_at'],
              latitude: data['address']['latitude'],
              longitude: data['address']['longitude'],
              postcode: data['address']['postcode'],
              point: data['address']['point']
            ) : null,
            categories: data['tags'] != null ? (data['tags'] as List).map((cat) {
              return BizCategory(
                id: cat['id'],
                name: cat['name']
              );
            }).toList() : [],
            owner: data['owner'] != null ? BizOwner(
              id: data['owner']['id'],
              email: data['owner']['email'],
              firstName: data['owner']['first_name'],
              lastName: data['owner']['last_name']
            ) : null,
            logoImage: data['logo_image'],
            coverImage: data['cover_image'],
            listMedia: data['list_media'] != null ? 
              (data['list_media'] as List).map((img) => img.toString()).toList()
              : [],
            approvedBy: data['approved_by'],
            isApproved: data['is_approved'],
            approvedOn: data['approved_on'],
            totalEarning: data['total_earning'] != null ? data['total_earning'].toDouble() : null,
            openHour: data['opening_hours'] != null ? 
              BizOpenHour.fromGetBizDetail(data['opening_hours']) : null,
            name: data['name'],
            abn: data['abn'],
            email: data['email'],
            contactNumber: data['contact_number'],
            code: data['code'],
            status: data['status'],
            offeringPercent: data['offering_percent'] != null ? data['offering_percent'].toDouble() : null,
            reason: data['reason'],
            rejectedOn: data['rejected_on'],
            rejectedBy: data['rejected_by'] != null ? {
              'id': data['rejected_by'][''],
              'first_name': data['rejected_by']['first_name'],
              'last_name': data['rejected_by']['last_name']
            } : null,
            isActive: data['is_active'],
            isDeleted: data['is_deleted'],
            createdAt: data['created_at'],
            description: data['description'],
            website: data['website'],
            inviteLink: data['invite_link']
          )
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> getUserPermission(String businessId) async {
    try {
      final url = ApiUrl.listPermissions.replaceAll('{business_id}', businessId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        final list = data as List;
        return ResPermission(
          businessId: businessId,
          permissions: list.map((obj) {
            return BusinessPermision.fromJson(obj);
          }).toList()
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('getUserPermission $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> loadMedias(String businessId, String next) async {
    try {
      final url = ApiUrl.mediaslUrl.replaceAll('{business_id}', businessId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        var list = data['results']!= null ? data['results'] as List : [];
        return ResMedias(
          count: data['count'],
          next: data['next'],
          previous: data['previous'],
          results: list.isNotEmpty ? list.map((media){
            var image = media['list_image'] ?? {};
            var sizes = image['image_size'] ?? {};
            return Media(
              id: media['id'],
              isCover: media['is_cover'],
              url: image['image_s3_url'],
              logo: sizes['logo']!=null ? ImageObject(
                height: sizes['logo']['height'],
                width: sizes['logo']['width'],
                url: sizes['logo']['image_s3_url']
              ) : null,
              thumbnail: sizes['thumbnail']!=null ? ImageObject(
                height: sizes['thumbnail']['height'],
                width: sizes['thumbnail']['width'],
                url: sizes['thumbnail']['image_s3_url']
              ) : null,
              medium: sizes['medium']!=null ? ImageObject(
                height: sizes['medium']['height'],
                width: sizes['medium']['width'],
                url: sizes['medium']['image_s3_url']
              ) : null,
              large: sizes['large']!=null ? ImageObject(
                height: sizes['large']['height'],
                width: sizes['large']['width'],
                url: sizes['large']['image_s3_url']
              ) : null
            );
          }).toList() :[]
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  //Upload image
  Future<ResModel> uploadMedia(String businessId, File file) async {
    try {
      final response = await postFileHttp(ApiUrl.mediaUploadUrl, file);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResUpload(
          imageId: data['image_s3_id'],
          file: file,
          message: data['message'],
          businessId: businessId
        );
      } else {
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      final msg = e.source;
      print('uploadMedia $msg');
      return ResError(
        code: 'Unhandle',
        message: msg
      );
    }
  }

  Future<ResModel> uploadGallery(String bizId, File file) async {
    try {
      final apiUrl = AppConfig().apiBaseUrl + ApiUrl.bizUploadGallery;
      debugPrint('uploadGallery apiUrl: $apiUrl');
      var postUri = Uri.parse(apiUrl);
      var requestFile = new http.MultipartRequest('POST', postUri);
      final _token = await UserDetail().getToken();
      final header = {
        'Authorization': 'Bearer $_token',
        'Content-Type': 'multipart/form-data'
      };
      requestFile.headers.addAll(header);
      requestFile.files.add(
        http.MultipartFile.fromString('business', bizId)
      );
      requestFile.files.add(
        await http.MultipartFile.fromPath('file', file.path)
      );
      final res = await requestFile.send();
      final response =  await http.Response.fromStream(res);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        //Complete upload
        final urlComplete = AppConfig().apiBaseUrl + ApiUrl.bizUploadGalleryComplete.replaceAll('{business_id}', bizId);
        debugPrint('uploadGallery urlComplete: $urlComplete');
        var requestComplete = new http.MultipartRequest('POST', Uri.parse(urlComplete));
        requestComplete.headers.addAll(header);
        requestComplete.files.add(
          http.MultipartFile.fromString('file_id', data['file_id'])
        );
        final resComplete = await requestComplete.send();
        final responseComplete =  await http.Response.fromStream(resComplete);
        final dataComplete = json.decode(responseComplete.body);
        if(responseComplete.statusCode == 200) { 
          return ResUploadPolicy(
            file: file,
            businessId: bizId,
            fileId: data['file_id'],
            policy: data['policy'],
            signature: data['signature'],
            key: data['key'],
            message: dataComplete['message'],
          );
        } else {
          catchAuthen(dataComplete['code'], dataComplete['message']);
          return ResError(
            code: dataComplete['code'],
            message: dataComplete['message']
          );
        }
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('postFile ${e.toString()}');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> deleteMedia(String businessId, String imageId) async {
    try {
      var url = ApiUrl.deleteMediaUrl.replaceAll('{business_id}', businessId).replaceAll('{image_id}', imageId);
      final response = await request(url, Method.delete);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResDeleteMedias(
          message: data['message'] ?? AppLang().success
        );
      } else {
        final data = json.decode(utf8.decode(response.bodyBytes));
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('deleteMedia $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> loadAgreement(String businessId) async {
    try {
      var url = ApiUrl.getAgreementUrl.replaceAll('{business_id}', businessId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: data['agreement_html'] ?? ''
        );
      } else {
        final data = json.decode(utf8.decode(response.bodyBytes));
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('loadSignature $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> updateAgreement(String businessId, String signatureId) async {
    try {
      var url = ApiUrl.getAgreementUrl.replaceAll('{business_id}', businessId);
      final body = jsonEncode({
        "business_signed_file": signatureId
      });
      final response = await request(url, Method.put, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: data['message'] ?? ''
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('updateAgreement $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }
}
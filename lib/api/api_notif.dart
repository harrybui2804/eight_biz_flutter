import 'dart:convert';

import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/models/notification.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_model.dart';
import 'package:eight_biz_flutter/response/res_notifications.dart';
import 'package:eight_biz_flutter/utils/string_utils.dart';

import 'api_client.dart';
import 'api_url.dart';

class ApiNotif extends ApiClient {

  Future<ResModel> loadListNotifs(String businessId, String next) async {
    try {
      final url = next != null ? removeBaseUrl(AppConfig().apiBaseUrl, next) : ApiUrl.listNotifs.replaceAll('{business_id}', businessId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        final list = data['results'] as List;
        return ResNotifications(
          count: data['count'],
          next: data['next'],
          previous: data['previous'],
          results: list.map((obj) {
            return Notification.fromJson(obj);
          }).toList()
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('loadListNotifs $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }
}
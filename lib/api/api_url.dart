class ApiUrl {
  static final checkExistUrl = 'v1.0/auth/get-started/';
  static final loginUrl = 'v1.0/auth/cognito-token/';
  static final verifyPhoneUrl = 'v1.0/auth/verify-phone/';
  static final sendverifyUrl = 'v1.0/auth/resend-verify-phone/';
  static final checkEmailVerify = 'v1.0/auth/check-verified-email/';
  static final sendVerifyEmail = 'v1.0/auth/resend-verify-email/';
  static final checkEmailExist = 'v1.0/auth/check-mail/';
  static final updatePhoneUrl = 'v1.0/auth/update-phone/';
  static final signupUrl = 'v1.0/auth/complete-signup/';
  static final profileUrl = 'v1.0/auth/profile/';
  static final profileImage = 'v1.0/auth/update-profile-image/';
  static final listBizUrl = 'v1.0/user/business/app/';
  static final listPermissions = 'v1.0/user/business/{business_id}/manager/list-perms/';
  static final categoriesUrl = 'v1.0/user/category/';
  static final createBizUrl = 'v1.0/user/business/';
  static final bizDetailUrl = 'v1.0/user/business/{business_id}/';
  static final mediaslUrl = 'v1.0/user/business/{business_id}/gallery/';
  static final deleteMediaUrl = 'v1.0/user/business/{business_id}/gallery/{image_id}/';
  static final changeLanguage = 'v1.0/user/language/';
  //Upload image: upload logo or cover
  static final mediaUploadUrl = 'v1.0/user/upload/'; //post method
  static final getAgreementUrl = 'v1.0/user/business/{business_id}/agreement/';

  static final bizUploadGallery = 'v1.0/upload/policy/';
  static final bizUploadGalleryComplete = 'v1.0/upload/policy/complete/{business_id}/';
  
  static final bizManager = 'v1.0/user/business/{business_id}/manager/';
  static final bizManagerDetail = 'v1.0/user/business/{business_id}/manager/detail/{manager_id}/';
  static final rolesUrl = 'v1.0/user/business/{business_id}/manager/role/';
  static final roleDetailUrl = 'v1.0/user/business/{business_id}/manager/role/{role_id}/';

  static final listBroadcast = 'v1.0/user/business/{business_id}/broadcast/';
  static final addBroadcast = 'v1.0/user/business/{business_id}/broadcast/add/';
  static final updateBroadcast = 'v1.0/user/business/{business_id}/broadcast/{broadcast_id}/';

  static final walletDetail = 'v1.0/user/business/{business_id}/wallet/';
  static final cashoutWithdraw = 'v1.0/user/wallet/request-withdraw/{business_id}/';
  static final cashoutWallet = 'v1.0/user/wallet/transfer/{business_id}/';
  static final bizBank = 'v1.0/user/business/{business_id}/bank/';
  static final bizBankUpdate = 'v1.0/user/business/{business_id}/bank/update/';
  static final bizBankTokenUpdate = 'v1.0/user/business/{business_id}/bank/bank-token/';
  static final listTransactions = 'v1.0/user/business/{business_id}/transactions/';
  static final stripeStatusUrl = 'v1.0/user/business/{business_id}/verified-status/';
  static final uploadPhotoIdUrl = 'v1.0/user/business/upload-id/';
  static final stripeVerifyUrl = 'v1.0/user/business/{business_id}/verify-account/';

  static final listNotifs = 'v1.0/user/notification/{business_id}/list-notices/';
}
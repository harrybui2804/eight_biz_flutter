import 'dart:convert';
import 'dart:io';
import 'package:eight_biz_flutter/api/api_url.dart';
import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/models/biz_bank.dart';
import 'package:eight_biz_flutter/models/enums.dart';
import 'package:eight_biz_flutter/models/transactions_graph.dart';
import 'package:eight_biz_flutter/models/wallet_balance.dart';
import 'package:eight_biz_flutter/models/wallet_transaction.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_get_bank.dart';
import 'package:eight_biz_flutter/response/res_model.dart';
import 'package:eight_biz_flutter/response/res_photoID.dart';
import 'package:eight_biz_flutter/response/res_stripe_status.dart';
import 'package:eight_biz_flutter/response/res_success.dart';
import 'package:eight_biz_flutter/response/res_transactions.dart';
import 'package:eight_biz_flutter/response/res_wallet_detail.dart';
import 'package:eight_biz_flutter/utils/string_utils.dart';
import 'package:eight_biz_flutter/utils/user_detail.dart';
import 'package:http/http.dart';

import 'api_client.dart';

class ApiWallet extends ApiClient {
  
  Future<ResModel> loadWalletDetail(String businessId) async {
    try {
      var url = ApiUrl.walletDetail.replaceAll('{business_id}', businessId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        final list = data['current_week_transactions_graph'] != null ? data['current_week_transactions_graph'] as List : [];
        return ResWalletDetail(
          balance: WalletBalance(
            totalBalance: data['total_balance'] != null ? data['total_balance'].toDouble() : 0,
            currentWeekEarning: data['current_week_earning'] != null ? data['current_week_earning'].toDouble() : 0,
            currentWeekGraph: list.map((obj) {
              return TransactionsGraph.fromJson(obj);
            }).toList()
          )
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('loadListManagers $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> cashout(String bizId, double amount, String remark) async {
    try {
      var url = ApiUrl.cashoutWithdraw.replaceAll('{business_id}', bizId);
      final body = jsonEncode({
        "remark": remark
      });
      final response = await request(url, Method.post, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: data['message'] ?? "SUCCESS!"
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('cashoutWithDraw $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> getBizBank(String businessId) async {
    try {
      final url = ApiUrl.bizBank.replaceAll('{business_id}', businessId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        var bank = data['bank'];
        if(data['bank']['routing_number'] == null) {
          bank = null;
        }
        return ResGetBizBank(
          bank: bank != null 
            ? BizBank(
              bankName: bank['bank_name'],
              bsb: bank['routing_number'] != null ? bank['routing_number'].toString().replaceAll(' ', '') : null,
              accountNo: bank['account_no'],
              accountName: bank['holder_name'],
              last4Number: bank['last_4_number'],
            ) : null
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('getBizBank $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> updateBizBank(String businessId, String bankToken) async {
    try {
      final url = ApiUrl.bizBankTokenUpdate.replaceAll('{business_id}', businessId);
      final body = jsonEncode({
        "bank_token": bankToken
      });
      final response = await request(url, Method.put, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: data['message']
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('updateBizBank $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> loadTransactions(
    String bizId, 
    String next,
    String fromDate, 
    String toDate, 
    EarningOrderFilter ordering, 
    EarningTransactionFilter transactionType, 
    EarningStatusFilter status
  ) async {
    try {
      var url = ApiUrl.listTransactions.replaceAll('{business_id}', bizId);
      url += '?ordering=-modified_at';
      if(next != null) {
        url = removeBaseUrl(AppConfig().apiBaseUrl, next);
      } else {
        url = _getListTransUrl(bizId, fromDate, toDate, ordering, transactionType, status);
      }
      print('loadTransactions $url');
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        final list = data['results'] != null ? data['results'] as List : [];
        return ResTransactions(
          count: data['count'],
          next: data['next'],
          prev: data['previous'],
          results: list.map((obj) {
            return WalletTransaction.fromJson(obj);
          }).toList()
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('getBizBank $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  String _getListTransUrl(
    String bizId, 
    String fromDate, 
    String toDate, 
    EarningOrderFilter ordering, 
    EarningTransactionFilter transactionType, 
    EarningStatusFilter status
  ) {
    var url = ApiUrl.listTransactions.replaceAll('{business_id}', bizId);
    if(ordering == null) {
      url += '?ordering=-modified_at';
    } else {
      url += '?ordering=';
      if(ordering == EarningOrderFilter.modifiedAtDsc) {
        url += '-modified_at';
      } else if(ordering == EarningOrderFilter.modifiedAtAsc) {
        url += 'modified_at';
      } else if(ordering == EarningOrderFilter.amountAsc) {
        url += 'amount';
      } else if(ordering == EarningOrderFilter.amountDsc) {
        url += '-amount';
      }
    }
    if(fromDate != null) {
      url += '&from_date=$fromDate';
    }
    if(toDate != null) {
      url += '&to_date=$toDate';
    }
    if(transactionType != null) {
      url += '&transaction_type=';
      if(transactionType == EarningTransactionFilter.withdraw) {
        url += 'Withdraw';
      } else if(transactionType == EarningTransactionFilter.userWallet) {
        url += 'User Wallet Transfer';
      } else if(transactionType == EarningTransactionFilter.bizEarning) {
        url += 'Business Earning';
      }
    }
    if(status != null) {
      url += '&status=';
      if(status == EarningStatusFilter.pending) {
        url += '0';
      } else if(status == EarningStatusFilter.paid) {
        url += '1';
      } else if(status == EarningStatusFilter.declined) {
        url += '-1';
      }
    }
    return url;
  }

  Future<ResModel> getStripeStatus(String bizId) async {
    try {
      final url = ApiUrl.stripeStatusUrl.replaceAll('{business_id}', bizId);
      final response = await request(url, Method.get);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResStripeVerify(
          verified: data['verified'],
          bank: data['bank'],
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('getStripeStatus $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<Response> _postPhotoID(File front, File back) async {
    try {
      final apiUrl = AppConfig().apiBaseUrl + ApiUrl.uploadPhotoIdUrl;
      var postUri = Uri.parse(apiUrl);
      var methodName = "POST";
      var request = new MultipartRequest(methodName, postUri);
      final _token = await UserDetail().getToken();
      final header = {
        'Authorization': 'Bearer $_token',
        'Content-Type': 'multipart/form-data'
      };
      request.headers.addAll(header);
      if(front != null) {
        request.files.add(
          await MultipartFile.fromPath('front_id', front != null ? front.path : null)
        );
      }
      if(back != null) {
        request.files.add(
          await MultipartFile.fromPath('back_id', back != null ? back.path : null)
        );
      }
      final res = await request.send();
      return await Response.fromStream(res);
    } catch (e) {
      print('postFile ${e.toString()}');
      return Response('{code: \'Unhandle\', message: ${e.toString()}}', 500);
    }
  }

  Future<ResModel> uploadPhotoId(File front, File back) async {
    try {
      final response = await _postPhotoID(front, back);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResPhotoID(
          frontId: data['front_id'],
          backId: data['back_id']
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('uploadPhotoId $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }

  Future<ResModel> submitVerifyStripe(String bizId, String frontId, String backId) async {
    try {
      final url = ApiUrl.stripeVerifyUrl.replaceAll('{business_id}', bizId);
      final body = jsonEncode({
        "front_id": frontId,
        "back_id": backId
      });
      final response = await request(url, Method.post, body: body);
      final data = json.decode(utf8.decode(response.bodyBytes));
      if(response.statusCode == 200) {
        return ResSuccess(
          message: data['message'] ?? "SUCCESS"
        );
      } else {
        catchAuthen(data['code'], data['message']);
        return ResError(
          code: data['code'],
          message: data['message']
        );
      }
    } catch (e) {
      print('submitVerifyStripe $e');
      return ResError(
        code: 'Unhandle',
        message: e.toString()
      );
    }
  }
}
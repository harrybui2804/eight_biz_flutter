import 'package:amazon_cognito_identity_dart/cognito.dart';
import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/models/signup.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_login.dart';
import 'package:eight_biz_flutter/response/res_model.dart';
import 'package:eight_biz_flutter/utils/user_detail.dart';

class AWSCognito {
  final userPool = new CognitoUserPool(AppConfig().userPoolIdCognito, AppConfig().clientIdCognito);
  
  Future<ResModel> loginCognito(String phone, String password) async {
    try {
      final cognitoUser = new CognitoUser(
        phone, userPool
      );
      final authDetails = new AuthenticationDetails(
        username: phone, 
        password: password
      );
      final session = await cognitoUser.authenticateUser(authDetails);
      final token = session.getIdToken().getJwtToken();
      final accessToken = session.getAccessToken().getJwtToken();
      return ResLogin(
        statusCode: 200,
        token: token,
        accessToken: accessToken
      );
    } on CognitoClientException catch(e) {
      return ResError(
        code: e.code,
        message: e.message
      ); 
    } catch (e) {
      print(e.toString());
      return ResError(
        code: e.code ?? 'Unhandle',
        message: e.message ?? e.toString()
      );
    }
  }

  Future<dynamic> signupCognito(ReqSignUp model) async {
    bool isSuccess = false;
    dynamic response;
    try {
      final userAttributes = [
        new AttributeArg(name: 'custom:first_name', value: model.firstName),
        new AttributeArg(name: 'custom:last_name', value: model.lastName),
        new AttributeArg(name: 'email', value: model.email),
        new AttributeArg(name: 'phone_number', value: model.phone),
        new AttributeArg(name: 'custom:public_key', value: model.publicVerifyKey),
        new AttributeArg(name: 'custom:private_key', value: model.privateVerifyKey),
      ];
      
      response = await userPool.signUp(
        model.registerId, 
        model.password,
        userAttributes: userAttributes
      );
      isSuccess = true;
    } catch (e) {
      isSuccess = false;
    }
    return {'isSuccess': isSuccess, 'response': response};
  }

  Future<bool> requestCodeResetPassword(String registerId) async {
    bool isSend = false;
    try {
      final cognitoUser = new CognitoUser(registerId, userPool);
      final data = await cognitoUser.forgotPassword();
      print(data);
      isSend = true;
    } catch (e) {
      throw e;
    }
    return isSend;
  }

  Future<bool> submitResetPassword(String registerId, String password, String code) async {
    bool passwordConfirmed = false;
    try {
      final cognitoUser = new CognitoUser(registerId, userPool);
      passwordConfirmed = await cognitoUser.confirmPassword(code, password);
    } catch (e) {
      throw e;
    }
    return passwordConfirmed;
  }

  Future<bool> changePassword(String oldPassword, String newPassword) async {
    bool passwordChanged = false;
    try {
      final login = await UserDetail().getLogin();
      final phone = login['phone'];
      final password = login['password'];
      final cognitoUser = new CognitoUser(
        phone, userPool
      );
      final authDetails = new AuthenticationDetails(
        username: phone, 
        password: password
      );
      await cognitoUser.authenticateUser(authDetails);
      passwordChanged = await cognitoUser.changePassword(oldPassword, newPassword);
    } catch (e) {
      throw e;
    }
    return passwordChanged;
  }
}
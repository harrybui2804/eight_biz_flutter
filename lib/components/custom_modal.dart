

import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:flutter/material.dart';

typedef void VoidCallback();

class CustomModal extends StatefulWidget {
  final String title;
  final String body;
  final TextStyle bodyStyle;
  final bool centerTitle;
  final bool centerBody;
  final Widget child;
  final String cancelText;
  final String acceptText;
  final bool cancelButtonColor;
  final bool acceptButtonColor;
  final VoidCallback onCancel;
  final VoidCallback onAccept;

  CustomModal({
    @required this.title,
    this.body,
    this.centerTitle = true,
    this.centerBody = true,
    this.bodyStyle,
    this.child,
    @required this.cancelText,
    @required this.acceptText,
    this.acceptButtonColor = false,
    this.cancelButtonColor = false,
    this.onCancel,
    this.onAccept
  }){
    assert(title != null);
    assert(body == null || child == null);
  }

  @override
  State<StatefulWidget> createState() => _CustomModal();
}

class _CustomModal extends State<CustomModal> {

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width - 40;
    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: Container(
        width: width,
        padding: EdgeInsets.all(20),
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 20),
              child: Center(
                child: Text(
                  widget.title,
                  style: AppStyles.titleTextCustom(),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            widget.child != null
            ? widget.child
            : widget.centerBody 
              ? Center(
                  child: Text(
                    widget.body,
                    style: widget.bodyStyle,
                  ),
                )
              : Text(
                  widget.body,
                  style: widget.bodyStyle,
                ),
            
            Container(
              padding: EdgeInsets.only(
                top: 10,
                bottom: 10,
                left: 20,
                right: 20
              ),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    onPressed: (){
                      Navigator.of(context).pop();
                      if(widget.onCancel != null) {
                        widget.onCancel();
                      }
                    },
                    color: widget.cancelButtonColor ? Colors.grey : Colors.white,
                    highlightColor: widget.cancelButtonColor ? Colors.grey : AppColors.primaryOrange.withOpacity(0.3),
                    elevation: 0,
                    highlightElevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                      side: BorderSide.none
                    ),
                    child: Center(
                      child: Text(
                        widget.cancelText,
                        style: TextStyle(
                          color: widget.cancelButtonColor ? Colors.white : AppColors.primaryOrange,
                          fontSize: 15,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    ),
                  ),
                  Container(width: 10),
                  RaisedButton(
                    onPressed: (){
                      Navigator.of(context).pop();
                      if(widget.onAccept != null) {
                        widget.onAccept();
                      }
                    },
                    color: widget.acceptButtonColor ? AppColors.primaryOrange : Colors.white,
                    highlightColor: widget.acceptButtonColor ? AppColors.primaryOrange : AppColors.primaryOrange.withOpacity(0.3),
                    elevation: 0,
                    highlightElevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                    ),
                    child: Center(
                      child: Text(
                        widget.acceptText,
                        style: TextStyle(
                          color: widget.acceptButtonColor ? AppColors.whiteColor : AppColors.primaryOrange,
                          fontSize: 15,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      )
    );
  }
}
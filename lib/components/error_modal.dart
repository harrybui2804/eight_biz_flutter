import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:flutter/material.dart';

typedef void VoidCallback();

class ErorrModal extends StatelessWidget {
  final String title;
  final String body;
  final String buttonText;

  final VoidCallback onClose;

  ErorrModal({
    this.onClose, 
    this.title,
    this.body,
    this.buttonText
  });

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width - 40;
    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: Container(
        width: width,
        padding: EdgeInsets.all(20),
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 10),
              alignment: Alignment.center,
              child: Center(
                child: Icon(Icons.error, color: Colors.red, size: 40,)
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 20),
              alignment: Alignment.center,
              child: Center(
                child: Text(
                  title ?? '',
                  style: AppStyles.titleTextCustom(fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 20),
              alignment: Alignment.center,
              child: Center(
                child: Text(
                  body ?? '',
                  style: AppStyles.titleTextCustom(fontWeight: FontWeight.w400),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: 10,
                bottom: 10
              ),
              color: Colors.white,
              alignment: Alignment.center,
              child: RaisedButton(
                onPressed: (){
                  Navigator.of(context).pop();
                  if(onClose != null) {
                    onClose();
                  }
                },
                color: AppColors.primaryOrange,
                highlightColor: AppColors.primaryOrange,
                elevation: 0,
                highlightElevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(6)),
                ),
                child: Text(
                  buttonText ?? AppLang().ok.toUpperCase(),
                  style: TextStyle(
                    color: AppColors.whiteColor,
                    fontSize: 15,
                    fontWeight: FontWeight.w500
                  ),
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
}
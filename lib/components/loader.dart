import 'package:eight_biz_flutter/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loader extends StatelessWidget {
  final bool isLock;
  final bool animation;

  Loader({this.isLock = true, this.animation = true});

  @override
  Widget build(BuildContext context) {
    if(animation) {
      if(isLock) {
        return Positioned(
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          child: SpinKitCircle(
            color: AppColors.primaryOrange,
            size: 50
          ),
        );
      }
      return SpinKitCircle(
        color: AppColors.primaryOrange,
        size: 50
      );
    }
    return Container();
  }
}
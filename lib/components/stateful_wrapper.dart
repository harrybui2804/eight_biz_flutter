import 'package:flutter/material.dart';

class StatefulWrapper extends StatefulWidget {
  final Function onInit;
  final Widget child;

  StatefulWrapper({this.onInit, @required this.child});

  @override
  State<StatefulWidget> createState() => _StatefulWrapper();
}

class _StatefulWrapper extends State<StatefulWrapper> {

  @override
  void initState() {
    if(widget.onInit != null) {
      widget.onInit();
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
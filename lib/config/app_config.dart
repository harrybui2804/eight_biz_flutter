import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

enum AppEnvironment { DEV, STAGE, PROD }

class AppConfig {
  AppEnvironment appEnvironment;
  String appName;
  String apiBaseUrl;
  String publicKeyStripe;
  String userPoolIdCognito;
  String clientIdCognito;
  String googleApiKey;
  String termsUrl;
  String stripeUrl;
  String language;
  Locale locale;
  BuildContext appContext;  // context for show dialog token timeout

  static final AppConfig _singleton =
      new AppConfig._internal();

  factory AppConfig() {
    return _singleton;
  }

  AppConfig._internal();

  setAppConfig({
    @required AppEnvironment appEnvironment,
    @required String appName,
    @required String apiBaseUrl,
    @required String publicKeyStripe,
    @required String userPoolIdCognito,
    @required String clientIdCognito,
    @required String googleApiKey,
    @required String termsUrl,
    @required String stripeUrl
  }) {
    this.appEnvironment = appEnvironment;
    this.appName = appName;
    this.apiBaseUrl = apiBaseUrl;
    this.publicKeyStripe = publicKeyStripe;
    this.userPoolIdCognito = userPoolIdCognito;
    this.clientIdCognito = clientIdCognito;
    this.googleApiKey = googleApiKey;
    this.termsUrl = termsUrl;
    this.stripeUrl = stripeUrl;
  }

  setLanguage(String language) {
    this.language = language ?? 'en';
  }

  setLocale(Locale locale) {
    this.locale = locale;
  }

  setAppContext(BuildContext context) {
    this.appContext = context;
  }
}
import 'dart:ui';

class AppColors {
  static const primaryOrange = Color(0xFFFC4D18);
  static const secondaryColor = Color(0xff152745);
  static const thirdColor = Color(0xff2ed8b6);
  static const darkBlue = Color(0xFF102747);
  static const strongCyan = Color(0xFF2ED8B6);
  static const darkGrayish = Color(0xFF343c46);
  static const whiteColor = Color(0xFFFFFFFF);
  static const mostlyWhite = Color(0xFFFAFAFA);
  static const lightGrey = Color(0xFFD9D9D9);

  static const grey1Color = Color(0xff949494);
  static const grey2Color = Color(0xff9b9b9b);
  static const grey3Color = Color(0xfff0f0f0);
  static const grey4Color = Color(0xfffafafa);
}

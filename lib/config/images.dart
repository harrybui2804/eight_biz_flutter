class AppImages {
  static const String logoAppBlack = 'assets/images/logo_black.png';
  static const String logoAppOrange = 'assets/images/logo_orange.png';
  static const String logoAppWhite = 'assets/images/logo_white.png';

  static const String introCashback = 'assets/images/Cashback.gif';
  static const String introLocate = 'assets/images/Locate.gif';
  static const String introSpend = 'assets/images/Spend.gif';

  static const String backgroundSplash = 'assets/images/splash@3x.png';
  static const String backgroundAuth = 'assets/images/onboarding.png';

  static const String iconChangePass = 'assets/images/svg/change_pass.svg';
  static const String iconEdit = 'assets/images/svg/edit_icon.svg';
  static const String iconEditBlack = 'assets/images/svg/edit_icon_black.svg';
  static const String iconSetting = 'assets/images/svg/setting.svg';
  static const String iconNotification = 'assets/images/svg/notification.svg';
  /// gif
  static const String locateGif = 'assets/images/gif/locate.gif';
  static const String spendShopGif = 'assets/images/gif/spend-shop.gif';
  static const String cashBackGif = 'assets/images/gif/cashback.gif';

  static const String tabWalletActive = 'assets/images/svg/wallet_active.svg';
  static const String tabWalletInactive = 'assets/images/svg/wallet.svg';

  static const String tabBizActive = 'assets/images/svg/biz_active.svg';
  static const String tabBizInactive = 'assets/images/svg/biz.svg';

  static const String tabAdminActive = 'assets/images/svg/admin_active.svg';
  static const String tabAdminInactive = 'assets/images/svg/admin.svg';

  static const String tabPayActive = 'assets/images/svg/pay_active.svg';
  static const String tabPayInactive = 'assets/images/svg/pay.svg';

  static const String tabProfileActive = 'assets/images/svg/profile_active.svg';
  static const String tabProfileInactive = 'assets/images/svg/profile.svg';
}
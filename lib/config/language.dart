import 'package:eight_biz_flutter/utils/app_localizations.dart';

class AppLang {
  static final askVerifyEmail = 'Please verify your email';
  static final noPermissionLoadBiz = 'You have not permission to show this business detail';

  final String wallet = AppLocalizations().trans('key_wallet');
  final String business = AppLocalizations().trans('key_business');
  final String admin = AppLocalizations().trans('key_admin');
  final String pay = AppLocalizations().trans('key_pay');
  final String profile = AppLocalizations().trans('key_profile');

  final String success = AppLocalizations().trans('key_success');
  final String save = AppLocalizations().trans('key_save');
  final String accept = AppLocalizations().trans('key_accept');
  final String close = AppLocalizations().trans('key_close');
  final String ok = AppLocalizations().trans('key_ok');
  final String confirm = AppLocalizations().trans('key_confirm');
  final String cancel = AppLocalizations().trans('key_cancel');
  final String next = AppLocalizations().trans('key_next');
  final String done = AppLocalizations().trans('key_done');
  final String draft = AppLocalizations().trans('key_draft');
  final String verify = AppLocalizations().trans('key_verify');
  final String add = AppLocalizations().trans('key_add');
  final String update = AppLocalizations().trans('key_update');
  final String edit = AppLocalizations().trans('key_edit');
  final String remove = AppLocalizations().trans('key_remove');
  final String takePhoto = AppLocalizations().trans('key_takePhoto');
  final String gallery = AppLocalizations().trans('key_gallery');

  final String errFillOut = AppLocalizations().trans('key_err_fill_out');
  //Get started
  final String getStarted = AppLocalizations().trans('key_get_started');
  final String mobileNumber = AppLocalizations().trans('key_mobile_number');
  final String phoneNumber = AppLocalizations().trans('key_phone_number');
  final String phoneNumberNotValid = AppLocalizations().trans('key_phone_invalid');
  final String bySignin = AppLocalizations().trans('key_by_sign_in');
  final String terms = AppLocalizations().trans('key_terms');
  final String and = AppLocalizations().trans('key_and');
  final String privacyPolicy = AppLocalizations().trans('key_privacy_policy');

  //Authen
  final String login = AppLocalizations().trans('key_login');
  final String password = AppLocalizations().trans('key_password');
  final String resetPassword = AppLocalizations().trans('key_reset_password');
  final String resend = AppLocalizations().trans('key_resend');
  final String resendCode = AppLocalizations().trans('key_resend_code');
  final String enter6VerificationCode = AppLocalizations().trans('key_enter_6_verification_code');
  final String enter4VerificationCode = AppLocalizations().trans('key_enter_4_verification_code');
  final String notReceiveAnything = AppLocalizations().trans('key_not_receive_anything');
  final String signUp = AppLocalizations().trans('key_sign_up');
  final String email = AppLocalizations().trans('key_email');
  final String referralCode = AppLocalizations().trans('key_referral_code');
  final String optional = AppLocalizations().trans('key_optional');
  final String verifyAccount = AppLocalizations().trans('key_verify_account');
  final String submit = AppLocalizations().trans('key_submit');

  //List biz
  final String switchToBiz = AppLocalizations().trans('key_switch_to_biz');
  final String listBizHeader = AppLocalizations().trans('key_list_biz_header');
  final String emptyBiz = AppLocalizations().trans('key_empty_biz');
  final String owner = AppLocalizations().trans('key_owner');
  final String manager = AppLocalizations().trans('key_manager');
  //Create/Edit biz
  final String createBiz = AppLocalizations().trans('key_create_biz');
  final String editBiz = AppLocalizations().trans('key_edit_biz');
  final String createBizHeader = AppLocalizations().trans('key_create_edit_header');
  final String updateBizSuccess = AppLocalizations().trans('key_update_biz_success');
  final String bizDetailTitle = AppLocalizations().trans('key_biz_details');
  final String bizName = AppLocalizations().trans('key_biz_name');
  final String bizNameRequired = AppLocalizations().trans('key_biz_name_required');
  
  final String abnRequired = AppLocalizations().trans('key_abn_required');
  final String description = AppLocalizations().trans('key_description');
  final String bizContactTitle = AppLocalizations().trans('key_contact_details');
  final String website = AppLocalizations().trans('key_website');
  final String emailAdd = AppLocalizations().trans('key_email_address');
  final String emailNotValid = AppLocalizations().trans('key_email_invalid');

  final String bizOtherTitle = AppLocalizations().trans('key_biz_other_info');
  final String categories = AppLocalizations().trans('key_categories');
  final String categoriesEmpty = AppLocalizations().trans('key_empty_categories');
  final String bizHours = AppLocalizations().trans('key_biz_hours');
  final String bizHoursRequired = AppLocalizations().trans('key_biz_hours_required');
  final String bizSetManually = AppLocalizations().trans('key_set_manually');
  final String bizSetAllManually = AppLocalizations().trans('key_set_all_manually');
  final String bizSetOpen24 = AppLocalizations().trans('key_set_open_24h');
  final String bizSetAllOpen24 = AppLocalizations().trans('key_set_all_open_24h');
  final String bizSetClosed = AppLocalizations().trans('key_set_closed');
  final String bizSetAllClosed = AppLocalizations().trans('key_set_all_closed');
  final String open24hours = AppLocalizations().trans('key_open_24_hours');
  final String closed = AppLocalizations().trans('key_closed');
  final String timeRange = AppLocalizations().trans('key_time_range');
  final String timeRangeError = AppLocalizations().trans('key_time_range_error');
  
  final String time = AppLocalizations().trans('key_time');
  final String begin = AppLocalizations().trans('key_begin');
  final String end = AppLocalizations().trans('key_end');
  final String from = AppLocalizations().trans('key_from');
  final String to = AppLocalizations().trans('key_to');

  final String daily = AppLocalizations().trans('key_daily');
  final String monday = AppLocalizations().trans('key_monday');
  final String tuesday = AppLocalizations().trans('key_tuesday');
  final String wednesday = AppLocalizations().trans('key_wednesday');
  final String thursday = AppLocalizations().trans('key_thursday');
  final String friday = AppLocalizations().trans('key_friday');
  final String saturday = AppLocalizations().trans('key_saturday');
  final String sunday = AppLocalizations().trans('key_sunday');
  final String mondayShort = AppLocalizations().trans('key_mon');
  final String tuesdayShort = AppLocalizations().trans('key_tue');
  final String wednesdayShort = AppLocalizations().trans('key_wed');
  final String thursdayShort = AppLocalizations().trans('key_thu');
  final String fridayShort = AppLocalizations().trans('key_fri');
  final String saturdayShort = AppLocalizations().trans('key_sat');
  final String sundayShort = AppLocalizations().trans('key_sun');

  final String january = AppLocalizations().trans('key_january');
  final String february = AppLocalizations().trans('key_february');
  final String march = AppLocalizations().trans('key_march');
  final String april = AppLocalizations().trans('key_april');
  final String may = AppLocalizations().trans('key_may');
  final String june = AppLocalizations().trans('key_june');
  final String july = AppLocalizations().trans('key_july');
  final String august = AppLocalizations().trans('key_august');
  final String september = AppLocalizations().trans('key_september');
  final String october = AppLocalizations().trans('key_october');
  final String november = AppLocalizations().trans('key_november');
  final String december = AppLocalizations().trans('key_december');

  final String askCreateBizTitle = AppLocalizations().trans('key_ask_create_biz_title');
  final String askCreateBizBody = AppLocalizations().trans('key_ask_create_biz_body');
  final String askUpdateBizTitle = AppLocalizations().trans('key_ask_update_biz_title');
  final String askUpdateBizBody = AppLocalizations().trans('key_ask_update_biz_body');

  final String almostThere = AppLocalizations().trans('key_almost_there');
  final String updateBizTitle = AppLocalizations().trans('key_update_biz_success');
  final String thankCreate = AppLocalizations().trans('key_thank_create');
  final String thankUpdate = AppLocalizations().trans('key_thank_update');
  final String bodyCreateUpdateReview = AppLocalizations().trans('key_create_update_review');
  final String bodyCreateUpdateNotif = AppLocalizations().trans('key_will_receive_notification');
  //Media
  final String bizLogoProfile = AppLocalizations().trans('key_logo_profile');
  final String askRemoveLogoTitle = AppLocalizations().trans('key_ask_remove_logo_title');
  final String askRemoveLogoBody = AppLocalizations().trans('key_ask_remove_logo_body');
  final String imageGallery = AppLocalizations().trans('key_image_gallery');
  final String coverImage = AppLocalizations().trans('key_cover_image');
  final String makeCoverDesc = AppLocalizations().trans('key_make_cover_description');
  final String emptyMedia = AppLocalizations().trans('key_empty_medias');
  final String setCover = AppLocalizations().trans('key_set_cover');
  final String removeCover = AppLocalizations().trans('key_remove_cover');
  final String askRemoveImageTitle = AppLocalizations().trans('key_ask_remove_image_title');
  final String askRemoveImageBody = AppLocalizations().trans('key_ask_remove_image_body');
  //Agreement
  final String agreement = AppLocalizations().trans('key_agreement');
  final String editSignature = AppLocalizations().trans('key_edit_signature');
  final String clear = AppLocalizations().trans('key_clear');
  final String emptySignature = AppLocalizations().trans('key_no_signature');

  //Biz detail
  final String myBizTitle = AppLocalizations().trans('key_my_biz');
  final String editDetails = AppLocalizations().trans('key_edit_details');
  final String openToday = AppLocalizations().trans('key_open_today');
  final String statusPending = AppLocalizations().trans('key_pending');
  final String statusApproval = AppLocalizations().trans('key_approval');
  final String statusReject = AppLocalizations().trans('key_reject');

  //Wallet
  final String balance = AppLocalizations().trans('key_balance');
  final String dashboard = AppLocalizations().trans('key_dashboard');
  final String myBank = AppLocalizations().trans('key_my_bank');
  final String earn = AppLocalizations().trans('key_earn');
  final String currentBalance = AppLocalizations().trans('key_current_balance');
  final String earnThisMonth = AppLocalizations().trans('key_earn_month');
  final String getCashOut = AppLocalizations().trans('key_get_cash_out');
  final String noEarning = AppLocalizations().trans('key_no_earn');
  final String bankDetails = AppLocalizations().trans('key_bank_details');
  final String bankAccDetails = AppLocalizations().trans('key_bank_acc_details');
  final String bankName = AppLocalizations().trans('key_bank_name');
  final String holderName = AppLocalizations().trans('key_holder_name');
  final String accountName = AppLocalizations().trans('key_acc_name');
  final String accountNameRequired = AppLocalizations().trans('key_acc_name_required');
  final String accountNumber = AppLocalizations().trans('key_acc_number');
  final String accountNumberRequired = AppLocalizations().trans('key_acc_number_required');
  final String last4Number = AppLocalizations().trans('key_last_4_number');
  final String bsbNumber = AppLocalizations().trans('key_bsb_number');
  final String bsbRequired = AppLocalizations().trans('key_bsb_required');
  final String bsbIsShort = AppLocalizations().trans('key_bsb_is_short');
  final String amount = AppLocalizations().trans('key_amount');
  final String remark = AppLocalizations().trans('key_remark');
  final String unverified = AppLocalizations().trans('key_stripe_unverified');
  final String pending = AppLocalizations().trans('key_stripe_pending');
  final String verified = AppLocalizations().trans('key_stripe_verified');
  final String verifyStripe = AppLocalizations().trans('key_verify_stripe');
  final String selectFrontID = AppLocalizations().trans('key_select_front_id');
  final String selectBackID = AppLocalizations().trans('key_select_back_id');
  final String checkCliking = AppLocalizations().trans('key_by_clicking');
  final String checkOurTerms = AppLocalizations().trans('key_our_terms');
  final String checkAndThe = AppLocalizations().trans('key_and_the');
  final String errDateFromTo = AppLocalizations().trans('key_date_from_to_error');
  final String askUpdateBankTitle = AppLocalizations().trans('key_ask_update_bank_title');
  final String askUpdateBankBody = AppLocalizations().trans('key_ask_update_bank_body');
  //Earning tab 
  final String modalFilterTitle = AppLocalizations().trans('key_earn_filter_title');
  final String buttonApplyFilter = AppLocalizations().trans('key_apply_filter');
  final String buttonResetFilter = AppLocalizations().trans('key_reset_filter');
  final String timePeriod = AppLocalizations().trans('key_time_period');
  final String selectTimePeriod = AppLocalizations().trans('key_select_time_period');
  
  final String sort = AppLocalizations().trans('key_sort');
  final String highestFirst = AppLocalizations().trans('key_highest_first');
  final String lowestFirst = AppLocalizations().trans('key_lowest_first');
  final String oldestFirst = AppLocalizations().trans('key_oldest_first');
  final String newestFirst = AppLocalizations().trans('key_newest_first');

  //Admin tab page
  final String manageStaffTitle = AppLocalizations().trans('key_manage_staff');
  final String noPermissionShowList = AppLocalizations().trans('key_no_permission_show_list');
  final String noManagers = AppLocalizations().trans('key_empty_managers');
  final String broadcastTitle = AppLocalizations().trans('key_broadcast');
  final String broadcastDescription = AppLocalizations().trans('key_broadcast_description');
  final String role = AppLocalizations().trans('key_role');
  final String enterRoleOrSelect = AppLocalizations().trans('key_enter_or_select_role');
  final String selectARole = AppLocalizations().trans('key_select_role');
  final String noPerCreateSelectRole = AppLocalizations().trans('key_no_permission_role');
  final String permissions = AppLocalizations().trans('key_permissions');
  final String saveDetails = AppLocalizations().trans('key_save_details');
  final String deleteUser = AppLocalizations().trans('key_delete_user');
  final String roleIsRequired = AppLocalizations().trans('key_role_required');
  final String noRoleForCreate = AppLocalizations().trans('key_no_role');
  final String noRoleUpdateManager = AppLocalizations().trans('key_no_permission_update_manager');
  final String noPermissionGrant = AppLocalizations().trans('key_no_permission_granted');
  final String title = AppLocalizations().trans('key_title');
  final String titleRequired = AppLocalizations().trans('key_title_required');
  final String body = AppLocalizations().trans('key_body');
  final String bodyRequired = AppLocalizations().trans('key_body_required');
  final String sendNow = AppLocalizations().trans('key_send_now');
  
  final String askAddManagerTitle = AppLocalizations().trans('key_ask_add_manager_title');
  final String askAddManagerBody = AppLocalizations().trans('key_ask_add_manager_body');
  final String saveManagerSuccessTitle = AppLocalizations().trans('key_success_title');
  final String saveManagerSuccessBody = AppLocalizations().trans('key_save_manager_success_body');
  
  final String askUpdateManagerTitle = AppLocalizations().trans('key_ask_update_manager_title');
  final String askUpdateManagerBody = AppLocalizations().trans('key_ask_update_manager_body');
  final String updateManagerSuccessTitle = AppLocalizations().trans('key_success_title');
  final String updateManagerSuccessBody = AppLocalizations().trans('key_update_manager_success_body');

  final String askDeleteManagerTitle = AppLocalizations().trans('key_ask_delete_manager_title');
  final String askDeleteManagerBody = AppLocalizations().trans('key_ask_delete_manager_body');
  final String deleteManagerSuccessTitle = AppLocalizations().trans('key_deleted_title');
  final String deleteManagerSuccessBody = AppLocalizations().trans('key_deleted_manager_success_body');

  final String askSaveBroadcastTitle = AppLocalizations().trans('key_ask_save_broadcast_title');
  final String askSaveBroadcastBody = AppLocalizations().trans('key_ask_save_broadcast_body');
  final String saveBroadcastSuccessTitle = AppLocalizations().trans('key_success_title');
  final String saveBroadcastSuccessBody = AppLocalizations().trans('key_save_broadcast_success_body');
  
  final String askUpdateBroadcastTitle =  AppLocalizations().trans('key_ask_update_broadcast_title');
  final String askUpdateBroadcastBody =  AppLocalizations().trans('key_ask_update_broadcast_body');
  final String updateBroadcastSuccessTitle = AppLocalizations().trans('key_updated_title');
  final String updateBroadcastSuccessBody =  AppLocalizations().trans('key_update_broadcast_success_body');

  final String askDeleteBroadcastTitle = AppLocalizations().trans('key_ask_delete_broadcast_title');
  final String askDeleteBroadcastBody = AppLocalizations().trans('key_ask_delete_broadcast_body');
  final String successDeleteBroadcast = AppLocalizations().trans('key_success');

  //My profile page
  final String profileTitle = AppLocalizations().trans('key_my_profile');
  final String editProfile = AppLocalizations().trans('key_edit_profile');
  final String switchBiz = AppLocalizations().trans('key_switch_biz');
  final String seeAll = AppLocalizations().trans('key_see_all');
  //Drawer
  final String setting = AppLocalizations().trans('key_setting');
  final String notifications = AppLocalizations().trans('key_notifications');
  final String changePassword = AppLocalizations().trans('key_change_password');
  final String changeLanguage = AppLocalizations().trans('key_change_language');
  final String bizPortal = AppLocalizations().trans('key_biz_portal');
  final String logOut = AppLocalizations().trans('key_logout');
  final String support = AppLocalizations().trans('key_support');
  final String legal = AppLocalizations().trans('key_legal');
  final String version = AppLocalizations().trans('key_version');
  final String askLogout = AppLocalizations().trans('key_ask_logout');

  //Change password
  final String currentPassword = AppLocalizations().trans('key_current_password');
  final String passRequired = AppLocalizations().trans('key_password_required');
  final String newPassword = AppLocalizations().trans('key_new_password');
  final String errPassLength = AppLocalizations().trans('key_password_length_error');
  final String confirmPassword = AppLocalizations().trans('key_confirm_password');
  final String errPassMatch = AppLocalizations().trans('key_password_not_match');
  final String updateSuccessTitle = AppLocalizations().trans('key_update_pass_done_title');
  final String updatePassSuccessBody = AppLocalizations().trans('key_update_pass_done_body');

  //Edit profile
  final String updateProfileSuccessBody = AppLocalizations().trans('key_update_pass_done_body');
  final String firstName = AppLocalizations().trans('key_first_name');
  final String firstNameRequired = AppLocalizations().trans('key_first_name_required');
  final String lastName = AppLocalizations().trans('key_last_name');
  final String lastNameRequired = AppLocalizations().trans('key_last_name_required');
  final String dateOfBirth = AppLocalizations().trans('key_date_of_birth');
  final String address = AppLocalizations().trans('key_address');
  final String addressRequired = AppLocalizations().trans('key_address_required');
}

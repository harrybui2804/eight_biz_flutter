import 'package:flutter/material.dart';

import 'colors.dart';

class AppStyles {

  static TextStyle bigTextCustom({ double fontSize = 35.0, Color color = AppColors.darkGrayish, FontWeight fontWeight = FontWeight.w500}) {
    return TextStyle(
        fontSize: fontSize,
        color: color,
        fontWeight: fontWeight
    );
  }

  static TextStyle navTitle({Color color = AppColors.primaryOrange}) {
    return TextStyle(
        fontSize: 20,
        color: color,
        fontWeight: FontWeight.w500
    );
  }

  static TextStyle headingTextCustom({ Color color = AppColors.secondaryColor, FontWeight fontWeight = FontWeight.w500}) {
    return TextStyle(
        fontSize: 25,
        color: color,
        fontWeight: fontWeight
    );
  }

  static TextStyle titleTextCustom({ Color color = AppColors.secondaryColor, FontWeight fontWeight = FontWeight.w500}) {
    return TextStyle(
        fontSize: 17,
        color: color,
        fontWeight: fontWeight
    );
  }

  static TextStyle normalTextCustom({ Color color = AppColors.secondaryColor, double fontSize = 15, FontWeight fontWeight = FontWeight.w300}) {
    return TextStyle(
        fontSize: fontSize,
        color: color,
        fontWeight: fontWeight
    );
  }

  static TextStyle smallTextCustom({ Color color = AppColors.secondaryColor, FontWeight fontWeight = FontWeight.w300}) {
    return TextStyle(
        fontSize: 12,
        color: color,
        fontWeight: fontWeight
    );
  }
}
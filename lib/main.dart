import 'dart:ui';

import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/screens/auth/get_started.dart';
import 'package:eight_biz_flutter/screens/auth/login_page.dart';
import 'package:eight_biz_flutter/screens/auth/reset_password_page.dart';
import 'package:eight_biz_flutter/screens/auth/signup_page.dart';
import 'package:eight_biz_flutter/screens/auth/splash_page.dart';
import 'package:eight_biz_flutter/screens/auth/verify_page.dart';
import 'package:eight_biz_flutter/screens/biz/create_biz.dart';
import 'package:eight_biz_flutter/screens/biz/list_biz_page.dart';
import 'package:eight_biz_flutter/screens/home/home_page.dart';
import 'package:eight_biz_flutter/screens/profile/change_laguage.dart';
import 'package:eight_biz_flutter/screens/profile/change_password_page.dart';
import 'package:eight_biz_flutter/screens/walkthrough/walkthrough_page.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'screens/biz/biz_agreement_page.dart';
import 'screens/notification/notification_page.dart';
import 'screens/profile/edit_profile_page.dart';
import 'utils/app_localizations_delegate.dart';

class MyApp extends StatefulWidget {
  final Store<AppState> store;
  MyApp(this.store);

  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  AppLocalizationsDelegate appLocalizationsDelegate;

  @override
  void initState() {
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      
      print('firebase token= $token');
    });
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState> (
      store: widget.store,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: new ThemeData(
          brightness: Brightness.light,
          accentColor: AppColors.primaryOrange,
          primaryColor: AppColors.primaryOrange,
          canvasColor: Colors.white
        ),
        supportedLocales: [  
          Locale('en'),
          Locale('zh'),
        ],  
        localizationsDelegates: [  
          AppLocalizationsDelegate(),  
          GlobalMaterialLocalizations.delegate,  
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        localeResolutionCallback: (Locale locale, Iterable<Locale> supportedLocales) {
          try {
            print('localeResolutionCallback locale ${locale.languageCode}');
            for (Locale supportedLocale in supportedLocales) {
              if(AppConfig().language != null && AppConfig().language == supportedLocale.languageCode) {
                AppConfig().setLocale(supportedLocale);
                return supportedLocale;
              } else if(locale != null) {
                if (supportedLocale.languageCode == locale.languageCode || supportedLocale.countryCode == locale.countryCode) {
                  AppConfig().setLocale(supportedLocale);
                  return supportedLocale;
                }
              }
            }
            AppConfig().setLocale(supportedLocales.first);
            return supportedLocales.first;
          } catch (e) {
            return supportedLocales.first;
          }
        },
        initialRoute: '/',
        navigatorKey: Keys.navKey,
        onGenerateRoute: (settings) {
          final name = settings.name;
          var content;
          if(name == SplashPage.routeName) {
            content = SplashPage();
          }
          if(name == WalkThroughPage.routeName) {
            content = WalkThroughPage();
          }
          if(name == GetStarterdPage.routeName) {
            content = GetStarterdPage();
          }
          if(name == LoginPage.routeName) {
            content = LoginPage();
          }
          if(name == VerifyPage.routeName) {
            content = VerifyPage();
          }
          if(name == SignUpPage.routeName) {
            content = SignUpPage();
          }
          if(name == ResetPasswordPage.routeName) {
            content = ResetPasswordPage();
          }
          if(name == ListBusinessPage.routeName) {
            var params = settings.arguments as ListBizPageParams;
            content = ListBusinessPage(params);
          }
          if(name == CreateBizPage.routeName) {
            var params = settings.arguments as CreateBizParams;
            content = CreateBizPage(params.index, params);
          } 
          if(name == BizAgreementPage.routeName) {
            String businessId = settings.arguments;
            content = BizAgreementPage(businessId);
          } 
          if(name == HomeTabPage.routeName) {
            content = HomeTabPage();
          }
          if(name == EditProfilePage.routeName) {
            content = EditProfilePage();
          }
          if(name == ChangePasswordPage.routeName) {
            content = ChangePasswordPage();
          }
          if(name == NotificationsPage.routeName) {
            content = NotificationsPage();
          }
          if(name == ChangeLanguagePage.routeName) {
            content = ChangeLanguagePage();
          }
          if(content != null) {
            return MaterialPageRoute(
              builder: (context) {
                AppConfig().setAppContext(context);
                return content;
              },
            );
          }
          return null;
        },
      ),
    );
  }
}
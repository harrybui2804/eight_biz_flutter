
import 'package:flutter/material.dart';
import 'config/app_config.dart';
import 'main.dart';
import 'redux/store.dart';

void main() {
  AppConfig().setAppConfig(
    appEnvironment: AppEnvironment.DEV,
    appName: 'Eight Biz - DEV',
    apiBaseUrl: 'https://dev-api.8app.online/api/',
    publicKeyStripe: 'pk_test_JSGdb6JQ2hW3QabaSwfHh4sp00WiCb8h4S',
    userPoolIdCognito: 'ap-southeast-2_2HVnBj9FL',
    clientIdCognito: '7gino4n5738t79tahgc1rdci4c',
    googleApiKey: 'AIzaSyDx7TjDPYGQWqAXgsC5yWCjOuQq1Iq24bs',
    termsUrl: 'https://eightapp.com/',
    stripeUrl: 'https://stripe.com/au/connect-account/legal',
  );

  runApp(MyApp(createStore()));
}
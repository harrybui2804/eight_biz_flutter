
import 'package:flutter/material.dart';
import 'config/app_config.dart';
import 'main.dart';
import 'redux/store.dart';

void main() {
  AppConfig().setAppConfig(
    appEnvironment: AppEnvironment.PROD,
    appName: 'Eight Biz',
    apiBaseUrl: 'https://api.eightapp.com/api/',
    publicKeyStripe: 'pk_live_3dHtaBv81zjPSPxBognyAHBk00ItGFlyQq',
    userPoolIdCognito: 'ap-southeast-2_4mTLLcOPe',
    clientIdCognito: '6iha5nr5ite37688aocgufg2c4',
    googleApiKey: 'AIzaSyDx7TjDPYGQWqAXgsC5yWCjOuQq1Iq24bs',
    termsUrl: 'https://eightapp.com/',
    stripeUrl: 'https://stripe.com/au/connect-account/legal',
  );
  runApp(MyApp(createStore()));
}
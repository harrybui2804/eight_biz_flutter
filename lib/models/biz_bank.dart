import 'package:equatable/equatable.dart';

class BizBank extends Equatable {
  final String bankName;
  final String bsb;
  final String accountNo;
  final String accountName;
  final String last4Number;

  BizBank({
    this.bankName,
    this.bsb,
    this.accountNo,
    this.accountName,
    this.last4Number
  });

  @override
  List<Object> get props => [bankName, bsb, accountNo, accountName, last4Number];

  
}
import 'package:equatable/equatable.dart';

class BizCategory extends Equatable {
  final String id;
  final String name;
  final String imageUrl;

  BizCategory({
    this.id,
    this.name,
    this.imageUrl
  });

  @override
  List<Object> get props => [id, name, imageUrl];
}
import 'package:equatable/equatable.dart';

class BizManager extends Equatable {
  final String id;
  final String role;
  final String roleId;
  final bool isNotification;
  final bool isActive;
  final UserManager user;

  BizManager({
    this.id,
    this.role,
    this.roleId,
    this.isNotification,
    this.isActive,
    this.user
  });

  @override
  List<Object> get props => [id, role, roleId, isNotification, isActive, user];

  @override
  String toString() => 'BizManager { id: $id, role: $role, roleId: $roleId, isNotification: $isNotification, isActive: $isActive, user: ${user.toString()} }';
}

class UserManager {
  final String id;
  final String firstName;
  final String lastName;
  final String customerCode;
  final String email;
  final String phone;

  UserManager({
    this.id,
    this.firstName,
    this.lastName,
    this.customerCode,
    this.email,
    this.phone
  });

  @override
  String toString() => 'UserManager { id: $id, firstName: $firstName, lastName: $lastName, customerCode: $customerCode, email: $email, phone: $phone }';
}
import 'package:equatable/equatable.dart';

class BizOpenHour extends Equatable {
  final List<String> monday;
  final List<String> tuesday;
  final List<String> wednesday;
  final List<String> thursday;
  final List<String> friday;
  final List<String> saturday;
  final List<String> sunday;

  BizOpenHour({
    this.monday,
    this.tuesday, 
    this.wednesday,
    this.thursday,
    this.friday,
    this.saturday,
    this.sunday
  });

  BizOpenHour copyWith({
    List<String> monday,
    List<String> tuesday,
    List<String> wednesday,
    List<String> thursday,
    List<String> friday,
    List<String> saturday,
    List<String> sunday
  }) {
    return BizOpenHour(
      monday: monday ?? this.monday,
      tuesday: tuesday ?? this.tuesday,
      wednesday: wednesday ?? this.wednesday,
      thursday: thursday ?? this.thursday,
      friday: friday ?? this.friday,
      saturday: saturday ?? this.saturday,
      sunday: sunday ?? this.sunday
    );
  }

  static BizOpenHour fromGetBizDetail(dynamic json) {
    return BizOpenHour(
      monday: json['mon'] != null ? (json['mon'] as String).split(',') : [],
      tuesday: json['tue'] != null ? (json['tue'] as String).split(',') : [],
      wednesday: json['wed'] != null ? (json['wed'] as String).split(',') : [],
      thursday: json['thu'] != null ? (json['thu'] as String).split(',') : [],
      friday: json['fri'] != null ? (json['fri'] as String).split(',') : [],
      saturday: json['sat'] != null ? (json['sat'] as String).split(',') : [],
      sunday: json['sun'] != null ? (json['sun'] as String).split(',') : []
    );
  }

  String openingToday() {
    DateTime date = DateTime.now();
    String res = '';
    switch(date.weekday) {
      case 1:
        res = monday!= null ? monday.join(',') : '';
        break;
      case 2:
        res = tuesday!= null ? tuesday.join(',') : '';
        break;
      case 3:
        res = wednesday!= null ? wednesday.join(',') : '';
        break;
      case 4:
        res = thursday!= null ? thursday.join(',') : '';
        break;
      case 5:
        res = friday!= null ? friday.join(',') : '';
        break;
      case 6:
        res = saturday!= null ? saturday.join(',') : '';
        break;
      case 7:
        res = sunday!= null ? sunday.join(',') : '';
        break;
      default:
        res = '';
        break;
    }
    return res;
  }

  List<dynamic> toJsonArray() {
    List res = [];
    res.add({
      "day":"Monday",
			"abbreviation":"mon",
			"opening_hours": monday != null ? monday.join(',') : []
    });
    res.add({
      "day":"Tuesday",
			"abbreviation":"tue",
			"opening_hours": tuesday != null ? tuesday.join(',') : []
    });
    res.add({
      "day":"Wednesday",
			"abbreviation":"wed",
			"opening_hours": wednesday != null ? wednesday.join(',') : []
    });
    res.add({
      "day":"Thursday",
			"abbreviation":"thu",
			"opening_hours": thursday != null ? thursday.join(',') : []
    });
    res.add({
      "day":"Friday",
			"abbreviation":"fri",
			"opening_hours": friday != null ? friday.join(','): []
    });
    res.add({
      "day":"Saturday",
			"abbreviation":"sat",
			"opening_hours": saturday != null ? saturday.join(',') : []
    });
    res.add({
      "day":"Sunday",
			"abbreviation":"sun",
			"opening_hours": sunday != null ? sunday.join(',') : []
    });
    return res;
  }

  bool valid() { 
    return monday != null && monday.length > 0 && 
      tuesday != null && tuesday.length > 0 && 
      wednesday != null && wednesday.length > 0 && 
      thursday != null && thursday.length > 0 &&
      friday != null && friday.length > 0 &&
      saturday != null && saturday.length > 0 &&
      sunday != null && sunday.length > 0;
  }

  @override
  String toString() {
    return 'BizOpenHour { monday: $monday, tuesday: $tuesday, wednesday: $wednesday' +
      ', thursday: $thursday, friday: $friday, saturday: $saturday, sunday: $sunday}';
  }

  @override
  List<Object> get props => [monday, tuesday, wednesday, thursday, friday, saturday, sunday];

}
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

enum BusinessRole {
  listBiz, getBiz, updateBiz, deleteBiz, getQrcode, printQrcode, updateQrcode, listDeals, addDeal, getDeal,
  updateDeal, deleteDeal, listBroadcast, getBroadcast, createBroadcast, updateBroadcast, deleteBroadcast,
  getGallery, uploadMedia, deleteMedia, listFavoriteMembers, listManagers, getManager, addManager, updateManager,
  deleteManager, managerActivities, getBizWallet, listBizWalletWithDraw, bizWalletWithDraw, getWalletTransfer,
  updateBizBank, getBizBank, listTransactions, getTransactions, listManagerRole, getManagerRole, createManagerRole,
  updateManagerRole, deleteManagerRole
}

class BusinessPermision extends Equatable {
  final String code;
  final String name;
  final bool isGranted;

  BusinessPermision({
    @required this.code,
    @required this.name,
    @required this.isGranted
  });

  BusinessPermision changeGranted(bool value) {
    return BusinessPermision(
      code: this.code,
      name: this.name,
      isGranted: value ?? this.isGranted
    );
  }

  static BusinessPermision fromJson(dynamic json) {
    return BusinessPermision(
      code: json['codename'] ?? '',
      name: json['name'] ?? '',
      isGranted: json['is_granted'],
    );
  }

  BusinessRole get role {
    BusinessRole bizRole;
    switch (code) {
      case 'admin_business_list_businesses':
        bizRole = BusinessRole.listBiz;
        break;
      case 'admin_business_get_business':
        bizRole = BusinessRole.getBiz;
        break;
      case 'admin_business_update_business':
        bizRole = BusinessRole.updateBiz;
        break;
      case 'admin_business_delete_business':
        bizRole = BusinessRole.deleteBiz;
        break;
      case 'admin_business_get_qrcode':
        bizRole = BusinessRole.getQrcode;
        break;
      case 'admin_business_print_qrcode':
        bizRole = BusinessRole.printQrcode;
        break;
      case 'admin_business_update_qrcode':
        bizRole = BusinessRole.updateQrcode;
        break;
      case 'admin_business_list_deals':
        bizRole = BusinessRole.listDeals;
        break;
      case 'admin_business_add_deal':
        bizRole = BusinessRole.addDeal;
        break;
      case 'admin_business_get_deal':
        bizRole = BusinessRole.getDeal;
        break;
      case 'admin_business_update_deal':
        bizRole = BusinessRole.updateDeal;
        break;
      case 'admin_business_delete_deal':
        bizRole = BusinessRole.deleteDeal;
        break;
      case 'admin_business_list_broadcast_notification':
        bizRole = BusinessRole.listBroadcast;
        break;
      case 'admin_business_get_broadcast_notification':
        bizRole = BusinessRole.getBroadcast;
        break;
      case 'admin_business_create_broadcast_notification':
        bizRole = BusinessRole.createBroadcast;
        break;
      case 'admin_business_update_broadcast_notification':
        bizRole = BusinessRole.updateBroadcast;
        break;
      case 'admin_business_delete_broadcast_notification':
        bizRole = BusinessRole.deleteBroadcast;
        break;
      case 'admin_business_get_gallery':
        bizRole = BusinessRole.getGallery;
        break;
      case 'admin_business_upload_media':
        bizRole = BusinessRole.uploadMedia;
        break;
      case 'admin_business_delete_media':
        bizRole = BusinessRole.deleteMedia;
        break;
      case 'admin_business_list_favourite_members':
        bizRole = BusinessRole.listFavoriteMembers;
        break;
      case 'admin_business_list_managers':
        bizRole = BusinessRole.listManagers;
        break;
      case 'admin_business_get_manager':
        bizRole = BusinessRole.getManager;
        break;
      case 'admin_business_add_manager':
        bizRole = BusinessRole.addManager;
        break;
      case 'admin_business_update_manager':
        bizRole = BusinessRole.updateManager;
        break;
      case 'admin_business_delete_manager':
        bizRole = BusinessRole.deleteManager;
        break;
      case 'admin_business_manager_activities':
        bizRole = BusinessRole.managerActivities;
        break;
      case 'admin_business_get_business_wallet':
        bizRole = BusinessRole.getBizWallet;
        break;
      case 'admin_business_list_business_wallet_withdraw':
        bizRole = BusinessRole.listBizWalletWithDraw;
        break;
      case 'admin_business_business_wallet_withdraw':
        bizRole = BusinessRole.bizWalletWithDraw;
        break;
      case 'admin_business_get_business_wallet_transfer':
        bizRole = BusinessRole.getWalletTransfer;
        break;
      case 'admin_business_update_bank':
        bizRole = BusinessRole.updateBizBank;
        break;
      case 'admin_business_get_bank':
        bizRole = BusinessRole.getBizBank;
        break;
      case 'admin_business_list_business_transactions':
        bizRole = BusinessRole.listTransactions;
        break;
      case 'admin_business_get_business_transactions':
        bizRole = BusinessRole.getTransactions;
        break;
      case 'admin_business_list_business_manager_role':
        bizRole = BusinessRole.listManagerRole;
        break;
      case 'admin_business_get_business_manager_role':
        bizRole = BusinessRole.getManagerRole;
        break;
      case 'admin_business_create_business_manager_role':
        bizRole = BusinessRole.createManagerRole;
        break;
      case 'admin_business_update_business_manager_role':
        bizRole = BusinessRole.updateManagerRole;
        break;
      case 'admin_business_delete_business_manager_role':
        bizRole = BusinessRole.deleteManagerRole;
        break;
    }
    return bizRole;
  }

  static List<dynamic> listToJson(List<BusinessPermision> permissions) {
    final lst = permissions.map((per) {
      return {
        "codename": per.code,
        "name": per.name,
        "is_granted": per.isGranted,
      };
    }).toList();
    return lst;
  }

  List<BusinessPermision> initialAll() {
    List<BusinessPermision> lst = [];
    lst.add(
      BusinessPermision(
        code: 'admin_business_list_businesses',
        name: 'List businesses',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_get_business',
        name: 'Get Business',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_update_business',
        name: 'Update Business',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_delete_business',
        name: 'Delete Business',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_get_qrcode',
        name: 'Get QR Code Details',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_print_qrcode',
        name: 'Print QR Code',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_update_qrcode',
        name: 'Update QR Code Details',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_list_deals',
        name: 'List Deals',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_add_deal',
        name: 'Add Deal',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_get_deal',
        name: 'Get Deal',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_update_deal',
        name: 'Update Deal',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_delete_deal',
        name: 'Delete Deal',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_list_broadcast_notification',
        name: 'List Business Broadcast Notification',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_get_broadcast_notification',
        name: 'Get Business Broadcast Notification Detail',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_create_broadcast_notification',
        name: 'Create Business Broadcast Notification',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_update_broadcast_notification',
        name: 'Delete Business Broadcast Notification',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_get_gallery',
        name: 'List Gallery',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_upload_media',
        name: 'Update Gallery',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_delete_media',
        name: 'Delete Gallery',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_list_favourite_members',
        name: 'List Favourite Members',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_list_managers',
        name: 'List Managers',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_get_manager',
        name: 'Get Manager Detail',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_add_manager',
        name: 'Add Manager',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_update_manager',
        name: 'Update Manager',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_delete_manager',
        name: 'Delete Manager',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_manager_activities',
        name: 'Block/Unlock Business Manager',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_get_business_wallet',
        name: 'Business Wallet',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_list_business_wallet_withdraw',
        name: 'List Withdraw Business Wallet',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_business_wallet_withdraw',
        name: 'Withdraw Business Wallet',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_get_business_wallet_transfer',
        name: 'Transfer Business Balance To Owner',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_update_bank',
        name: 'Update Business Bank',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_get_bank',
        name: 'Get Business Bank',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_list_business_transactions',
        name: 'List Business Transactions',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_get_business_transactions',
        name: 'Get Business Transactions Details',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_list_business_manager_role',
        name: 'List Business Manager Role',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_get_business_manager_role',
        name: 'Get Business Manager Role Detail',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_create_business_manager_role',
        name: 'Create Business Manager Role Detail',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_update_business_manager_role',
        name: 'Update Business Manager Role Detail',
        isGranted: false
      )
    );
    lst.add(
      BusinessPermision(
        code: 'admin_business_delete_business_manager_role',
        name: 'Delete Business Manager Role Detail',
        isGranted: false
      )
    );
    return lst;
  }

  @override
  List<Object> get props => [code, name, isGranted];

  @override
  String toString() => '$name $code $isGranted';
}


/*
{
    "codename": "admin_business_list_businesses",
    "name": "List businesses",
    "is_granted": true
},
{
    "codename": "admin_business_get_business",
    "name": "Get Business",
    "is_granted": true
},
{
    "codename": "admin_business_update_business",
    "name": "Update Business",
    "is_granted": true
},
{
    "codename": "admin_business_delete_business",
    "name": "Delete Business",
    "is_granted": true
},
{
    "codename": "admin_business_get_qrcode",
    "name": "Get QR Code Details",
    "is_granted": true
},
{
    "codename": "admin_business_print_qrcode",
    "name": "Print QR Code",
    "is_granted": true
},
{
    "codename": "admin_business_update_qrcode",
    "name": "Update QR Code Details",
    "is_granted": true
},
{
    "codename": "admin_business_list_deals",
    "name": "List Deals",
    "is_granted": true
},
{
    "codename": "admin_business_add_deal",
    "name": "Add Deal",
    "is_granted": true
},
{
    "codename": "admin_business_get_deal",
    "name": "Get Deal",
    "is_granted": true
},
{
    "codename": "admin_business_update_deal",
    "name": "Update Deal",
    "is_granted": true
},
{
    "codename": "admin_business_delete_deal",
    "name": "Delete Deal",
    "is_granted": true
},
{
    "codename": "admin_business_list_broadcast_notification",
    "name": "List Business Broadcast Notification",
    "is_granted": true
},
{
    "codename": "admin_business_get_broadcast_notification",
    "name": "Get Business Broadcast Notification Detail",
    "is_granted": true
},
{
    "codename": "admin_business_create_broadcast_notification",
    "name": "Create Business Broadcast Notification",
    "is_granted": true
},
{
    "codename": "admin_business_update_broadcast_notification",
    "name": "Update Business Broadcast Notification",
    "is_granted": true
},
{
    "codename": "admin_business_delete_broadcast_notification",
    "name": "Delete Business Broadcast Notification",
    "is_granted": true
},
{
    "codename": "admin_business_get_gallery",
    "name": "List Gallery",
    "is_granted": true
},
{
    "codename": "admin_business_upload_media",
    "name": "Update Gallery",
    "is_granted": true
},
{
    "codename": "admin_business_delete_media",
    "name": "Delete Gallery",
    "is_granted": true
},
{
    "codename": "admin_business_list_favourite_members",
    "name": "List Favourite Members",
    "is_granted": true
},
{
    "codename": "admin_business_list_managers",
    "name": "List Managers",
    "is_granted": true
},
{
    "codename": "admin_business_get_manager",
    "name": "Get Manager Detail",
    "is_granted": true
},
{
    "codename": "admin_business_add_manager",
    "name": "Add Manager",
    "is_granted": true
},
{
    "codename": "admin_business_update_manager",
    "name": "Update Manager",
    "is_granted": true
},
{
    "codename": "admin_business_delete_manager",
    "name": "Delete Manager",
    "is_granted": true
},
{
    "codename": "admin_business_manager_activities",
    "name": "Block/Unlock Business Manager",
    "is_granted": true
},
{
    "codename": "admin_business_get_business_wallet",
    "name": "Business Wallet",
    "is_granted": true
},
{
    "codename": "admin_business_list_business_wallet_withdraw",
    "name": "List Withdraw Business Wallet",
    "is_granted": true
},
{
    "codename": "admin_business_business_wallet_withdraw",
    "name": "Withdraw Business Wallet",
    "is_granted": true
},
{
    "codename": "admin_business_get_business_wallet_transfer",
    "name": "Transfer Business Balance To Owner",
    "is_granted": true
},
{
    "codename": "admin_business_update_bank",
    "name": "Update Business Bank",
    "is_granted": true
},
{
    "codename": "admin_business_get_bank",
    "name": "Get Business Bank",
    "is_granted": true
},
{
    "codename": "admin_business_list_business_transactions",
    "name": "List Business Transactions",
    "is_granted": true
},
{
    "codename": "admin_business_get_business_transactions",
    "name": "Get Business Transactions Details",
    "is_granted": true
},
{
    "codename": "admin_business_list_business_manager_role",
    "name": "List Business Manager Role",
    "is_granted": true
},
{
    "codename": "admin_business_get_business_manager_role",
    "name": "Get Business Manager Role Detail",
    "is_granted": true
},
{
    "codename": "admin_business_create_business_manager_role",
    "name": "Create Business Manager Role Detail",
    "is_granted": true
},
{
    "codename": "admin_business_update_business_manager_role",
    "name": "Update Business Manager Role Detail",
    "is_granted": true
},
{
    "codename": "admin_business_delete_business_manager_role",
    "name": "Delete Business Manager Role Detail",
    "is_granted": true
}
*/
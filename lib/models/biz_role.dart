import 'package:equatable/equatable.dart';

class BizRole extends Equatable {
  final String id;
  final String role;

  BizRole({this.id, this.role});

  @override
  List<Object> get props => [id, role];

  
}
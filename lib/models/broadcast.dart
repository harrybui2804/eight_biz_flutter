import 'package:equatable/equatable.dart';

class Broadcast extends Equatable {
  final String id;
  final String title;
  final String body;
  final String description;
  final String createdAt;
  final bool status;
  final int totalSend;
  final int totalReceived;
  final int totalRead;

  Broadcast({
    this.id,
    this.title,
    this.body,
    this.description,
    this.createdAt,
    this.status,
    this.totalSend,
    this.totalReceived,
    this.totalRead
  });

  Broadcast copyWith({
    String title,
    String body,
    String description,
    String createdAt,
    bool status,
    int totalSend,
    int totalReceived,
    int totalRead
  }) {
    return Broadcast(
      id: this.id,
      title: title ?? this.title,
      body: body ?? this.body,
      description: description ?? this.description,
      createdAt: createdAt ?? this.createdAt,
      status: status ?? this.status,
      totalSend: totalSend ?? this.totalSend,
      totalReceived: totalReceived ?? this.totalReceived,
      totalRead: totalRead ?? this.totalRead
    );
  }

  @override
  List<Object> get props => [id, title, body, description, createdAt, status, totalSend, totalReceived, totalRead];
  
}
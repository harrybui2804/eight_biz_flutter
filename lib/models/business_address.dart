import 'package:equatable/equatable.dart';

class BizAddress extends Equatable {
  final String id;
  final String address1;
  final String address2;
  final String city;
  final String country;
  final String state;
  final String createdAt;
  final double latitude;
  final double longitude;
  final String postcode;
  final String point;

  BizAddress({
    this.id,
    this.address1,
    this.address2,
    this.city,
    this.country,
    this.state,
    this.createdAt,
    this.latitude,
    this.longitude,
    this.postcode,
    this.point
  });

  @override
  String toString() {
    var str = '';
    if(address1 != null && address1.length > 0) {
      str += address1;
    } else if(address2 != null && address2.length > 0) {
      str += ', ' + address2;
    }
    if(city != null && city.length > 0) {
      str +=  ', ' + city + ', ';
    }
    if(state != null && state.length > 0) {
      str += ', ' + state;
    }
    if(country != null && country.length > 0) {
      str += ', ' + country;
    }
    return str;
  }

  BizAddress copyWith({
    String id,
    String address1,
    String address2,
    String city,
    String country,
    String state,
    String createdAt,
    double latitude,
    double longitude,
    String postcode,
    String point
  }){
    return BizAddress(
      id: id ?? this.id,
      address1: address1 ?? this.address1,
      address2: address2 ?? this.address2,
      city: city ?? this.city,
      country: country ?? this.country,
      state: state ?? this.state,
      createdAt: createdAt ?? this.createdAt,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      postcode: postcode ?? this.postcode,
      point: point ?? this.point
    );
  }

  @override
  List<Object> get props => [id, address1, address2, city, country, state, createdAt, latitude, longitude, postcode, point];
}
import 'package:equatable/equatable.dart';

import 'biz_category.dart';
import 'biz_open_hour.dart';
import 'biz_owner.dart';
import 'business_address.dart';

class BusinessDetail extends Equatable {
  final String id;
  final int status;
  final String email;
  final String website;
  final BizOpenHour openHour;
  final String abn;
  final String addresses; 
  final BizAddress address;
  final List<BizCategory> categories;
  final BizOwner owner;
  final String logoImage;
  final String coverImage;
  final List<String> listMedia;
  final Map<String, Object> approvedBy;
  final bool isApproved;
  final dynamic approvedOn;
  final double totalEarning;
  final String name;
  final String contactNumber;
  final String code;
  final dynamic offeringPercent;
  final String reason;
  final String rejectedOn;
  final Map<String, Object> rejectedBy;
  final bool isActive;
  final bool isDeleted;
  final String createdAt;
  final String description;
  final String inviteLink;

  BusinessDetail({
    this.id, this.address, this.addresses, this.categories, this.owner, this.logoImage, this.coverImage, 
    this.listMedia, this.approvedBy, this.isApproved, this.approvedOn, this.totalEarning, this.openHour, 
    this.name, this.abn, this.email, this.contactNumber, this.code, this.status, this.offeringPercent, 
    this.reason, this.rejectedOn, this.rejectedBy, this.isActive, this.isDeleted, this.createdAt, 
    this.description, this.website, this.inviteLink
  });

  BusinessDetail copyWith({
    String id, BizAddress address, String addresses, List<BizCategory> categories, BizOwner owner,
    String logoImage, String coverImage, List<dynamic> listMedia, Map<String, Object> approvedBy,
    bool isApproved, dynamic approvedOn, double totalEarning, BizOpenHour openHour, String name,
    String abn, String email, String contactNumber, String code, int status,  dynamic offeringPercent,
    String reason, String rejectedOn, Map<String, Object> rejectedBy, bool isActive, bool isDeleted,
    String createdAt, String description, String website, String inviteLink
  }) {
    return BusinessDetail(
      id: id ?? this.id, 
      address: address ?? this.address, 
      addresses: addresses ?? this.addresses, 
      categories: categories ?? this.categories, 
      owner: owner ?? this.owner, 
      logoImage: logoImage ?? this.logoImage, 
      coverImage: coverImage ?? this.coverImage, 
      listMedia: listMedia ?? this.listMedia, 
      approvedBy: approvedBy ?? this.approvedBy, 
      isApproved: isApproved ?? this.isApproved, 
      approvedOn: approvedOn ?? this.approvedOn, 
      totalEarning: totalEarning ?? this.totalEarning, 
      openHour: openHour ?? this.openHour, 
      name: name ?? this.name, 
      abn: abn ?? this.abn, 
      email: email ?? this.email, 
      contactNumber: contactNumber ?? this.contactNumber, 
      code: code ?? this.code, 
      status: status ?? this.status, 
      offeringPercent: offeringPercent ?? this.offeringPercent, 
      reason: reason ?? this.reason, 
      rejectedOn: rejectedOn ?? this.rejectedOn, 
      rejectedBy: rejectedBy ?? this.rejectedBy, 
      isActive: isActive ?? this.isActive, 
      isDeleted: isDeleted ?? this.isDeleted, 
      createdAt: createdAt ?? this.createdAt, 
      description: description ?? this.description, 
      website: website ?? this.website, 
      inviteLink: inviteLink ?? this.inviteLink
    );
  }

  BusinessDetail removeLogo() {
    return BusinessDetail(
      id: this.id, 
      address: this.address, 
      addresses: this.addresses, 
      categories: this.categories, 
      owner: this.owner, 
      logoImage: null, 
      coverImage: this.coverImage, 
      listMedia: this.listMedia, 
      approvedBy: this.approvedBy, 
      isApproved: this.isApproved, 
      approvedOn: this.approvedOn, 
      totalEarning: this.totalEarning, 
      openHour: this.openHour, 
      name: this.name, 
      abn: this.abn, 
      email: this.email, 
      contactNumber: this.contactNumber, 
      code: this.code, 
      status: this.status, 
      offeringPercent: this.offeringPercent, 
      reason: this.reason, 
      rejectedOn: this.rejectedOn, 
      rejectedBy: this.rejectedBy, 
      isActive: this.isActive, 
      isDeleted: this.isDeleted, 
      createdAt: this.createdAt, 
      description: this.description, 
      website: this.website, 
      inviteLink: this.inviteLink
    );
  }

  BusinessDetail removeCover() {
    return BusinessDetail(
      id: this.id, 
      address: this.address, 
      addresses: this.addresses, 
      categories: this.categories, 
      owner: this.owner, 
      logoImage: this.logoImage, 
      coverImage: null, 
      listMedia: this.listMedia, 
      approvedBy: this.approvedBy, 
      isApproved: this.isApproved, 
      approvedOn: this.approvedOn, 
      totalEarning: this.totalEarning, 
      openHour: this.openHour, 
      name: this.name, 
      abn: this.abn, 
      email: this.email, 
      contactNumber: this.contactNumber, 
      code: this.code, 
      status: this.status, 
      offeringPercent: this.offeringPercent, 
      reason: this.reason, 
      rejectedOn: this.rejectedOn, 
      rejectedBy: this.rejectedBy, 
      isActive: this.isActive, 
      isDeleted: this.isDeleted, 
      createdAt: this.createdAt, 
      description: this.description, 
      website: this.website, 
      inviteLink: this.inviteLink
    );
  }

  @override
  List<Object> get props => [
    id, address, addresses, categories, owner, logoImage, coverImage, listMedia, 
    approvedBy, isApproved, approvedOn, totalEarning, openHour, name, abn, 
    email, contactNumber, code, status, offeringPercent, reason, rejectedOn, 
    rejectedBy, isActive, isDeleted, createdAt, description, website, inviteLink
  ];

}
import 'package:equatable/equatable.dart';

class BusinessList extends Equatable {
  final Data business;
  final bool isOwner;
  final bool isActive;
  //final dynamic totalEarning;
  

  BusinessList({
    this.business,
    this.isOwner,
    this.isActive,
    //this.totalEarning
  });

  @override
  List<Object> get props => [business, isOwner, isActive];

}

class Data extends Equatable {
  final String id;
  final String name;
  //final String logoImage;
  //final List<ListBizTag> tags;  // {id, name}
  final String addresses;
  //final int status;
  //final bool isActive;
  

  Data({
    this.id,
    this.name,
    this.addresses,
  });

  @override
  List<Object> get props => [id, name, addresses];
}

class ListBizTag extends Equatable {
  final String id;
  final String name;

  ListBizTag({
    this.id,
    this.name
  });

  @override
  List<Object> get props => [id, name];
}
enum EarningOrderFilter {
  modifiedAtAsc,
  modifiedAtDsc,
  amountAsc,
  amountDsc
}

enum EarningTransactionFilter {
  withdraw,
  userWallet,
  bizEarning
}

enum EarningStatusFilter {
  all,
  pending,
  paid,
  declined
}

class EarningFilterHelp {
  static String statusValue(EarningStatusFilter status) {
    switch(status){
      case EarningStatusFilter.pending: 
        return "All";
      case EarningStatusFilter.pending: 
        return "Pending";
      case EarningStatusFilter.paid: 
        return "Declined";
      case EarningStatusFilter.declined: 
        return "Paid";  
      default:
        return "";
    }
  }
}
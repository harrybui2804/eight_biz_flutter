import 'dart:io';

class Media {
  final String id;
  final String url;
  final bool isCover;
  final ImageObject logo;
  final ImageObject thumbnail;
  final ImageObject medium;
  final ImageObject large;
  final File localFile;
  final bool isLocal;

  Media({
    this.id,
    this.url,
    this.isCover = false,
    this.logo,
    this.thumbnail,
    this.medium,
    this.large,
    this.localFile,
    this.isLocal = false
  });

  Media copyWith({
    String id,
    String url,
    bool isCover,
    ImageObject logo,
    ImageObject thumbnail,
    ImageObject medium,
    ImageObject large,
    File localFile,
    bool isLocal
  }) {
    return Media(
      id: id ?? this.id,
      url: url ?? this.url,
      isCover: isCover ?? this.isCover,
      logo: logo ?? this.logo,
      thumbnail: thumbnail ?? this.thumbnail,
      medium: medium ?? this.medium,
      large: large ?? this.large,
      localFile: localFile ?? this.localFile,
      isLocal: isLocal ?? this.isLocal
    );
  }

  @override
  String toString() => 'id: $id, url: $url, isCover: $isCover';
}

class ImageObject {
  final int height;
  final int width;
  final String url;

  ImageObject({
    this.height,
    this.width,
    this.url = ''
  });
}
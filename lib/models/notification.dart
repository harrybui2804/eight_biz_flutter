import 'package:equatable/equatable.dart';

class Notification extends Equatable {
  final String id;
  final NotifSender sendFrom;
  final NotifBiz business;
  final String createdAt;
  final bool isDelete;
  final bool isReceived;
  final bool isRead;
  final String title;
  final String text;
  final int type;
  final String associationId;
  final String associationName;
  final String description;
  final String sendTo;

  Notification({
    this.id,
    this.sendFrom,
    this.business,
    this.createdAt,
    this.isDelete,
    this.isReceived,
    this.isRead,
    this.title,
    this.text,
    this.type,
    this.associationId,
    this.associationName,
    this.description,
    this.sendTo
  });

  static Notification fromJson(obj) {
    var send = obj['sendFrom'];
    var biz = obj['business'];
    return Notification(
      id: obj['id'],
      sendFrom: send != null ? NotifSender(
        id: send['id'],
        email: send['email'],
        firstName: send['first_name'],
        lastName: send['last_name']
      ) : null,
      business: biz != null ? NotifBiz(
        id: biz['id'],
        name: biz['name'],
        logoImage: biz['logo_image']
      ) : null,
      createdAt: obj['created_at'],
      isDelete: obj['is_deleted'],
      isReceived: obj['is_received'],
      isRead: obj['is_read'],
      title: obj['title'],
      text: obj['text'],
      type: obj['type'],
      associationId: obj['association_id'],
      associationName: obj['association_name'],
      description: obj['description'],
      sendTo: obj['sent_to']
    );
  }

  @override
  List<Object> get props => [
    id, sendFrom, business, createdAt, isDelete, isReceived, title, 
    text, type, associationId, associationName, description, sendTo
  ];
}

class NotifBiz {
  final String id;
  final String name;
  final String logoImage;

  NotifBiz({
    this.id,
    this.name,
    this.logoImage
  });
}

class NotifSender {
  final String id;
  final String email;
  final String firstName;
  final String lastName;

  NotifSender({
    this.id,
    this.email,
    this.firstName,
    this.lastName
  });
}
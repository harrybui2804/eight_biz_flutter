import 'package:flutter/material.dart';

class ReqSignUp {
  final String registerId;
  final String firstName;
  final String lastName;
  final String phone;
  final String email;
  final String password;
  final String confirmPassword;
  final String referralCode;
  final String publicVerifyKey;
  final String privateVerifyKey;

  ReqSignUp({
    @required this.registerId,
    @required this.firstName, 
    @required this.lastName, 
    @required this.phone, 
    @required this.email, 
    @required this.password, 
    @required this.confirmPassword, 
    this.referralCode,
    @required this.publicVerifyKey,
    @required this.privateVerifyKey
  });
}
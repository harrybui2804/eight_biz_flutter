import 'package:equatable/equatable.dart';

class TransactionsGraph extends Equatable {
  final String weekDay;
  final String date;
  final double bizEarning;
  final double withDraw;
  final double userTransfer;

  TransactionsGraph({
    this.weekDay,
    this.date,
    this.bizEarning,
    this.withDraw,
    this.userTransfer
  });

  static TransactionsGraph fromJson(dynamic obj) {
    return TransactionsGraph(
      weekDay: obj['week_day'] ?? '',
      date: obj['date'] ?? '',
      bizEarning: obj['business_earning'] != null ? obj['business_earning'].toDouble() : 0,
      withDraw: obj['withdraw'] != null ? obj['withdraw'].toDouble() : 0,
      userTransfer: obj['user_transfer'] != null ? obj['user_transfer'].toDouble() : 0,
    );
  }

  @override
  List<Object> get props => [weekDay, date, bizEarning, withDraw, userTransfer];

}
import 'package:equatable/equatable.dart';

import 'business_address.dart';

class UserProfile extends Equatable {
  final String id;
  final String firstName;
  final String lastName;
  final String email;
  final String phone;
  final String displayName;
  final bool isActive;
  final String customerCode;
  final bool isStaff;
  final String imageUrl;
  final String dateOfBirth;
  final BizAddress address;
  final String inviteLink;
  final bool isRankUp;
  final double point;
  final double totalSpent;
  final int totalReferrals;

  UserProfile({
    this.id,
    this.firstName = '',
    this.lastName = '',
    this.email,
    this.phone,
    this.displayName,
    this.isActive,
    this.customerCode,
    this.isStaff,
    this.imageUrl,
    this.dateOfBirth,
    this.address,
    this.inviteLink,
    this.isRankUp,
    this.point,
    this.totalSpent,
    this.totalReferrals
  });

  @override
  List<Object> get props => [
    id, firstName, lastName, email, phone, displayName, isActive, customerCode, isStaff,
    imageUrl, dateOfBirth, address, inviteLink, isRankUp, point, totalSpent, totalReferrals
  ];

  @override
  String toString() => 'UserProfile { ' +
    'id : $id, firstName: $firstName, lastName: $lastName, email: $email, phone: $phone, ' +
    'displayName: $displayName, isActive: $isActive, customerCode: $customerCode, isStaff: $isStaff, ' +
    'imageUrl: $imageUrl, dateOfBirth: $dateOfBirth, address: $address, inviteLink: $inviteLink, ' +
    'isRankUp: $isRankUp, point: $point, totalSpent: $totalSpent, totalReferrals: $totalReferrals}';
}

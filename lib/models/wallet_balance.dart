import 'package:equatable/equatable.dart';

import 'transactions_graph.dart';

class WalletBalance extends Equatable {
  final double totalBalance;
  final double currentWeekEarning;
  final List<TransactionsGraph> currentWeekGraph;

  WalletBalance({
    this.totalBalance = 0,
    this.currentWeekEarning = 0,
    this.currentWeekGraph
  });

  @override
  List<Object> get props => [totalBalance, currentWeekEarning, currentWeekGraph];

}


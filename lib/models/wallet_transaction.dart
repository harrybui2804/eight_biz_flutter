import 'package:equatable/equatable.dart';

class WalletTransaction extends Equatable {
  final String id;
  final String transactionType;
  final bool isCredit;
  final int status;
  final String statusString;
  final double amount;
  final String createdAt;
  final String modifiedAt;
  final String bizEarning;

  WalletTransaction({
    this.id,
    this.transactionType,
    this.isCredit,
    this.status,
    this.statusString,
    this.amount,
    this.createdAt,
    this.modifiedAt,
    this.bizEarning
  });

  static WalletTransaction fromJson(json) {
    return WalletTransaction(
      id: json['id'],
      transactionType: json['transaction_type'],
      isCredit: json['is_credit'],
      status: json['status'],
      statusString: json['status_string'],
      amount: json['amount'] != null ? json['amount'].toDouble() : 0.0,
      createdAt: json['created_at'],
      modifiedAt: json['modified_at'],
      bizEarning: json['business_earning']
    );
  }

  @override
  List<Object> get props => [id, transactionType, isCredit, status, statusString, amount, createdAt, modifiedAt, bizEarning];

}
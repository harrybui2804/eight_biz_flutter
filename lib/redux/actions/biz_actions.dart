import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/models/business_list.dart';
import 'package:eight_biz_flutter/response/res_listbiz.dart';
import 'package:flutter/material.dart';

typedef void StringCallback(String message);
typedef void PermissionsCallback(List<BusinessPermision> permissions);

class ListBizLoadingAnimationAction {
  final bool loading;
  final bool loadMore;

  ListBizLoadingAnimationAction({this.loading = false, this.loadMore = false});
}

class RequestListBizAction {
  final String next;
  final StringCallback onError;

  RequestListBizAction({this.next, this.onError});
}

class RequestListBizSuccess {
  final ResListBiz data;
  final bool reload;

  RequestListBizSuccess({@required this.data, this.reload = false});
}

class SwitchBizDetailAction {
  final BusinessList businessList;
  final StringCallback onSuccess;
  final StringCallback onError;

  SwitchBizDetailAction({@required this.businessList, this.onSuccess, this.onError});
}

class EditBizDetailAction {
  final BusinessDetail business;

  EditBizDetailAction({@required this.business});
}

class LoadBizDetailAction {
  final String businessId;
  final StringCallback onSuccess;
  final StringCallback onError;

  LoadBizDetailAction(this.businessId, {this.onSuccess, this.onError});
}

class LoadUserPermissionsAction {
  final String businessId;
  final PermissionsCallback onSuccess;
  final StringCallback onError;

  LoadUserPermissionsAction(this.businessId, {this.onSuccess, this.onError});
}

class LoadPermissionSuccess {
  final List<BusinessPermision> permissions;

  LoadPermissionSuccess({this.permissions});
}
import 'package:eight_biz_flutter/models/biz_category.dart';
import 'package:flutter/material.dart';

typedef void StringCallback(String message);

class CategoryLoadingAnimationAction {
  final bool loading;
  final bool loadMore;

  CategoryLoadingAnimationAction({this.loading = false, this.loadMore = false});
}

class RequestCategoriesAction {
  final StringCallback onError;

  RequestCategoriesAction({this.onError});
}

class RequestCategoriesSuccess {
  final List<BizCategory> categories;
  final bool reload;

  RequestCategoriesSuccess({@required this.categories, this.reload = true});
}
import 'package:eight_biz_flutter/screens/biz/create_biz.dart';
import 'package:flutter/material.dart';

class NavResetToWalkThrough {

}

class NavResetToGetStarted {

  @override
  String toString() => 'NavResetToGetStarted';
}

class NavNavigateToGetStarted {
  
}

class NavNavigateToLogin {

}

class NavNavigateToSignup {
  
}

class NavNavigateToVerify {

}

class NavNavigateToResetPass {

}

class NavNavigateToChangeProfile {

}

class NavPushAndResetToListBiz {
  
}

class NavNavigateToCreateBiz {
  final CreateBizParams params;

  NavNavigateToCreateBiz({
    @required this.params
  });
}

class NavNavigateToAgreement {
  final String businessId;

  NavNavigateToAgreement({
    @required this.businessId
  });
}

class NavNavigateToNotifications {

}

class NavPushAndResetToHome {
  
}
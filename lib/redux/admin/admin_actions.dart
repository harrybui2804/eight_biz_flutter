import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/biz_role.dart';
import 'package:eight_biz_flutter/response/res_broadcasts.dart';
import 'package:eight_biz_flutter/response/res_get_manager.dart';
import 'package:eight_biz_flutter/response/res_managers.dart';
import 'package:flutter/material.dart';

typedef void StringCallback(String message);
typedef void LoadListRolesCallback(List<BizRole> roles);
typedef void LoadRoleDetailCallback(List<BusinessPermision> permissions);
typedef void LoadManagerDetailCallback(ResGetManager detail);

class LoadListUserManager {
  final String businessId;
  final String next;
  final StringCallback onSuccess;
  final StringCallback onError;

  LoadListUserManager({@required this.businessId, this.next, this.onSuccess, this.onError});
}

class LoadListManagerSuccess {
  final String businessId;
  final bool isNext;
  final ResManagers resManagers;

  LoadListManagerSuccess({@required this.businessId, this.isNext, this.resManagers});
}

class LoadManagerDetailAction {
  final String businessId;
  final String managerId;
  final LoadManagerDetailCallback onSuccess;
  final StringCallback onError;

  LoadManagerDetailAction({@required this.businessId, @required this.managerId, this.onSuccess, this.onError});
}

class LoadListRolesAction {
  final String businessId;
  final LoadListRolesCallback onSuccess;
  final StringCallback onError;

  LoadListRolesAction({@required this.businessId, this.onSuccess, this.onError});
}

class LoadRoleDetailAction {
  final String businessId;
  final BizRole role;
  final LoadRoleDetailCallback onSuccess;
  final StringCallback onError;

  LoadRoleDetailAction({
    @required this.businessId,
    @required this.role,
    this.onSuccess,
    this.onError
  });
} 

class SaveUserManagerAction {
  final String businessId;
  final String phone;
  final bool createRole;
  final String role;
  final List<BusinessPermision> permissions;
  final StringCallback onSuccess;
  final StringCallback onError;

  SaveUserManagerAction({@required this.businessId, @required this.phone, this.createRole = false, @required this.role, @required this.permissions, this.onSuccess, this.onError});
}

class UpdateUserManagerAction {
  final String businessId;
  final String managerId;
  final String roleId;
  final List<BusinessPermision> permissions;
  final StringCallback onSuccess;
  final StringCallback onError;

  UpdateUserManagerAction({
    @required this.businessId, 
    @required this.managerId, 
    @required this.roleId,
    @required this.permissions, 
    @required this.onSuccess, 
    @required this.onError
  });
}


class DeleteUserManagerAction {
  final String businessId;
  final String managerId;
  final StringCallback onSuccess;
  final StringCallback onError; 

  DeleteUserManagerAction({@required this.businessId, @required this.managerId, this.onSuccess, this.onError});
}

class LoadListBroadcastAction {
  final String businessId;
  final String next;
  final StringCallback onSuccess;
  final StringCallback onError;

  LoadListBroadcastAction({@required this.businessId, this.next, this.onSuccess, this.onError});
}

class LoadListBroadcastSuccess {
  final String businessId;
  final bool isNext;
  final ResBroadcasts resBroadcasts;

  LoadListBroadcastSuccess({@required this.businessId, this.isNext, this.resBroadcasts});
}

class SaveBroadcastAction {
  final String businessId;
  final String title;
  final String body;
  final String description;
  final bool status;
  final StringCallback onSuccess;
  final StringCallback onError;

  SaveBroadcastAction({
    @required this.businessId,
    @required this.title,
    @required this.body,
    @required this.description, 
    @required this.status,
    this.onSuccess,
    this.onError
  });
}

class UpdateBroadcastAction {
  final String businessId;
  final String broadcastId;
  final String title;
  final String body;
  final String description;
  final bool status;
  final StringCallback onSuccess;
  final StringCallback onError;

  UpdateBroadcastAction({
    @required this.businessId,
    @required this.broadcastId,
    @required this.title,
    @required this.body,
    @required this.description,
    @required this.status,
    this.onSuccess,
    this.onError
  });
}

class UpdateBroadcastSuccess {
  final String broadcastId;
  final String title;
  final String body;
  final String description;
  final bool status;

  UpdateBroadcastSuccess({
    @required this.broadcastId,
    @required this.title,
    @required this.body,
    @required this.description,
    @required this.status,
  });
}

class DeleteBroadcastAction {
  final String businessId;
  final String broadcastId;
  final StringCallback onSuccess;
  final StringCallback onError;

  DeleteBroadcastAction({
    @required this.businessId,
    @required this.broadcastId,
    this.onSuccess,
    this.onError
  });
}

class DeleteBroadcastSuccess {
  final String businessId;
  final String broadcastId;

  DeleteBroadcastSuccess({
    @required this.businessId,
    @required this.broadcastId
  });
}
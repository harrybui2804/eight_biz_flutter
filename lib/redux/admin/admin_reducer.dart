import 'package:eight_biz_flutter/redux/home/home_actions.dart';
import 'package:redux/redux.dart';

import 'admin_actions.dart';
import 'admin_state.dart';


final adminReducer = combineReducers<AdminState>([
  TypedReducer<AdminState, LoadListManagerSuccess>(_loadManagersSuccess),
  TypedReducer<AdminState, LoadListBroadcastSuccess>(_loadBroadcastSuccess),
  TypedReducer<AdminState, UpdateBroadcastSuccess>(_updateBroadcast),
  TypedReducer<AdminState, DeleteBroadcastSuccess>(_deleteBroadcast),
  TypedReducer<AdminState, ClearHomeDataAction>(_clearWhenSwitch),
  TypedReducer<AdminState, ClearDataAfterLogout>(_clearData),
]);

AdminState _loadManagersSuccess(AdminState state, LoadListManagerSuccess action) {
  if(action.isNext) {
    return state.addManager(action.resManagers);
  }
  return state.copyWith(
    resManagers: action.resManagers,
    managers: action.resManagers.results,
  );
}

AdminState _loadBroadcastSuccess(AdminState state, LoadListBroadcastSuccess action) {
  if(action.isNext) {
    return state.addBroadcast(action.resBroadcasts);
  }
  return state.copyWith(
    resBroadcasts: action.resBroadcasts
  );
}

AdminState _updateBroadcast(AdminState state, UpdateBroadcastSuccess action) {
  return state.copyWith(
    resBroadcasts: state.resBroadcasts.update(
      action.broadcastId, 
      action.title, 
      action.body, 
      action.description, 
      action.status
    )
  );
}

AdminState _deleteBroadcast(AdminState state, DeleteBroadcastSuccess action) {
  return state.copyWith(
    resBroadcasts: state.resBroadcasts.delete(action.broadcastId)
  );
}

AdminState _clearWhenSwitch(AdminState state, ClearHomeDataAction action) {
  return AdminState.initial();
}

AdminState _clearData(AdminState state, ClearDataAfterLogout action) {
  return AdminState.initial();
}
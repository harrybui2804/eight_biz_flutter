import 'package:eight_biz_flutter/models/biz_manager.dart';
import 'package:eight_biz_flutter/response/res_broadcasts.dart';
import 'package:eight_biz_flutter/response/res_managers.dart';
import 'package:meta/meta.dart';

@immutable
class AdminState {
  final ResManagers resManagers;
  final List<BizManager> managers;
  final ResBroadcasts resBroadcasts;

  AdminState({
    @required this.resManagers,
    @required this.managers,
    @required this.resBroadcasts
  });

  factory AdminState.initial() {
    return AdminState(
      resManagers: ResManagers.initial(),
      managers: [],
      resBroadcasts: ResBroadcasts.initial()
    );
  }

  AdminState copyWith({
    ResManagers resManagers,
    List<BizManager> managers,
    ResBroadcasts resBroadcasts
  }) {
    return AdminState(
      resManagers: resManagers ?? this.resManagers,
      managers: managers ?? this.managers,
      resBroadcasts: resBroadcasts ?? this.resBroadcasts
    );
  }

  AdminState addManager(ResManagers resManagers) {
    final _managers = managers ?? [];
    return AdminState(
      resManagers: resManagers,
      managers: List.from(_managers)..addAll(resManagers.results),
      resBroadcasts: this.resBroadcasts
    );
  }

  AdminState addBroadcast(ResBroadcasts res) {
    final _broadcast = this.resBroadcasts != null && resBroadcasts.results != null ? resBroadcasts.results : [];
    return AdminState(
      resManagers: this.resManagers,
      managers: this.managers,
      resBroadcasts: ResBroadcasts(
        count: res.count,
        next: res.next,
        previous: res.previous,
        results: List.from(_broadcast)..addAll(res.results ?? [])
      )
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is AdminState &&
        managers == other.managers;

  @override
  int get hashCode =>
    managers.hashCode;
}
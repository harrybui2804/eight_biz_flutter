import 'package:eight_biz_flutter/redux/admin/admin_reducer.dart';
import 'package:eight_biz_flutter/redux/create_biz/create_biz_reducer.dart';
import 'package:eight_biz_flutter/redux/notifications/notification_reducer.dart';

import 'app_state.dart';
import 'auth/auth_reducer.dart';
import 'category/category_redux.dart';
import 'home/home_reducer.dart';
import 'list_biz/list_biz_reducer.dart';
import 'profile/profile_reducer.dart';
import 'wallet/wallet_reducer.dart';

AppState appReducer(AppState state, dynamic action) =>
  new AppState(
    authState: authReducer(state.authState, action),
    profileState: profileReducer(state.profileState, action),
    listBizState: listBizReducer(state.listBizState, action),
    createBizState: createBizReducer(state.createBizState, action),
    categoryState: categoryReducer(state.categoryState, action),
    homeState: homeReducer(state.homeState, action),
    walletState: walletReducer(state.walletState, action),
    adminState: adminReducer(state.adminState, action),
    notificationState: notificationReducer(state.notificationState, action)
  );
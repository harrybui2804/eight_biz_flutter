import 'package:eight_biz_flutter/redux/admin/admin_state.dart';
import 'package:eight_biz_flutter/redux/notifications/notification_state.dart';
import 'package:flutter/material.dart';

import 'auth/auth_state.dart';
import 'category/category_state.dart';
import 'create_biz/create_biz_state.dart';
import 'home/home_state.dart';
import 'list_biz/list_biz_state.dart';
import 'profile/profile_state.dart';
import 'wallet/wallet_state.dart';

@immutable
class AppState {
  final AuthState authState;
  final ProfileState profileState;
  final ListBizState listBizState;
  final CreateBizState createBizState;
  final CategoryState categoryState;
  final HomeState homeState;
  final WalletState walletState;
  final AdminState adminState;
  final NotificationState notificationState;

  AppState({
    @required this.authState,
    @required this.profileState,
    @required this.listBizState,
    @required this.createBizState,
    @required this.categoryState,
    @required this.homeState,
    @required this.walletState,
    @required this.adminState,
    @required this.notificationState
  });

  factory AppState.initial() {
    return AppState(
      authState: AuthState.initial(),
      profileState: ProfileState.initial(),
      listBizState: ListBizState.initial(),
      createBizState: CreateBizState.initial(),
      categoryState: CategoryState.initial(),
      homeState: HomeState.initial(),
      walletState: WalletState.initial(),
      adminState: AdminState.initial(),
      notificationState: NotificationState.initial()
    );
  }

  AppState copyWith({
    AuthState authState,
    ProfileState profileState,
    ListBizState listBizState,
    CreateBizState createBizState,
    CategoryState categoryState,
    HomeState homeState,
    WalletState walletState,
    AdminState adminState,
    NotificationState notificationState
  }) {
    return AppState(
      authState: authState ?? this.authState,
      profileState: profileState ?? this.profileState,
      listBizState: listBizState ?? this.listBizState,
      createBizState: createBizState ?? this.createBizState,
      categoryState: categoryState ?? this.categoryState,
      homeState: homeState ?? this.homeState,
      walletState: walletState ?? this.walletState,
      adminState: adminState ?? this.adminState,
      notificationState: notificationState ?? this.notificationState
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is AppState &&
        authState == other.authState &&
        profileState == other.profileState &&
        listBizState == other.listBizState &&
        createBizState == other.createBizState &&
        categoryState == other.categoryState &&
        homeState == other.homeState &&
        walletState == other.walletState &&
        adminState == other.adminState &&
        notificationState == other.notificationState;

  @override
  int get hashCode =>
      authState.hashCode ^
      profileState.hashCode ^
      listBizState.hashCode ^
      createBizState.hashCode ^
      categoryState.hashCode ^
      homeState.hashCode ^
      walletState.hashCode ^
      adminState.hashCode ^
      notificationState.hashCode;
}
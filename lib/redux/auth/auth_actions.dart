import 'package:eight_biz_flutter/models/signup.dart';

typedef void StringCallback(String message);
typedef void MultiStringCallback(String message, String para);
typedef void VoidCallback();

class GetStoreLogin {

  @override
  String toString() => 'GetStoreLogin';
}

class LoginRequest {
  final String phone;
  final String password;
  final StringCallback onError;
  final MultiStringCallback onUnverifyEmail;

  LoginRequest(this.phone, this.password, this.onError, this.onUnverifyEmail);
}

class LoginSuccess {
  final String token;
  final String phone;
  final String password;

  LoginSuccess({this.token, this.phone, this.password});
}

class LoginFailed {
  final String errorCode;
  final String errorMessage;

  LoginFailed({this.errorCode, this.errorMessage});
}

class ValidatePhoneAction {
  final String phone;
  final double latitude;
  final double longitude;
  final StringCallback onError;

  ValidatePhoneAction({this.phone, this.latitude, this.longitude, this.onError});
}

class CheckExistPhoneResponse {
  final bool isExist;
  final String phone;
  final String registerId;

  CheckExistPhoneResponse({this.isExist, this.phone, this.registerId});
}

class SignUpRequestAction {
  final ReqSignUp model;
  final StringCallback onSuccess;
  final StringCallback onError;

  SignUpRequestAction(this.model, this.onSuccess, this.onError);
}

class ChangeAuthMessageSuccess {
  final String message;

  ChangeAuthMessageSuccess(this.message);
}

class ClearAuthSuccess {

}

class ClearAuthError {
  
}

class ChangeAuthMessageFailed {
  final String code;
  final String message;

  ChangeAuthMessageFailed(this.code, this.message);
}

class ResendPhoneCodeAction {
  final String phone;
  final String registerId;
  final StringCallback onSuccess;
  final StringCallback onError;

  ResendPhoneCodeAction(this.phone, this.registerId, this.onSuccess, this.onError);

  @override
  String toString() => 'ResendPhoneCodeAction($phone, $registerId, $onSuccess, $onError)';
}

class RequestPhoneCodeReset {
  final String registerId;
  final StringCallback onSuccess;
  final StringCallback onError;

  RequestPhoneCodeReset(this.registerId, this.onSuccess, this.onError);

  @override
  String toString() => 'RequestPhoneCodeReset($registerId, $onSuccess, $onError)';
}

class SubmitResetPassword {
  final String registerId;
  final String password;
  final String code;
  final StringCallback onSuccess;
  final StringCallback onError;

  SubmitResetPassword(this.registerId, this.password, this.code, this.onSuccess, this.onError);
}

class ChangePhoneAndRegisterId {
  final String phone;
  final String registerId;

  ChangePhoneAndRegisterId(this.phone, this.registerId);
}

class VerifyPhoneCodeAction {
  final String registerId;
  final String code;
  final bool isExistAccount;
  final StringCallback onSuccess;
  final StringCallback onError;

  VerifyPhoneCodeAction(this.registerId, this.code, this.isExistAccount, this.onSuccess, this.onError);
}

class VerifyPhoneCodeSuccess {
  final String registerId;
  final String publicVerifyKey;
  final String privateVerifyKey;

  VerifyPhoneCodeSuccess({this.registerId, this.publicVerifyKey, this.privateVerifyKey});
}

class CheckEmailExist {
  final String phone;
  final String email;
  final VoidCallback onExist;
  final VoidCallback onNotExist;
  final StringCallback onError;

  CheckEmailExist({this.phone, this.email, this.onExist, this.onNotExist, this.onError});
}

class VisibleLoadingAction {
  final bool visible;

  VisibleLoadingAction(this.visible);
}

class ResendVerifyEmailAction {
  final String email;
  final StringCallback onSuccess;
  final StringCallback onError;

  ResendVerifyEmailAction({this.email, this.onSuccess, this.onError});
}

class LogoutAppAction {
  
}
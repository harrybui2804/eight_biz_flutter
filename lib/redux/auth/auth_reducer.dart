import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';
import 'package:eight_biz_flutter/redux/home/home_actions.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/auth/auth_state.dart';

final authReducer = combineReducers<AuthState>([
  TypedReducer<AuthState, VisibleLoadingAction>(_visibleLoadingAnimation),
  TypedReducer<AuthState, ChangePhoneAndRegisterId>(_changePhoneAndRegisterId),
  TypedReducer<AuthState, CheckExistPhoneResponse>(_onCheckExistResponse),
  TypedReducer<AuthState, VerifyPhoneCodeSuccess>(_onVerifyPhoneSuccess),
  TypedReducer<AuthState, ChangeAuthMessageSuccess>(_changeSuccessMessage),
  TypedReducer<AuthState, ChangeAuthMessageFailed>(_changeFailedMessage),
  TypedReducer<AuthState, ClearAuthSuccess>(_clearSuccessMessage),
  TypedReducer<AuthState, ClearAuthError>(_clearErrorMessage),
  TypedReducer<AuthState, ClearDataAfterLogout>(_clearData),
]);

AuthState _visibleLoadingAnimation(AuthState state, VisibleLoadingAction action) {
  return state.copyWith(loading: action.visible);
}

AuthState _changePhoneAndRegisterId(AuthState state, ChangePhoneAndRegisterId action) {
  return state.copyWith(phone: action.phone, registerId: action.registerId);
}

AuthState _onCheckExistResponse(AuthState state, CheckExistPhoneResponse action) {
  return state.copyWith(
    isExist: action.isExist,
    phone: action.phone,
    registerId: action.registerId
  );
}

AuthState _onVerifyPhoneSuccess(AuthState state, VerifyPhoneCodeSuccess action) {
  return state.copyWith(
    registerId: action.registerId,
    publicVerifyKey: action.publicVerifyKey,
    privateVerifyKey: action.privateVerifyKey
  );
}

AuthState _changeSuccessMessage(AuthState state, ChangeAuthMessageSuccess action) {
  return state.copyWith(sucMessage: action.message);
}

AuthState _changeFailedMessage(AuthState state, ChangeAuthMessageFailed action) {
  return state.copyWith(errMessage: action.message);
}

AuthState _clearSuccessMessage(AuthState state, ClearAuthSuccess action) {
  return state.clearSuccess();
}

AuthState _clearErrorMessage(AuthState state, ClearAuthError action) {
  return state.clearError();
}

AuthState _clearData(AuthState state, ClearDataAfterLogout action) {
  AppConfig().setLanguage(null);
  AppConfig().setLocale(null);
  return AuthState.initial();
}
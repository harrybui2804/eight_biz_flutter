import 'package:meta/meta.dart';

@immutable
class AuthState {
  final String phone;
  final String password;
  final String registerId;
  final bool isExist;
  final String token;
  final bool loading;
  final String sucMessage;
  final String errMessage;
  final String publicVerifyKey;
  final String privateVerifyKey;

  AuthState({
    @required this.phone,
    @required this.password,
    @required this.registerId,
    @required this.isExist,
    @required this.token,
    @required this.loading,
    @required this.sucMessage,
    @required this.errMessage,
    @required this.publicVerifyKey,
    @required this.privateVerifyKey,
  });

  factory AuthState.initial() {
    return AuthState(
      phone: '',
      password: '',
      registerId: null,
      isExist: false,
      token: '',
      loading: false,
      sucMessage: '',
      errMessage: '',
      publicVerifyKey: '',
      privateVerifyKey: ''
    );
  }

  AuthState copyWith({
    String phone,
    String password,
    String registerId,
    bool isExist,
    String token,
    bool loading,
    String sucMessage,
    String errMessage,
    String publicVerifyKey,
    String privateVerifyKey
  }) {
    return AuthState(
      phone: phone ?? this.phone,
      password: password ?? this.password,
      registerId: registerId ?? this.registerId,
      isExist: isExist ?? this.isExist,
      token: token ?? this.token,
      loading: loading ?? this.loading,
      sucMessage: sucMessage ?? this.sucMessage,
      errMessage: errMessage ?? this.errMessage,
      publicVerifyKey: publicVerifyKey ?? this.publicVerifyKey,
      privateVerifyKey: privateVerifyKey ?? this.privateVerifyKey
    );
  }

  AuthState clearSuccess() {
    return AuthState(
      phone: this.phone,
      password: this.password,
      registerId: this.registerId,
      isExist: this.isExist,
      token: this.token,
      loading: this.loading,
      sucMessage: null,
      errMessage: this.errMessage,
      publicVerifyKey: this.publicVerifyKey,
      privateVerifyKey: this.privateVerifyKey
    );
  }

  AuthState clearError() {
    return AuthState(
      phone: this.phone,
      password: this.password,
      registerId: this.registerId,
      isExist: this.isExist,
      token: this.token,
      loading: this.loading,
      sucMessage: this.sucMessage,
      errMessage: null,
      publicVerifyKey: this.publicVerifyKey,
      privateVerifyKey: this.privateVerifyKey
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is AuthState &&
        phone == other.phone &&
        password == other.password &&
        registerId == other.registerId &&
        isExist == other.isExist &&
        token == other.token &&
        loading == other.loading &&
        publicVerifyKey == other.publicVerifyKey &&
        privateVerifyKey == other.privateVerifyKey &&
        sucMessage == other.sucMessage &&
        errMessage == other.errMessage;

  @override
  int get hashCode =>
    phone.hashCode ^
    password.hashCode ^
    registerId.hashCode ^
    isExist.hashCode ^
    token.hashCode ^
    loading.hashCode ^
    publicVerifyKey.hashCode ^
    privateVerifyKey.hashCode ^
    sucMessage.hashCode ^
    errMessage.hashCode;
}
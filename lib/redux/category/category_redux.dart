import 'package:eight_biz_flutter/redux/actions/category_actions.dart';
import 'package:eight_biz_flutter/redux/home/home_actions.dart';
import 'package:redux/redux.dart';

import 'category_state.dart';


final categoryReducer = combineReducers<CategoryState>([
  TypedReducer<CategoryState, RequestCategoriesSuccess>(_loadingAnimation),
  TypedReducer<CategoryState, ClearDataAfterLogout>(_clearData),
]);

CategoryState _loadingAnimation(CategoryState state, RequestCategoriesSuccess action) {
  return state.copyWith(categories: action.categories);
}

CategoryState _clearData(CategoryState state, ClearDataAfterLogout action) {
  return CategoryState.initial();
}
import 'package:eight_biz_flutter/models/biz_category.dart';
import 'package:meta/meta.dart';

@immutable
class CategoryState {
  final List<BizCategory> categories;

  CategoryState({
    @required this.categories
  });

  factory CategoryState.initial() {
    return CategoryState(
      categories: []
    );
  }

  CategoryState copyWith({
    List<BizCategory> categories
  }) {
    return CategoryState(
      categories: categories ?? this.categories
    );
  }

  CategoryState addList({
    List<BizCategory> categories
  }) {
    return CategoryState(
      categories: List.from(this.categories ?? [])..addAll(categories)
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is CategoryState &&
        categories == other.categories;

  @override
  int get hashCode =>
    categories.hashCode;
}
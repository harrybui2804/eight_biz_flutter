import 'dart:io';

import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/models/media.dart';
import 'package:eight_biz_flutter/response/res_medias.dart';
import 'package:flutter/material.dart';
import 'package:signature/signature.dart';

typedef void StringCallback(String message);

class CreateBizLoadingAnimationAction {
  final bool loading;

  CreateBizLoadingAnimationAction({this.loading = false});
}

class ChangeEditingInfoAction {
  final BusinessDetail editing;

  ChangeEditingInfoAction(this.editing);
}

class UpdateBizAfterUpdate {
  final BusinessDetail business;

  UpdateBizAfterUpdate({this.business});
}

class ClearEditingBizAction {
  
}


class SubmitCreateBizAction {
  final BusinessDetail business;
  final StringCallback onSuccess;
  final StringCallback onError;

  SubmitCreateBizAction(this.business, this.onSuccess, this.onError);
}

class SubmitEditBizAction {
  final BusinessDetail business;
  final bool updateLogo;
  final bool updateCover;
  final StringCallback onSuccess;
  final StringCallback onError;

  SubmitEditBizAction(this.business, this.onSuccess, this.onError, {this.updateLogo = false, this.updateCover = false});
}

class LoadingListMediasAction {
  final bool loading;

  LoadingListMediasAction({this.loading = false});
}

class RequestListMediasAction {
  final String businessId;
  final String next;
  final StringCallback onError;

  RequestListMediasAction({@required this.businessId, this.next, this.onError});
}

class RequestMediasSuccess {
  final ResMedias medias;
  final bool reload;

  RequestMediasSuccess({this.medias, this.reload});
}

class UploadMediaAction {
  final BusinessDetail business;
  final String businessId;
  final File file;
  final bool isLogo;
  final StringCallback onSuccess;
  final StringCallback onError;

  UploadMediaAction({this.business, this.businessId, this.file, this.isLogo = false, this.onSuccess, this.onError});
}

class UploadMediaSuccess {
  final String businessId;
  final File file;
  final String imageId;

  UploadMediaSuccess({this.businessId, this.file, this.imageId});
}

class UpdateCoverImageAction {
  final BusinessDetail business;
  final String imageId;
  final bool isSetCover; // true: set is cover / false: delete cover
  final StringCallback onSuccess;
  final StringCallback onError;

  UpdateCoverImageAction({this.business, this.imageId, this.isSetCover, this.onSuccess, this.onError});
}

class UpdateCoverImageSuccess {
  final String businessId;
  final String imageId;
  final bool isSetCover; // true: set is cover / false: delete cover

  UpdateCoverImageSuccess({this.businessId, this.imageId, this.isSetCover});
}

class UpdateBizSuccess {
  final BusinessDetail business;

  UpdateBizSuccess(this.business);
}

class UpdateLogoSuccess {
  final File logoFile;
  final String logoUrl;

  UpdateLogoSuccess(this.logoFile, this.logoUrl);
}

class DeleteBizLogoAction {
  final BusinessDetail business;
  final StringCallback onSuccess;
  final StringCallback onError;

  DeleteBizLogoAction({@required this.business, this.onSuccess, this.onError});
}

class DeleteBizLogoSuccess {
  final BusinessDetail business;
  

  DeleteBizLogoSuccess({@required this.business});
}

class DeleteMediaAction {
  final String businessId;
  final Media image;
  final StringCallback onSuccess;
  final StringCallback onError;

  DeleteMediaAction({
    @required this.businessId,
    @required this.image,
    this.onSuccess,
    this.onError
  });
}

class DeleteMediaSuccess {
  final String businessId;
  final String imageId;

  DeleteMediaSuccess({
    @required this.businessId,
    @required this.imageId
  });
}

class LoadAgreementAction {
  final String businessId;
  final StringCallback onSuccess;
  final StringCallback onError;

  LoadAgreementAction({
    @required this.businessId,
    this.onSuccess,
    this.onError
  });
}

class LoadAgreementSuccess {
  final String businessId;
  final String agreement;

  LoadAgreementSuccess({
    @required this.businessId,
    @required this.agreement
  });
}

class ChangeSignatureAction {
  final List<Point> signature;
  final bool isClear;

  ChangeSignatureAction(this.signature, {this.isClear = false});
}

class UpdateSignatureAction {
  final String businessId;
  final File image;
  final StringCallback onSuccess;
  final StringCallback onError; 

  UpdateSignatureAction({this.businessId, this.image, this.onSuccess, this.onError});
}
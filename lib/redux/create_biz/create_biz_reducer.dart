import 'package:eight_biz_flutter/models/media.dart';
import 'package:eight_biz_flutter/redux/actions/biz_actions.dart';
import 'package:eight_biz_flutter/redux/home/home_actions.dart';
import 'package:redux/redux.dart';

import 'create_biz_actions.dart';
import 'create_biz_state.dart';

final createBizReducer = combineReducers<CreateBizState>([
  TypedReducer<CreateBizState, CreateBizLoadingAnimationAction>(_loadingAnimation),
  TypedReducer<CreateBizState, LoadingListMediasAction>(_loadingListMedias),
  TypedReducer<CreateBizState, RequestMediasSuccess>(_requestMediasSuccess),
  TypedReducer<CreateBizState, UpdateLogoSuccess>(_updateLogoSuccess),
  TypedReducer<CreateBizState, DeleteBizLogoSuccess>(_deleteLogoSuccess),
  TypedReducer<CreateBizState, UploadMediaSuccess>(_uploadMediaSuccess),
  TypedReducer<CreateBizState, DeleteMediaSuccess>(_deleteMediaSuccess),
  TypedReducer<CreateBizState, ChangeEditingInfoAction>(_changeEditingInfo),
  TypedReducer<CreateBizState, ClearEditingBizAction>(_clearEditingInfo),
  TypedReducer<CreateBizState, EditBizDetailAction>(_submitEditEditingInfo),
  TypedReducer<CreateBizState, UpdateCoverImageSuccess>(_updateCoverSuccess),
  TypedReducer<CreateBizState, LoadAgreementSuccess>(_loadAgreementSuccess),
  TypedReducer<CreateBizState, ChangeSignatureAction>(_changeSignature),
  TypedReducer<CreateBizState, ClearDataAfterLogout>(_clearData),
]);

CreateBizState _loadingAnimation(CreateBizState state, CreateBizLoadingAnimationAction action) {
  return state.copyWith(loading: action.loading);
}

CreateBizState _changeEditingInfo(CreateBizState state, ChangeEditingInfoAction action) {
  return state.copyWith(editing: action.editing);
}

CreateBizState _clearEditingInfo(CreateBizState state, ClearEditingBizAction action) {
  return state.deleteEditing();
}

CreateBizState _submitEditEditingInfo(CreateBizState state, EditBizDetailAction action) {
  print('createBizReducer EditBizDetailAction');
  return state.copyWith(editing: action.business);
}

CreateBizState _loadingListMedias(CreateBizState state, LoadingListMediasAction action) {
  return state.copyWith(loadingMedias: action.loading);
}

CreateBizState _requestMediasSuccess(CreateBizState state, RequestMediasSuccess action) {
  if(action.reload) {
    return state.copyWith(resMedias: action.medias);
  }
  return state.addMedias(action.medias);
}

CreateBizState _updateLogoSuccess(CreateBizState state, UpdateLogoSuccess action) {
  return state.copyWith(localLogo: action.logoFile, editing: state.editing.copyWith(logoImage: action.logoUrl));
}

CreateBizState _deleteLogoSuccess(CreateBizState state, DeleteBizLogoSuccess action) {
  return state.removeLogo();
}

CreateBizState _uploadMediaSuccess(CreateBizState state, UploadMediaSuccess action) {
  List<Media> results = state.resMedias.results ?? [];
  List<Media> medias = List.from([
    Media(
      localFile: action.file, 
      isLocal: true,
      id: action.imageId
    )
  ])..addAll(results);
  return state.copyWith(
    resMedias: state.resMedias.copyWith(
      results: medias
    )
  );
}

CreateBizState _deleteMediaSuccess(CreateBizState state, DeleteMediaSuccess action) {
  List<Media> results = state.resMedias.results ?? [];
  List<Media> medias = results.where((media) => media.id != action.imageId).toList();
  print(medias.length);
  return state.copyWith(
    resMedias: state.resMedias.copyWith(
      results: medias
    )
  );
}

CreateBizState _updateCoverSuccess(CreateBizState state, UpdateCoverImageSuccess action) {
  return state.setCoverImage(action.imageId, action.isSetCover);
}

CreateBizState _loadAgreementSuccess(CreateBizState state, LoadAgreementSuccess action) {
  return state.copyWith(agreement: action.agreement);
}

CreateBizState _changeSignature(CreateBizState state, ChangeSignatureAction action) {
  print('action.isClear ${action.isClear}');
  if(action.isClear) {
    return state.removeSignature();
  }
  return state.copyWith(signature: action.signature);
}

CreateBizState _clearData(CreateBizState state, ClearDataAfterLogout action) {
  return CreateBizState.initial();
}

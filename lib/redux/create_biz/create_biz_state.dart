import 'dart:io';

import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/response/res_medias.dart';
import 'package:meta/meta.dart';
import 'package:signature/signature.dart';

@immutable
class CreateBizState {
  final bool loading;
  final BusinessDetail editing;
  final bool loadingMedias;
  final ResMedias resMedias;
  final File localLogo;
  final String agreement;
  final List<Point> signature;

  CreateBizState({
    @required this.loading,
    @required this.editing,
    this.agreement,
    this.signature,
    @required this.resMedias,
    this.loadingMedias = false,
    this.localLogo   //File update logo
  }); 

  factory CreateBizState.initial() {
    return CreateBizState(
      loading: false,
      editing: null,
      loadingMedias: false,
      resMedias: ResMedias(),
      localLogo: null,
      agreement: null,
      signature: null
    );
  }

  CreateBizState copyWith({
    bool loading,
    BusinessDetail editing,
    bool loadingMedias,
    ResMedias resMedias,
    File localLogo,
    String agreement,
    List<Point> signature
  }) {
    return CreateBizState(
      loading: loading ?? this.loading,
      editing: editing ?? this.editing,
      loadingMedias: loadingMedias ?? this.loadingMedias,
      resMedias: resMedias ?? this.resMedias,
      localLogo: localLogo ?? this.localLogo,
      agreement: agreement ?? this.agreement,
      signature: signature ?? this.signature
    );
  }

  CreateBizState addMedias(ResMedias medias) {
    return CreateBizState(
      editing: this.editing,
      agreement: this.agreement,
      signature: this.signature,
      localLogo: this.localLogo,
      loading: this.loading,
      loadingMedias: false,
      resMedias: ResMedias(
        count: medias.count,
        next: medias.next,
        previous: medias.previous,
        results: List.from(this.resMedias.results)..addAll(medias.results)
      )
    );
  }

  CreateBizState removeLogo() {
    return CreateBizState(
      loading: this.loading,
      editing: this.editing.copyWith(logoImage: ''),
      loadingMedias: this.loadingMedias,
      resMedias: this.resMedias,
      localLogo: null,
      agreement: this.agreement,
      signature: this.signature
    );
  }

  CreateBizState deleteEditing() {
    return CreateBizState(
      loading: this.loading,
      editing: null,
      loadingMedias: false,
      resMedias: ResMedias(),
      localLogo: null,
      agreement: null,
      signature: null
    );
  }

  CreateBizState removeSignature() {
    return CreateBizState(
      loading: this.loading,
      editing: this.editing,
      loadingMedias: this.loadingMedias,
      resMedias: this.resMedias,
      localLogo: this.localLogo,
      agreement: this.agreement,
      signature: null
    );
  }

  CreateBizState setCoverImage(String imageId, bool isSet) {
    return CreateBizState(
      loading: this.loading,
      editing: isSet ? this.editing.copyWith(coverImage: imageId) : this.editing.removeCover(),
      loadingMedias: this.loadingMedias,
      localLogo: this.localLogo,
      resMedias: ResMedias(
        count: this.resMedias.count,
        next: this.resMedias.next,
        previous: this.resMedias.previous,
        results: this.resMedias.results != null
          ? isSet //if set cover
            ? this.resMedias.results.map((media) {
                var _media = media;
                if(media.id == imageId) {
                  _media = media.copyWith(isCover: true);
                } else {
                  _media = media.copyWith(isCover: false);
                }
                return _media;
              }).toList()
            : this.resMedias.results.map((media) {  //delete
                var _media = media;
                if(media.id == imageId) {
                  _media = media.copyWith(isCover: false);
                } 
                return _media;
              }).toList()
          : []
      )
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is CreateBizState &&
        loading == other.loading &&
        editing == other.editing &&
        agreement == other.agreement &&
        signature == other.signature &&
        loadingMedias == other.loadingMedias &&
        resMedias == other.resMedias &&
        localLogo == localLogo;

  @override
  int get hashCode =>
    loading.hashCode ^
    editing.hashCode ^
    agreement.hashCode ^
    signature.hashCode ^
    loadingMedias.hashCode ^
    resMedias.hashCode ^
    localLogo.hashCode;
}
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:flutter/material.dart';

class SetSelectedBusinessAction {
  final BusinessDetail business;
  final List<BusinessPermision> permissions;
  final bool isOwner;
  final bool isActive;

  SetSelectedBusinessAction({
    @required this.business,
    @required this.permissions,
    @required this.isOwner,
    @required this.isActive
  });
}

class AddListMediaToHome {

}

class ClearDataAfterLogout {

}

class ClearHomeDataAction {
  //Clear data of business selected before
}
import 'package:eight_biz_flutter/redux/actions/biz_actions.dart';
import 'package:eight_biz_flutter/redux/create_biz/create_biz_actions.dart';
import 'package:eight_biz_flutter/redux/home/home_actions.dart';
import 'package:redux/redux.dart';

import 'home_state.dart';

final homeReducer = combineReducers<HomeState>([
  TypedReducer<HomeState, SetSelectedBusinessAction>(_setBusinessDetail),
  TypedReducer<HomeState, UpdateBizSuccess>(_updateBusiness),
  TypedReducer<HomeState, LoadPermissionSuccess>(_updatePermissions),
  TypedReducer<HomeState, ClearHomeDataAction>(_clearHomeData),
  TypedReducer<HomeState, ClearDataAfterLogout>(_clearData),
]);

HomeState _setBusinessDetail(HomeState state, SetSelectedBusinessAction action) {
  return state.copyWith(
    isActive: action.isActive,
    isOwner: action.isOwner,
    business: action.business,
    permissions: action.permissions
  );
}

HomeState _updateBusiness(HomeState state, UpdateBizSuccess action) {
  return state.copyWith(
    business: action.business
  );
}

HomeState _updatePermissions(HomeState state, LoadPermissionSuccess action) {
  return state.copyWith(
    permissions: action.permissions
  );
}

HomeState _clearData(HomeState state, ClearDataAfterLogout action) {
  return HomeState.initial();
}

HomeState _clearHomeData(HomeState state, ClearHomeDataAction action) {
  return HomeState.initial();
}

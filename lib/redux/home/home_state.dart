import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:meta/meta.dart';

@immutable
class HomeState {
  final BusinessDetail business;
  final bool isOwner;
  final bool isActive;
  //final double totalEarning;
  final List<BusinessPermision> permissions;

  HomeState({
    @required this.business,
    @required this.isOwner,
    @required this.isActive,
    @required this.permissions
  });

  factory HomeState.initial() {
    return HomeState(
      isOwner: false,
      isActive: false,
      business: null,
      permissions: []
    );
  }

  HomeState copyWith({
    bool isOwner,
    bool isActive,
    BusinessDetail business,
    List<BusinessPermision> permissions
  }) {
    return HomeState(
      isOwner: isOwner ?? this.isOwner,
      isActive: isActive ?? this.isActive,
      business: business ?? this.business,
      permissions: permissions ?? this.permissions,
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is HomeState &&
        isOwner == other.isOwner &&
        isActive == other.isActive &&
        business == other.business &&
        permissions == other.permissions;

  @override
  int get hashCode =>
    isOwner.hashCode ^
    isActive.hashCode ^
    business.hashCode ^
    permissions.hashCode;
}
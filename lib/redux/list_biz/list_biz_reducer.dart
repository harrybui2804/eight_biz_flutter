import 'package:eight_biz_flutter/redux/actions/biz_actions.dart';
import 'package:eight_biz_flutter/redux/home/home_actions.dart';
import 'package:redux/redux.dart';

import 'list_biz_state.dart';

final listBizReducer = combineReducers<ListBizState>([
  TypedReducer<ListBizState, ListBizLoadingAnimationAction>(_loadingAnimation),
  TypedReducer<ListBizState, RequestListBizSuccess>(_onRequestListBizSuccess),
  TypedReducer<ListBizState, ClearDataAfterLogout>(_clearData),
]);

ListBizState _loadingAnimation(ListBizState state, ListBizLoadingAnimationAction action) {
  return state.copyWith(loading: action.loading, loadMore: action.loadMore);
}

ListBizState _onRequestListBizSuccess(ListBizState state, RequestListBizSuccess action) {
  if(action.reload){
    return state.copyWith(data: action.data);
  }
  return state.addData(action.data);
}

ListBizState _clearData(ListBizState state, ClearDataAfterLogout action) {
  return ListBizState.initial();
}
import 'package:eight_biz_flutter/response/res_listbiz.dart';
import 'package:meta/meta.dart';

@immutable
class ListBizState {
  final bool loading;
  final bool loadMore;
  final ResListBiz data;

  ListBizState({
    @required this.loading,
    @required this.loadMore,
    @required this.data
  });

  factory ListBizState.initial() {
    return ListBizState(
      loading: false,
      loadMore: false,
      data: ResListBiz()
    );
  }

  ListBizState copyWith({
    bool loading,
    bool loadMore,
    ResListBiz data
  }) {
    return ListBizState(
      loading: loading ?? this.loading,
      loadMore: loadMore ?? this.loadMore,
      data: data ?? this.data
    );
  }

  ListBizState addData(ResListBiz data) {
    return ListBizState(
      loading: this.loading,
      loadMore: this.loadMore,
      data: ResListBiz(
        count: data.count,
        next: data.next,
        prev: data.prev,
        results: List.from(this.data.results)..addAll(data.results)
      )
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is ListBizState &&
        loading == other.loading &&
        loadMore == other.loadMore &&
        data == other.data;

  @override
  int get hashCode =>
    loading.hashCode ^
    loadMore.hashCode ^
    data.hashCode;
}
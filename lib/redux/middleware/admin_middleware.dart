import 'package:eight_biz_flutter/api/api_admin.dart';
import 'package:eight_biz_flutter/api/api_broadcast.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/redux/admin/admin_actions.dart';
import 'package:eight_biz_flutter/response/res_add_manager.dart';
import 'package:eight_biz_flutter/response/res_broadcasts.dart';
import 'package:eight_biz_flutter/response/res_create_role.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_get_manager.dart';
import 'package:eight_biz_flutter/response/res_managers.dart';
import 'package:eight_biz_flutter/response/res_role_detail.dart';
import 'package:eight_biz_flutter/response/res_roles.dart';
import 'package:eight_biz_flutter/response/res_success.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';

class AdminMiddleware extends MiddlewareClass<AppState> {

  @override
  void call(Store<AppState> store, action, NextDispatcher next) {
    if(action is LoadListUserManager) {
      _loadListManagers(action, next);
      return;
    } else if(action is LoadListRolesAction) {
      _loadListRoles(action, next);
      return;
    } else if(action is LoadRoleDetailAction) {
      _loadRoleDetail(action, next);
      return;
    } else if(action is SaveUserManagerAction) {
      _saveUserManager(action, next);
      return;
    } else if(action is LoadManagerDetailAction) {
      _loadManagerDetail(action, next);
      return;
    } else if(action is UpdateUserManagerAction) {
      _updateManager(action, next);
      return;
    } else if(action is DeleteUserManagerAction) {
      _deleteManager(action, next);
      return;
    } else if(action is LoadListBroadcastAction) {
      _loadListBroadcast(action, next);
      return;
    } else if(action is SaveBroadcastAction) {
      _createBroadcast(action, next);
      return;
    } else if(action is UpdateBroadcastAction) {
      _updateBroadcast(action, next);
      return;
    } else if(action is DeleteBroadcastAction) {
      _deleteBroadcast(action, next);
      return;
    }
    else {
      next(action);
    }
  }

  void _loadListManagers(LoadListUserManager action, NextDispatcher next) async {
    final model = await ApiAdmin().loadListManagers(action.businessId, action.next);
    if(model is ResManagers) {
      //set to app state
      next(LoadListManagerSuccess(
        businessId: action.businessId,
        isNext: action.next != null,
        resManagers: model
      ));
      if(action.onSuccess != null) {
        action.onSuccess('');
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    } else {
      if(action.onError != null) {
        action.onError('Unhandle');
      }
    }
  }

  void _loadListRoles(LoadListRolesAction action, NextDispatcher next) async {
    final model = await ApiAdmin().loadListRoles(action.businessId);
    if(model is ResRoles) {
      if(action.onSuccess != null) {
        action.onSuccess(model.roles);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    } else {
      if(action.onError != null) {
        action.onError('Unhandle');
      }
    }
  }

  void _loadRoleDetail(LoadRoleDetailAction action, NextDispatcher next) async {
    final model = await ApiAdmin().loadRoleDetail(action.businessId, action.role);
    if(model is ResRoleDetail) {
      if(action.onSuccess != null) {
        action.onSuccess(model.permissions);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    } else {
      if(action.onError != null) {
        action.onError('Unhandle');
      }
    }
  }

  void _saveUserManager(SaveUserManagerAction action, NextDispatcher next) async {
    if(action.createRole) {
      final model = await ApiAdmin().addRole(action.businessId, action.role, action.permissions);
      if(model is ResCreateRole) {
        await _completeSaveManager(action, model.roleId, action.permissions, next);
      } else if(model is ResError) {
        if(action.onError != null) {
          action.onError(model.message);
        }
      }
    } else {
      await _completeSaveManager(action, action.role, action.permissions, next);
    }
  }

  Future<void> _completeSaveManager(SaveUserManagerAction action, String roleId, List<BusinessPermision> permissions, NextDispatcher next) async {
    final model = await ApiAdmin().addManager(action.businessId, action.phone, roleId, permissions);
    if(model is ResAddManager) {
      if(action.onSuccess != null) {
        action.onSuccess('SUCCESS');
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
    //Reload list manager
    _loadListManagers(LoadListUserManager(businessId: action.businessId, next: null), next);
  }

  void _loadManagerDetail(LoadManagerDetailAction action, NextDispatcher next) async {
    final model = await ApiAdmin().loadManagerDetail(action.businessId, action.managerId);
    if(model is ResGetManager) {
      if(action.onSuccess != null) {
        action.onSuccess(model);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  void _updateManager(UpdateUserManagerAction action, NextDispatcher next) async {
    final model = await ApiAdmin().updateManager(action.businessId, action.roleId, action.managerId, action.permissions);
    if(model is ResSuccess) {
      if(action.onSuccess != null) {
        final message = model.message == 'SUCCESS' ? AppLang().success : model.message;
        action.onSuccess(message);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  void _deleteManager(DeleteUserManagerAction action, NextDispatcher next) async {
    final model = await ApiAdmin().deleteManager(action.businessId, action.managerId);
    if(model is ResSuccess) {
      //Reload list manager
      _loadListManagers(LoadListUserManager(
        businessId: action.businessId,
        next: null
      ), next);
      if(action.onSuccess != null) {
        final message = model.message == 'SUCCESS' ? AppLang().success : model.message;
        action.onSuccess(message);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  void _loadListBroadcast(LoadListBroadcastAction action, NextDispatcher next) async {
    final model = await ApiBroadCast().loadListBroadcast(action.businessId, action.next);
    if(model is ResBroadcasts) {
      next(LoadListBroadcastSuccess(
        businessId: action.businessId,
        isNext: action.next != null,
        resBroadcasts: model
      ));
      if(action.onSuccess != null) {
        action.onSuccess('SUCCESS');
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  void _createBroadcast(SaveBroadcastAction action, NextDispatcher next) async {
    final model = await ApiBroadCast().addBroadcast(action.businessId, action.title, action.body, action.description, action.status);
    if(model is ResSuccess) {
      //reload list
      _loadListBroadcast(LoadListBroadcastAction(
        businessId: action.businessId,
        next: null
      ), next);
      if(action.onSuccess != null) {
        action.onSuccess('SUCCESS');
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  void _updateBroadcast(UpdateBroadcastAction action, NextDispatcher next) async {
    final model = await ApiBroadCast().updateBroadcast(
      action.businessId, action.broadcastId, action.title, action.body, action.description, action.status
    );
    if(model is ResSuccess) {
      //update in list
      next(UpdateBroadcastSuccess(
        broadcastId: action.broadcastId,
        title: action.title,
        body: action.body,
        description: action.description,
        status: action.status
      ));
      if(action.onSuccess != null) {
        action.onSuccess('SUCCESS');
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  void _deleteBroadcast(DeleteBroadcastAction action, NextDispatcher next) async {
    final model = await ApiBroadCast().deleteBroadcast(action.businessId, action.broadcastId);
    if(model is ResSuccess) {
      //update in list
      next(DeleteBroadcastSuccess(
        businessId: action.businessId,
        broadcastId: action.broadcastId
      ));
      if(action.onSuccess != null) {
        action.onSuccess('SUCCESS');
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

}
import 'package:amazon_cognito_identity_dart/cognito.dart';
import 'package:eight_biz_flutter/api/api_auth.dart';
import 'package:eight_biz_flutter/api/aws_cognito.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';
import 'package:eight_biz_flutter/redux/actions/navigation_actions.dart';
import 'package:eight_biz_flutter/redux/home/home_actions.dart';
import 'package:eight_biz_flutter/redux/profile/profile_actions.dart';
import 'package:eight_biz_flutter/response/res_checkexist.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_exist_mail.dart';
import 'package:eight_biz_flutter/response/res_login.dart';
import 'package:eight_biz_flutter/response/res_resend_code.dart';
import 'package:eight_biz_flutter/response/res_signup.dart';
import 'package:eight_biz_flutter/response/res_success.dart';
import 'package:eight_biz_flutter/response/res_verify_code.dart';
import 'package:eight_biz_flutter/utils/user_detail.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';

class AuthMiddleware extends MiddlewareClass<AppState> {

  @override
  void call(Store<AppState> store, action, NextDispatcher next) {
    if(action is GetStoreLogin) {
      next(VisibleLoadingAction(true));
      _getStoreLogin(next);
      return;
    } else if(action is LoginRequest) {
      next(VisibleLoadingAction(true));
      _doLogin(false, action.phone, action.password, action.onError, action.onUnverifyEmail, next);
      return;
    } else if(action is ResendVerifyEmailAction) {
      _resendVerifyEmail(action, next);
      return;
    } else if(action is ValidatePhoneAction) {
      next(VisibleLoadingAction(true));
      _checkExistPhone(action, next);
      return;
    } else if(action is CheckEmailExist) {
      _checkExitMail(action, next);
      return;
    } else if(action is ResendPhoneCodeAction) {
      next(VisibleLoadingAction(true));
      _resendPhoneCode(action, next);
      return;
    } else if(action is VerifyPhoneCodeAction) {
      next(VisibleLoadingAction(true));
      _submitVerifyPhoneCode(action, next);
      return;
    } else if(action is SignUpRequestAction) {
      next(VisibleLoadingAction(true));
      _submitSignup(action, next);
      return;
    } else if(action is RequestPhoneCodeReset) {
      next(VisibleLoadingAction(true));
      _sendPhoneCodeReset(action, next);
      return;
    } else if(action is SubmitResetPassword) {
      next(VisibleLoadingAction(true));
      _submitResetPassword(action, next);
      return;
    } else if(action is LogoutAppAction) {
      UserDetail().clearData();
      next(NavResetToGetStarted());
      next(ClearDataAfterLogout());
    }
    else {
      next(action);
    }
  }

  void _getStoreLogin(NextDispatcher next) async {
    final isLogined = await UserDetail().isLogined();
    if(isLogined) {
      final login = await UserDetail().getLogin();
      var phone = login['phone'] as String;
      var password = login['password'] as String;
      await _doLogin(true, phone, password, null, null, next);
    } else {
      next(VisibleLoadingAction(false));
      //next to walk through
      next(NavResetToWalkThrough());
      //next(NavResetToGetStarted());
    }
  }

  _doLogin(bool isSplash, String phone, String password, onError, onUnverifyEmail, NextDispatcher next) async {
    final model = await AWSCognito().loginCognito(phone, password);
    //final model = await ApiAuth().login(phone, password);
    next(VisibleLoadingAction(false));
    print('login with $phone, $password');
    if(model is ResLogin) {
      print('login success with token: ${model.token}');
      await UserDetail().setToken(model.token);
      await UserDetail().setLogin(phone, password);
      //check email verified?
      final emailVerify = await ApiAuth().checkEmailVerify();
      if(emailVerify is ResError) {
        String email = emailVerify.data;
        if(onUnverifyEmail != null && email!= null) {
          onUnverifyEmail(emailVerify.message, email);
        } else if(onError != null) {
          onError(emailVerify.message);
        }
        if(isSplash) {
          next(NavResetToGetStarted());
        }
        return;
      }
      next(RequestUserProfileAction());
      next(NavPushAndResetToListBiz());
    } else if(model is ResError) {
      print('login failed, ${model.message}');
      if(isSplash) {
        next(NavResetToGetStarted());
        return;
      }
      var message = model.message;
      if(model.code == 'COGNITO_3') {
        message = 'User does not exist.';
      }
      if(onError != null) {
        onError(message);
      }
    }
  }

  _checkExitMail(CheckEmailExist action, NextDispatcher next) async {
    final model = await ApiAuth().checkExistMail(action.phone, action.email);
    if(model is ResExistMail) {
      if(model.isExist) {
        if(action.onExist != null) {
          action.onExist();
        }
      } else {
        if(action.onNotExist != null) {
          action.onNotExist();
        }
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _checkExistPhone(ValidatePhoneAction action, NextDispatcher next) async {
    final model = await ApiAuth().checkExistPhone(action.phone, action.latitude, action.longitude);
    next(VisibleLoadingAction(false));
    if(model is ResCheckExist) {
      next(CheckExistPhoneResponse(isExist: model.isAlreadyUser, phone: action.phone, registerId: model.registeredId));
      if(model.isAlreadyUser) {
        next(NavNavigateToLogin());
      } else {
        next(NavNavigateToVerify());
      }
    } else if(model is ResError) {
      if(model.code == 'AUTH_5') {
        next(CheckExistPhoneResponse(isExist: false, phone: action.phone, registerId: model.data['registered_id']));
        next(NavNavigateToVerify());
        return;
      }
      action.onError(model.message);
    }
  }

  _resendPhoneCode(ResendPhoneCodeAction action, NextDispatcher next) async {
    final model = await ApiAuth().resendPhoneCode(action.phone);
    next(VisibleLoadingAction(false));
    if(model is ResResendCode) {
      next(ChangePhoneAndRegisterId(action.phone, model.registeredId));
      final message = model.message == 'SUCCESS' ? AppLang().success : model.message;
      action.onSuccess(message);
    } else if(model is ResError) {
      action.onError(model.message);
    }
  }

  _submitVerifyPhoneCode(VerifyPhoneCodeAction action, NextDispatcher next) async {
    final model = await ApiAuth().submitVerifyCode(action.registerId, action.code);
    next(VisibleLoadingAction(false));
    if(model is ResVerifyCode) {
      final message = model.message == 'SUCCESS' ? AppLang().success : model.message;
      action.onSuccess(message);
      next(VerifyPhoneCodeSuccess(registerId: action.registerId, publicVerifyKey: model.publicKey, privateVerifyKey: model.privateKey));
      if(action.isExistAccount) {
        next(NavNavigateToLogin());
      } else {
        next(NavNavigateToSignup());
      }
    } else if(model is ResError) {
      action.onError(model.message);
    }
  }

  _submitSignup(SignUpRequestAction action, NextDispatcher next) async {
    //Signup with api
    final model = await ApiAuth().signup(action.model);
    if(model is ResSignup) {
      //Signup with cognito
      try {
        var resCognito = await AWSCognito().signupCognito(action.model);
        next(VisibleLoadingAction(false));
        final userCognito = resCognito['response'];
        if(resCognito['isSuccess'] && userCognito != null && userCognito is CognitoUserPoolData) {
          action.onSuccess('Register successful');
          next(NavNavigateToLogin());
        } 
        else {
          action.onError('Unhandle exception');
        }
      } catch (e) {
        next(VisibleLoadingAction(false));
        var msg = e.toString();
        if(e is String) {
          msg = e.toString();
        } else {
          if(e.message != null) {
            msg = e.message;
          } else if(e['message'] != null) {
            msg = e['message'].toString();
          }
        }
        action.onError(msg);
        return;
      }
    } else if(model is ResError) {
      next(VisibleLoadingAction(false));
      action.onError(model.message);
    }
  }

  _sendPhoneCodeReset(RequestPhoneCodeReset action, NextDispatcher next) async {
    try {
      final isSend = await AWSCognito().requestCodeResetPassword(action.registerId);
      next(VisibleLoadingAction(false));
      if(isSend) {
        action.onSuccess('A verification code was sent');
      } else {
        action.onError('Verification code was not sent');
      }
    } catch (e) {
      next(VisibleLoadingAction(false));
      action.onError(e);
    }
  }

  _submitResetPassword(SubmitResetPassword action, NextDispatcher next) async {
    try {
      final isConfirm = await AWSCognito().submitResetPassword(action.registerId, action.password, action.code);
      next(VisibleLoadingAction(false));
      if(isConfirm) {
        action.onSuccess('Password updated successful');
        next(NavNavigateToLogin());
      } else {
        action.onError('Password not updated');
      }
    } catch (e) {
      next(VisibleLoadingAction(false));
      var msg = e.toString();
      if(e is String) {
        msg = e.toString();
      } else {
        msg = e.message ?? e.toString();
      }
      action.onError(msg);
    }
  }

  _resendVerifyEmail(ResendVerifyEmailAction action, NextDispatcher next) async {
    final model = await ApiAuth().resendVerifyEmail(action.email);
    if(model is ResSuccess) {
      if(action.onSuccess != null) {
        action.onSuccess(model.message);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }
}
import 'package:eight_biz_flutter/api/api_listbiz.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/redux/actions/biz_actions.dart';
import 'package:eight_biz_flutter/redux/actions/category_actions.dart';
import 'package:eight_biz_flutter/redux/actions/navigation_actions.dart';
import 'package:eight_biz_flutter/redux/create_biz/create_biz_actions.dart';
import 'package:eight_biz_flutter/redux/home/home_actions.dart';
import 'package:eight_biz_flutter/response/res_biz_detail.dart';
import 'package:eight_biz_flutter/response/res_categories.dart';
import 'package:eight_biz_flutter/response/res_create_biz.dart';
import 'package:eight_biz_flutter/response/res_delete_media.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_listbiz.dart';
import 'package:eight_biz_flutter/response/res_medias.dart';
import 'package:eight_biz_flutter/response/res_permission.dart';
import 'package:eight_biz_flutter/response/res_success.dart';
import 'package:eight_biz_flutter/response/res_update_biz.dart';
import 'package:eight_biz_flutter/response/res_upload.dart';
import 'package:eight_biz_flutter/response/res_upload_policy.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import '../app_state.dart';

class BizMiddleware extends MiddlewareClass<AppState> {
  @override
  void call(Store<AppState> store, action, NextDispatcher next) {
    if(action is RequestListBizAction) {
      final loading = action.next == null;
      final loadMore = !loading;
      next(ListBizLoadingAnimationAction(loading: loading, loadMore: loadMore));
      _loadListBiz(action, next);
      return;
    } else if(action is RequestCategoriesAction) {
      _loadCategories(action, next);
      return;
    } else if(action is SubmitCreateBizAction) {
      next(CreateBizLoadingAnimationAction(loading: true));
      _createBiz(action, next);
      return;
    } else if(action is SwitchBizDetailAction) {
      next(ListBizLoadingAnimationAction(loading: true));
      _switchBizDetail(action, next);
    } else if(action is RequestListMediasAction) {
      next(LoadingListMediasAction(loading: true));
      _requestListMedias(action, next);
    } else if(action is UploadMediaAction) {
      next(CreateBizLoadingAnimationAction(loading: true));
      _uploadMedia(action, next);
      return;
    } else if(action is UpdateCoverImageAction) {
      next(CreateBizLoadingAnimationAction(loading: true));
      _updateCoverImage(action, next);
      return;
    } else if(action is SubmitEditBizAction) {
      next(CreateBizLoadingAnimationAction(loading: true));
      _requestUpdateBusiness(action, next);
      return;
    } else if(action is DeleteBizLogoAction) {
      next(CreateBizLoadingAnimationAction(loading: true));
      _deleteLogo(action, next);
      return;
    } else if(action is DeleteMediaAction) {
      next(CreateBizLoadingAnimationAction(loading: true));
      _deleteMedia(action, next);
      return;
    } else if(action is LoadAgreementAction) {
      next(CreateBizLoadingAnimationAction(loading: true));
      _loadAgreement(action, next);
      return;
    } else if(action is UpdateSignatureAction) {
      next(CreateBizLoadingAnimationAction(loading: true));
      _saveSignature(action, next);
      return;
    } else if(action is LoadBizDetailAction) {
      _loadBizDetail(action, next);
      return;
    } else if(action is LoadUserPermissionsAction) {
      _loadPermissions(action, next);
      return;
    }
    else {
      next(action);
    }

  }

  _loadListBiz(RequestListBizAction action, NextDispatcher next) async {
    final model = await ApiBiz().loadListBiz(action.next);
    next(ListBizLoadingAnimationAction(loading: false, loadMore: false));
    if(model is ResListBiz) {
      next(RequestListBizSuccess(data: model, reload: action.next == null));
    } else if(model is ResError) {
      print(model.message);
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _loadCategories(RequestCategoriesAction action, NextDispatcher next) async {
    final model = await ApiBiz().loadCategories();
    if(model is ResCategories) {
      next(RequestCategoriesSuccess(categories: model.categories));
    } else if(model is ResError) {
      print(model.message);
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _createBiz(SubmitCreateBizAction action, NextDispatcher next) async {
    final model = await ApiBiz().createBusiness(action.business);
    next(CreateBizLoadingAnimationAction(loading: false));
    if(model is ResCreateBiz) {
      if(action.onSuccess != null) {
        action.onSuccess(model.businessId);
      }
    } else if(model is ResError) {
      print(model.message);
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _switchBizDetail(SwitchBizDetailAction action, NextDispatcher next) async {
    //load user permissions with biz
    final pModel = await ApiBiz().getUserPermission(action.businessList.business.id);
    if(pModel is ResError) {
      if(action.onError != null) {
        action.onError(pModel.message);
      }
      return;
    }
    if(pModel is ResPermission) {
      //check user can get biz detail
      var lst = pModel.permissions.where((per) => per.role == BusinessRole.getBiz && per.isGranted);
      if(lst.length > 0) {
        //has permissiojn => load biz detail
        final model = await ApiBiz().loadBizDetail(action.businessList.business.id);
        next(ListBizLoadingAnimationAction(loading: false, loadMore: false));
        if(model is ResBizDetail) {
          next(ClearHomeDataAction());
          if(action.onSuccess != null) {
            action.onSuccess('');
          }
          next(
            SetSelectedBusinessAction(
              business: model.business,
              permissions: pModel.permissions,
              isActive: action.businessList.isActive,
              isOwner: action.businessList.isOwner
            )
          );
          next(NavPushAndResetToHome());
        } else if(model is ResError) {
          if(action.onError != null) {
            action.onError(model.message);
          }
        }
      } else {
        //not has permission
        if(action.onError != null) {
          action.onError(AppLang.noPermissionLoadBiz);
        }
      }
    }
  }

  _loadBizDetail(LoadBizDetailAction action, NextDispatcher next) async {
    final model = await ApiBiz().loadBizDetail(action.businessId);
    if(model is ResBizDetail) {
      if(action.onSuccess != null) {
        action.onSuccess('');
      }
      next(UpdateBizSuccess(model.business));
      _loadCategories(RequestCategoriesAction(onError: action.onError), next);
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _loadPermissions(LoadUserPermissionsAction action, NextDispatcher next) async {
    final model = await ApiBiz().getUserPermission(action.businessId);
    if(model is ResPermission) {
      if(action.onSuccess != null) {
        action.onSuccess(model.permissions);
      }
      next(LoadPermissionSuccess(permissions: model.permissions));
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _requestUpdateBusiness(SubmitEditBizAction action, NextDispatcher next) async {
    final model = await ApiBiz().updateBusiness(action.business, action.updateLogo, action.updateCover);
    next(CreateBizLoadingAnimationAction(loading: false));
    if(model is ResUpdateBiz) {
      next(UpdateBizSuccess(action.business));
      if(action.onSuccess != null) {
        final message = model.message != null && model.message == 'SUCCESS' ? AppLang().success : model.message;
        action.onSuccess(message ?? AppLang().updateBizSuccess);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _requestListMedias(RequestListMediasAction action, NextDispatcher next) async {
    final model = await ApiBiz().loadMedias(action.businessId, action.next);
    next(LoadingListMediasAction(loading: false));
    if(model is ResMedias) {
      next(RequestMediasSuccess(medias: model, reload: next != null));
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _uploadMedia(UploadMediaAction action, NextDispatcher next) async {
    if(action.isLogo) {
      //Upload file
      final model = await ApiBiz().uploadMedia(action.businessId, action.file);
      if(model is ResUpload) {
        //update biz
        final biz = action.business.copyWith(logoImage: model.imageId);
        final modelUpdate = await ApiBiz().updateBusiness(biz, true, false);
        next(CreateBizLoadingAnimationAction(loading: false));
        if(modelUpdate is ResUpdateBiz) {
          //update biz to home
          final modelBiz = await ApiBiz().loadBizDetail(biz.id);
          if(modelBiz is ResBizDetail) {
            next(UpdateBizSuccess(modelBiz.business));
            //set logo to show on editing
            next(UpdateLogoSuccess(action.file, modelBiz.business.logoImage));
          } else {
            next(UpdateLogoSuccess(action.file, ''));
          }
        } else if(modelUpdate is ResError) {
          if(action.onError != null) {
            action.onError(modelUpdate.message);
          }
        }
      } else if(model is ResError) {
        next(CreateBizLoadingAnimationAction(loading: false));
        if(action.onError != null) {
          action.onError(model.message);
        }
      }
    } else {
      final model = await ApiBiz().uploadGallery(action.businessId, action.file);
      next(CreateBizLoadingAnimationAction(loading: false));
      if(model is ResUploadPolicy) {
        next(UploadMediaSuccess(
          imageId: model.fileId,
          file: model.file,
          businessId: model.businessId
        ));
        if(action.onSuccess != null) {
          final message = model.message != null && model.message == 'SUCCESS' ? AppLang().success : model.message;
          action.onSuccess(message);
        }
        //update biz to home
        final modelBiz = await ApiBiz().loadBizDetail(model.businessId);
        if(modelBiz is ResBizDetail) {
          next(UpdateBizSuccess(modelBiz.business));
        }
      } else if(model is ResError) {
        if(action.onError != null) {
          action.onError(model.message);
        }
      }
    }
    next(CreateBizLoadingAnimationAction(loading: false));
  }

  _deleteMedia(DeleteMediaAction action, NextDispatcher next) async {
    print('_deleteMedia ${action.image.id}');
    final model = await ApiBiz().deleteMedia(action.businessId, action.image.id);
    next(CreateBizLoadingAnimationAction(loading: false));
    if(model is ResDeleteMedias) {
      next(DeleteMediaSuccess(businessId: action.businessId, imageId: action.image.id));
      //update biz to home+
      final modelBiz = await ApiBiz().loadBizDetail(action.businessId);
      if(modelBiz is ResBizDetail) {
        next(UpdateBizSuccess(modelBiz.business));
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _updateCoverImage(UpdateCoverImageAction action, NextDispatcher next) async {
    final biz = action.isSetCover ? action.business.copyWith(
      coverImage: action.imageId
    ) : action.business.removeCover();
    final modelUpdate = await ApiBiz().updateBusiness(biz, false, true);
    next(CreateBizLoadingAnimationAction(loading: false));
    if(modelUpdate is ResUpdateBiz) {
      //set cover to show on editing
      next(UpdateCoverImageSuccess(
        businessId: action.business.id,
        imageId: action.imageId,
        isSetCover: action.isSetCover
      ));
      //update biz to home
      final modelBiz = await ApiBiz().loadBizDetail(biz.id);
      if(modelBiz is ResBizDetail) {
        print('_updateCoverImage ${modelBiz.business.coverImage}');
        next(UpdateBizSuccess(modelBiz.business));
      }
    } else if(modelUpdate is ResError) {
      if(action.onError != null) {
        action.onError(modelUpdate.message);
      }
    }
  }

  _deleteLogo(DeleteBizLogoAction action, NextDispatcher next) async {
    final biz = action.business.removeLogo();
    final model = await ApiBiz().updateBusiness(biz, true, false);
    next(CreateBizLoadingAnimationAction(loading: false));
    if(model is ResUpdateBiz) {
      if(action.onSuccess != null) {
        final message = model.message == 'SUCCESS' ? AppLang().success : model.message;
        action.onSuccess(message);
      }
      next(DeleteBizLogoSuccess(business: biz));
      final modelBiz = await ApiBiz().loadBizDetail(action.business.id);
      if(modelBiz is ResBizDetail) {
        debugPrint('_deleteLogo update biz to home');
        next(UpdateBizSuccess(modelBiz.business));
      } else {
        next(UpdateBizSuccess(biz));
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _loadAgreement(LoadAgreementAction action, NextDispatcher next) async {
    final model = await ApiBiz().loadAgreement(action.businessId);
    next(CreateBizLoadingAnimationAction(loading: false));
    if(model is ResSuccess) {
      next(LoadAgreementSuccess(
        businessId: action.businessId,
        agreement: model.message
      ));
      if(action.onSuccess != null) {
        action.onSuccess(model.message);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _saveSignature(UpdateSignatureAction action, NextDispatcher next) async {
    final upload = await ApiBiz().uploadMedia(action.businessId, action.image);
    if(upload is ResUpload) {
      final model = await ApiBiz().updateAgreement(action.businessId, upload.imageId);
      next(CreateBizLoadingAnimationAction(loading: false));
      if(model is ResSuccess) {
        if(action.onSuccess != null) {
          action.onSuccess(model.message);
        }
        _loadAgreement(
          LoadAgreementAction(
            businessId: action.businessId,
            onError: action.onError
          ),
          next
        );
      } else {
        if(action.onError != null) {
          action.onError(upload.message);
        }
      }
    } else if(upload is ResError) {
      next(CreateBizLoadingAnimationAction(loading: false));
      if(action.onError != null) {
        action.onError(upload.message);
      }
    }
  }
}
import 'package:eight_biz_flutter/redux/actions/biz_actions.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/redux/actions/navigation_actions.dart';
import 'package:eight_biz_flutter/redux/profile/profile_actions.dart';
import 'package:eight_biz_flutter/screens/auth/get_started.dart';
import 'package:eight_biz_flutter/screens/auth/login_page.dart';
import 'package:eight_biz_flutter/screens/auth/reset_password_page.dart';
import 'package:eight_biz_flutter/screens/auth/signup_page.dart';
import 'package:eight_biz_flutter/screens/auth/verify_page.dart';
import 'package:eight_biz_flutter/screens/biz/biz_agreement_page.dart';
import 'package:eight_biz_flutter/screens/biz/create_biz.dart';
import 'package:eight_biz_flutter/screens/biz/list_biz_page.dart';
import 'package:eight_biz_flutter/screens/home/home_page.dart';
import 'package:eight_biz_flutter/screens/notification/notification_page.dart';
import 'package:eight_biz_flutter/screens/profile/edit_profile_page.dart';
import 'package:eight_biz_flutter/screens/walkthrough/walkthrough_page.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class NavigationMiddleware extends MiddlewareClass<AppState>{
  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) {
    if(action is NavResetToWalkThrough) {
      Keys.navKey.currentState.pushNamedAndRemoveUntil(WalkThroughPage.routeName, (Route<dynamic> route) => false);
    } else if(action is NavResetToGetStarted){
      Keys.navKey.currentState.pushNamedAndRemoveUntil(GetStarterdPage.routeName, (Route<dynamic> route) => false);
      return;
    } else if(action is NavNavigateToGetStarted) {
      Keys.navKey.currentState.pushNamed(GetStarterdPage.routeName);
      return;
    } else if(action is NavNavigateToLogin) {
      Keys.navKey.currentState.pushNamed(LoginPage.routeName);
      return;
    } else if(action is NavNavigateToVerify) {
      Keys.navKey.currentState.pushNamed(VerifyPage.routeName);
      return;
    } else if(action is NavNavigateToResetPass) {
      Keys.navKey.currentState.pushNamed(ResetPasswordPage.routeName);
      return;
    } else if(action is NavNavigateToSignup) {
      Keys.navKey.currentState.pushNamed(SignUpPage.routeName);
      return;
    } else if(action is NavPushAndResetToListBiz) {

      Keys.navKey.currentState.pushNamedAndRemoveUntil(ListBusinessPage.routeName, (Route<dynamic> route) => false, arguments: ListBizPageParams() );
      return;
    } else if(action is NavNavigateToCreateBiz) {
      Keys.navKey.currentState.pushNamed(CreateBizPage.routeName, arguments: action.params);
      return;
    } else if(action is NavNavigateToAgreement) {
      Keys.navKey.currentState.pushNamed(BizAgreementPage.routeName, arguments: action.businessId);
      return;
    } else if(action is NavPushAndResetToHome) {
      Keys.navKey.currentState.pushNamedAndRemoveUntil(HomeTabPage.routeName, (Route<dynamic> route) => false);
      return;
    } else if(action is EditBizDetailAction) {
      Keys.navKey.currentState.pushNamed(CreateBizPage.routeName, arguments: CreateBizParams(index: 0, isNew: false));
      next(action);
    } else if(action is SwitchToOtherBusiness) {
      final params = ListBizPageParams(canPop: true, withoutBizId: action.businessId);
      Keys.navKey.currentState.pushNamed(ListBusinessPage.routeName, arguments: params);
      return;
    } else if(action is NavNavigateToChangeProfile) {
      Keys.navKey.currentState.pushNamed(EditProfilePage.routeName);
    } else if(action is NavNavigateToNotifications) {
      Keys.navKey.currentState.pushNamed(NotificationsPage.routeName);
      return;
    }
    else {
      next(action);
    }
  }
}
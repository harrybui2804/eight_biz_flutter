import 'package:eight_biz_flutter/api/api_notif.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/redux/notifications/notification_actions.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_notifications.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';

class NotificationMiddleware extends MiddlewareClass<AppState> {

  @override
  void call(Store<AppState> store, action, NextDispatcher next) {
    if(action is RequestNotificationsAction) {
      _loadNotifs(action, next);
      return;
    }
    next(action);
  }

  _loadNotifs(RequestNotificationsAction action, NextDispatcher next) async {
    final model = await ApiNotif().loadListNotifs(action.businessId, action.next);
    if(model is ResNotifications) {
      next(RequestNotificationsSuccess(
        businessId: action.businessId,
        isNext: action.next != null,
        resNotifications: model
      ));
      if(action.onSuccess != null) {
        action.onSuccess(AppLang().success);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }
}
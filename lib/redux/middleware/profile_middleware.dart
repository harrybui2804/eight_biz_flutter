import 'package:eight_biz_flutter/api/api_auth.dart';
import 'package:eight_biz_flutter/api/api_notif.dart';
import 'package:eight_biz_flutter/api/aws_cognito.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/redux/profile/profile_actions.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_get_profile.dart';
import 'package:eight_biz_flutter/response/res_notifications.dart';
import 'package:eight_biz_flutter/response/res_success.dart';
import 'package:eight_biz_flutter/response/res_update_profile.dart';
import 'package:eight_biz_flutter/response/res_upload.dart';
import 'package:eight_biz_flutter/utils/user_detail.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';

class ProfileMiddleware extends MiddlewareClass<AppState> {

  @override
  void call(Store<AppState> store, action, NextDispatcher next) {
    if(action is RequestUserProfileAction) {
      _requestUserProfile(action, next);
      return;
    } else if(action is SubmitUpdateProfileAction) {
      _updateProfile(action, next);
      return;
    } else if(action is SubmitChangePassAction) {
      _changePassword(action, next);
      return;
    } else if(action is LoadProfileNotificationsActions) {
      _loadNotifications(action, next);
      return;
    } else if(action is UploadProfileAvatarAction) {
      _updateAvatar(action, next);
      return;
    } else if(action is ChangeLanguageAction) {
      _changeLanguage(action, next);
      return;
    }
    else {
      next(action);
    }
  }

  _requestUserProfile(RequestUserProfileAction action, NextDispatcher next) async {
    try {
      final model = await ApiAuth().loadProfile();
      if(model is ResGetProfile) {
        next(RequestProfileSuccess(model.userProfile));
      } else if(model is ResError) {
        if(action.onError != null) {
          action.onError(model.message);
        }
      }
    } catch (e) {
      var msg = e.toString();
      if(e is String) {
        msg = e.toString();
      } else {
        msg = e.message ?? e.toString();
      }
      if(action.onError != null) {
        action.onError(msg);
      }
    }
  }

  _updateProfile(SubmitUpdateProfileAction action, NextDispatcher next) async {
    next(EditProfileLoadingAnimation(loading: true));
    final model = await ApiAuth().updateProfile(action.firstName, action.lastName, action.dateOfBirth, action.phoneNumber, action.address);
    next(EditProfileLoadingAnimation(loading: false));
    if(model is ResUpdateProfile) {
      if(action.onSuccess != null) {
        final message = model.message == 'SUCCESS' ? AppLang().success : model.message;
        action.onSuccess(message);
      }
      //reload profile
      _requestUserProfile(RequestUserProfileAction(), next);
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  } 

  _updateAvatar(UploadProfileAvatarAction action, NextDispatcher next) async {
    final model = await ApiAuth().uploadProfileImage(action.file);
    if(model is ResUpload) {
      next(UploadAvatarSuccess(file: action.file));
      if(action.onSuccess != null) {
        final message = model.message == 'SUCCESS' ? AppLang().success : model.message;
        action.onSuccess(message);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _changePassword(SubmitChangePassAction action, NextDispatcher next) async {
    next(ChangePassLoadingAnimation(loading: true));
    try {
      final changed = await AWSCognito().changePassword(action.currentPassword, action.newPassword);
      next(ChangePassLoadingAnimation(loading: false));
      if(changed && action.onSuccess != null) {
        action.onSuccess(AppLang().success);
      }
      if(!changed) {
        if(action.onError != null) {
          action.onError('ERROR');
        }
      }
      //update save password
      UserDetail().updateStorePass(action.newPassword);
    } catch (e) {
      next(ChangePassLoadingAnimation(loading: false));
      if(action.onError != null) {
        var message = e.toString();
        if(e.message != null) {
          message = e.message;
        }
        action.onError(message);
      }
    }
  }

  _loadNotifications(LoadProfileNotificationsActions action, NextDispatcher next) async {
    try {
      final model = await ApiNotif().loadListNotifs(action.businessId, null);
      if(model is ResNotifications) {
        next(LoadProfileNotifsSuccess(notifications: model.results));
      }
    } catch (e) {
      print('_loadNotifications $e');
    }
  }

  _changeLanguage(ChangeLanguageAction action, NextDispatcher next) async {
    final model = await ApiAuth().changeLanguage(action.language);
    if(model is ResSuccess) {
      if(action.onSuccess != null) {
        final message = model.message == 'SUCCESS' ? AppLang().success : model.message;
        action.onSuccess(message);
      }
      
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }
}
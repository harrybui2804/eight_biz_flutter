import 'package:eight_biz_flutter/api/api_wallet.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/redux/wallet/wallet_actions.dart';
import 'package:eight_biz_flutter/response/res_error.dart';
import 'package:eight_biz_flutter/response/res_get_bank.dart';
import 'package:eight_biz_flutter/response/res_photoID.dart';
import 'package:eight_biz_flutter/response/res_stripe_status.dart';
import 'package:eight_biz_flutter/response/res_success.dart';
import 'package:eight_biz_flutter/response/res_transactions.dart';
import 'package:eight_biz_flutter/response/res_wallet_detail.dart';

import '../app_state.dart';
import 'package:redux/redux.dart';

class WalletMiddleware extends MiddlewareClass<AppState> {
  @override
  void call(Store<AppState> store, action, NextDispatcher next) {
    if(action is LoadWalletDashboardAction) {
      _loadWalletDetail(action, next);
      return;
    } else if(action is GetCashoutAction) {
      _getCashout(action, next);
      return;
    } else if(action is GetBizBankAction) {
      _loadBizBank(action, next);
      return;
    } else if(action is LoadEarningAction) {
      _loadEarning(action, next);
      return;
    } else if(action is UpdateBizBankAction) {
      _updateBank(action, next);
      return;
    } else if(action is GetStripeVerifyStatusAction) {
      _getStripeStatus(action, next);
      return;
    } else if(action is SubmitVerifyStripe) {
      _submitVerifyStripe(action, next);
      return;
    }
    next(action);
  }

  _loadWalletDetail(LoadWalletDashboardAction action, NextDispatcher next) async {
    final model = await ApiWallet().loadWalletDetail(action.businessId);
    if(model is ResWalletDetail) {
      next(LoadWalletDashboardSuccess(
        businessId: action.businessId,
        balance: model.balance
      ));
      if(action.onSuccess != null) {
        action.onSuccess(AppLang().success);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _getCashout(GetCashoutAction action, NextDispatcher next) async {
    final model = await ApiWallet().cashout(action.businessId, action.amount, action.remark);
    if(model is ResSuccess) {
      next(GetCashoutSuccess(
        businessId: action.businessId,
        amount: action.amount,
        remark: action.remark
      ));
      if(action.onError != null) {
        final message = model.message != null && model.message == 'SUCCESS' ? AppLang().success : model.message;
        action.onSuccess(message);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _loadBizBank(GetBizBankAction action, NextDispatcher next) async {
    final model = await ApiWallet().getBizBank(action.businessId);
    if(model is ResGetBizBank) {
      next(GetBizBankSuccess(
        businessId: action.businessId,
        bank: model.bank
      ));
      if(action.onSuccess != null) {
        action.onSuccess(AppLang().success);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _updateBank(UpdateBizBankAction action, NextDispatcher next) async {
    final model = await ApiWallet().updateBizBank(action.businessId, action.bankToken);
    if(model is ResSuccess) {
      //Update state
      next(UpdateBizBankSuccess(businessId: action.businessId, bank: action.bank));
      if(action.onSuccess != null) {
        final message = model.message != null && model.message == 'SUCCESS' ? AppLang().success : model.message;
        action.onSuccess(message);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _loadEarning(LoadEarningAction action, NextDispatcher next) async {
    final model = await ApiWallet().loadTransactions(
      action.businessId, action.next, action.fromDate, action.toDate, action.ordering, action.transaction, action.status
    );
    if(model is ResTransactions) {
      next(LoadEarningSuccess(
        businessId: action.businessId,
        resTransactions: model,
        isNext: action.next != null
      ));
      if(action.onSuccess != null) {
        action.onSuccess(AppLang().success);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _getStripeStatus(GetStripeVerifyStatusAction action, NextDispatcher next) async {
    final model = await ApiWallet().getStripeStatus(action.businessId);
    if(model is ResStripeVerify) {
      if(action.onSuccess != null) {
        action.onSuccess(model.verified);
      }
    } else if(model is ResError) {
      if(action.onError != null) {
        action.onError(model.message);
      }
    }
  }

  _submitVerifyStripe(SubmitVerifyStripe action, NextDispatcher next) async {
    final upload = await ApiWallet().uploadPhotoId(action.frontID, action.backID);
    if(upload is ResPhotoID) {
      final model = await ApiWallet().submitVerifyStripe(action.businessId, upload.frontId, upload.backId);
      if(model is ResSuccess) {
        if(action.onSuccess != null) {
          final message = model.message != null && model.message == 'SUCCESS' ? AppLang().success : model.message;
          action.onSuccess(message);
        }
      } else if(model is ResError) {
        if(action.onError != null) {
          action.onError(model.message);
        }
      }
    } else if(upload is ResError) {
      if(action.onError != null) {
        action.onError(upload.message);
      }
    }
  }
  
}
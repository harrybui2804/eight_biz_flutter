import 'package:eight_biz_flutter/response/res_notifications.dart';
import 'package:flutter/material.dart';

typedef void StringCallback(String message);

class RequestNotificationsAction {
  final String businessId;
  final String next;
  final StringCallback onSuccess;
  final StringCallback onError;

  RequestNotificationsAction({
    @required this.businessId,
    this.next,
    this.onSuccess,
    this.onError
  });
}

class RequestNotificationsSuccess {
  final String businessId;
  final bool isNext;
  final ResNotifications resNotifications;

  RequestNotificationsSuccess({
    this.businessId,
    this.isNext,
    this.resNotifications
  });
}

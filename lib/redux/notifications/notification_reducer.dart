import 'package:eight_biz_flutter/redux/home/home_actions.dart';
import 'package:redux/redux.dart';

import 'notification_actions.dart';
import 'notification_state.dart';


final notificationReducer = combineReducers<NotificationState>([
  TypedReducer<NotificationState, RequestNotificationsSuccess>(_loadNotifsSuccess),
  TypedReducer<NotificationState, ClearHomeDataAction>(_clearWhenSwitch),
  TypedReducer<NotificationState, ClearDataAfterLogout>(_clearData),
]);

NotificationState _loadNotifsSuccess(NotificationState state, RequestNotificationsSuccess action) {
  if(action.isNext) {
    return state.addData(action.resNotifications);
  }
  return state.copyWith(resNotifications: action.resNotifications);
}

NotificationState _clearWhenSwitch(NotificationState state, ClearHomeDataAction action) {
  return NotificationState.initial();
}

NotificationState _clearData(NotificationState state, ClearDataAfterLogout action) {
  return NotificationState.initial();
}
import 'package:eight_biz_flutter/models/notification.dart';
import 'package:eight_biz_flutter/response/res_notifications.dart';
import 'package:meta/meta.dart';

@immutable
class NotificationState {
  final ResNotifications resNotifications;

  NotificationState({
    @required this.resNotifications
  });

  factory NotificationState.initial() {
    return NotificationState(
      resNotifications: null
    );
  }

  NotificationState copyWith({
    ResNotifications resNotifications
  }) {
    return NotificationState(
      resNotifications: resNotifications ?? this.resNotifications
    );
  }

  NotificationState addData(ResNotifications data) {
    List<Notification> list = this.resNotifications != null 
      ? this.resNotifications.results ?? []
      : [];
    return NotificationState(
      resNotifications: ResNotifications(
        count: data.count,
        next: data.next,
        previous: data.previous,
        results: List.from(list)..addAll(data.results)
      )
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is NotificationState &&
        resNotifications == other.resNotifications;

  @override
  int get hashCode =>
    resNotifications.hashCode;
}
import 'dart:io';

import 'package:eight_biz_flutter/models/business_address.dart';
import 'package:eight_biz_flutter/models/notification.dart' as notif;
import 'package:eight_biz_flutter/models/user_profile.dart';
import 'package:flutter/material.dart';

typedef void StringCallback(String message);

class RequestUserProfileAction {
  final StringCallback onError;

  RequestUserProfileAction({this.onError});
}

class RequestProfileSuccess {
  final UserProfile user;
  

  RequestProfileSuccess(this.user);
}

class ProfileTabLoadingAnimation {
  final bool loading;

  ProfileTabLoadingAnimation({this.loading = false});
}

class EditProfileLoadingAnimation {
  final bool loading;

  EditProfileLoadingAnimation({this.loading = false});
}

class ChangePassLoadingAnimation {
  final bool loading;

  ChangePassLoadingAnimation({this.loading = false});
}

class SwitchToOtherBusiness {
  final String businessId;
  
  SwitchToOtherBusiness(this.businessId);
}

class SubmitUpdateProfileAction {
  final String firstName;
  final String lastName;
  final String dateOfBirth;
  final String phoneNumber;
  final BizAddress address;
  final StringCallback onSuccess;
  final StringCallback onError;

  SubmitUpdateProfileAction({
    @required this.firstName,
    @required this.lastName,
    @required this.dateOfBirth,
    @required this.phoneNumber,
    @required this.address,
    this.onSuccess,
    this.onError
  });
}

class SubmitChangePassAction {
  final String currentPassword;
  final String newPassword;
  final StringCallback onSuccess;
  final StringCallback onError;

  SubmitChangePassAction(this.currentPassword, this.newPassword, {this.onSuccess, this.onError});
}

class LoadProfileNotificationsActions {
  final String businessId;

  LoadProfileNotificationsActions(this.businessId);
}

class LoadProfileNotifsSuccess {
  final String businessId;
  final List<notif.Notification> notifications;

  LoadProfileNotifsSuccess({this.businessId, this.notifications});
}

class UploadProfileAvatarAction {
  final File file;
  final StringCallback onSuccess;
  final StringCallback onError;

  UploadProfileAvatarAction({this.file, this.onSuccess, this.onError});
}

class UploadAvatarSuccess {
  final File file;

  UploadAvatarSuccess({this.file});
}

class ChangeLanguageAction {
  final String language;
  final StringCallback onSuccess;
  final StringCallback onError;

  ChangeLanguageAction({this.language, this.onSuccess, this.onError});
}
import 'package:eight_biz_flutter/redux/home/home_actions.dart';
import 'package:redux/redux.dart';

import 'profile_actions.dart';
import 'profile_state.dart';


final profileReducer = combineReducers<ProfileState>([
  TypedReducer<ProfileState, ProfileTabLoadingAnimation>(_loadingAnimation),
  TypedReducer<ProfileState, LoadProfileNotifsSuccess>(_loadNotifsSuccess),
  TypedReducer<ProfileState, EditProfileLoadingAnimation>(_loadingEditingAnimation),
  TypedReducer<ProfileState, ChangePassLoadingAnimation>(_loadingChangePassAnimation),
  TypedReducer<ProfileState, RequestProfileSuccess>(_onRequestProfileSuccess),
  TypedReducer<ProfileState, UploadAvatarSuccess>(_updateProfile),
  TypedReducer<ProfileState, ClearDataAfterLogout>(_clearData),
]);

ProfileState _loadingAnimation(ProfileState state, ProfileTabLoadingAnimation action) {
  return state.copyWith(loading: action.loading);
}

ProfileState _loadNotifsSuccess(ProfileState state, LoadProfileNotifsSuccess action) {
  return state.copyWith(notifications: action.notifications);
}

ProfileState _loadingEditingAnimation(ProfileState state, EditProfileLoadingAnimation action) {
  return state.copyWith(loadingEdit: action.loading);
}

ProfileState _loadingChangePassAnimation(ProfileState state, ChangePassLoadingAnimation action) {
  return state.copyWith(loadingChangePass: action.loading);
}

ProfileState _onRequestProfileSuccess(ProfileState state, RequestProfileSuccess action) {
  print(action.user.toString());
  return state.copyWith(userDetail: action.user);
}

ProfileState _updateProfile(ProfileState state, UploadAvatarSuccess action) {
  return state.copyWith(localAvatar: action.file);
}

ProfileState _clearData(ProfileState state, ClearDataAfterLogout action) {
  return ProfileState.initial();
}


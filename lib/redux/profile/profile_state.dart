import 'dart:io';

import 'package:eight_biz_flutter/models/notification.dart';
import 'package:eight_biz_flutter/models/user_profile.dart';
import 'package:meta/meta.dart';

@immutable
class ProfileState {
  final bool loading;
  final UserProfile userDetail;
  final File localAvatar;
  final bool loadingEdit;
  final bool loadingChangePass;
  final List<Notification> notifications;

  ProfileState({
    @required this.loading,
    @required this.userDetail,
    @required this.localAvatar,
    @required this.loadingEdit,
    @required this.loadingChangePass,
    @required this.notifications
  });

  factory ProfileState.initial() {
    return ProfileState(
      loading: false,
      loadingEdit: false,
      loadingChangePass: false,
      userDetail: UserProfile(),
      localAvatar: null,
      notifications: []
    );
  }

  ProfileState copyWith({
    bool loading,
    bool loadingEdit,
    bool loadingChangePass,
    UserProfile userDetail,
    List<Notification> notifications,
    File localAvatar
  }) {
    return ProfileState(
      loading: loading ?? this.loading,
      loadingEdit: loadingEdit ?? this.loadingEdit,
      loadingChangePass: loadingChangePass ?? this.loadingChangePass,
      userDetail: userDetail ?? this.userDetail,
      localAvatar: localAvatar ?? this.localAvatar,
      notifications: notifications ?? this.notifications
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is ProfileState &&
        loading == other.loading &&
        loadingEdit == other.loadingEdit &&
        loadingChangePass == other.loadingChangePass &&
        userDetail == other.userDetail &&
        localAvatar == other.localAvatar &&
        notifications == other.notifications;

  @override
  int get hashCode =>
    loading.hashCode ^
    loadingEdit.hashCode ^
    loadingChangePass.hashCode ^
    userDetail.hashCode ^
    localAvatar.hashCode ^
    notifications.hashCode;
}
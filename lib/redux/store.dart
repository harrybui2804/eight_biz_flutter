import 'package:eight_biz_flutter/redux/middleware/wallet_middleware.dart';
import 'package:redux/redux.dart';

import 'app_reducer.dart';
import 'app_state.dart';
import 'middleware/admin_middleware.dart';
import 'middleware/auth_middleware.dart';
import 'middleware/biz_middleware.dart';
import 'middleware/navigation_middleware.dart';
import 'middleware/notification_middleware.dart';
import 'middleware/profile_middleware.dart';

Store<AppState> createStore() {
  return Store(
    appReducer,
    initialState: AppState.initial(),
    middleware: [
      AuthMiddleware(),
      BizMiddleware(),
      WalletMiddleware(),
      AdminMiddleware(),
      ProfileMiddleware(),
      NotificationMiddleware(),
      NavigationMiddleware()
    ]
  );
}
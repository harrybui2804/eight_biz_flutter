import 'dart:io';

import 'package:eight_biz_flutter/models/biz_bank.dart';
import 'package:eight_biz_flutter/models/enums.dart';
import 'package:eight_biz_flutter/models/wallet_balance.dart';
import 'package:eight_biz_flutter/response/res_transactions.dart';
import 'package:flutter/material.dart';

typedef void StringCallback(String message);

class LoadWalletDashboardAction {
  final String businessId;
  final StringCallback onSuccess;
  final StringCallback onError;

  LoadWalletDashboardAction({
    @required this.businessId, 
    this.onSuccess, 
    this.onError
  });
}

class LoadWalletDashboardSuccess {
  final String businessId;
  final WalletBalance balance;

  LoadWalletDashboardSuccess({this.businessId, this.balance});
}

class GetCashoutAction {
  final String businessId;
  final double amount;
  final String remark;
  final StringCallback onSuccess;
  final StringCallback onError;

  GetCashoutAction({
    @required this.businessId,
    @required this.amount,
    @required this.remark,
    this.onSuccess,
    this.onError
  });
}

class GetCashoutSuccess {
  final String businessId;
  final double amount;
  final String remark;

  GetCashoutSuccess({
    @required this.businessId,
    @required this.amount,
    @required this.remark
  });
}

class GetBizBankAction {
  final String businessId;
  final StringCallback onSuccess;
  final StringCallback onError;

  GetBizBankAction({
    @required this.businessId,
    this.onSuccess,
    this.onError
  });
}

class GetBizBankSuccess {
  final String businessId;
  final BizBank bank;

  GetBizBankSuccess({
    this.businessId,
    this.bank
  });
}

class ChangeFilterTransactionsAction {
  final int selectedMonth;
  final String fromDate;
  final String toDate;
  final EarningStatusFilter status;
  final EarningTransactionFilter transaction;
  final EarningOrderFilter ordering;
  final bool canEmptyDate;

  ChangeFilterTransactionsAction({
    this.selectedMonth,
    this.fromDate,
    this.toDate,
    this.status,
    this.transaction,
    this.ordering,
    this.canEmptyDate = false
  });
}

class ResetFilterTransactionsAction {

}

class LoadEarningAction {
  final String businessId;
  final String next;
  final String fromDate;
  final String toDate;
  final EarningStatusFilter status;
  final EarningTransactionFilter transaction;
  final EarningOrderFilter ordering; 
  final StringCallback onSuccess;
  final StringCallback onError;


  LoadEarningAction({
    @required this.businessId,
    this.next,
    this.fromDate,
    this.toDate,
    this.status,
    this.transaction, 
    this.ordering,
    this.onSuccess,
    this.onError
  });
}

class LoadEarningSuccess {
  final String businessId;
  final bool isNext;
  final ResTransactions resTransactions;

  LoadEarningSuccess({this.businessId, this.isNext, this.resTransactions});
}

class UpdateBizBankAction {
  final String businessId;
  final BizBank bank;
  final String bankToken;
  final StringCallback onSuccess;
  final StringCallback onError;

  UpdateBizBankAction({
    @required this.businessId,
    @required this.bankToken,
    this.bank,
    this.onSuccess,
    this.onError
  });
}

class UpdateBizBankSuccess {
  final String businessId;
  final BizBank bank;

  UpdateBizBankSuccess({
    @required this.businessId,
    @required this.bank
  });
}

class GetStripeVerifyStatusAction {
  final String businessId;
  final StringCallback onSuccess;
  final StringCallback onError;

  GetStripeVerifyStatusAction({
    @required this.businessId,
    this.onSuccess,
    this.onError
  });
}

class SelectPhotoIDAction {
  final File frontID;
  final File backID;

  SelectPhotoIDAction({this.frontID, this.backID});
}

class SubmitVerifyStripe {
  final String businessId;
  final File frontID;
  final File backID;
  final StringCallback onSuccess;
  final StringCallback onError;

  SubmitVerifyStripe({
    this.businessId,
    this.frontID,
    this.backID,
    this.onSuccess,
    this.onError
  });
}
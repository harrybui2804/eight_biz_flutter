import 'package:eight_biz_flutter/models/wallet_balance.dart';
import 'package:eight_biz_flutter/redux/home/home_actions.dart';
import 'package:eight_biz_flutter/response/res_transactions.dart';
import 'package:redux/redux.dart';

import 'wallet_actions.dart';
import 'wallet_state.dart';


final walletReducer = combineReducers<WalletState>([
  TypedReducer<WalletState, LoadWalletDashboardSuccess>(_loadWalletDetailSuccess),
  TypedReducer<WalletState, GetBizBankSuccess>(_getBankSuccess),
  TypedReducer<WalletState, UpdateBizBankSuccess>(_updateBankSuccess),
  TypedReducer<WalletState, GetCashoutSuccess>(_getCashoutSuccess),
  TypedReducer<WalletState, LoadEarningSuccess>(_loadEarningSuccess),
  TypedReducer<WalletState, ChangeFilterTransactionsAction>(_changeFilterTransactions),
  TypedReducer<WalletState, ResetFilterTransactionsAction>(_resetFilterTransactions),
  TypedReducer<WalletState, SelectPhotoIDAction>(_selectPhotoID),
  TypedReducer<WalletState, ClearHomeDataAction>(_clearWhenSwitch),
  TypedReducer<WalletState, ClearDataAfterLogout>(_clearData),
]);

WalletState _loadWalletDetailSuccess(WalletState state, LoadWalletDashboardSuccess action) {
  return state.copyWith(balance: action.balance);
}

WalletState _getCashoutSuccess(WalletState state, GetCashoutSuccess action) {
  return state.copyWith(balance: WalletBalance(
    totalBalance: state.balance.totalBalance - action.amount
  ));
}

WalletState _getBankSuccess(WalletState state, GetBizBankSuccess action) {
  return state.copyWith(bank: action.bank);
}

WalletState _updateBankSuccess(WalletState state, UpdateBizBankSuccess action) {
  return state.copyWith(bank: action.bank);
}

WalletState _loadEarningSuccess(WalletState state, LoadEarningSuccess action) {
  if(action.isNext) {
    return state.copyWith(
      resTransactions: state.resTransactions != null
        ? state.resTransactions.addData(action.resTransactions)
        : ResTransactions().addData(action.resTransactions)
    );
  }
  return state.copyWith(resTransactions: action.resTransactions);
}

WalletState _changeFilterTransactions(WalletState state, ChangeFilterTransactionsAction action) {
  return state.copyWith(
    canEmptyDate: action.canEmptyDate,
    selectedMonth: action.selectedMonth,
    fromDate: action.fromDate,
    toDate: action.toDate,
    statusFilter: action.status,
    transactionFilter: action.transaction,
    orderFilter: action.ordering
  );
}

WalletState _resetFilterTransactions(WalletState state, ResetFilterTransactionsAction action) {
  return state.resetFilter();
}

WalletState _selectPhotoID(WalletState state, SelectPhotoIDAction action) {
  return state.copyWith(
    frontID: action.frontID,
    backID: action.backID
  );
}

WalletState _clearWhenSwitch(WalletState state, ClearHomeDataAction action) {
  return WalletState.initial();
}

WalletState _clearData(WalletState state, ClearDataAfterLogout action) {
  return WalletState.initial();
}
import 'dart:io';

import 'package:eight_biz_flutter/models/biz_bank.dart';
import 'package:eight_biz_flutter/models/enums.dart';
import 'package:eight_biz_flutter/models/wallet_balance.dart';
import 'package:eight_biz_flutter/response/res_transactions.dart';
import 'package:meta/meta.dart';

@immutable
class WalletState {
  final WalletBalance balance;
  final BizBank bank;
  final ResTransactions resTransactions;
  //Transactions filter
  final int selectedMonth;
  final String fromDate;
  final String toDate;
  final EarningStatusFilter statusFilter;
  final EarningTransactionFilter transactionFilter;
  final EarningOrderFilter orderFilter; 
  //Verify account
  final File frontID;
  final File backID;
  

  WalletState({
    @required this.balance,
    @required this.bank,
    this.selectedMonth,
    this.fromDate,
    this.toDate,
    this.statusFilter,
    this.transactionFilter,
    this.orderFilter = EarningOrderFilter.modifiedAtDsc,
    @required this.resTransactions,
    this.frontID,
    this.backID
  });

  factory WalletState.initial() {
    return WalletState(
      balance: null,
      bank: null,
      selectedMonth: DateTime.now().month,
      fromDate: null,
      toDate: null,
      statusFilter: null,
      transactionFilter: null,
      orderFilter: EarningOrderFilter.modifiedAtDsc,
      resTransactions: null,
      frontID: null,
      backID: null,
    );
  }

  WalletState copyWith({
    WalletBalance balance,
    BizBank bank,
    int selectedMonth,
    String fromDate,
    String toDate,
    EarningStatusFilter statusFilter,
    EarningTransactionFilter transactionFilter,
    EarningOrderFilter orderFilter,
    ResTransactions resTransactions,
    bool canEmptyDate = false,
    File frontID,
    File backID
  }) {
    return WalletState(
      balance: balance ?? this.balance,
      bank: bank ?? this.bank,
      selectedMonth: selectedMonth ?? this.selectedMonth,
      fromDate: canEmptyDate ? fromDate : fromDate ?? this.fromDate,
      toDate: canEmptyDate ? toDate : toDate ?? this.toDate,
      statusFilter: statusFilter ?? this.statusFilter,
      transactionFilter: transactionFilter ?? this.transactionFilter,
      orderFilter: orderFilter ?? this.orderFilter,
      resTransactions: resTransactions ?? this.resTransactions,
      frontID: frontID ?? this.frontID,
      backID: backID ?? this.backID
    );
  }

  WalletState resetFilter() {
    return WalletState(
      balance: this.balance,
      bank: this.bank,
      resTransactions: resTransactions ?? this.resTransactions,
      selectedMonth: this.selectedMonth,
      fromDate: null,
      toDate: null,
      statusFilter: null,
      transactionFilter: null,
      orderFilter: EarningOrderFilter.modifiedAtDsc,
      frontID: this.frontID,
      backID: this.backID
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is WalletState &&
        balance == other.balance &&
        bank == other.bank &&
        selectedMonth == other.selectedMonth &&
        fromDate == other.fromDate &&
        toDate == other.toDate &&
        statusFilter == other.statusFilter &&
        transactionFilter == other.transactionFilter &&
        orderFilter == other.orderFilter &&
        resTransactions == other.resTransactions &&
        frontID == other.frontID &&
        backID == other.backID;

  @override
  int get hashCode =>
    balance.hashCode ^
    bank.hashCode ^
    selectedMonth.hashCode ^
    fromDate.hashCode ^
    toDate.hashCode ^
    statusFilter.hashCode ^
    transactionFilter.hashCode ^
    orderFilter.hashCode ^
    resTransactions.hashCode ^
    frontID.hashCode ^
    backID.hashCode;
}
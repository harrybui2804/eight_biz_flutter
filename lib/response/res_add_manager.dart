import 'res_model.dart';

class ResAddManager extends ResModel {
  final String message;

  ResAddManager({this.message});

  @override
  String toString() => 'ResManager { message: $message }';

  @override
  List<Object> get props => [message];
}

import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:flutter/material.dart';

import 'res_model.dart';

class ResBizDetail extends ResModel {
  final BusinessDetail business;

  ResBizDetail({@required this.business});

  @override
  String toString() => 'ResBizDetail { business: $business }';

  @override
  List<Object> get props => [business];
}
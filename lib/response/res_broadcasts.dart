import 'package:eight_biz_flutter/models/broadcast.dart';
import 'package:eight_biz_flutter/response/res_model.dart';

class ResBroadcasts extends ResModel {
  final int count;
  final String next;
  final String previous;
  final List<Broadcast> results;

  ResBroadcasts({
    this.count,
    this.next,
    this.previous,
    this.results = const []
  });

  static ResBroadcasts initial() {
    return ResBroadcasts(
      count: 0,
      next: null,
      previous: null,
      results: []
    );
  }

  ResBroadcasts update(String id, String title, String body, String desc, bool status) {
    return ResBroadcasts(
      count: this.count,
      next: this.next,
      previous: this.previous,
      results: this.results.map((broad) {
        if(broad.id == id) {
          final _broad = broad.copyWith(
            title: title,
            body: body,
            description: desc,
            status: status
          );
          return _broad;
        }
        return broad;
      })
    );
  }

  ResBroadcasts delete(String id) {
    return ResBroadcasts(
      count: this.count,
      next: this.next,
      previous: this.previous,
      results: this.results.where((broad) => broad.id != id).toList()
    );
  }

  @override
  List<Object> get props => null;

}
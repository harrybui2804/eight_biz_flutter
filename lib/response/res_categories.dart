import 'package:eight_biz_flutter/models/biz_category.dart';

import 'res_model.dart';

class ResCategories extends ResModel {
  final List<BizCategory> categories;

  ResCategories({this.categories = const []});

  @override
  String toString() => 'ResCategories { categories: $categories }';

  @override
  List<Object> get props => [categories];
}
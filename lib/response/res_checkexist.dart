import 'res_model.dart';

class ResCheckExist extends ResModel {
  final bool isAlreadyUser;
  final String message;
  final String registeredId;

  ResCheckExist({this.isAlreadyUser, this.message, this.registeredId});

  @override
  String toString() => 'ResCheckExist { is_already_user: $isAlreadyUser, message: $message, registered_id: $registeredId }';

  @override
  List<Object> get props => [isAlreadyUser, message, registeredId];
}
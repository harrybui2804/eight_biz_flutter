import 'res_model.dart';

class ResCreateBiz extends ResModel {
  final String businessId;
  final String message;

  ResCreateBiz({this.businessId, this.message});

  @override
  String toString() => 'ResCreateBiz { businessId: $businessId, message: $message }';

  @override
  List<Object> get props => [businessId, message];
}
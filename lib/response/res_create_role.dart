import 'res_model.dart';

class ResCreateRole extends ResModel {
  final String message;
  final String roleId;

  ResCreateRole({this.message, this.roleId});

  @override
  String toString() => 'ResCreateRole { message: $message, roleId: $roleId }';

  @override
  List<Object> get props => [message];
}
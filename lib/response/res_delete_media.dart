import 'res_model.dart';

class ResDeleteMedias extends ResModel {
  final String message;
  
  ResDeleteMedias({
    this.message
  });

  @override
  String toString() => 'ResDeleteMedias { message: $message }';

  @override
  List<Object> get props => [message];
}
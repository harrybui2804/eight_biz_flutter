import 'res_model.dart';

class ResError extends ResModel {
  final String code;
  final String message;
  final dynamic data;

  ResError({this.code, this.message, this.data});

  @override
  String toString() => 'ResError { code: $code, message: $message, data: $data }';

  @override
  List<Object> get props => [code, message, data];
}
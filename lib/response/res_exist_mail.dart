import 'res_model.dart';

class ResExistMail extends ResModel {
  final bool isExist;
  final String registeredId;

  ResExistMail({this.isExist, this.registeredId});

  @override
  String toString() => 'ResExistMail { isExist: $isExist, registered_id: $registeredId }';

  @override
  List<Object> get props => [isExist, registeredId];
}
import 'package:eight_biz_flutter/models/biz_bank.dart';

import 'res_model.dart';

class ResGetBizBank extends ResModel {
  final BizBank bank;

  ResGetBizBank({
    this.bank
  });

  @override
  List<Object> get props => [bank];
}
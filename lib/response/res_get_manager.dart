import 'package:eight_biz_flutter/models/biz_manager.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';

import 'res_model.dart';

class ResGetManager extends ResModel {
  final String id;
  final String role;
  final bool isNotification;
  final bool isOwner;
  final bool isActive;
  final UserManager user;
  final List<BusinessPermision> permissions;

  ResGetManager({
    this.id, 
    this.role,
    this.isActive,
    this.isNotification,
    this.isOwner,
    this.user,
    this.permissions
  });

  @override
  List<Object> get props => [id, role, isNotification, isOwner, isActive, user, permissions];
}
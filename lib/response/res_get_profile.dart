import 'package:eight_biz_flutter/models/user_profile.dart';

import 'res_model.dart';

class ResGetProfile extends ResModel {
  final UserProfile userProfile;

  ResGetProfile({this.userProfile});

  @override
  String toString() => 'ResGetProfile { userProfile: $userProfile }';

  @override
  List<Object> get props => [userProfile];
}
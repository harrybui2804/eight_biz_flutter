import 'package:eight_biz_flutter/models/business_list.dart';

import 'res_model.dart';

class ResListBiz extends ResModel {
  final int count;
  final String next;
  final String prev;
  final List<BusinessList> results;

  ResListBiz({this.count, this.next, this.prev, this.results = const []});

  @override
  String toString() => 'ResListBiz { count: $count, next: $next, prev: $prev, results: $results }';

  @override
  List<Object> get props => [count, next, prev, results];
}
import 'res_model.dart';

class ResLogin extends ResModel {
  final int statusCode;
  final String token;
  final String accessToken;

  ResLogin({this.statusCode, this.token, this.accessToken});

  @override
  String toString() => 'ResLogin { statusCode: $statusCode, token: $token, accessToken: $accessToken }';

  @override
  List<Object> get props => [statusCode, token, accessToken];
}
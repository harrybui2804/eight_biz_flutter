import 'package:eight_biz_flutter/models/biz_manager.dart';

import 'res_model.dart';

class ResManagers extends ResModel {
  final int count;
  final String next;
  final String prev;
  final List<BizManager> results;

  ResManagers({this.count, this.next, this.prev, this.results = const []});

  static ResManagers initial() {
    return ResManagers(
      count: 0,
      next: null,
      prev: null,
      results: []
    );
  }

  @override
  String toString() => 'ResManagers { count: $count, next: $next, prev: $prev, results: $results }';

  @override
  List<Object> get props => [count, next, prev, results];
}

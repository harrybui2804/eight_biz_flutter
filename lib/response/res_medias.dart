import 'package:eight_biz_flutter/models/media.dart';

import 'res_model.dart';

class ResMedias extends ResModel {
  final int count;
  final String next;
  final String previous;
  final List<Media> results;
  
  ResMedias({
    this.count,
    this.next,
    this.previous,
    this.results = const []
  });

  ResMedias copyWith({
    int count,
    String next,
    String previous,
    List<Media> results
  }) {
    return ResMedias(
      count: count ?? this.count,
      next: next ?? this.next,
      previous: previous ?? this.previous,
      results: results ?? this.results
    );
  }

  @override
  String toString() => 'ResMedias { count: $count, next: $next, previous: $previous, results: $results }';

  @override
  List<Object> get props => [count, next, previous, results];
}
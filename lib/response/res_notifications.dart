import 'package:eight_biz_flutter/models/notification.dart';

import 'res_model.dart';

class ResNotifications extends ResModel {
  final int count;
  final String next;
  final String previous;
  final List<Notification> results;
  
  ResNotifications({
    this.count,
    this.next,
    this.previous,
    this.results = const []
  });

  ResNotifications copyWith({
    int count,
    String next,
    String previous,
    List<Notification> results
  }) {
    return ResNotifications(
      count: count ?? this.count,
      next: next ?? this.next,
      previous: previous ?? this.previous,
      results: results ?? this.results
    );
  }

  @override
  String toString() => 'ResNotifications { count: $count, next: $next, previous: $previous, results: $results }';

  @override
  List<Object> get props => [count, next, previous, results];
}
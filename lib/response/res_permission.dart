import 'package:eight_biz_flutter/models/biz_permission.dart';

import 'res_model.dart';

class ResPermission extends ResModel {
  final String businessId;
  final List<BusinessPermision> permissions;

  ResPermission({this.businessId, this.permissions = const []});

  @override
  String toString() => 'ResPermission { businessId: $businessId, permissions: $permissions }';

  @override
  List<Object> get props => [businessId, permissions];
}
import 'res_model.dart';

class ResPhotoID extends ResModel {
  final String frontId;
  final String backId;

  ResPhotoID({this.frontId, this.backId});

  @override
  String toString() => 'ResPhotoID { frontId: $frontId, backId: $backId }';

  @override
  List<Object> get props => [frontId, backId];
}
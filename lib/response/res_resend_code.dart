import 'res_model.dart';

class ResResendCode extends ResModel {
  final String message;
  final String registeredId;

  ResResendCode({this.message, this.registeredId});

  @override
  String toString() => 'ResResendCode { message: $message, registered_id: $registeredId }';

  @override
  List<Object> get props => [message, registeredId];
}
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/biz_role.dart';

import 'res_model.dart';

class ResRoleDetail extends ResModel {
  final BizRole role;
  final List<BusinessPermision> permissions;

  ResRoleDetail({this.role, this.permissions = const []});

  @override
  String toString() => 'ResRoleDetail { role: $role, permissions: $permissions }';

  @override
  List<Object> get props => [role, permissions];
}

import 'package:eight_biz_flutter/models/biz_role.dart';

import 'res_model.dart';

class ResRoles extends ResModel {
  final List<BizRole> roles;

  ResRoles({this.roles = const []});

  static ResRoles initial() {
    return ResRoles(
      roles: []
    );
  }

  @override
  String toString() => 'ResRoles { roles: $roles }';

  @override
  List<Object> get props => [roles];
}

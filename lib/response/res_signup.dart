import 'res_model.dart';

class ResSignup extends ResModel {
  final String message;

  ResSignup({this.message});

  @override
  String toString() => 'ResSignup { tokmessageen: $message }';

  @override
  List<Object> get props => [message];
}
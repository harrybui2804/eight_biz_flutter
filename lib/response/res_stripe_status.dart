import 'res_model.dart';

class ResStripeVerify extends ResModel {
  final bool bank;
  final String verified;

  ResStripeVerify({this.bank, this.verified});

  @override
  String toString() => 'ResStripeVerify { bank: $bank, verified: $verified }';

  @override
  List<Object> get props => [bank, verified];
}
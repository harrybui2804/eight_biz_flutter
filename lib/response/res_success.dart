import 'res_model.dart';

class ResSuccess extends ResModel {
  final String message;

  ResSuccess({this.message});

  @override
  String toString() => 'ResSuccess { message: $message }';

  @override
  List<Object> get props => [message];
}
import 'package:eight_biz_flutter/models/wallet_transaction.dart';
import 'package:eight_biz_flutter/response/res_model.dart';

class ResTransactions extends ResModel {
  final int count;
  final String next;
  final String prev;
  final List<WalletTransaction> results;

  ResTransactions({
    this.count,
    this.next,
    this.prev,
    this.results
  });

  ResTransactions addData(ResTransactions value) {
    final res = this.results ?? [];
    return ResTransactions(
      count: value.count,
      next: value.next,
      prev: value.prev,
      results: List.from(res)..addAll(value.results)
    );
  }
  
  @override
  List<Object> get props => [count, next, prev, results];

}
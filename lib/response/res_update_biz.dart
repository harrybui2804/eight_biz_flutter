import 'package:eight_biz_flutter/models/business_detail.dart';

import 'res_model.dart';

class ResUpdateBiz extends ResModel {
  final BusinessDetail business;
  final String message;
  final bool updateLogo;
  final bool updateCover;

  ResUpdateBiz({this.business, this.message, this.updateLogo, this.updateCover});

  @override
  String toString() => 'ResUpdateBiz { business: $business, message: $message, updateLogo: $updateLogo, updateCover: $updateCover }';

  @override
  List<Object> get props => [business, message, updateLogo, updateCover];
}
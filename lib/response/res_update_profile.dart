import 'package:eight_biz_flutter/response/res_model.dart';

class ResUpdateProfile extends ResModel {
  final String message;

  ResUpdateProfile({this.message});
  
  @override
  List<Object> get props => [];

}
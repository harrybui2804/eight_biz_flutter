import 'dart:io';

import 'res_model.dart';

class ResUpload extends ResModel {
  final File file;
  final String businessId;
  final String imageId;
  final String message;

  ResUpload({this.imageId, this.businessId, this.file, this.message});

  @override
  String toString() => 'ResUpload { imageId: $imageId, file: $file, message: $message, businessId: $businessId }';

  @override
  List<Object> get props => [file, businessId, imageId, message];
}
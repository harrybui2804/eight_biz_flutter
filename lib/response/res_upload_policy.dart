import 'dart:io';

import 'res_model.dart';

class ResUploadPolicy extends ResModel {
  final File file;
  final String businessId;
  final String fileId;
  final String policy;
  final String signature;
  final String key;
  final String message;
  
  ResUploadPolicy({this.file, this.businessId, this.fileId, this.key, this.policy, this.signature, this.message});

  @override
  String toString() => 'ResUploadPolicy { fileId: $fileId, file: $file, businessId: $businessId, message: $message }';

  @override
  List<Object> get props => [file, businessId, fileId, policy, signature, key, message];
}
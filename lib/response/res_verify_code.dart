import 'res_model.dart';

class ResVerifyCode extends ResModel {
  final String message;
  final String registeredId; //registered_id
  final String publicKey; //public_key
  final String privateKey;  //private_key

  ResVerifyCode({this.message, this.registeredId, this.publicKey, this.privateKey});

  @override
  String toString() => 'ResVerifyCode { message: $message, registered_id: $registeredId }';

  @override
  List<Object> get props => [message, registeredId, publicKey, privateKey];
}
import 'package:eight_biz_flutter/models/wallet_balance.dart';
import 'package:flutter/material.dart';

import 'res_model.dart';

class ResWalletDetail extends ResModel {
  final WalletBalance balance;

  ResWalletDetail({@required this.balance});
  
  @override
  List<Object> get props => [];

}
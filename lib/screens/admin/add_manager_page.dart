import 'package:eight_biz_flutter/components/complete_modal.dart';
import 'package:eight_biz_flutter/components/custom_modal.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_manager.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/biz_role.dart';
import 'package:eight_biz_flutter/redux/admin/admin_actions.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/response/res_get_manager.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/manager_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:phone_number/phone_number.dart';

typedef OnPopToRoot(dynamic params);

class AddManagerPage extends StatefulWidget {
  final BizManager manager;
  final bool isAdd;

  AddManagerPage({Key key, this.manager, this.isAdd = true}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddManagerState();
}

class _AddManagerState extends State<AddManagerPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _roleNamecontroller = new TextEditingController();
  final _roleNameFocus = FocusNode();
  Widget phoneNumberWidget;
  bool loading = false;
  bool loadingRoles = true;
  bool isPhoneValid = false;
  String _phoneNumber = '';
  List<BizRole> roles = [];
  BizRole selectedRole;
  String roleName;
  List<BusinessPermision> permissions = [];//BusinessPermision.initialAll();
  ResGetManager managerDetail;

  @override
  void initState() {
    super.initState();
    _buildPhoneInput();
    if(!widget.isAdd && widget.manager != null) {
      setState(() {
        selectedRole = BizRole(
          id: widget.manager.roleId,
          role: widget.manager.role
        );
      });
    }
  }

  _buildPhoneInput() async {
    if(!widget.isAdd) {
      _roleNamecontroller.text = widget.manager.role;
      var countryCode = '61';
      var phoneNumber = '';
      bool _isPhoneValid = false;
      try {
        final dynamic result = await PhoneNumber.parse(widget.manager.user.phone);
        phoneNumber = result['national_number'];
        countryCode = result['country_code'];
        _isPhoneValid = true;
      } catch (e) {
        _isPhoneValid = false;
        print('parse phone $e');
        Helper.showError(context, e.toString());
      }
      setState(() {
        isPhoneValid = _isPhoneValid;
        phoneNumberWidget = Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(
            vertical: 18.0, horizontal: 20.0
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6.0),
            border: Border.all(
              width: 1.0, color: AppColors.strongCyan
            )
          ),
          child: Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: Text(
                  '+$countryCode',
                  style: AppStyles.normalTextCustom(
                    color: AppColors.strongCyan,
                    fontSize: 16,
                    fontWeight: FontWeight.w400
                  ),
                ),
              ),
              Text(
                phoneNumber,
                style: AppStyles.normalTextCustom(
                  fontSize: 16,
                  fontWeight: FontWeight.w400
                ),
              )
            ],
          ),
        );
      });
    }
  }

  void _canNotLoadRoles() {
    Future.delayed(const Duration(milliseconds: 10), () {
      setState(() {
        loadingRoles = false;
      });
    });
  }

  void onPhoneNumberChanged(String phoneNumber) {
    setState(() {
      _phoneNumber = phoneNumber;
    });
  }

  void onInputChanged(bool value) {
    setState(() {
      isPhoneValid = value;
    });
  }

  void _loadRolesSuccess(ManagerViewModel model, List<BizRole> lst) {
    var _role = selectedRole;
    if(!widget.isAdd) {
      _role = BizRole(id: widget.manager.roleId, role: widget.manager.role);
    }
    setState(() {
      loadingRoles = false;
      roles = lst;
      selectedRole = _role;
    });
  }

  void _loadRolesError(String message) {
    setState(() {
      loadingRoles = false;
    });
    Helper.showError(context, message);
  }

  void _loadRoleDetail(ManagerViewModel model, BizRole newRole) {
    if(selectedRole != newRole && !loading) {
      _roleNamecontroller.text = newRole.role;
      setState(() {
        loading = true;
        selectedRole = newRole;
        roleName = newRole.role;
      });
      model.loadRoleDetail(
        model.business.id, 
        newRole, 
        _loadRoleDetailSuccess, 
        (message) {
          setState(() {
            loading = false;
          });
          Helper.showError(context, message);
        }
      );
    }
  }

  void _loadRoleDetailSuccess(List<BusinessPermision> newPermissions) {
    setState(() {
      loading = false;
      permissions = newPermissions;
    });
  }

  void changePermission(BusinessPermision permision) {
    var _permissions = permissions.map((per) {
      if(per.code == permision.code) {
        per = permision.changeGranted(!permision.isGranted);
      }
      return per;
    }).toList();
    setState(() {
      permissions = _permissions;
    });
  }

  void changeAllPermission(bool value) {
    var _permissions = permissions.map((per) {
      per = per.changeGranted(value);
      return per;
    }).toList();
    setState(() {
      permissions = _permissions;
    });
  }

  bool validForm() {
    final list = permissions.where((per) => per.isGranted).toList();
    return isPhoneValid && list.length > 0;
  }

  void _saveUser(ManagerViewModel model) {
    if(widget.isAdd) {
      if(!loading) {
        final addRole = model.permissions.where((per) => per.role == BusinessRole.createManagerRole && per.isGranted)
          .toList().length > 0;
        final unselected = permissions.where((per) => per.isGranted).toList().length == 0; 
        if(selectedRole == null && !addRole) {
          Helper.showError(context, AppLang().roleIsRequired);
          return;
        }
        if(selectedRole == null && _roleNamecontroller.text.trim().length == 0) {
          Helper.showError(context, AppLang().noRoleForCreate);
          return;
        }
        if(unselected) {
          Helper.showError(context, AppLang().noPermissionGrant);
          return;
        }
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return CustomModal(
              title: AppLang().askAddManagerTitle,
              body: AppLang().askAddManagerBody,
              cancelText: AppLang().cancel.toUpperCase(),
              acceptText: AppLang().accept.toUpperCase(),
              onAccept: () {
                setState(() {
                  loading = true;
                });
                final isCreateRole = selectedRole == null;
                final role = isCreateRole ? _roleNamecontroller.text.trim() : selectedRole.id;
                model.saveManager(_phoneNumber, isCreateRole, role, permissions, _onSuccessSave, _onError);
              },
            );
          }
        );
      }
    } else {
      if(!loading) {
        final canUpdate = model.permissions.where((per) => per.role == BusinessRole.updateManager && per.isGranted)
          .toList().length > 0;
        final unselected = permissions.where((per) => per.isGranted).toList().length == 0; 
        if(unselected) {
          Helper.showError(context, AppLang().noPermissionGrant);
          return;
        }
        if(!canUpdate) {
          Helper.showError(context, AppLang().noRoleUpdateManager);
          return;
        }
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return CustomModal(
              title: AppLang().askUpdateManagerTitle,
              body: AppLang().askUpdateManagerBody,
              cancelText: AppLang().cancel.toUpperCase(),
              acceptText: AppLang().accept.toUpperCase(),
              onAccept: () {
                setState(() {
                  loading = true;
                });
                model.updateManger(widget.manager.id, selectedRole.id, permissions, _onUpdateSuccess, _onError);
              },
            );
          }
        );
      }
    }
  }

  _onSuccessSave(String message) {
    setState(() {
      loading = false;
    });
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CompleteModal(
          title: AppLang().saveManagerSuccessTitle,
          body: AppLang().saveManagerSuccessBody,
          onClose: () {
            _pop();
          },
        );
      }
    );
  }

  _onUpdateSuccess(String message) {
    setState(() {
      loading = false;
    });
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CompleteModal(
          title: AppLang().updateManagerSuccessTitle,
          body: AppLang().updateManagerSuccessBody,
          onClose: () { },
        );
      }
    );
  }

  _onError(String message) {
    setState(() {
      loading = false;
    });
    Helper.showError(context, message);
  }

  void _loadDetailManagerSuccess(ResGetManager response) {
    setState(() {
      loading = false;
      managerDetail = response;
      permissions = response.permissions;
    });
  }

  void _deleteUser(ManagerViewModel model) {
    if(!loading) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return CustomModal(
            title: AppLang().askDeleteManagerTitle,
            body: AppLang().askDeleteManagerBody,
            cancelText: AppLang().cancel.toUpperCase(),
            acceptText: AppLang().accept.toUpperCase(),
            onAccept: () {
              setState(() {
                loading = true;
              });
              model.deleteManager(widget.manager.id, _onSuccessDelete, _onError);
            },
          );
        }
      );
    }
  }

  _onSuccessDelete(String message) {
    setState(() {
      loading = false;
    });
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CompleteModal(
          title: AppLang().deleteManagerSuccessTitle,
          body: AppLang().deleteManagerSuccessBody,
          onClose: () { 
            _pop();
          },
        );
      }
    );
  }

  _pop() {
    Navigator.pop(context);
  }

  @override
  void dispose() {
    _roleNamecontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            iconTheme: IconThemeData(color: AppColors.primaryOrange),
            title: Text(
              AppLang().manageStaffTitle,
              style: AppStyles.navTitle(),
            ),
          ),
        ),
      ),
      body: SafeArea(
        child: LayoutBuilder(
          builder: (context, constraints) => Container(
            color: Colors.white,
            child: StoreConnector<AppState, ManagerViewModel> (
              onInit: (store) {
                final pers = store.state.homeState.permissions;
                Future.delayed(const Duration(milliseconds: 2), () {
                  setState(() {
                    permissions = pers.map((per){
                      return BusinessPermision(
                        code: per.code,
                        name: per.name,
                        isGranted: false
                      );
                    }).toList();
                  });
                });
                final canLoad = pers.where((per) => per.role == BusinessRole.listManagerRole && per.isGranted).toList().length > 0;
                var bizId = store.state.homeState.business.id;
                if(canLoad) {
                  store.dispatch(LoadListRolesAction(
                    businessId: bizId,
                    onSuccess: (List<BizRole> roles) {
                      final model = ManagerViewModel.fromStore(store);
                      _loadRolesSuccess(model, roles);
                    } ,
                    onError: _loadRolesError
                  ));
                } else {
                  _canNotLoadRoles();
                }
                if(!widget.isAdd && widget.manager != null) {
                  //Load edit detail
                  Future.delayed(const Duration(milliseconds: 10), () {
                    setState(() {
                      loading = true;
                    });
                  });
                  store.dispatch(LoadManagerDetailAction(
                    businessId: bizId,
                    managerId: widget.manager.id,
                    onSuccess: _loadDetailManagerSuccess,
                    onError: _onError
                  ));
                  //Load role if in editing mode
                  final model = ManagerViewModel.fromStore(store);
                  model.loadRoleDetail(
                    model.business.id, 
                    BizRole(id: widget.manager.roleId, role: widget.manager.role), 
                    _loadRoleDetailSuccess, 
                    (message) {
                      setState(() {
                        loading = false;
                      });
                      Helper.showError(context, message);
                    }
                  );
                }
              },
              converter: (store) => ManagerViewModel.fromStore(store),
              builder: (_, viewModel) => content(viewModel)
            )
          ),
        ),
      ),
    );
  }

  Widget content(ManagerViewModel model) {
    double width = MediaQuery.of(context).size.width;
    List<BusinessPermision> pers = model.permissions ?? [];
    final showListRoles = pers.where((per) => per.role == BusinessRole.listManagerRole && per.isGranted).toList().length > 0;
    final addRole = pers.where((per) => per.role == BusinessRole.createManagerRole && per.isGranted).toList().length > 0;
    final addUser = pers.where((per) => per.role == BusinessRole.addManager && per.isGranted).toList().length > 0;
    final updateUser = pers.where((per) => per.role == BusinessRole.updateManager && per.isGranted).toList().length > 0;
    final deleteUser = pers.where((per) => per.role == BusinessRole.deleteManager && per.isGranted).toList().length > 0;
    return Stack(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
                child: SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 5.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 10.0),
                                child: Text(
                                  AppLang().mobileNumber,
                                  style: AppStyles.normalTextCustom(color: Colors.black),
                                ),
                              ),
                              widget.isAdd 
                              ? InternationalPhoneNumberInput.withCustomDecoration(
                                showFlag: false,
                                showDropDownIcon: false,
                                phoneCodeStyle: TextStyle(
                                  color: AppColors.strongCyan,
                                  fontSize: 16
                                ),
                                onInputChanged: onPhoneNumberChanged,
                                onInputValidated: onInputChanged,
                                initialCountry2LetterCode: 'AU',
                                inputDecoration: InputDecoration(
                                  hintText: AppLang().phoneNumber,
                                  prefixIcon: SizedBox(width: 30.0,),
                                  errorText: !isPhoneValid ? AppLang().phoneNumberNotValid : null,
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                                  ),
                                  focusedErrorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0, color: AppColors.primaryOrange),
                                  ),
                                ),
                                formatInput: false,
                              ) : phoneNumberWidget ?? Container(),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 5.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 10.0),
                                child: Text(
                                  AppLang().role,
                                  style: AppStyles.normalTextCustom(color: Colors.black),
                                ),
                              ),
                              Container(
                                width: width - 40,
                                height: 60,
                                padding: EdgeInsets.only(
                                  left: 20
                                ),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black, width: 1),
                                  borderRadius: BorderRadius.all(Radius.circular(6))
                                ),
                                child: !loadingRoles 
                                ? Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: TextField(
                                        controller: _roleNamecontroller,
                                        focusNode: _roleNameFocus,
                                        enabled: addRole && widget.isAdd,
                                        onChanged: (name) {
                                          final list = roles.where((role) => role.role.trim() == name.trim()).toList();
                                          final _newRole = list.length > 0 ? list[0] : null;
                                          setState(() {
                                            selectedRole = _newRole;
                                            roleName = name;
                                          });
                                        },
                                        onEditingComplete: () {
                                          FocusScope.of(context).requestFocus(FocusNode());
                                          if(selectedRole != null) {
                                            setState(() {
                                              loading = true;
                                            });
                                            model.loadRoleDetail(
                                              model.business.id, 
                                              selectedRole, 
                                              _loadRoleDetailSuccess, 
                                              (message) {
                                                setState(() {
                                                  loading = false;
                                                });
                                                Helper.showError(context, message);
                                              }
                                            );
                                          }
                                        },
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: addRole 
                                          ? AppLang().enterRoleOrSelect
                                          : showListRoles ? AppLang().selectARole : AppLang().noPerCreateSelectRole,
                                        ),
                                        style: TextStyle(
                                          fontSize: !addRole && !showListRoles ? 13 : 16
                                        ),
                                      ),
                                      
                                    ),
                                    roles.length > 0 && showListRoles
                                    ? InkWell(
                                      child: Icon(Icons.arrow_drop_down),
                                      onTap: () => _showPickerRoles(model),
                                    ) : Container(),
                                  ],
                                ) : SpinKitCircle(
                                    color: AppColors.primaryOrange,
                                    size: 35,
                                  ),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                            bottom: 10,
                            right: 0
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text(
                                AppLang().permissions,
                                style: AppStyles.normalTextCustom(color: Colors.black),
                              ),
                              InkWell(
                                child: permissions.where((per) => !per.isGranted).toList().length == 0
                                  ? Icon(Icons.check_circle, color: AppColors.primaryOrange)
                                  : Icon(FontAwesomeIcons.circle, color: AppColors.grey2Color),
                                onTap: () {
                                  bool checkedAlll = permissions.where((per) => !per.isGranted).toList().length == 0;
                                  changeAllPermission(!checkedAlll);
                                },
                              )
                            ],
                          ),
                        ),
                        Column(
                          children: permissions.map((permission) {
                            return ListTile(
                              contentPadding: EdgeInsets.all(0),
                              onTap: () {
                                changePermission(permission);
                              },
                              trailing: permission.isGranted 
                                ? Icon(Icons.check_circle, color: AppColors.primaryOrange)
                                : Icon(FontAwesomeIcons.circle, color: AppColors.grey2Color),
                              
                              title: Text(
                                permission.name,
                                style: TextStyle(
                                  color: permission.isGranted 
                                  ? Colors.black
                                  : AppColors.grey2Color
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20
              ),
              child: ((addUser && widget.isAdd) || (updateUser && !widget.isAdd))
              ? RaisedButton(
                onPressed: validForm() ? () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  _saveUser(model);
                } : null,
                child: Container(
                  height: 45,
                  child: Center(
                    child: Text(
                      AppLang().saveDetails,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                color: AppColors.primaryOrange,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6.0),
                ),
              ): Container(),
            ),
            widget.manager != null && deleteUser
            ? Container(
                padding: EdgeInsets.only(
                  top: 20,
                  left: 20,
                  right: 20
                ),
                child: RaisedButton(
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    _deleteUser(model);
                  },
                  child: Container(
                    height: 45,
                    child: Center(
                      child: Text(
                        AppLang().deleteUser,
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  ),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6.0),
                    side: BorderSide(color: Colors.black)
                  ),
                ),
              )
            : Container()
          ],
        ),
        loading ? SpinKitCircle(
          color: AppColors.primaryOrange,
          size: 50,
        ): Container()
      ],
    );
  }

  /*
  Widget content(ManagerViewModel model) {
    double width = MediaQuery.of(context).size.width;
    List<BusinessPermision> pers = model.permissions ?? [];
    final showListRoles = pers.where((per) => per.role == BusinessRole.listManagerRole && per.isGranted).toList().length > 0;
    final addRole = pers.where((per) => per.role == BusinessRole.createManagerRole && per.isGranted).toList().length > 0;
    final addUser = pers.where((per) => per.role == BusinessRole.addManager && per.isGranted).toList().length > 0;
    final updateUser = pers.where((per) => per.role == BusinessRole.updateManager && per.isGranted).toList().length > 0;
    final deleteUser = pers.where((per) => per.role == BusinessRole.deleteManager && per.isGranted).toList().length > 0;
    return Stack(
      children: [
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: 5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.0),
                          child: Text(
                            AppLang().mobileNumber,
                            style: AppStyles.normalTextCustom(color: Colors.black),
                          ),
                        ),
                        widget.isAdd 
                        ? InternationalPhoneNumberInput.withCustomDecoration(
                          showFlag: false,
                          showDropDownIcon: false,
                          phoneCodeStyle: TextStyle(
                            color: AppColors.strongCyan,
                            fontSize: 16
                          ),
                          onInputChanged: onPhoneNumberChanged,
                          onInputValidated: onInputChanged,
                          initialCountry2LetterCode: 'AU',
                          inputDecoration: InputDecoration(
                            hintText: AppLang().phoneNumber,
                            prefixIcon: SizedBox(width: 30.0,),
                            errorText: !isPhoneValid ? AppLang().phoneNumberNotValid : null,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0, color: AppColors.primaryOrange),
                            ),
                          ),
                          formatInput: false,
                        ) : phoneNumberWidget ?? Container(),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.0),
                          child: Text(
                            AppLang().role,
                            style: AppStyles.normalTextCustom(color: Colors.black),
                          ),
                        ),
                        Container(
                          width: width - 40,
                          height: 60,
                          padding: EdgeInsets.only(
                            left: 20
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black, width: 1),
                            borderRadius: BorderRadius.all(Radius.circular(6))
                          ),
                          child: !loadingRoles 
                          ? Row(
                            children: <Widget>[
                              Expanded(
                                child: TextField(
                                  controller: _roleNamecontroller,
                                  focusNode: _roleNameFocus,
                                  enabled: addRole && widget.isAdd,
                                  onChanged: (name) {
                                    final list = roles.where((role) => role.role.trim() == name.trim()).toList();
                                    final _newRole = list.length > 0 ? list[0] : null;
                                    setState(() {
                                      selectedRole = _newRole;
                                      roleName = name;
                                    });
                                  },
                                  onEditingComplete: () {
                                    FocusScope.of(context).requestFocus(FocusNode());
                                    if(selectedRole != null) {
                                      setState(() {
                                        loading = true;
                                      });
                                      model.loadRoleDetail(
                                        model.business.id, 
                                        selectedRole, 
                                        _loadRoleDetailSuccess, 
                                        (message) {
                                          setState(() {
                                            loading = false;
                                          });
                                          Helper.showError(context, message);
                                        }
                                      );
                                    }
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: addRole 
                                    ? AppLang().enterRoleOrSelect
                                    : showListRoles ? AppLang().selectARole : AppLang().noPerCreateSelectRole,
                                  ),
                                  style: TextStyle(
                                    fontSize: !addRole && !showListRoles ? 13 : 16
                                  ),
                                ),
                                
                              ),
                              roles.length > 0 && showListRoles
                              ? InkWell(
                                child: Icon(Icons.arrow_drop_down),
                                onTap: () => _showPickerRoles(model),
                              ) : Container(),
                            ],
                          ) : SpinKitCircle(
                              color: AppColors.primaryOrange,
                              size: 35,
                            ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 10,
                      bottom: 10,
                      right: 0
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          AppLang().permissions,
                          style: AppStyles.normalTextCustom(color: Colors.black),
                        ),
                        InkWell(
                          child: permissions.where((per) => !per.isGranted).toList().length == 0
                            ? Icon(Icons.check_circle, color: AppColors.primaryOrange)
                            : Icon(FontAwesomeIcons.circle, color: AppColors.grey2Color),
                          onTap: () {
                            bool checkedAlll = permissions.where((per) => !per.isGranted).toList().length == 0;
                            changeAllPermission(!checkedAlll);
                          },
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.only(
                        left: 0,
                        right: 0,
                        top: 5,
                        bottom: 5
                      ),
                      itemCount: permissions.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                          contentPadding: EdgeInsets.all(0),
                          onTap: () {
                            changePermission(permissions[index]);
                          },
                          trailing: permissions[index].isGranted 
                            ? Icon(Icons.check_circle, color: AppColors.primaryOrange)
                            : Icon(FontAwesomeIcons.circle, color: AppColors.grey2Color),
                          
                          title: Text(
                            permissions[index].name,
                            style: TextStyle(
                              color: permissions[index].isGranted 
                              ? Colors.black
                              : AppColors.grey2Color
                            ),
                          ),
                          
                        );
                      }
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 20,
                    ),
                    child: ((addUser && widget.isAdd) || (updateUser && !widget.isAdd))
                    ? RaisedButton(
                      onPressed: validForm() ? () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        _saveUser(model);
                      } : null,
                      child: Container(
                        height: 45,
                        child: Center(
                          child: Text(
                            AppLang().saveDetails,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      color: AppColors.primaryOrange,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                    ): Container(),
                  ),
                  widget.manager != null && deleteUser
                  ? Container(
                      padding: EdgeInsets.only(
                        top: 20,
                      ),
                      child: RaisedButton(
                        onPressed: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          _deleteUser(model);
                        },
                        child: Container(
                          height: 45,
                          child: Center(
                            child: Text(
                              AppLang().deleteUser,
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        ),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6.0),
                          side: BorderSide(color: Colors.black)
                        ),
                      ),
                    )
                  : Container()
                ],
              ),
            ),
          ),
        ),
        loading ? SpinKitCircle(
          color: AppColors.primaryOrange,
          size: 50,
        ): Container()
      ],
    );
  }
  */

  void _showPickerRoles(ManagerViewModel model) {
    FocusScope.of(context).requestFocus(FocusNode());
    Picker(
        adapter: PickerDataAdapter(
          data: roles.map((rol) {
            return PickerItem(
              text: Text(
                rol.role
              ), 
              value: rol
            );
          }).toList()
        ),
        title: Text(AppLang().selectARole),
        cancelText: AppLang().cancel,
        confirmText: AppLang().confirm,
        selectedTextStyle: TextStyle(color: AppColors.primaryOrange),
        cancelTextStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
        confirmTextStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: 16, color: AppColors.primaryOrange),
        onConfirm: (Picker picker, List value) {
          final role = picker.getSelectedValues()[0] as BizRole;
          _loadRoleDetail(model, role);
        },
    ).show(_scaffoldKey.currentState);
  }
}
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/manager_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'broadcast_container.dart';
import 'manager_staff_container.dart';

typedef OnPushToDetail(dynamic params);

class AdminRootPage extends StatefulWidget {
  final OnPushToDetail onPush;
  final OnPushToDetail onPushBroadcast;

  AdminRootPage({@required this.onPush, @required this.onPushBroadcast});
  @override
  State<StatefulWidget> createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminRootPage> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool loadingManagers = false;
  bool loadingBroadcast = false;

  @override
  void initState() {
    super.initState();
  }

  _loadManagers(ManagerViewModel model) {
    if(model.managers == null || model.managers.length == 0) {
      setState(() {
        loadingManagers = true;
      });
    }
    model.loadManagers(null, _loadManagersSuccess, _loadManagersError);
  }

  _loadManagersSuccess(String message) {
    if(mounted) {
      setState(() {
        loadingManagers = false;
      });
    }
  }

  _loadManagersError(String message) {
    setState(() {
      loadingManagers = false;
    });
    Helper.showError(context, message);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: AppColors.grey4Color,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            title: Text(
              AppLang().admin,
              style: AppStyles.navTitle(),
            )
          ),
        ),
      ),
      body: LayoutBuilder(
        builder:  (context, constraints) => SafeArea(
          child: StoreConnector<AppState, ManagerViewModel> (
            onInitialBuild: (model){
              print('onInitialBuild');
              final canLoad = model.permissions.where((per) => per.role == BusinessRole.listManagers && per.isGranted).length > 0;
              if(canLoad) {
                _loadManagers(model);
              }
            },
            converter: (store) => ManagerViewModel.fromStore(store),
            builder: (_, viewModel) => content(viewModel)
          ),
        ),
      ),
    );
  }

  Widget content(ManagerViewModel viewModel) {
    return Stack(
      children: <Widget>[
        Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    //Content page here ...
                    ManagerStaffContainer(
                      loading: loadingManagers,
                      onAddClick: () {
                        if(widget.onPush != null) {
                          widget.onPush(null);
                        }
                      },
                      onEditManager: (manager) {
                        if(widget.onPush != null) {
                          widget.onPush({
                            'manager': manager,
                            'isAdd': false
                          });
                        }
                      },
                    ),
                    Container(height: 20,),
                    BroadcastContainer(
                      onAddClick: () {
                        if(widget.onPushBroadcast != null) {
                          widget.onPushBroadcast({'isNew': true});
                        }
                      },
                      onEditClick: (broad) {
                        if(widget.onPushBroadcast != null) {
                          widget.onPushBroadcast({'isNew': false, 'data': broad});
                        }
                      },
                    )
                  ],
                ),
              ),
            )
          ],
        ),
        //Add circle loading here

      ],
    );
  }
}
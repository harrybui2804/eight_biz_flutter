import 'package:eight_biz_flutter/screens/admin/broadcast_detail_page.dart';
import 'package:flutter/material.dart';

import 'add_manager_page.dart';
import 'admin_root_page.dart';

class NavigatorRoutes {
  static const String root = '/';
  static const String createOrUpdate = '/create_update';
  static const String broadcast = '/broadcast';
}

class AdminStackPage extends StatelessWidget {
  //AdminStackPage({this.navigatorKey});
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  void _push(BuildContext context, {dynamic params}) {
    var routeBuilders = _routeBuilders(context, params: params);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => routeBuilders[NavigatorRoutes.createOrUpdate](context),
      ),
    );
  }

  void _pushBroadcast(BuildContext context, {dynamic params}) {
    var routeBuilders = _routeBuilders(context, params: params);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => routeBuilders[NavigatorRoutes.broadcast](context),
      ),
    );
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, {params = const {}}) {
    return {
      NavigatorRoutes.root: (context) => AdminRootPage(
        onPush: (params) => _push(context, params: params),
        onPushBroadcast: (params) => _pushBroadcast(context, params: params),
      ),
      NavigatorRoutes.createOrUpdate: (context) => AddManagerPage(
        manager: params != null ? params['manager'] : null,
        isAdd: params != null && params['isAdd'] != null ? params['isAdd'] : true
      ),
      NavigatorRoutes.broadcast: (context) => BroadcastDetailPage(
        data: params != null ? params['data'] : null,
        isNew: params != null && params['isNew'] != null ? params['isNew'] : true
      ),
    };
  }

  @override
  Widget build(BuildContext context) {
    final routeBuilders = _routeBuilders(context);
    return Navigator(
      key: navigatorKey,
      initialRoute: NavigatorRoutes.root,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(
          builder: (context) => routeBuilders[routeSettings.name](context),
        );
      },
    );
  }
}
import 'package:eight_biz_flutter/components/custom_modal.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/broadcast.dart';
import 'package:eight_biz_flutter/redux/admin/admin_actions.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/broadcast_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'components/broadcast_row.dart';

typedef OnPushToDetail(dynamic params);
typedef void OnEditCallback(Broadcast broadcast);

class BroadcastContainer extends StatefulWidget {
  final OnVoidCallback onAddClick;
  final OnEditCallback onEditClick;

  BroadcastContainer({@required this.onAddClick, @required this.onEditClick});

  @override
  State<StatefulWidget> createState() => _BroadcastState();
}

class _BroadcastState extends State<BroadcastContainer> {
  final _scrollController = new ScrollController();
  final slidableController = SlidableController();
  bool loading = false;
  bool loadMore = false;

  _loadMoreList(BroadcastViewModel model) {
    if(model.nextBroadcastUrl != null && !loadMore)
    model.loadListBroadcast(model.nextBroadcastUrl, _loadListSuccess, _onError);
  }

  _loadListSuccess(String message) {
    if(mounted) {
      setState(() {
        loading = false;
        loadMore = false;
      });
    }
  }

  _onError(String message) {
    setState(() {
      loading = false;
      loadMore = false;
    });
    Helper.showError(context, message);
  }

  _onScroll(BroadcastViewModel model) {
    double maxScroll = _scrollController.position.maxScrollExtent;
    double currentScroll = _scrollController.position.pixels;
    double delta = 50;
    if ( maxScroll - currentScroll <= delta) { 
      //Load more 
      _loadMoreList(model);
    }
  }

  _deleteBroadcast(BroadcastViewModel model, Broadcast data) {
    if(!loading) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return CustomModal(
            title: AppLang().askDeleteBroadcastTitle,
            body: AppLang().askDeleteBroadcastBody,
            cancelText: AppLang().cancel.toUpperCase(),
            acceptText: AppLang().accept.toUpperCase(),
            onAccept: () {
              setState(() {
                loading = true;
              });
              model.deleteBroadcast(data.id, _deleteSuccess, _onError);
            },
          );
        }
      );
    }
  }

  _deleteSuccess(String _) {
    setState(() {
      loading = false;
    });
    Helper.showSuccess(context, AppLang().successDeleteBroadcast);
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.white,
      width: width,
      padding: EdgeInsets.all(20),
      child: LayoutBuilder(
        builder: (context, constraints)=> Container(
          child: Container(
            color: Colors.white,
            child: StoreConnector<AppState, BroadcastViewModel> (
              onInit: (store){
                final model = BroadcastViewModel.fromStore(store);
                _scrollController.addListener(() => _onScroll(model));
                final res = store.state.adminState.resBroadcasts;
                //if not load anywhere
                if(res == null || (res.results != null && res.results.length == 0)) {
                  store.dispatch(LoadListBroadcastAction(
                    businessId: store.state.homeState.business.id,
                    next: null,
                    onSuccess: _loadListSuccess,
                    onError: _onError
                  ));
                }
              },
              converter: (store) => BroadcastViewModel.fromStore(store),
              builder: (_, viewModel) => content(viewModel)
            )
          ),
        ),
      ),
    );
  }

  Widget content(BroadcastViewModel model) {
    final canCreate = model.permissions.where((per) => per.role == BusinessRole.createBroadcast && per.isGranted).toList().length > 0;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              AppLang().broadcastTitle,
              style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w600, fontSize: 20)
            ),
            canCreate ? InkWell(
              child: Icon(Icons.add_circle, color: AppColors.primaryOrange, size: 30),
              onTap: () {
                if(widget.onAddClick != null) {
                  widget.onAddClick();
                }
              },
            ) : Container()
          ],
        ),
        Container(
          margin: EdgeInsets.only(top: 20),
          child: Text(
            AppLang().broadcastDescription
          ),
        ),
        _buildList(model)
      ],
    );
  }

  Widget _buildList(BroadcastViewModel model) {
    final canShow = model.permissions.where((per) => per.role == BusinessRole.listBroadcast && per.isGranted).toList().length > 0;
    final canEdit = model.permissions.where((per) => per.role == BusinessRole.updateBroadcast && per.isGranted).toList().length > 0;
    final canDelete = model.permissions.where((per) => per.role == BusinessRole.deleteBroadcast && per.isGranted).toList().length > 0;
    if(!canShow) {
      return Container();
    }
    return Stack(
      children: <Widget>[
        ListView.builder(
          controller: _scrollController,
          itemCount:  model.broadcast.length,
          primary: false,
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int position) {
            final data = model.broadcast[position];
            return BroadcastRow(
              controller: slidableController,
              data: data,
              canEdit: canEdit,
              canDelete: canDelete,
              onPressed: () {
                slidableController.activeState?.close();
              },
              onEdit: () {
                if(widget.onEditClick != null) {
                  widget.onEditClick(data);
                }
              },
              onDelete: () {
                _deleteBroadcast(model, data);
              },
            );
          }
        ),
        loading ? SpinKitCircle(
          size: 50,
          color: AppColors.primaryOrange,
        ) : Container()
      ],
    );
  }

}
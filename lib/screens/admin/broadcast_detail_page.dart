import 'dart:io';

import 'package:eight_biz_flutter/components/complete_modal.dart';
import 'package:eight_biz_flutter/components/custom_modal.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/broadcast.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/broadcast_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class BroadcastDetailPage extends StatefulWidget {
  final Broadcast data;
  final bool isNew;

  BroadcastDetailPage({
    this.data,
    this.isNew
  });

  @override
  State<StatefulWidget> createState() => _BroadcastDetailState();
}

class _BroadcastDetailState extends State<BroadcastDetailPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _titleController = new TextEditingController();
  final _titleFocus = FocusNode();
  final TextEditingController _bodyController = new TextEditingController();
  final _bodyFocus = FocusNode();
  final TextEditingController _descController = new TextEditingController();
  final _descFocus = FocusNode();

  bool loading = false;
  bool isSend = true;
  
  @override
  void initState() {
    if(!widget.isNew && widget.data != null) {
      setState(() {
        isSend = widget.data.status;
      });
      _titleController.text = widget.data.title;
      _bodyController.text = widget.data.body;
      _descController.text = widget.data.description;
    }
    super.initState();
  }

  bool validForm() {
    if(!widget.isNew && widget.data != null && widget.data.status) {
      return false;
    }
    if(_titleController.text.trim().length == 0) {
      return false;
    }
    if(_bodyController.text.trim().length == 0) {
      return false;
    }
    return true;
  }

  _createBroadcast(BroadcastViewModel model) {
    if(!validForm() || loading) {
      return;
    }
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CustomModal(
          title: AppLang().askSaveBroadcastTitle,
          body: AppLang().askSaveBroadcastBody,
          cancelText: AppLang().cancel.toUpperCase(),
          acceptText: AppLang().accept.toUpperCase(),
          onAccept: () {
            setState(() {
              loading = true;
            });
            var title = _titleController.text.trim();
            var body = _bodyController.text.trim();
            var desc = _descController.text.trim();
            model.createBroadcast(title, body, desc, isSend, _onSaveSuccess, _onError);
          },
        );
      }
    );
  }

  _onSaveSuccess(String _) {
    setState(() {
      loading = false;
    });
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CompleteModal(
          title: AppLang().saveBroadcastSuccessTitle,
          body: AppLang().saveBroadcastSuccessBody,
          onClose: () {
            _pop();
          },
        );
      }
    );
  }

  _onError(String message) {
    setState(() {
      loading = false;
    });
    Helper.showError(context, message);
  }

  _updateBroadcast(BroadcastViewModel model) {
    if(!validForm() || loading) {
      return;
    }
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CustomModal(
          title: AppLang().askUpdateBroadcastTitle,
          body: AppLang().askUpdateBroadcastBody,
          cancelText: AppLang().cancel.toUpperCase(),
          acceptText: AppLang().accept.toUpperCase(),
          onAccept: () {
            setState(() {
              loading = true;
            });
            var title = _titleController.text.trim();
            var body = _bodyController.text.trim();
            var desc = _descController.text.trim();
            model.updateBroadcast(widget.data.id, title, body, desc, isSend, _onUpdateSuccess, _onError);
          },
        );
      }
    );
  }

  _onUpdateSuccess(String _) {
    setState(() {
      loading = false;
    });
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CompleteModal(
          title: AppLang().updateBroadcastSuccessTitle,
          body: AppLang().updateBroadcastSuccessBody,
          onClose: () {
            _pop();
          },
        );
      }
    );
  }

  _pop() {
    Navigator.pop(context);
  }
  
  @override
  void dispose() {
    _titleController.dispose();
    _bodyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            title: Text(
              AppLang().broadcastTitle,
              style: AppStyles.navTitle(),
            ),
            iconTheme: IconThemeData(color: AppColors.primaryOrange),
          ),
        ),
      ),
      body: SafeArea(
        child: LayoutBuilder(
          builder: (context, constraints) => Container(
            color: Colors.white,
            child: StoreConnector<AppState, BroadcastViewModel> (
              onInit: (store) {
                
              },
              converter: (store) => BroadcastViewModel.fromStore(store),
              builder: (_, viewModel) => content(viewModel)
            )
          ),
        ),
      ),
    );
  }

  Widget content(BroadcastViewModel model) {
    final canCreate = model.permissions.where((per) => per.role == BusinessRole.createBroadcast && per.isGranted).toList().length > 0;
    var canUpdate = model.permissions.where((per) => per.role == BusinessRole.updateBroadcast && per.isGranted).toList().length > 0;
    bool showButton = false;
    if(canCreate && widget.isNew) {
      showButton = true;
    } else if(canUpdate && !widget.isNew && widget.data != null) {
      showButton = true;
    }
    var canEdit = true;
    if(!widget.isNew && widget.data != null && widget.data.status) {
      canUpdate = false;
      canEdit = false;
    }
    return Stack(
      children: <Widget>[
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            child: Text(
                              '${AppLang().title} *',
                              style: AppStyles.normalTextCustom(color: Colors.black),
                            ),
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                vertical: 18.0, horizontal: 20.0
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(width: 1.0),
                              ),
                              errorText: _titleController.text.trim().length == 0
                                ? AppLang().titleRequired
                                : null
                            ),
                            controller: _titleController,
                            focusNode: _titleFocus,
                            enabled: canEdit,
                            style: AppStyles.normalTextCustom(
                              fontSize: 16,
                              fontWeight: FontWeight.w400
                            ),
                            autocorrect: false,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (_) {
                              _titleFocus.unfocus();
                              FocusScope.of(context).requestFocus(_bodyFocus);
                            },
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            child: Text(
                              '${AppLang().body} *',
                              style: AppStyles.normalTextCustom(color: Colors.black),
                            ),
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                vertical: 18.0, horizontal: 20.0
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(width: 1.0),
                              ),
                              errorText: _titleController.text.trim().length == 0
                                ? AppLang().bodyRequired
                                : null
                            ),
                            controller: _bodyController,
                            focusNode: _bodyFocus,
                            enabled: canEdit,
                            style: AppStyles.normalTextCustom(
                              fontSize: 16,
                              fontWeight: FontWeight.w400
                            ),
                            autocorrect: false,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (_) {
                              _bodyFocus.unfocus();
                              FocusScope.of(context).requestFocus(_descFocus);
                            },
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            child: Text(
                              AppLang().description,
                              style: AppStyles.normalTextCustom(color: Colors.black),
                            ),
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                              )
                            ),
                            controller: _descController,
                            focusNode: _descFocus,
                            enabled: canEdit,
                            onFieldSubmitted: (_) {
                              
                            },
                            autocorrect: true,
                            minLines: 5,
                            maxLines: 5,
                            textInputAction: TextInputAction.done,
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  AppLang().sendNow, 
                                  style: AppStyles.normalTextCustom(color: Colors.black),
                                ),
                                Platform.isIOS 
                                ? CupertinoSwitch(
                                    value: isSend,
                                    activeColor: AppColors.primaryOrange,
                                    onChanged: (send) {
                                      if(canEdit) {
                                        setState(() {
                                          isSend = send;
                                        });
                                      }
                                    },
                                  ) 
                                : Switch(
                                    value: isSend,
                                    onChanged: (send) {
                                      if(canEdit) {
                                        setState(() {
                                          isSend = send;
                                        });
                                      }
                                    },
                                  )
                              ],
                            ),
                          )
                        ],
                      )
                    ),
                  ),
                ),
                showButton ?
                Container(
                  padding: EdgeInsets.all(20),
                  child: RaisedButton(
                    onPressed: validForm() ? () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      if(widget.isNew) {
                        _createBroadcast(model);
                      } else {
                        _updateBroadcast(model);
                      }
                    } : null,
                    child: Container(
                      height: 45,
                      child: Center(
                        child: Text(
                          AppLang().broadcastTitle,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                    color: AppColors.primaryOrange,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6.0),
                    )
                  )
                ) : Container()
              ],
            ),
          ),
        ),
        loading ? SpinKitCircle(
          size: 50,
          color: AppColors.primaryOrange,
        ) : Container()
      ],
    );
  }
}
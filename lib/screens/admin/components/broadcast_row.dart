import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/broadcast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';

typedef void OnVoidCallback();

class BroadcastRow extends StatelessWidget {
  final SlidableController controller;
  final Broadcast data;
  final bool canEdit;
  final bool canDelete;
  final OnVoidCallback onPressed;
  final OnVoidCallback onEdit;
  final OnVoidCallback onDelete;

  BroadcastRow({
    this.controller,
    this.data,
    this.canEdit = false,
    this.canDelete = false,
    this.onPressed,
    this.onEdit,
    this.onDelete
  }) {
    assert(data != null);
  }

  bool notNull(Object o) => o != null;

  @override
  Widget build(BuildContext context) {
    return Slidable(
      controller: controller,
      actionPane: SlidableDrawerActionPane(),
      
      actionExtentRatio: 0.25,
      child: Container(
        color: Colors.white,
        child: InkWell(
          onTap: () {
            Slidable.of(context)?.close();
            if(onPressed != null) {
              onPressed();
            }
          },
          child: Container(
            padding: EdgeInsets.all(15),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      data?.title,
                      style: AppStyles.titleTextCustom(),
                    ),
                    data != null && data.status 
                    ? Container(
                      padding: EdgeInsets.only(
                        top: 5,
                        bottom: 5
                      ),
                      width: 60,
                      decoration: BoxDecoration(
                        color: Colors.green.withOpacity(0.2),
                        borderRadius:BorderRadius.all(Radius.circular(20.0)),
                      ),
                      child: Center(
                        child: Text(
                          //'DONE',
                          AppLang().done.toUpperCase(),
                          style: TextStyle(
                            color: Colors.green
                          ),
                        ),
                      ),
                    ) : Container(
                      padding: EdgeInsets.only(
                        top: 5,
                        bottom: 5
                      ),
                      width: 60,
                      decoration: BoxDecoration(
                        color: Colors.blue.withOpacity(0.2),
                        borderRadius:BorderRadius.all(Radius.circular(20.0)),
                      ),
                      child: Center(
                        child: Text(
                          //'DRAFT',
                          AppLang().draft.toUpperCase(),
                          style: TextStyle(
                            color: Colors.blue
                          ),
                        )
                      ),
                    )
                    
                  ],
                ),
                Container(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      data?.body,
                      style: AppStyles.normalTextCustom(),
                    ),
                    Text(
                      DateFormat('dd-MM-yyyy').format(DateTime.parse(data?.createdAt))
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      secondaryActions: <Widget>[
        canEdit && !data.status ? IconSlideAction(
          color: AppColors.grey2Color,
          iconWidget: Icon(Icons.edit, color: Colors.white),
          onTap: () {
            if(onEdit != null) {
              onEdit();
            }
          },
        ): null,
        canDelete ? IconSlideAction(
          color: Colors.red,
          icon: Icons.delete,
          onTap: () {
            if(onDelete != null) {
              onDelete();
            }
          },
        ): null,
      ].where(notNull).toList(),
    );
  }
}
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_manager.dart';
import 'package:flutter/material.dart';

typedef void OnVoidCallback();

class StaffRow extends StatelessWidget {
  final bool canEdit;
  final BizManager data;
  final OnVoidCallback onEdit;

  StaffRow({@required this.canEdit, @required this.data, @required this.onEdit});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  data.role ?? '',
                  style: AppStyles.smallTextCustom(color: Colors.black)
                ),
                Container(
                  height: 10,
                ),
                Text(
                  data.user != null 
                  ? data.user.firstName + ' ' + data.user.lastName 
                  : '',
                  style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w400, fontSize: 16),
                ),
              ],
            ),
          ),
          InkWell(
            child: Text(
              AppLang().edit,
              style: AppStyles.normalTextCustom(color: AppColors.primaryOrange, fontWeight: FontWeight.w500),
            ),
            onTap: () {
              if(onEdit != null) {
                onEdit();
              }
            },
          )
        ],
      ),
    );
  }
}
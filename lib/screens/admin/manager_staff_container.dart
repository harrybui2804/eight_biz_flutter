import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_manager.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/screens/admin/components/staff_row.dart';
import 'package:eight_biz_flutter/view_models/manager_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

typedef void OnVoidCallback();
typedef void OnEditCallback(BizManager manager);

class ManagerStaffContainer extends StatefulWidget {
  final bool loading;
  final OnVoidCallback onAddClick;
  final OnEditCallback onEditManager;
  
  ManagerStaffContainer({this.loading = false, @required this.onAddClick, @required this.onEditManager});

  @override
  State<StatefulWidget> createState() => _ManagerStaffState();
}

class _ManagerStaffState extends State<ManagerStaffContainer> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.white,
          width: width,
          padding: EdgeInsets.all(20),
          child: LayoutBuilder(
            builder: (context, constraints)=> Container(
              child: Container(
                color: Colors.white,
                child: StoreConnector<AppState, ManagerViewModel> (
                  onInit: (store){
                    
                  },
                  converter: (store) => ManagerViewModel.fromStore(store),
                  builder: (_, viewModel) => content(viewModel)
                )
              ),
            ),
          ),
        ),
        widget.loading ? SpinKitCircle(
          color: AppColors.primaryOrange,
          size: 50,
        ) : Container()
      ],
    );
  }

  Widget content(ManagerViewModel model) {
    final canAdd = model.permissions.where((per) => per.role == BusinessRole.addManager && per.isGranted).toList().length > 0;
    final canShow = model.permissions.where((per) => per.role == BusinessRole.listManagers && per.isGranted).toList().length > 0;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              AppLang().manageStaffTitle,
              style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w600, fontSize: 20)
            ),
            canAdd ? InkWell(
              child: Icon(Icons.add_circle, color: AppColors.primaryOrange, size: 30),
              onTap: () {
                if(widget.onAddClick != null) {
                  widget.onAddClick();
                }
              },
            ) : Container()
          ],
        ),
        (model.managers != null && model.managers.length > 0 && canShow)
        ? _buildList(model)
        : Container(
          padding: EdgeInsets.all(10),
          child: Center(
            child: Text(
              !canShow ? AppLang().noPermissionShowList : AppLang().noManagers,
              style: TextStyle(fontStyle: FontStyle.italic),
              textAlign: TextAlign.center,
            ),
          ),
        )
      ],
    );
  }

  Widget _buildList(ManagerViewModel model) {
    final canEdit = model.permissions.where((per) => per.role == BusinessRole.updateManager && per.isGranted).toList().length > 0;
    return ListView.builder(
      itemCount:  model.managers.length,
      primary: false,
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int position) {
        final manager = model.managers[position];
        return StaffRow(
          canEdit: canEdit,
          data: manager,
          onEdit: () {
            if(widget.onEditManager != null) {
              widget.onEditManager(manager);
            } 
          }
        );
      }
    );
  }
}
import 'package:flutter/material.dart';

class AuthBackground extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/onboarding@3x.png'), fit: BoxFit.fill
        )
      ),

    );
  }
}
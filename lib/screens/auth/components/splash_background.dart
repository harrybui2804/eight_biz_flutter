import 'package:eight_biz_flutter/config/images.dart';
import 'package:flutter/material.dart';

class SplashBackground extends StatelessWidget {
  final bool hasLogo;

  SplashBackground({this.hasLogo = true});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(AppImages.backgroundSplash), fit: BoxFit.fill
        )
      ),
      child: _buildLogo(context),
    );
  }

  Widget _buildLogo(BuildContext context) {
    if(hasLogo) {
      return Center(
        child: Image.asset(AppImages.logoAppWhite),
      );
    }
    return null;
  }
}
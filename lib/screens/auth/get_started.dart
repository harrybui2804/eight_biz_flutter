import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/images.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/utils/intl_phone_number_input/lib/intl_phone_number_input.dart';
import 'package:eight_biz_flutter/view_models/started_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';

class GetStarterdPage extends StatefulWidget {
  static const routeName = '/started';

  @override
  State<StatefulWidget> createState() => _GetStarterdState();
}

class _GetStarterdState extends State<GetStarterdPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isPhoneValid = false;
  String _phoneNumber = '';
  double latitude;
  double longtitude;

  @override
  void initState() {
    super.initState();
    //_phoneNode.addListener((){setState(() {});});
    try {
      Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
      geolocator.getLastKnownPosition(desiredAccuracy: LocationAccuracy.best).then((location) {
        print(location);
        if(location != null) {
          setState(() {
            latitude = location.latitude;
            longtitude = location.longitude;
          });
        }
      }).catchError((err) {
        print(err);
      });
    } catch (e) {
      debugPrint(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Stack(
              children: [
                Container(
                  width: width,
                  height: height,
                  child: Image.asset(
                    AppImages.backgroundAuth,
                    fit: BoxFit.fitWidth,
                  ),
                ),
                SafeArea(
                  child: SizedBox(
                    width: width,
                    height: height - 80.0, // minus height SafeArea
                    child: Column(
                      children: [
                        Expanded(
                          child: Stack(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(top: 30.0, bottom: 20.0),
                                child: OverflowBox(
                                  minWidth: 0.0,
                                  minHeight: 0.0,
                                  maxWidth: double.infinity,
                                  child: Column(
                                    children: <Widget>[
                                      Image.asset(AppImages.logoAppBlack, width: width / 3.5,)
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: -1.0,
                                child: Container(
                                  height: height / 10,
                                  width: width,
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          colors: [
                                            Colors.transparent,
                                            Colors.white.withOpacity(0.5),
                                            Colors.white,
                                          ])),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: AppColors.whiteColor,
                          padding: EdgeInsets.symmetric(vertical: 10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                                child: Text(AppLang().getStarted, style: AppStyles.titleTextCustom(),),
                              ),
                              //GetStartedForm()
                              LayoutBuilder(
                                builder: (context, constraints)=> Container(
                                  child: StoreConnector<AppState, GetStartedViewModel> (
                                    onInit: (store){  
                                      
                                    },
                                    converter: (store) => GetStartedViewModel.fromStore(store),
                                    builder: (_, viewModel) => content(viewModel)
                                  )
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                )
              ],
            ),
          )
        ],
      )
    );
  }

  Widget content(GetStartedViewModel viewModel) {
    final content = Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Form(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      AppLang().mobileNumber,
                      style: AppStyles.smallTextCustom(),
                    ),
                  ),
                  InternationalPhoneNumberInput.withCustomDecoration(
                    showFlag: false,
                    showDropDownIcon: false,
                    phoneCodeStyle: TextStyle(
                      color: AppColors.strongCyan,
                      fontSize: 16
                    ),
                    onInputChanged: onPhoneNumberChanged,
                    onInputValidated: onInputChanged,
                    onSubmitted: (value) {
                       _onGetStartedButtonPressed(viewModel);
                    },
                    initialCountry2LetterCode: 'AU',
                    inputAction: TextInputAction.done,
                    inputDecoration: InputDecoration(
                      hintText: AppLang().phoneNumber,
                      prefixIcon: SizedBox(width: 30.0,),
                      errorText: !isPhoneValid ? AppLang().phoneNumberNotValid : null,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6.0),
                        borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6.0),
                        borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6.0),
                        borderSide: BorderSide(width: 1.0, color: AppColors.primaryOrange),
                      ),
                    ),
                    formatInput: false,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 40.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 45.0,
                child: RaisedButton(
                  onPressed: () {
                    _onGetStartedButtonPressed(viewModel);
                  },
                  child: Text(
                    AppLang().getStarted,
                    style: TextStyle(color: Colors.white),
                  ),
                  color: AppColors.primaryOrange,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 6.0),
              child: Wrap(
                alignment: WrapAlignment.center,
                children: <Widget>[
                  Text(
                    AppLang().bySignin,
                    style: AppStyles.normalTextCustom(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 3.0, right: 3.0),
                    child: InkWell(
                      onTap: () {
                        Helper.makeLaunched(context, 'https://eightapp.com/term-and-condition.html');
                      },
                      child: Text(
                        AppLang().terms,
                        style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  Text(
                    AppLang().and,
                    style: AppStyles.normalTextCustom(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 3.0),
                    child: InkWell(
                      onTap: () {
                        Helper.makeLaunched(context, 'https://eightapp.com/privacy-policy.html');
                      },
                      child: Text(
                        AppLang().privacyPolicy,
                        style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );

    return Stack(
      children: <Widget>[
        viewModel.loading ? SpinKitCircle(
          color: AppColors.primaryOrange,
          size: 50
        ): Center(),
        content
      ],
    );
  }
 
  _onGetStartedButtonPressed(GetStartedViewModel viewModel) {
    FocusScope.of(context).requestFocus(new FocusNode());
    if(isPhoneValid && !viewModel.loading) {
      viewModel.checkExistPhone(_phoneNumber, latitude, longtitude, _showError);
    }
  }

  _showError(String message) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Colors.red,
        content: Text(message, style: TextStyle(color: Colors.white)),
      )
    );
  }

  void onPhoneNumberChanged(String phoneNumber) {
    print(phoneNumber);
    setState(() {
      _phoneNumber = phoneNumber;
    });
  }

  void onInputChanged(bool value) {
    setState(() {
      isPhoneValid = value;
    });
  }
}
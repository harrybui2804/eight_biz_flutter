import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/images.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/login_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:phone_number/phone_number.dart';

class LoginPage extends StatefulWidget {
  static const routeName = '/login';

  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _passController = new TextEditingController();
  bool _isValidPass = false;
  bool _isShowPassword = false;
  String _password = '';
  String _phoneNumber = '';
  String _countryCode = '';
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: width,
                    height: height,
                    child: Image.asset(
                      AppImages.backgroundAuth,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  SafeArea(
                    child: SizedBox(
                      width: width,
                      height: height - 80.0, // minus height SafeArea
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Stack(
                              children: <Widget>[
                                Positioned(
                                  bottom: -1.0,
                                  child: Container(
                                    height: height / 10,
                                    width: width,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter,
                                            colors: [
                                              Colors.transparent,
                                              Colors.white.withOpacity(0.5),
                                              Colors.white,
                                            ])),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 30.0, bottom: 20.0),
                                  child: OverflowBox(
                                    minWidth: 0.0,
                                    minHeight: 0.0,
                                    maxWidth: double.infinity,
                                    child: Column(
                                      children: <Widget>[
                                        Image.asset(AppImages.logoAppBlack, width: width / 3.5,)
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            color: AppColors.whiteColor,
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    IconButton(
                                      onPressed: () {
                                        Keys.navKey.currentState.pop();
                                      },
                                      color: Colors.black,
                                      icon: Icon(Icons.arrow_back_ios),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                                      child: Text(AppLang().login, style: AppStyles.titleTextCustom(),),
                                    )
                                  ],
                                ),
                                LayoutBuilder(
                                  builder: (context, constraints)=> Container(
                                    child: StoreConnector<AppState, LoginViewModel> (
                                      onInit: (store){  
                                        store.dispatch(ClearAuthSuccess());
                                        store.dispatch(ClearAuthError());
                                        _convertPhone(store.state.authState.phone);
                                      },

                                      onWillChange: (model) {
                                        _convertPhone(model.phone);
                                      },
                                      converter: (store) => LoginViewModel.fromStore(store),
                                      builder: (_, viewModel) => content(viewModel)
                                    )
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        )
      ),
      /*
      body: 
      */
    );
  }

  Widget content(LoginViewModel viewModel) {
    return Stack(
      children: <Widget>[
        viewModel.loading || isLoading ? SpinKitCircle(
          color: AppColors.primaryOrange,
          size: 50
        ): Center(),
        _buildForm(viewModel)
      ],
    );
  }

  Widget _buildForm(LoginViewModel viewModel) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Form(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      AppLang().mobileNumber,
                      style: AppStyles.smallTextCustom(),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(
                      vertical: 18.0, horizontal: 20.0
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.0),
                      border: Border.all(
                        width: 1.0, color: AppColors.strongCyan
                      )
                    ),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 10.0),
                          child: Text(
                            '+$_countryCode',
                            style: AppStyles.normalTextCustom(
                              color: AppColors.strongCyan,
                              fontSize: 16,
                              fontWeight: FontWeight.w400
                            ),
                          ),
                        ),
                        Text(
                          _phoneNumber,
                          style: AppStyles.normalTextCustom(
                            fontSize: 16,
                            fontWeight: FontWeight.w400
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.0),
                          child: Text(
                            AppLang().password,
                            style: AppStyles.smallTextCustom(),
                          ),
                        ),
                        Stack(
                          alignment: Alignment.centerRight,
                          children: <Widget>[
                            TextFormField(
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 18.0, horizontal: 20.0),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0),
                                  ),
                                  errorText: !_isValidPass
                                      ? AppLang().errPassLength
                                      : null),
                              controller: _passController,
                              style: AppStyles.normalTextCustom(
                                fontSize: 16,
                                fontWeight: FontWeight.w400
                              ),
                              onChanged: (value) {
                                setState(() {
                                  _password = value;
                                  _isValidPass = value.length >= 8;
                                });
                              },
                              obscureText: _isShowPassword ? false : true,
                              autocorrect: false,
                              textInputAction: TextInputAction.done,
                              onFieldSubmitted: _isValidPass && !viewModel.loading
                                  ? (_) {
                                      _submitLogin(viewModel);
                                    }
                                  : null,
                            ),
                            Positioned(
                              top: 20.0,
                              right: 15.0,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    _isShowPassword = !_isShowPassword;
                                  });
                                },
                                child: _isShowPassword
                                    ? Icon(
                                        Icons.visibility,
                                        size: 17.0,
                                      )
                                    : Icon(
                                        Icons.visibility_off,
                                        size: 17.0,
                                      ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        InkWell(
                          onTap: () => _onResetPassword(viewModel),
                          child: Text(
                            AppLang().resetPassword,
                            style: AppStyles.smallTextCustom(
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 45.0,
                      child: RaisedButton(
                        onPressed: () => _submitLogin(viewModel),
                        child: Text(
                          AppLang().login,
                          style: TextStyle(color: Colors.white),
                        ),
                        color: AppColors.primaryOrange,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _convertPhone(String phoneNumber) async {
    final dynamic result = await PhoneNumber.parse(phoneNumber);
    print(result);
    setState(() {
      _phoneNumber = result['national_number'];
      _countryCode = result['country_code'];
    });
  }

  _submitLogin(LoginViewModel viewModel) {
    FocusScope.of(context).requestFocus(new FocusNode());
    if(_isValidPass && !viewModel.loading) {
      viewModel.doLogin(viewModel.phone, _password, _showError, (String message, String email) => _onUnverifyEmail(viewModel, message, email));
    }
  }

  _onUnverifyEmail(LoginViewModel model, String message, String email) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Colors.red,
        content: Text(message, style: TextStyle(color: Colors.white)),
        action: SnackBarAction(
          label: AppLang().resend,
          textColor: Colors.white,
          onPressed: () {
            setState(() {
              isLoading = true;
            });
            model.resendVerifyEmail(
              email, 
              (String message) {
                setState(() {
                  isLoading = false;
                });
                Helper.showSuccess(_scaffoldKey, message);
              },
              (String message) {
                setState(() {
                  isLoading = false;
                });
                Helper.showError(_scaffoldKey, message);
              },
            );
          },
        ),
      )
    );
  }

  _onResetPassword(LoginViewModel viewModel) {
    viewModel.navigateToResetPass();
  }

  _showError(message) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Colors.red,
        content: Text(message, style: TextStyle(color: Colors.white)),
      )
    );
  }
}
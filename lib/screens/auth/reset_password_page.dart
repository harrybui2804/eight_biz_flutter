import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/images.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/view_models/reset_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:pin_input_text_field/pin_input_text_field.dart';

class ResetPasswordPage extends StatefulWidget {
  static const routeName = '/reset';

  @override
  State<StatefulWidget> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPasswordPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final TextEditingController _confirmCodeController = TextEditingController();
  final TextEditingController _newPasswordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();

  final FocusNode _newPasswordFocus = FocusNode();
  final FocusNode _confirmPasswordFocus = FocusNode();
  final FocusNode _confirmCodeFocus = FocusNode();

  String _newPassword = '';
  String _confirmPassword = '';
  String _confirmCode = '';
  bool _isShowPass = false;
  bool _isShowConfirm = false;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: LayoutBuilder(
          builder: (context, constraints)=> SafeArea(
            child: Container(
              color: Colors.white,
              child: StoreConnector<AppState, ResetPassViewModel> (
                onInit: (store){  
                  store.dispatch(ClearAuthSuccess());
                  store.dispatch(ClearAuthError());
                },
                converter: (store) => ResetPassViewModel.fromStore(store),
                builder: (_, viewModel) => Stack(
                  children: <Widget>[
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: width,
                            height: height,
                            child: Image.asset(
                              AppImages.backgroundAuth,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          SafeArea(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  width: width,
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        bottom: -1.0,
                                        child: Container(
                                          height: height / 10,
                                          width: width,
                                          decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                  begin: Alignment.topCenter,
                                                  end: Alignment.bottomCenter,
                                                  colors: [
                                                    Colors.transparent,
                                                    Colors.white.withOpacity(0.5),
                                                    Colors.white,
                                                  ])),
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              top: 30.0, bottom: 20.0),
                                          child: Image.asset(
                                            AppImages.logoAppBlack,
                                            width: width / 3.5,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  color: AppColors.whiteColor,
                                  padding: EdgeInsets.symmetric(vertical: 10.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          IconButton(
                                            onPressed: () {
                                              Keys.navKey.currentState.pop();
                                            },
                                            color: Colors.black,
                                            icon: Icon(Icons.arrow_back_ios),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
                                            child: Text(AppLang().resetPassword, style: AppStyles.titleTextCustom(),),
                                          )
                                        ],
                                      ),
                                      content(viewModel)
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ]
                )
              )
            ),
          ),
        ),
      ),
    );
  }

  Widget content(ResetPassViewModel viewModel) {
    return Stack(
      children: <Widget>[
        viewModel.loading ? SpinKitCircle(
          color: AppColors.primaryOrange,
          size: 50
        ): Center(),
        _buildForm(viewModel)
      ],
    );
  }

  Widget _buildForm(ResetPassViewModel viewModel) {
    return Container( 
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Form(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      AppLang().password,
                      style: AppStyles.smallTextCustom(),
                    ),
                  ),
                  Stack(
                    alignment: Alignment.centerRight,
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0),
                            ),
                            errorText: _newPassword.length < 8
                                ? AppLang().errPassLength
                                : null),
                        controller: _newPasswordController,
                        focusNode: _newPasswordFocus,
                        obscureText: !_isShowPass,
                        onChanged: (value) {
                          setState(() {
                            _newPassword = value;
                          });
                        },
                        autocorrect: false,
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (_) {
                          _fieldFocusChange(
                              context, _newPasswordFocus, _confirmPasswordFocus);
                        },
                      ),
                      Positioned(
                        top: 20.0,
                        right: 15.0,
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              _isShowPass = !_isShowPass;
                            });
                          },
                          child: _isShowPass
                            ? Icon(
                                Icons.visibility_off,
                                size: 17.0,
                              )
                            : Icon(
                                Icons.visibility,
                                size: 17.0,
                              ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      AppLang().confirmPassword,
                      style: AppStyles.smallTextCustom(),
                    ),
                  ),
                  Stack(
                    alignment: Alignment.centerRight,
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6.0),
                            borderSide: BorderSide(width: 1.0),
                          ),
                          errorText: (_confirmPassword.length == 0 || _confirmPassword != _newPassword)
                            ? AppLang().errPassMatch
                            : null,
                        ),
                        controller: _confirmPasswordController,
                        focusNode: _confirmPasswordFocus,
                        obscureText: !_isShowConfirm,
                        autocorrect: false,
                        textInputAction: TextInputAction.next,
                        onChanged: (value) {
                          setState(() {
                            _confirmPassword = value;
                          });
                        },
                        onFieldSubmitted: (_) {
                          _fieldFocusChange(context, _confirmPasswordFocus, _confirmCodeFocus);
                        },
                      ),
                      Positioned(
                        top: 20.0,
                        right: 15.0,
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              _isShowConfirm = !_isShowConfirm;
                            });
                          },
                          child: _isShowConfirm
                            ? Icon(
                                Icons.visibility_off,
                                size: 17.0,
                              )
                            : Icon(
                                Icons.visibility,
                                size: 17.0,
                              ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 30.0, bottom: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${AppLang().enter6VerificationCode} **** *** ${viewModel.phone.substring(viewModel.phone.length - 3)}.',
                    style: AppStyles.normalTextCustom(),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20.0),
                    height: 70.0,
                    child: PinInputTextField(
                      pinLength: 6,
                      decoration: BoxLooseDecoration(
                        strokeColor: AppColors.darkGrayish,
                        strokeWidth: 1.5,
                        textStyle: TextStyle(
                          color: AppColors.thirdColor,
                          fontSize: 40,
                        ),
                        enteredColor: AppColors.thirdColor,
                        obscureStyle: ObscureStyle(
                          isTextObscure: true,
                          obscureText: '•',
                        )
                      ),
                      focusNode: _confirmCodeFocus,
                      controller: _confirmCodeController,
                      textInputAction: TextInputAction.done,
                      onChanged: (pin) {
                        setState(() {
                          _confirmCode = pin;
                        });
                      },
                      onSubmit: (pin) {
                        print('submit pin:$pin');
                        _submitResetPassword(viewModel);
                      },
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.0, bottom: 10.0),
              child: Wrap(
                alignment: WrapAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 10.0),
                    child: Text(
                      AppLang().notReceiveAnything,
                      style: AppStyles.smallTextCustom(),
                    ),
                  ),
                  InkWell(
                    onTap: () => _resendCodeResetPassword(viewModel),
                    child: Text(
                      AppLang().resendCode,
                      style: AppStyles.smallTextCustom(
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 45.0,
                child: RaisedButton(
                  onPressed: () => _submitResetPassword(viewModel),
                  child: Text(
                    AppLang().resetPassword,
                    style: TextStyle(color: Colors.white),
                  ),
                  color: checkValid() ? AppColors.primaryOrange : Colors.grey,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      )
    );
  }

  _fieldFocusChange(BuildContext context, FocusNode currentNode, FocusNode nextNode) {
    currentNode.unfocus();
    FocusScope.of(context).requestFocus(nextNode);
  }

  _resendCodeResetPassword(ResetPassViewModel viewModel) {
    FocusScope.of(context).requestFocus(new FocusNode());
    if(!viewModel.loading) {
      viewModel.resendCodeReset(viewModel.registerId, _showSuccess, _showError);
    }
  }

  bool checkValid() {
    if(_newPassword.length < 8) {
      return false;
    }
    if(_confirmPassword != _newPassword) {
      return false;
    }
    if(_confirmCode.length < 6) {
      return false;
    }
    return true;
  }

  _submitResetPassword(ResetPassViewModel viewModel) {
    FocusScope.of(context).requestFocus(new FocusNode());
    if(checkValid() && !viewModel.loading) {
      viewModel.submitResetPass(viewModel.registerId, _newPassword, _confirmCode, _showSuccess, _showError);
    }
  }

  _showSuccess(message) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Colors.green,
        content: Text(message)
      )
    );
  }

  _showError(message) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Colors.red,
        content: Text(message, style: TextStyle(color: Colors.white)),
      )
    );
  }
}
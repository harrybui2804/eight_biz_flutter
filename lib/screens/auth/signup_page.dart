import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/images.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/signup.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/utils/string_utils.dart';
import 'package:eight_biz_flutter/view_models/signup_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SignUpPage extends StatefulWidget {
  static const routeName = '/signup';

  @override
  State<StatefulWidget> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _firstNameController = new TextEditingController();
  final FocusNode _firstNameFocus = FocusNode();
  final TextEditingController _lastNameController = new TextEditingController();
  final FocusNode _lastNameFocus = FocusNode();
  final TextEditingController _emailController = new TextEditingController();
  final FocusNode _emailFocus = FocusNode();
  final TextEditingController _passController = new TextEditingController();
  final FocusNode _passFocus = FocusNode();
  final TextEditingController _confirmController = new TextEditingController();
  final FocusNode _confirmFocus = FocusNode();
  final TextEditingController _referralController = new TextEditingController();
  final FocusNode _referralFocus = FocusNode();

  String _firstName = '';
  String _lastName = '';
  String _email = '';
  String _password = '';
  String _confirmPass = '';
  String _referralCode = '';
  bool _isShowPass = false;
  bool _isShowConfirm = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    var safePaddingTop = MediaQuery.of(context).padding.top;
    var safePaddingBottom = MediaQuery.of(context).padding.bottom;
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Container(
              width: width,
              height: height,
              child: Image.asset(
                AppImages.backgroundAuth,
                fit: BoxFit.fitWidth,
              ),
            ),
            SingleChildScrollView(
              child: SafeArea(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: height - safePaddingTop - safePaddingBottom
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Center(
                        child: Padding(
                          padding: EdgeInsets.only(top: 30.0),
                          child: Image.asset(
                            AppImages.logoAppBlack,
                            width: width / 3.5,
                          ),
                        ),
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            height: height / 10,
                            width: width,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Colors.transparent,
                                  Colors.white.withOpacity(0.1),
                                  Colors.white.withOpacity(0.5),
                                  Colors.white,
                                ])),
                          ),
                          buildForm()
                        ],
                      )
                    ]
                  ),
                )
              )
            ),
          ],
        )
      )
    );
  }

  Widget buildForm() {
    return Container(
      color: AppColors.whiteColor,
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              IconButton(
                onPressed: () {
                  Keys.navKey.currentState.pop();
                },
                color: Colors.black,
                icon: Icon(Icons.arrow_back_ios),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: 20.0, vertical: 20.0),
                child: Text(
                  AppLang().signUp,
                  style: AppStyles.titleTextCustom(),
                ),
              )
            ],
          ),
          LayoutBuilder(
            builder: (context, constraints)=> Container(
              child: StoreConnector<AppState, SignupViewModel> (
                onInit: (store){  
                  store.dispatch(ClearAuthSuccess());
                  store.dispatch(ClearAuthError());
                },
                converter: (store) => SignupViewModel.fromStore(store),
                builder: (_, viewModel) => content(viewModel)
              )
            ),
          )
        ],
      ),
    );
  }

  Widget content(SignupViewModel viewModel) {
    return Stack(
      children: [
        viewModel.loading ? SpinKitCircle(
          color: AppColors.primaryOrange,
          size: 50
        ): Center(),
        _buildForm(viewModel)
      ],
    );
  }

  Widget _buildForm(SignupViewModel viewModel) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Form(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      '${AppLang().firstName} *',
                      style: AppStyles.smallTextCustom(),
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                          borderSide: BorderSide(width: 1.0),
                        ),
                        errorText: _firstName.trim().length == 0
                            ? AppLang().firstNameRequired
                            : null),
                    controller: _firstNameController,
                    focusNode: _firstNameFocus,
                    onChanged: (value) {
                      setState(() {
                        _firstName = value.trim();
                      });
                    },
                    onFieldSubmitted: (_) {
                      _fieldFocusChange(context, _firstNameFocus, _lastNameFocus);
                    },
                    autocorrect: true,
                    textInputAction: TextInputAction.next,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      '${AppLang().lastName} *',
                      style: AppStyles.smallTextCustom(),
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                          borderSide: BorderSide(width: 1.0),
                        ),
                        errorText: _lastName.trim().length == 0
                            ? AppLang().lastNameRequired
                            : null),
                    controller: _lastNameController,
                    focusNode: _lastNameFocus,
                    onChanged: (value) {
                      setState(() {
                        _lastName = value.trim();
                      });
                    },
                    onFieldSubmitted: (_) {
                      _fieldFocusChange(context, _lastNameFocus, _emailFocus);
                    },
                    autocorrect: false,
                    textInputAction: TextInputAction.next,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      '${AppLang().email} *',
                      style: AppStyles.smallTextCustom(),
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                          borderSide: BorderSide(width: 1.0),
                        ),
                        errorText:
                        !validateEmail(_email) ? AppLang().emailNotValid : null),
                    keyboardType: TextInputType.emailAddress,
                    controller: _emailController,
                    focusNode: _emailFocus,
                    onChanged: (value) {
                      setState(() {
                        _email = value.trim();
                      });
                    },
                    onFieldSubmitted: (_) {
                      _fieldFocusChange(context, _emailFocus, _passFocus);
                    },
                    autocorrect: false,
                    textInputAction: TextInputAction.next,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      '${AppLang().password} *',
                      style: AppStyles.smallTextCustom(),
                    ),
                  ),
                  Stack(
                    alignment: Alignment.centerRight,
                    children: [
                      TextFormField(
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0),
                            ),
                            errorText: _password.length < 8
                                ? AppLang().errPassLength
                                : null),
                        controller: _passController,
                        focusNode: _passFocus,
                        onChanged: (value) {
                          setState(() {
                            _password = value;
                          });
                        },
                        onFieldSubmitted: (_) {
                          _fieldFocusChange(context, _passFocus, _confirmFocus);
                        },
                        autocorrect: false,
                        obscureText: !_isShowPass,
                        textInputAction: TextInputAction.next,
                      ),
                      Positioned(
                        top: 20.0,
                        right: 15.0,
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              _isShowPass = !_isShowPass;
                            });
                          },
                          child: _isShowPass
                            ? Icon(
                                Icons.visibility,
                                size: 17.0,
                              )
                            : Icon(
                                Icons.visibility_off,
                                size: 17.0,
                            ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      '${AppLang().confirmPassword} *',
                      style: AppStyles.smallTextCustom(),
                    ),
                  ),
                  Stack(
                    alignment: Alignment.centerRight,
                    children: [
                      TextFormField(
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0),
                            ),
                            errorText: _confirmPass != _password
                                ? AppLang().errPassMatch
                                : null),
                        controller: _confirmController,
                        focusNode: _confirmFocus,
                        onChanged: (value) {
                          setState(() {
                            _confirmPass = value;
                          });
                        },
                        onFieldSubmitted: (_) {
                          _fieldFocusChange(context, _confirmFocus, _referralFocus);
                        },
                        autocorrect: false,
                        obscureText: !_isShowConfirm,
                        textInputAction: TextInputAction.next,
                      ),
                      Positioned(
                        top: 20.0,
                        right: 15.0,
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              _isShowConfirm = !_isShowConfirm;
                            });
                          },
                          child: _isShowConfirm
                            ? Icon(
                              Icons.visibility,
                              size: 17.0,
                            )
                            : Icon(
                                Icons.visibility_off,
                                size: 17.0,
                            ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      '${AppLang().referralCode} (${AppLang().optional})',
                      style: AppStyles.smallTextCustom(),
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6.0),
                        borderSide: BorderSide(width: 1.0),
                      ),),
                    controller: _referralController,
                    focusNode: _referralFocus,
                    onChanged: (value){
                      setState(() {
                        _referralCode = value;
                      });
                    },
                    autocorrect: false,
                    textInputAction: TextInputAction.done,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 45.0,
                child: RaisedButton(
                  onPressed: () => _submitSignup(viewModel),
                  child: Text(
                    AppLang().signUp,
                    style: TextStyle(color: Colors.white),
                  ),
                  color: checkValid() ? AppColors.primaryOrange : Colors.grey,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                ),
              ),
            )
          ]
        )
      )
    );
  }

  _fieldFocusChange(BuildContext context, FocusNode currentNode, FocusNode nextNode) {
    currentNode.unfocus();
    FocusScope.of(context).requestFocus(nextNode);
  }

  _submitSignup(SignupViewModel viewModel) {
    if(checkValid() && !viewModel.loading) {
      var model = ReqSignUp(
        registerId: viewModel.registerId,
        firstName: _firstName,
        lastName: _lastName,
        phone: viewModel.phone,
        email: _email,
        password: _password,
        confirmPassword: _confirmPass,
        referralCode: _referralCode,
        publicVerifyKey: viewModel.publicVerifyKey,
        privateVerifyKey: viewModel.privateVerifyKey
      );
      viewModel.submitSignup(model, _showSuccess, _showError);
    }
  }

  _showSuccess(message) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Colors.green,
        content: Text(message)
      )
    );
  }

  _showError(message) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Colors.red,
        content: Text(message, style: TextStyle(color: Colors.white)),
      )
    );
  }

  bool checkValid() {
    if(_firstName.trim().length == 0) {
      return false;
    }
    if(_lastName.trim().length == 0) {
      return false;
    }
    if(!validateEmail(_email)) {
      return false;
    }
    if(_password.length < 8) {
      return false;
    }
    if(_confirmPass != _password) {
      return false;
    }
    return true;
  }


  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _emailController.dispose();
    _passController.dispose();
    _confirmController.dispose();
    _referralController.dispose();
    super.dispose();
  }
}
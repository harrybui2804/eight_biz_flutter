import 'package:eight_biz_flutter/components/stateful_wrapper.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';
import 'package:eight_biz_flutter/screens/auth/components/splash_background.dart';
import 'package:eight_biz_flutter/view_models/splash_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class SplashPage extends StatefulWidget {
  static const routeName = '/';

  @override
  State<StatefulWidget> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, SplashViewModel> (
        onInit: (store) {  
          store.dispatch(GetStoreLogin());
        },
        converter: (store) => SplashViewModel.fromStore(store),
        builder: (_, viewModel) => content(viewModel)
      )
    );
  }

  Widget content(SplashViewModel model) {
    return StatefulWrapper(
      onInit: () {
        
      },
      child: Stack(
        children: <Widget>[
          SplashBackground(),
          Center(
            child: CircularProgressIndicator()
          )
        ],
      ),
    );
  }
}
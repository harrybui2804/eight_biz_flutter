import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/images.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/view_models/verify_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:pin_input_text_field/pin_input_text_field.dart';

class VerifyPage extends StatefulWidget {
  static const routeName = '/verify';

  @override
  State<StatefulWidget> createState() => _VerifyPageState();
}

class _VerifyPageState extends State<VerifyPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _verifyCodeEditingController = TextEditingController();

  bool _isCompleteCode = false;
  String _code = '';

  @override
  void initState() {
    _verifyCodeEditingController.addListener(_onVerifyCodeChanged);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Container(
                  width: width,
                  height: height,
                  child: Image.asset(
                    AppImages.backgroundAuth,
                    fit: BoxFit.fitWidth,
                  ),
                ),
                SafeArea(
                  child: SizedBox(
                    width: width,
                    height: height - 80.0, // minus height SafeArea
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: Stack(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(
                                    top: 30.0, bottom: 20.0),
                                child: OverflowBox(
                                  minWidth: 0.0,
                                  minHeight: 0.0,
                                  maxWidth: double.infinity,
                                  child: Column(
                                    children: <Widget>[
                                      Image.asset(
                                        AppImages.logoAppBlack,
                                        width: width / 3.5,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: -1.0,
                                child: Container(
                                  height: height / 10,
                                  width: width,
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          colors: [
                                        Colors.transparent,
                                        Colors.white.withOpacity(0.5),
                                        Colors.white,
                                      ])),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: AppColors.whiteColor,
                          padding: EdgeInsets.symmetric(vertical: 10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  IconButton(
                                    onPressed: () {
                                      Keys.navKey.currentState.pop();
                                    },
                                    color: Colors.black,
                                    icon: Icon(Icons.arrow_back_ios),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 20.0, vertical: 20.0),
                                    child: Text(
                                      AppLang().verifyAccount,
                                      style: AppStyles.titleTextCustom(),
                                    ),
                                  )
                                ],
                              ),
                              LayoutBuilder(
                                builder: (context, constraints) => Container(
                                  color: Colors.white,
                                  child: StoreConnector<AppState, VerifyViewModel> (
                                    distinct: true,
                                    converter: (store) => VerifyViewModel.fromStore(store),
                                    builder: (_, viewModel) => _buildContent(viewModel)
                                  )
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      )
    );
  }

  Widget _buildContent(VerifyViewModel viewModel) {
    return Stack(
      children: <Widget>[
        viewModel.loading ? SpinKitCircle(
          color: AppColors.primaryOrange,
          size: 50
        ): Center(),
        GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: Container(
          padding: EdgeInsets.all(20.0),
            child: Form(
              child: Column(
                children: <Widget>[
                  Text(
                    '${AppLang().enter4VerificationCode} **** *** ${viewModel.phone.substring(viewModel.phone.length - 3)}.',
                    style: AppStyles.normalTextCustom(),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    margin: EdgeInsets.only(top: 30),
                    height: 100.0,
                    child: PinInputTextField(
                      pinLength: 4,
                      decoration: BoxLooseDecoration(
                        strokeColor: AppColors.darkGrayish,
                        strokeWidth: 1.5,
                        textStyle: TextStyle(
                          color: AppColors.thirdColor,
                          fontSize: 55,
                        ),
                        enteredColor: AppColors.thirdColor,
                        obscureStyle: ObscureStyle(
                          isTextObscure: true,
                          obscureText: '•',
                        ),
                      ),
                      controller: _verifyCodeEditingController,
                      autoFocus: true,
                      onChanged: (value) {
                        setState(() {
                          _code = value;
                          _isCompleteCode = value.length == 4;
                        });
                      },
                      textInputAction: TextInputAction.done,
                      onSubmit: (pin) {
                        print('submit pin:$pin');
                        _submitVerifyCode(viewModel);
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 10.0),
                          child: Text(
                            AppLang().notReceiveAnything,
                            style: AppStyles.smallTextCustom(),
                          ),
                        ),
                        InkWell(
                          onTap: () => _resendVerifyCode(viewModel),
                          child: Text(
                            AppLang().resendCode,
                            style: AppStyles.smallTextCustom(
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 45.0,
                      child: RaisedButton(
                        onPressed:() => _submitVerifyCode(viewModel),
                        child: Text(
                          AppLang().submit,
                          style: TextStyle(color: Colors.white),
                        ),
                        color: AppColors.primaryOrange,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  void _onVerifyCodeChanged() {
    
  }

  _submitVerifyCode(VerifyViewModel viewModel) {
    if(_isCompleteCode && !viewModel.loading) {
      viewModel.submitPhoneCode(viewModel.registerId, _code, viewModel.isExist, _showSuccess, _showError);
    }
  }

  _showSuccess(message) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Colors.green,
        content: Text(message)
      )
    );
  }

  _showError(message) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Colors.red,
        content: Text(message, style: TextStyle(color: Colors.white)),
      )
    );
  }

  _resendVerifyCode(VerifyViewModel viewModel) {
    if(!viewModel.loading) {
      viewModel.resendVerifyCode(viewModel.phone, viewModel.registerId, _showSuccess, _showError);
    }
  }
}
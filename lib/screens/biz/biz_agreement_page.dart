import 'dart:io';

import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/create_biz_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:path_provider/path_provider.dart';
import 'package:signature/signature.dart';

import 'list_biz_page.dart';

class BizAgreementPage extends StatefulWidget {
  static const routeName = '/agreement';
  final String businessId;
  
  BizAgreementPage(this.businessId);

  @override
  State<StatefulWidget> createState() => _BizAgreementState();
}

class _BizAgreementState extends State<BizAgreementPage> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool loading = true;
  String _agreement;
  
  List<Point> points;
  Signature _signatureCanvas;

  initState() {
    super.initState();
  }

  _saveSignature(CreateBizViewModel model) async {
    if(_signatureCanvas.key.currentState == null || _signatureCanvas.key.currentState.isEmpty()) {
      Helper.showError(_scaffoldKey, AppLang().emptySignature);
      return;
    }
    setState(() {
      loading = true;
    });
    final bytes = await _signatureCanvas.exportBytes();
    Directory appDocDirectory = await getApplicationDocumentsDirectory();
    var imageFile = File('${appDocDirectory.path}/my_signature.jpg');
    await imageFile.writeAsBytes(bytes);
    model.saveSignature(widget.businessId, imageFile, (message) {
      _saveSignatureSuccess(model, message);
    }, _showError);
  }

  _saveSignatureSuccess(CreateBizViewModel model, String message) {
    setState(() {
      loading = false;
    });
    _signatureCanvas.clear();
    model.changeSignature(null, true);
    model.popToListBiz();
    model.clearEditingBiz();
  }

  _showError(String error) {
    setState(() {
      loading = false;
    });
    Helper.showError(_scaffoldKey, error);
  }

  _loadAgreementSuccess(String agreement) {
    setState(() {
      _agreement = agreement;
      loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            title: Text(
              AppLang().agreement,
              style: AppStyles.navTitle(),
            ),
            leading: InkWell(
              onTap: () => Keys.navKey.currentState.pushNamedAndRemoveUntil(
                ListBusinessPage.routeName, 
                (Route<dynamic> route) => false, 
                arguments: ListBizPageParams() 
              ),
              child: Icon(
                Icons.chevron_left, 
                color: AppColors.primaryOrange,
                size: 35,
              ),
            ),
          ),
        ),
      ),
      body: LayoutBuilder(
        builder: (context, constraints)=> SafeArea(
          child: Container(
            color: Colors.white,
            child: StoreConnector<AppState, CreateBizViewModel> (
              onInit: (store){
                final model = CreateBizViewModel.fromStore(store);
                Future.delayed(const Duration(milliseconds: 5), () {
                  double width = MediaQuery.of(context).size.width;
                  setState(() {
                    _signatureCanvas = Signature(
                      height: 250,
                      width: width - 40,
                      //points: model.signature ?? [],
                      backgroundColor: Colors.black12,
                      onChanged: (_points) {
                        //model.changeSignature(_points, false);
                        setState(() {
                          points = _points;
                        });
                      },
                    );
                    loading = true;
                  });
                  model.loadAgreement(widget.businessId, _loadAgreementSuccess, _showError);
                });
              },
              converter: (store) => CreateBizViewModel.fromStore(store),
              builder: (_, viewModel) => content(viewModel)
            )
          ),
        ),
      ),
    );
  }

  Widget content(CreateBizViewModel model) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
            padding: EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                _agreement != null
                ? Html(
                  data: _agreement ?? ''
                ): Container(),
                Column(
                  children: <Widget>[
                    GestureDetector(
                      onVerticalDragUpdate: (_) {},
                      onHorizontalDragUpdate: (_) {},
                      child: Container(
                        //width: 300,
                        height: 300,
                        child: Padding(
                          padding: const EdgeInsets.all(0),
                          child: _signatureCanvas ?? Container()
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        if(_signatureCanvas != null && _signatureCanvas.key.currentState != null) {
                          _signatureCanvas.clear();
                        }
                        setState(() {
                          points = [];
                        });
                      },
                      child: Center(
                        child: Text(
                          AppLang().clear,
                          style: TextStyle(
                            color: AppColors.primaryOrange,
                            fontSize: 15,
                            fontWeight: FontWeight.w400
                          ),
                        ),
                      )
                    ),
                    Container(height: 20),
                    RaisedButton(
                      onPressed: points != null && points.length > 0
                      ? () => _saveSignature(model) : null,
                      color: AppColors.primaryOrange,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(6))),
                      child: Center(
                        child: Text(
                          AppLang().save,
                          style: AppStyles.normalTextCustom(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            //fontSize: 20
                          ),
                        )
                      )
                    ),
                    Container(height: 20)
                  ],
                )
              ],
            ),
          ),
        loading ? 
        Positioned( 
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.black.withOpacity(0.05),
            alignment: Alignment.center,
            child: SpinKitCircle(
              size: 50,
              color: AppColors.primaryOrange,
            ),
          ),
        ) : Container()
      ],
    );
  }
}
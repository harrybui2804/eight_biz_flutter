import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:flutter/material.dart';

class ModalAskCreateUpdate extends StatefulWidget {
  final bool isCreate;
  final VoidCallback onAccept;

  ModalAskCreateUpdate({
    this.isCreate = true,
    this.onAccept
  });

  @override
  State<StatefulWidget> createState() => _ModalAskCreateUpdate();
}

class _ModalAskCreateUpdate extends State<ModalAskCreateUpdate> {

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width - 40;
    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: Container(
        width: width,
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Center(
                child: Text(
                  widget.isCreate
                  ? AppLang().askCreateBizTitle 
                  : AppLang().askUpdateBizTitle,
                  style: AppStyles.titleTextCustom(),
                ),
              ),
            ),
            Container(
              child: Center(
                child: Text(
                  widget.isCreate
                  ? AppLang().askCreateBizBody
                  : AppLang().askUpdateBizBody
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: 10,
                bottom: 10,
                left: 20,
                right: 20
              ),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                    color: Colors.grey,
                    elevation: 0,
                    highlightElevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                      side: BorderSide.none
                    ),
                    child: Center(
                      child: Text(
                        AppLang().cancel,
                        style: TextStyle(
                          color: AppColors.whiteColor,
                          fontSize: 15,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    ),
                  ),
                  Container(width: 10),
                  RaisedButton(
                    onPressed: (){
                      Navigator.of(context).pop();
                      if(widget.onAccept != null) {
                        widget.onAccept();
                      }
                    },
                    color: AppColors.primaryOrange,
                    highlightColor: AppColors.primaryOrange,
                    elevation: 0,
                    highlightElevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                    ),
                    child: Center(
                      child: Text(
                        AppLang().accept,
                        style: TextStyle(
                          color: AppColors.whiteColor,
                          fontSize: 15,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
            ,
          ],
        ),
      )
    );
  }
}
import 'dart:io';

import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/media.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:responsive_grid/responsive_grid.dart';

import 'biz_galery_item.dart';

typedef void OnSelectedMedia(File media);
typedef void OnImageCallback(Media image);

class GalleryBizSection extends StatefulWidget {
  final bool isLoadingMedias;
  final List<Media> medias;
  final bool canUpdateBiz;
  final bool canUpload;
  final bool canDelete;

  final OnSelectedMedia onSelectedFile;
  final OnImageCallback onDeleteImage;
  final OnImageCallback onChangeCover;
  
  GalleryBizSection({
    this.isLoadingMedias = false,
    this.medias = const [],
    @required this.onSelectedFile,
    @required this.onDeleteImage,
    @required this.onChangeCover,
    @required this.canUpdateBiz,
    @required this.canUpload,
    @required this.canDelete
  }){
    assert(onSelectedFile != null);
    assert(onDeleteImage != null);
    assert(onChangeCover != null);
  }

  @override
  _GalleryBizSectionState createState() => _GalleryBizSectionState();
}

class _GalleryBizSectionState extends State<GalleryBizSection> {

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              AppLang().imageGallery,
              style: AppStyles.titleTextCustom(),
              textAlign: TextAlign.left,
            ),
            widget.canUpload ? InkWell(
              onTap: _openGalleryDevice,
              child: Text(
                AppLang().add,
                style:
                    AppStyles.normalTextCustom(color: AppColors.primaryOrange),
                textAlign: TextAlign.left,
              ),
            ) : Container()
          ],
        ),
        _buildContent()
      ],
    );
  }

  Widget _buildContent() {
    if(widget.isLoadingMedias) {
      //loading
      return Center(
        child: SpinKitCircle(
          color: AppColors.primaryOrange,
          size: 50,
        )
      );
    }
    if(widget.medias == null || widget.medias.isEmpty) {
      return Center(
        child: Text(
          AppLang().emptyMedia
        )
      );
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
          child: Row(
            children: <Widget>[
              Text(
                AppLang().coverImage,
                style: AppStyles.smallTextCustom(),
              ),
              Container(
                padding: EdgeInsets.all(3.0),
                margin: EdgeInsets.only(left: 10.0),
                decoration: BoxDecoration(
                    color: AppColors.thirdColor,
                    borderRadius:
                      BorderRadius.all(Radius.circular(30.0))),
                child: Icon(
                  Icons.check,
                  color: AppColors.whiteColor,
                  size: 14.0,
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 4.0, bottom: 5.0),
          child: Text(
            AppLang().makeCoverDesc,
            style: TextStyle(
                fontStyle: FontStyle.italic,
                fontSize: 10.0,
                color: AppColors.secondaryColor),
          ),
        ),
        ResponsiveGridRow(
          children: widget.medias.map((media) {
            return ResponsiveGridCol(
              lg: 4,
              xl: 4,
              md: 4,
              xs: 4,
              child: BizGaleryItem(
                data: media,
                canUpdate: widget.canUpdateBiz,
                canDelete: widget.canDelete,
                onChangeCover: (_) {
                  widget.onChangeCover(media);
                },
                onDeleteImage: (_) {
                  widget.onDeleteImage(media);
                }
              ),
            );
          }).toList(),
        )
      ],
    );
  }

  _openGalleryDevice() async {
    final File file = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (file != null) {
      widget.onSelectedFile(file);
    }
  }
}
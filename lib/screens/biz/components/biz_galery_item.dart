import 'package:eight_biz_flutter/components/custom_modal.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/media.dart';
import 'package:flutter/material.dart';

typedef void OnImageCallback(Media image);

class BizGaleryItem extends StatelessWidget {
  final Media data;
  final bool canUpdate;
  final bool canDelete;
  final OnImageCallback onChangeCover;
  final OnImageCallback onDeleteImage;

  BizGaleryItem({
    this.data,
    this.canUpdate = true,
    this.canDelete = true,
    this.onChangeCover,
    this.onDeleteImage
  }){
    assert(data != null);
  }

  bool notNull(Object o) => o != null;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width / 4,
          height: MediaQuery.of(context).size.width / 4,
          margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
          child: InkWell(
            onLongPress: () {
              if(canUpdate) {
                _optionModalBottomSheet(context, data.isCover);
              }
            },
            child: Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                  child: Container(
                    width: double.infinity,
                    height: double.infinity,
                    decoration: BoxDecoration(
                      border: Border.all(color: AppColors.grey3Color),
                      color: AppColors.grey3Color,
                    ),
                    child: data.isLocal 
                      ? Image.file(data.localFile, fit: BoxFit.cover,)
                      : Image.network(
                        data.url,
                        fit: BoxFit.cover,
                      ),
                  ),
                ),
                data.isCover
                    ? ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                        child: Container(
                          width: double.infinity,
                          height: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.6),
                          ),
                        ),
                      )
                    : null,
                data.isCover
                    ? Positioned(
                        bottom: 5.0,
                        right: 5.0,
                        child: Container(
                          padding: EdgeInsets.all(3.0),
                          decoration: BoxDecoration(
                              color: AppColors.thirdColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0))),
                          child: Icon(
                            Icons.check,
                            color: AppColors.whiteColor,
                            size: 14.0,
                          ),
                        ),
                      )
                    : null,
              ].where(notNull).toList(),
            ),
          ),
        ),
        canDelete ? InkWell(
          onTap: () => _removeImage(context),
          child: Text(
            AppLang().remove,
            style: AppStyles.smallTextCustom(
                color: AppColors.primaryOrange, fontWeight: FontWeight.w500),
          ),
        ) : Container()
      ],
    );
  }

  _removeImage(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) {
        return CustomModal(
          title: AppLang().askRemoveImageTitle,
          body: AppLang().askRemoveImageBody,
          acceptText: AppLang().accept,
          cancelText: AppLang().cancel,
          acceptButtonColor: true,
          cancelButtonColor: true,
          onAccept: () {
            if(onDeleteImage != null) {
              onDeleteImage(data);
            }
          },
        );
      }
    );
    
  }

  void _optionModalBottomSheet(context, bool isCover) {
    if(!isCover) {
      showModalBottomSheet(
        context: context,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
        builder: (BuildContext bc) {
          return Container(
            padding: EdgeInsets.symmetric(vertical: 20.0),
            child: Wrap(
              children: <Widget>[
                !isCover
                    ? ListTile(
                        leading: new Icon(Icons.add_circle),
                        title: new Text(AppLang().setCover),
                        onTap: () async {
                          Navigator.pop(context);
                          if(onChangeCover != null) {
                            onChangeCover(data);
                          }
                        },
                      )
                    : null,
                isCover
                    ? ListTile(
                        leading: new Icon(Icons.remove_circle),
                        title: new Text(AppLang().removeCover),
                        onTap: () async {
                          Navigator.pop(context);
                          if(onChangeCover != null) {
                            onChangeCover(data);
                          }
                        },
                      )
                    : null,
              ].where(notNull).toList(),
            ),
          );
        },
      );
    }
    
  }
}
import 'dart:io';

import 'package:eight_biz_flutter/components/custom_modal.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:transparent_image/transparent_image.dart';

typedef void OnSelectedLogo(File logo);
typedef void OnVoidCallback();

class LogoProfileBizSection extends StatefulWidget {
  final BusinessDetail business;
  final bool canUpdate;
  final File logoFile;
  final OnSelectedLogo onSelectedFile;
  final OnVoidCallback onDeleteLogo;

  LogoProfileBizSection({
    @required this.business,
    @required this.onSelectedFile,
    @required this.onDeleteLogo,
    @required this.canUpdate,
    this.logoFile,
  }) {
    assert(onSelectedFile != null);
    assert(onDeleteLogo != null);
  }

  @override
  _LogoProfileBizSectionState createState() => _LogoProfileBizSectionState();
}

class _LogoProfileBizSectionState extends State<LogoProfileBizSection> {

  @override
  Widget build(BuildContext context) {
    bool notNull(Object o) => o != null;
    final biz = widget.business ?? BusinessDetail();
    final localLogo = widget.logoFile;
    final showUpdate = localLogo != null || (biz.logoImage == null || biz.logoImage.isNotEmpty);
    final needAdd = localLogo == null && (biz.logoImage == null || biz.logoImage.isEmpty);
    final canUpdate = widget.canUpdate;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              AppLang().bizLogoProfile,
              style: AppStyles.titleTextCustom(),
              textAlign: TextAlign.left,
            ),
            showUpdate && canUpdate
              ? InkWell(
                  onTap: _openGalleryDevice,
                  child: Text(
                    AppLang().update,
                    style: AppStyles.normalTextCustom(
                        color: AppColors.primaryOrange),
                    textAlign: TextAlign.left,
                  ),
                )
              : Container(),
          ],
        ),
        Column(
          children: [
            // upload icon if null
            needAdd && canUpdate
              ? Container(
                  width: MediaQuery.of(context).size.width / 4,
                  height: MediaQuery.of(context).size.width / 4,
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  child: InkWell(
                    onTap: _openGalleryDevice,
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.all(Radius.circular(4.0)),
                          color: AppColors.grey3Color),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.add,
                            color: AppColors.primaryOrange,
                            size: 30.0,
                          )
                        ],
                      ),
                    ),
                  ),
                )
              : null,
            // show local logo
            localLogo != null 
            ? Container(
                width: MediaQuery.of(context).size.width / 4,
                height: MediaQuery.of(context).size.width / 4,
                margin: EdgeInsets.only(top: 10.0, bottom: 3.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                  child: Container(
                    width: double.infinity,
                    height: double.infinity,
                    decoration: BoxDecoration(
                      border: Border.all(color: AppColors.grey3Color),
                      color: AppColors.grey3Color,
                    ),
                    child: Image.file(localLogo, fit: BoxFit.cover),
                  ),
                ),
              ) 
              : (biz.logoImage != null && biz.logoImage.isNotEmpty) //show online logo
                ? Container(
                    width: MediaQuery.of(context).size.width / 4,
                    height: MediaQuery.of(context).size.width / 4,
                    margin: EdgeInsets.only(top: 10.0, bottom: 3.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                      child: Container(
                        width: double.infinity,
                        height: double.infinity,
                        decoration: BoxDecoration(
                          border: Border.all(color: AppColors.grey3Color),
                          color: AppColors.grey3Color,
                        ),
                        child: FadeInImage.memoryNetwork(
                          image: biz.logoImage,
                          placeholder: kTransparentImage,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  )
                : null,
            !needAdd && canUpdate
            ? InkWell(
                onTap: _removeLogo,
                child: Text(
                  AppLang().remove,
                  style: AppStyles.smallTextCustom(color: AppColors.primaryOrange, fontWeight: FontWeight.w500),
                ),
              )
            : null,
          ].where(notNull).toList(),
        )
      ],
    );
  }

  _openGalleryDevice() async {
    final File file = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (file != null && widget.onSelectedFile != null) {
      widget.onSelectedFile(file);
    }
  }

  _removeLogo() {
    showDialog(
      context: context,
      builder: (_) {
        return CustomModal(
          title: AppLang().askRemoveLogoTitle,
          body: AppLang().askRemoveLogoBody,
          acceptButtonColor: true,
          cancelButtonColor: true,
          acceptText: AppLang().accept.toUpperCase(),
          cancelText: AppLang().cancel.toUpperCase(),
          onAccept: () {
            widget.onDeleteLogo();
          },
        );
      }
    );
  }
}
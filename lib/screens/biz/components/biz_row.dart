import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/business_list.dart';
import 'package:flutter/material.dart';

typedef void SelectedCallback(BusinessList message);

class BusinessRow extends StatelessWidget {
  final BusinessList business;
  final bool isSelected;
  final SelectedCallback onSelected;
  final double top;
  final double bottom;

  BusinessRow({@required this.business, this.isSelected = false, this.onSelected, this.top = 0, this.bottom = 0 }) {
    assert(business != null);
  }

  @override
  Widget build(BuildContext context) {

    final _itemColor = isSelected ? Colors.black : Colors.grey;

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if(onSelected != null) {
          onSelected(business);
        }
      },
      child: Container(
        margin: EdgeInsets.only(top: top, bottom: bottom),
        decoration: BoxDecoration(
          border: Border.all(color: _itemColor),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8),
            bottomLeft: Radius.circular(8),
            bottomRight: Radius.circular(8)
          )
        ),
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.centerRight,
                margin: EdgeInsets.only(bottom: 5),
                child: Container(
                  padding: EdgeInsets.all(3),
                  color: _itemColor,
                  child: Text(
                    (business.isOwner != null && business.isOwner) ? AppLang().owner : AppLang().manager,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                )
              ),
              Container(
                padding: EdgeInsets.only(left: 10, bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.check_circle, color: _itemColor,),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    business.business.name,
                                    style: AppStyles.titleTextCustom(color: _itemColor)
                                  ),
                                ),
                                Container(
                                padding: EdgeInsets.all(3),
                                color: Colors.white,
                                child: Text(
                                  (business.isOwner != null && business.isOwner) ? AppLang().owner : AppLang().manager,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                              )
                              ],
                            ),
                            Container(height: 10),
                            Text(
                              business.business.addresses,
                              style: AppStyles.normalTextCustom(color: _itemColor),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      )
    );

  }
}
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/view_models/create_biz_viewmodel.dart';
import 'package:flutter/material.dart';

class CreateBizBasicForm extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final CreateBizViewModel model;
  //final CreateBizParams params;

  CreateBizBasicForm({
    @required this.scaffoldKey,
    @required this.model,
    //@required this.params
  }) {
    assert(scaffoldKey != null);
    assert(model != null);
    //assert(params != null);
  }

  @override
  State<StatefulWidget> createState() => _CreateBizBasicState();
}

class _CreateBizBasicState extends State<CreateBizBasicForm> {
  final TextEditingController _nameController = new TextEditingController();
  final TextEditingController _abnController = new TextEditingController();
  final TextEditingController _descController = new TextEditingController();

  final _nameFocusNode = FocusNode();
  final _abnFocusNode = FocusNode();
  final _descFocusNode = FocusNode();
  
  @override
  void initState() {
    final editing = widget.model.editing ?? BusinessDetail();
    final _name = editing.name;
    final _abn = editing.abn;
    final _desc = editing.description;
    if(_name != null && _name.length > 0) {
      _nameController.text = _name;
    }
    if(_abn != null && _abn.length > 0) {
      _abnController.text = _abn;
    }
    if(_desc != null && _desc.length > 0) {
      _descController.text = _desc;
    }
    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _abnController.dispose();
    _descController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final editing = widget.model.editing ?? BusinessDetail();
    final _name = editing.name;
    final _abn = editing.abn;
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            top: 30,
                            bottom: 30
                          ),
                          decoration: BoxDecoration(
                            color: AppColors.mostlyWhite,
                            border: Border(
                              bottom: BorderSide(color: Colors.grey, width: 1)
                            )
                          ),
                          child: Text(
                            AppLang().createBizHeader,
                            style: TextStyle(
                              color: Colors.black,
                              height: 1.25
                            ),
                          ),
                        ),
                        Container(
                          color: Colors.white,
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.all(20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppLang().bizDetailTitle,
                                style: AppStyles.bigTextCustom(
                                  color: Colors.black,
                                  fontSize: 20
                                ),
                              ),
                              Container(height: 40),
                              Text(
                                AppLang().bizName,
                                style: AppStyles.normalTextCustom(color: Colors.black),
                              ),
                              Container(height: 20),
                              TextFormField(
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                                  ),
                                  errorText: _name!= null && _name.trim().length == 0
                                      ? AppLang().bizNameRequired
                                      : null
                                ),
                                style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500, fontSize: 16),
                                controller: _nameController,
                                focusNode: _nameFocusNode,
                                onTap: () {
                                  if(_name == null) {
                                    final _editing = editing.copyWith(name: '');
                                    widget.model.changeEditing(_editing);
                                  }
                                },
                                onChanged: (value) {
                                  final _editing = editing.copyWith(name: value.trim());
                                  widget.model.changeEditing(_editing);
                                },
                                onFieldSubmitted: (_) {
                                  _fieldFocusChange(context, _nameFocusNode, _abnFocusNode);
                                },
                                autocorrect: false,
                                textInputAction: TextInputAction.next,
                              ),
                              Container(height: 20),
                              Text(
                                'ABN',
                                style: AppStyles.normalTextCustom(color: Colors.black),
                              ),
                              Container(height: 20),
                              TextFormField(
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                                  ),
                                  errorText: _abn != null && _abn.trim().length == 0
                                    ? AppLang().abnRequired
                                    : null
                                ),
                                controller: _abnController,
                                focusNode: _abnFocusNode,
                                onTap: () {
                                  if(_abn == null) {
                                    final _editing = editing.copyWith(abn: '');
                                    widget.model.changeEditing(_editing);
                                  }
                                },
                                onChanged: (value) {
                                  final _editing = editing.copyWith(abn: value.trim());
                                  widget.model.changeEditing(_editing);
                                },
                                onFieldSubmitted: (_) {
                                  _fieldFocusChange(context, _abnFocusNode, _descFocusNode);
                                },
                                autocorrect: false,
                                textInputAction: TextInputAction.next,
                              ),
                              Container(height: 20),
                              Text(
                                AppLang().description,
                                style: AppStyles.normalTextCustom(color: Colors.black),
                              ),
                              Container(height: 20),
                              TextFormField(
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                                  )
                                ),
                                controller: _descController,
                                focusNode: _descFocusNode,
                                onChanged: (value) {
                                  final _editing = editing.copyWith(description: value.trim());
                                  widget.model.changeEditing(_editing);
                                },
                                onFieldSubmitted: (_) {
                                  _fieldFocusChange(context, _abnFocusNode, _descFocusNode);
                                },
                                autocorrect: true,
                                minLines: 5,
                                maxLines: 5,
                                textInputAction: TextInputAction.next,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                )
              ]
            )
          )
        ],
      )
    );
  }

  _fieldFocusChange(BuildContext context, FocusNode currentNode, FocusNode nextNode) {
    currentNode.unfocus();
    FocusScope.of(context).requestFocus(nextNode);
  }
}
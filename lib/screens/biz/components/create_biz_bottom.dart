import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/screens/biz/components/paging_dot.dart';
import 'package:eight_biz_flutter/view_models/create_biz_viewmodel.dart';
import 'package:flutter/material.dart';

typedef void OnPressedCallback();

class CreateBizBottom extends StatelessWidget {
  final CreateBizViewModel model;
  final int dotIndex;
  final bool isNew;
  final OnPressedCallback onPressed;

  CreateBizBottom({
    this.model,
    this.dotIndex,
    this.onPressed,
    this.isNew = true,
  }) {
    assert(model != null);
    assert(dotIndex != null);
    //assert(onPressed != null);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: 0,
              left: 20,
              right: 20,
              bottom: 10
            ),
            child: PagingDot(
              color: AppColors.primaryOrange, 
              index: dotIndex, 
              borderWidth: 1.5,
              length: isNew ? 3 : 5,
              size: 20
            ),
          ),
          RaisedButton(
            onPressed: onPressed,
            color: isNew 
              ? dotIndex == 3 ? AppColors.primaryOrange : Colors.white 
              : AppColors.primaryOrange,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(6)
              ),
              side: BorderSide(
                color: isNew 
                  ? dotIndex == 3 ? AppColors.primaryOrange : Colors.black 
                  : AppColors.primaryOrange
              )
            ),
            child: Center(
              child: Text(
                isNew 
                ? dotIndex < 3 ? AppLang().next : AppLang().done
                : AppLang().save,
                style: TextStyle(
                  color: isNew 
                    ? dotIndex == 3 ? Colors.white : Colors.black 
                    : Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.w500
                ),
              ),
            ),
          ),
        ],
      ) 
    );
  }
}
import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/business_address.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/utils/string_utils.dart';
import 'package:eight_biz_flutter/view_models/create_biz_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:phone_number/phone_number.dart';

typedef void OnValidPhoneCallback(bool);

class CreateBizContactForm extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final CreateBizViewModel model;
  final bool isNew;
  final OnValidPhoneCallback onPhoneValidChange;

  CreateBizContactForm({
    @required this.scaffoldKey,
    @required this.model,
    this.isNew = true,
    this.onPhoneValidChange
  }) {
    assert(scaffoldKey != null);
    assert(model != null);
  }

  @override
  State<StatefulWidget> createState() => _CreateBizContactState();

}

class _CreateBizContactState extends State<CreateBizContactForm> {
  final TextEditingController _addressController = new TextEditingController();
  final TextEditingController _webController = new TextEditingController();
  final TextEditingController _emailController = new TextEditingController();

  final _addressFocusNode = FocusNode();
  final _phoneFocusNode = FocusNode();
  final _webFocusNode = FocusNode();
  final _emailFocusNode = FocusNode();

  bool isPhoneValid = true;
  String _phoneNumber;
  String _phoneCode;
  InternationalPhoneNumberInput phoneNumberInput;

  @override
  void initState() {
    var editing = widget.model.editing ?? BusinessDetail();
    var phone;
    if(widget.model.editing != null && 
      widget.model.editing.contactNumber != null && 
      widget.model.editing.contactNumber.length > 0
    ) {
      phone = widget.model.editing.contactNumber;
    }
    if(widget.isNew) {
      if(editing.email == null || editing.email.length == 0) {
        editing = editing.copyWith(email: widget.model.currentUser.email);
        _emailController.text = widget.model.currentUser.email;
      }
      if(phone == null) {
        phone = widget.model.currentUser.phone;
        editing = editing.copyWith(contactNumber: phone);
      }
      widget.model.changeEditing(editing);
    }
    _convertPhone(phone);
    final _address = editing.addresses;
    _addressController.text = _address ?? '';
    _webController.text = editing.website ?? '';
    _emailController.text = editing.email ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final editing = widget.model.editing ?? BusinessDetail();
    final _address = editing.addresses;
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                      top: 30,
                      bottom: 30
                    ),
                    decoration: BoxDecoration(
                      color: AppColors.mostlyWhite,
                      border: Border(
                        bottom: BorderSide(color: Colors.grey, width: 1)
                      )
                    ),
                    child: Text(
                      AppLang().createBizHeader,
                      style: TextStyle(
                        color: Colors.black,
                        height: 1.25
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppLang().bizContactTitle,
                          style: AppStyles.bigTextCustom(
                            color: Colors.black,
                            fontSize: 20
                          ),
                        ),
                        Container(height: 40),
                        Text(
                          AppLang().address,
                          style: AppStyles.normalTextCustom(color: Colors.black),
                        ),
                        Container(height: 20),
                        TextFormField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                            ),
                            errorText: _address == null || _address.trim().length == 0
                                ? AppLang().addressRequired
                                : null
                          ),
                          controller: _addressController,
                          focusNode: _addressFocusNode,
                          onTap: () {
                            if(_address == null) {
                              final _editing = editing.copyWith(addresses: '');
                              widget.model.changeEditing(_editing);
                            }
                            _searchPlace();
                          },
                          onFieldSubmitted: (_) {
                            _fieldFocusChange(context, _addressFocusNode, _phoneFocusNode);
                          },
                          autocorrect: false,
                          textInputAction: TextInputAction.next,
                        ),
                        Container(height: 20),
                        Text(
                          AppLang().phoneNumber,
                          style: AppStyles.normalTextCustom(color: Colors.black),
                        ),
                        Container(height: 20),
                        phoneNumberInput ?? Container(),
                        Container(height: 20),
                        Text(
                          AppLang().website,
                          style: AppStyles.normalTextCustom(color: Colors.black),
                        ),
                        Container(height: 20),
                        TextFormField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                            )
                          ),
                          controller: _webController,
                          focusNode: _webFocusNode,
                          onChanged: (value) {
                            final _editing = editing.copyWith(website: value.trim());
                            widget.model.changeEditing(_editing);
                          },
                          onFieldSubmitted: (_) {
                            _fieldFocusChange(context, _webFocusNode, _emailFocusNode);
                          },
                          //keyboardType: TextInputType.url,
                          textInputAction: TextInputAction.next,
                        ),
                        Container(height: 20),
                        Text(
                          AppLang().emailAdd,
                          style: AppStyles.normalTextCustom(color: Colors.black),
                        ),
                        Container(height: 20),
                        TextFormField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                            ),
                            errorText: !_validEmail()
                                ? AppLang().emailNotValid
                                : null
                          ),
                          controller: _emailController,
                          focusNode: _emailFocusNode,
                          onChanged: (value) {
                            final _editing = editing.copyWith(email: value.trim());
                            widget.model.changeEditing(_editing);
                          },
                          keyboardType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.done,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          )
        ]
      )
    );
  }

  _fieldFocusChange(BuildContext context, FocusNode currentNode, FocusNode nextNode) {
    currentNode.unfocus();
    FocusScope.of(context).requestFocus(nextNode);
  }

  _searchPlace() async {
    var kGoogleApiKey = AppConfig().googleApiKey;
    GoogleMapsPlaces _places = new GoogleMapsPlaces(apiKey: kGoogleApiKey);
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: kGoogleApiKey,
      mode: Mode.overlay, // Mode.fullscreen
      language: "en",
      components: [new Component(Component.country, "au")]
    );
    if (p != null) {
      // get detail (lat/lng)
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;
      _addressController.text = p.description;

      final editing = widget.model.editing ?? BusinessDetail();
      final _editing = editing.copyWith(
        addresses: p.description,
        address: BizAddress(
          latitude: lat,
          longitude: lng,
          address1: p.description,
          country: 'AU'
        )
      );
      widget.model.changeEditing(_editing);
    }
  }

  _convertPhone(String phoneNumber) async {
    try {
      final dynamic result = await PhoneNumber.parse(phoneNumber);
      setState(() {
        _phoneNumber = result['national_number'];
        _phoneCode = result['country_code'];
        phoneNumberInput = _buildPhone();
      });
    } catch (e) {
      setState(() {
        phoneNumberInput = _buildPhone();
      });
    }
  }

  void onPhoneNumberChanged(String phoneNumber) {
    var editing = widget.model.editing ?? BusinessDetail(); 
    editing = editing.copyWith(contactNumber: phoneNumber);
    widget.model.changeEditing(editing);
  }

  void onInputChanged(bool value) {
    setState(() {
      isPhoneValid = value;
    });
    if(widget.onPhoneValidChange != null) {
      widget.onPhoneValidChange(value);
    }
  }

  void onInputClear(String text) {
    setState(() {
      isPhoneValid = true;
    });
    if(widget.onPhoneValidChange != null) {
      widget.onPhoneValidChange(true);
    }
    var editing = widget.model.editing ?? BusinessDetail(); 
    editing = editing.copyWith(contactNumber: '');
    widget.model.changeEditing(editing);
  }

  bool _validEmail() {
    final editing = widget.model.editing ?? BusinessDetail();
    if(editing.email != null && editing.email.length > 0 && !validateEmail(editing.email)) {
      return false;
    }
    return true;
  }

  Widget _buildPhone() {
    return InternationalPhoneNumberInput.withCustomDecoration(
      showFlag: false,
      showDropDownIcon: false,
      phoneCodeStyle: TextStyle(
        color: AppColors.strongCyan,
        fontSize: 16
      ),
      onInputChanged: onPhoneNumberChanged,
      onInputValidated: onInputChanged,
      onInputClear: onInputClear,
      isRequiredPhone: false,
      initialCountry2LetterCode: 'AU',
      initialSelectPhoneCode: _phoneCode ?? '61',
      initialSelectPhoneNumber: _phoneNumber ?? '',
      inputDecoration: InputDecoration(
        hintText: AppLang().phoneNumber,
        prefixIcon: SizedBox(width: 30.0,),
        errorText: !isPhoneValid ? AppLang().phoneNumberNotValid : null,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6.0),
          borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6.0),
          borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6.0),
          borderSide: BorderSide(width: 1.0, color: AppColors.primaryOrange),
        ),
      ),
      formatInput: false
    );
  }
}
import 'dart:io';

import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/media.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/create_biz_viewmodel.dart';
import 'package:flutter/material.dart';

import 'biz_galery.dart';
import 'biz_logo.dart';

typedef void OnSelectedMedia(List<File> medias);

class CreateBizMediaForm extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final CreateBizViewModel model;
  final OnSelectedMedia onSelectedMedia;

  CreateBizMediaForm({
    @required this.scaffoldKey,
    @required this.model,
    this.onSelectedMedia
  }) {
    assert(scaffoldKey != null);
    assert(model != null);
  }
  
  @override
  State<StatefulWidget> createState() => _CreateBizMediaState();
}

class _CreateBizMediaState extends State<CreateBizMediaForm> {
  final _scrollController = new ScrollController();

  @override
  Widget build(BuildContext context) {
    final model = widget.model;
    final pers = model.permissions ?? [];
    final canUpdateBiz = pers.where((per) => per.role == BusinessRole.updateBiz && per.isGranted).toList().length > 0;
    final canUpload = pers.where((per) => per.role == BusinessRole.uploadMedia && per.isGranted).toList().length > 0;
    final canDelete = pers.where((per) => per.role == BusinessRole.deleteMedia && per.isGranted).toList().length > 0;
    return SingleChildScrollView(
      controller: _scrollController,
      child: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            decoration: BoxDecoration(
                color: AppColors.whiteColor,
                border: Border(
                  bottom: BorderSide(color: AppColors.grey3Color),
                )),
            child: LogoProfileBizSection(
              business: model.editing,
              logoFile: model.localLogo,
              onSelectedFile: _onUploadLogo,
              onDeleteLogo: _onDeleteLogo,
              canUpdate: canUpdateBiz,
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 20.0),
            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            decoration: BoxDecoration(
                color: AppColors.whiteColor,
                border: Border(
                  top: BorderSide(color: AppColors.grey3Color),
                  bottom: BorderSide(color: AppColors.grey3Color),
                )),
            child: GalleryBizSection(
              canUpdateBiz: canUpdateBiz,
              canUpload: canUpload,
              canDelete: canDelete,
              onSelectedFile: (file){
                _onUploadImages(file);
              },
              onDeleteImage: _onDeleteImage,
              onChangeCover: _onChangeCover,
              isLoadingMedias: model.loadingMedias,
              medias: model.resMedias.results
            ),
          )
        ],
      ),
    );
  }

  _onUploadLogo(File file) {
    widget.model.uploadLogo(widget.model.editing, widget.model.editing.id, file);
  }

  _onDeleteLogo() {
    widget.model.deleteLogo(widget.model.business);
  }

  _onUploadImages(File file) {
    widget.model.uploadMedia(widget.model.editing.id, file, false, _showSuccess, _showError);
  }

  _onDeleteImage(Media image) {
    widget.model.deleteMedia(widget.model.editing.id, image, _showSuccess, _showError);
  }

  _onChangeCover(Media image) {
    widget.model.updateCoverImage(widget.model.editing, image.id, !image.isCover);
  }

  _showError(String message) {
    Helper.showError(context, message);
  }

  _showSuccess(String message) {
    Helper.showSuccess(context, message);
  }
}
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_open_hour.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/view_models/create_biz_viewmodel.dart';
import 'package:flutter/material.dart';

import 'open_hour.dart';

class CreateBizOtherForm extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final CreateBizViewModel model;

  CreateBizOtherForm({
    @required this.scaffoldKey,
    @required this.model
  }) {
    assert(scaffoldKey != null);
    assert(model != null);
  }

  @override
  State<StatefulWidget> createState() => _CreateBizOtherState();
}

class _CreateBizOtherState extends State<CreateBizOtherForm> {
  
  @override
  Widget build(BuildContext context) {
    var editing = widget.model.editing ?? BusinessDetail();
    var openHour = editing.openHour ?? BizOpenHour();
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                      top: 30,
                      bottom: 30
                    ),
                    decoration: BoxDecoration(
                      color: AppColors.mostlyWhite,
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.grey, 
                          width: 1
                        )
                      )
                    ),
                    child: Text(
                      AppLang().createBizHeader,
                      style: TextStyle(
                        color: Colors.black,
                        height: 1.25
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppLang().bizOtherTitle,
                          style: AppStyles.bigTextCustom(
                            color: Colors.black,
                            fontSize: 20
                          ),
                        ),
                        Container(height: 40),
                        Text(
                          AppLang().categories,
                          style: AppStyles.normalTextCustom(color: Colors.black),
                        ),
                        Container(height: 20),
                        Container(
                          width: MediaQuery.of(context).size.width - 40,
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.all(Radius.circular(8))
                          ),
                          child: Wrap(
                            alignment: WrapAlignment.start,
                            spacing: 10,
                            children: _buildCategories(),
                          ),
                        ),
                        Container(height: 20),
                        Text(
                          AppLang().bizHours,
                          style: AppStyles.normalTextCustom(color: Colors.black),
                        ),
                        Container(height: 20),
                        OpenHour(
                          title: AppLang().monday,
                          isDaily: false,
                          value: openHour.monday,
                          onChangeValue: (value) {
                            var _openHour = openHour.copyWith(monday: value);
                            if(value == null) {
                              _openHour = openHour.copyWith(monday: []);
                            }
                            final _editing = editing.copyWith(openHour: _openHour);
                            widget.model.changeEditing(_editing);
                          },
                        ),
                        Container(height: 20),
                        OpenHour(
                          title: AppLang().tuesday,
                          isDaily: false,
                          value: openHour.tuesday,
                          onChangeValue: (value) {
                            var _openHour = openHour.copyWith(tuesday: value);
                            if(value == null) {
                              _openHour = openHour.copyWith(tuesday: []);
                            }
                            final _editing = editing.copyWith(openHour: _openHour);
                            widget.model.changeEditing(_editing);
                          },
                        ),
                        Container(height: 20),
                        OpenHour(
                          title: AppLang().wednesday,
                          isDaily: false,
                          value: openHour.wednesday,
                          onChangeValue: (value) {
                            var _openHour = openHour.copyWith(wednesday: value);
                            if(value == null) {
                              _openHour = openHour.copyWith(wednesday: []);
                            }
                            final _editing = editing.copyWith(openHour: _openHour);
                            widget.model.changeEditing(_editing);
                          },
                        ),
                        Container(height: 20),
                        OpenHour(
                          title: AppLang().thursday,
                          isDaily: false,
                          value: openHour.thursday,
                          onChangeValue: (value) {
                            var _openHour = openHour.copyWith(thursday: value);
                            if(value == null) {
                              _openHour = openHour.copyWith(thursday: []);
                            }
                            final _editing = editing.copyWith(openHour: _openHour);
                            widget.model.changeEditing(_editing);
                          },
                        ),
                        Container(height: 20),
                        OpenHour(
                          title: AppLang().friday,
                          isDaily: false,
                          value: openHour.friday,
                          onChangeValue: (value) {
                            var _openHour = openHour.copyWith(friday: value);
                            if(value == null) {
                              _openHour = openHour.copyWith(friday: []);
                            }
                            final _editing = editing.copyWith(openHour: _openHour);
                            widget.model.changeEditing(_editing);
                          },
                        ),
                        Container(height: 20),
                        OpenHour(
                          title: AppLang().saturday,
                          isDaily: false,
                          value: openHour.saturday,
                          onChangeValue: (value) {
                            print('Saturday change');
                            var _openHour = openHour.copyWith(saturday: value);
                            if(value == null) {
                              _openHour = openHour.copyWith(saturday: []);
                            }
                            final _editing = editing.copyWith(openHour: _openHour);
                            widget.model.changeEditing(_editing);
                          },
                        ),
                        Container(height: 20),
                        OpenHour(
                          title: AppLang().sunday,
                          isDaily: false,
                          value: openHour.sunday,
                          onChangeValue: (value) {
                            print('Sunday change');
                            var _openHour = openHour.copyWith(sunday: value);
                            if(value == null) {
                              _openHour = openHour.copyWith(sunday: []);
                            }
                            final _editing = editing.copyWith(openHour: _openHour);
                            widget.model.changeEditing(_editing);
                          },
                        ),
                        Container(height: 20),
                      ],
                    ),
                  )
                ],
              ),
            )
          )
        ]
      )
    );
  }

  List<Widget> _buildCategories() {
    final editing = widget.model.editing ?? BusinessDetail();
    var selected = editing.categories ?? [];
    if(selected == null) {
      selected = [];
    }
    List<Widget> list = [];
    var categories = widget.model.categories ?? [];
    if(categories.length > 0) {
      for (var cat in categories) {
        var isSelected = selected.where((item) => item.id == cat.id).toList().length > 0;
        list.add(
          RawChip(
            key: ValueKey<String>(cat.id),
            label: Text(
              cat.name,
              style: TextStyle(
                color: isSelected ? AppColors.whiteColor : AppColors.darkBlue
              ),
            ),
            backgroundColor: isSelected ? AppColors.darkBlue : AppColors.lightGrey,
            tapEnabled: true,
            onPressed: () {
              var _selected = selected;
              if(isSelected) {
                _selected = selected.where((item) => item.id != cat.id).toList();
              } else {
                _selected.add(cat);
              }
              var _editing = editing.copyWith(categories: _selected);
              widget.model.changeEditing(_editing);
            }
          )
        );
      }
    } else {
      list.add(
        Container(
          padding: EdgeInsets.only(
            top: 15,
            bottom: 15
          ),
          child: Text(
            AppLang().categoriesEmpty
          ),
        )
      );
    }
    
    return list;
  }
}
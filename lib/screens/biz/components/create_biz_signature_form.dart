import 'dart:io';
import 'dart:math';

import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/create_biz_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:path_provider/path_provider.dart';
import 'package:signature/signature.dart';

class CreateBizSignatureForm extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final CreateBizViewModel model;

  CreateBizSignatureForm({
    @required this.scaffoldKey,
    @required this.model
  }) {
    assert(scaffoldKey != null);
    assert(model != null);
  }

  @override
  State<StatefulWidget> createState() => _SignatureFormState();
}

class _SignatureFormState extends State<CreateBizSignatureForm> {
  //var color = Colors.red;
  //var strokeWidth = 5.0;
  List<Point> points;
  
  Signature _signatureCanvas;

  @override
  void initState() {
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    if(_signatureCanvas == null) {
      setState(() {
        _signatureCanvas = Signature(
          height: 250,
          width: MediaQuery.of(context).size.width - 40,
          points: widget.model.signature ?? [],
          backgroundColor: Colors.black12,
          onChanged: (_points) {
            widget.model.changeSignature(_points, false);
            setState(() {
              points = _points;
            });
          },
        );
      });
    }
    print('signature ${widget.model.signature}');
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                      top: 30,
                      bottom: 30
                    ),
                    decoration: BoxDecoration(
                      color: AppColors.mostlyWhite,
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.grey, 
                          width: 1
                        )
                      )
                    ),
                    child: Text(
                      AppLang().createBizHeader,
                      style: TextStyle(
                        color: Colors.black,
                        height: 1.25
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppLang().agreement,
                          style: AppStyles.bigTextCustom(
                            color: Colors.black,
                            fontSize: 20
                          ),
                        ),
                        Container(height: 40),
                        widget.model.agreement != null
                        ? Html(
                          data: widget.model.agreement,
                          
                        ): Container(),
                        //_img.buffer.lengthInBytes == 0 ? Container() : LimitedBox(maxHeight: 200.0, child: Image.memory(_img.buffer.asUint8List())),
                        widget.model.signature != null 
                        ? Column(
                          children: <Widget>[
                            GestureDetector(
                              onVerticalDragUpdate: (_) {},
                              onHorizontalDragUpdate: (_) {},
                              child: Container(
                                //width: 300,
                                height: 300,
                                child: Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: _signatureCanvas ?? Container()
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                MaterialButton(
                                  onPressed: _saveSignature,
                                  child: Text(AppLang().save)
                                ),
                                MaterialButton(
                                  onPressed: () {
                                    widget.model.changeSignature([], false);
                                    _signatureCanvas.clear();
                                    setState(() {
                                      points = [];
                                    });
                                  },
                                  child: Text(AppLang().clear)),
                                MaterialButton(
                                  onPressed: () {
                                    setState(() {
                                      points = [];
                                    });
                                    _signatureCanvas.clear();
                                    widget.model.changeSignature(null, true);
                                  },
                                  child: Text(AppLang().cancel)),
                              ],
                            ),
                          ],
                        ) : Container(
                          child: widget.model.agreement != null 
                          ? InkWell(
                            onTap: () {
                              if(_signatureCanvas != null && _signatureCanvas.key.currentState != null) {
                                _signatureCanvas.clear();
                              }
                              widget.model.changeSignature([], false);
                            },
                            child: Center(
                              child: Text(
                                AppLang().editSignature,
                                style: TextStyle(
                                  color: AppColors.primaryOrange,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.italic
                                ),
                              ),
                            )
                          ) : Container()
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
          )
        ]
      )
    );
  }

  _saveSignature() async {
    final signature = widget.model.signature;
    if(signature == null || signature.length == 0) {
      Helper.showError(widget.scaffoldKey, AppLang().emptySignature);
      return;
    }

    final bytes = await _signatureCanvas.exportBytes();
    Directory appDocDirectory = await getApplicationDocumentsDirectory();
    var imageFile = File('${appDocDirectory.path}/signature_${widget.model.business.id}.jpg');
    await imageFile.writeAsBytes(bytes);
    widget.model.saveSignature(widget.model.business.id, imageFile, _saveSignatureSuccess, _showError);
  }

  _saveSignatureSuccess(String message) {
    _signatureCanvas.clear();
    widget.model.changeSignature(null, true);
  }

  _showError(String error) {
    Helper.showError(widget.scaffoldKey, error);
  }
}
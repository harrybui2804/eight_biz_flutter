import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';

typedef void VoidCallback(List<String> value);

class OpenHour extends StatefulWidget {
  final String title;
  final List<String> value;
  final bool isDaily;
  final VoidCallback onChangeValue;


  OpenHour({
    @required this.title, 
    this.value, 
    this.onChangeValue,
    this.isDaily = false
  }) {
    assert(title != null);
  }

  @override
  State<StatefulWidget> createState() => _OpenHourState();
}

class _OpenHourState extends State<OpenHour> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool isNull = false;
    if(!widget.isDaily) {
      if(widget.value == null || widget.value.length == 0) {
        isNull = true;
      }
    }
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: !isNull ? Colors.grey : Colors.red,
              width: 1
            ),
            borderRadius: BorderRadius.all(Radius.circular(8)),
          ),
          child: Row(
            //crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.isDaily ? AppLang().daily : widget.title,
                      style: AppStyles.bigTextCustom(
                        fontSize: 18,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    widget.value != null && widget.value.length > 0 && !widget.isDaily
                    ? 
                    Column(
                      children: _buildValue(),
                    )
                    : Container()
                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  RaisedButton(
                    color: Colors.white,
                    highlightColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      side: BorderSide(color: Colors.black, width: 1)
                    ),
                    onPressed: (){
                      _showPickerDateRange();
                    },
                    child: Container(
                      width: 110,
                      child: Center(
                        child: Text(
                          widget.isDaily ? AppLang().bizSetAllManually : AppLang().bizSetManually, 
                          style: TextStyle(color: Colors.black)
                        ),
                      ),
                    ),
                  ),
                  RaisedButton(
                    color: Colors.white,
                    highlightColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      side: BorderSide(color: Colors.black, width: 1)
                    ),
                    onPressed: (){
                      if(widget.onChangeValue != null) {
                        widget.onChangeValue(['Open 24 hours']);
                      }
                    },
                    child: Container(
                      width: 110,
                      child: Center(
                        child: Text(
                          widget.isDaily ? AppLang().bizSetAllOpen24 : AppLang().bizSetOpen24, 
                          style: TextStyle(color: Colors.black)
                        ),
                      ),
                    )
                  ),
                  RaisedButton(
                    color: Colors.white,
                    highlightColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      side: BorderSide(color: Colors.black, width: 1),
                    ),
                    onPressed: (){
                      if(widget.onChangeValue != null) {
                        widget.onChangeValue(['Closed']);
                      }
                    },
                    child: Container(
                      width: 110,
                      child: Center(
                        child: Text(
                          widget.isDaily ? AppLang().bizSetAllClosed : AppLang().bizSetClosed, 
                          style: TextStyle(color: Colors.black)
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        isNull 
        ? Container(
          margin: EdgeInsets.only(top: 5),
            alignment: Alignment.centerLeft,
            child: Text(
              AppLang().bizHoursRequired,
              style: TextStyle(color: Colors.red),
            ),
          )
        : Container()
      ],
    );
  }

  List<Widget> _buildValue() {
    List<Widget> chips = [];
    if(widget.value.length == 1 && (widget.value[0] == 'Open 24 hours' || widget.value[0] == 'Closed')) {
      var title = widget.value[0];
      if(widget.value[0].contains('Open 24 hours')) {
        title = AppLang().open24hours;
      }
      if(widget.value[0].contains('Closed')) {
        title = AppLang().closed;
      }
      chips.add(
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            title,
            style: AppStyles.normalTextCustom(
              color: Colors.black
            ),
          ),
        )
      );
    } else {
      for (var item in widget.value) {
        var title = item;
        if(item.contains('Open 24 hours')) {
          title = AppLang().open24hours;
        }
        if(item.contains('Closed')) {
          title = AppLang().closed;
        }
        chips.add(
          Chip(
            label: Text(title),
            backgroundColor: AppColors.lightGrey,
            deleteIconColor: Colors.black,
            onDeleted: () {
              widget.value.remove(item);
              if(widget.onChangeValue != null) {
                widget.onChangeValue(widget.value);
              }
            },
          )
        );
      }
    }
    
    return chips;
  }

  bool _validPicker(Picker ps, Picker pe) {
    var valid = true;
    final openHour = ps.selecteds[0];
    final openMin = ps.selecteds[1];
    final openS = ps.selecteds[2];

    final closeHour = pe.selecteds[0];
    final closeMin = pe.selecteds[1];
    final closeS = pe.selecteds[2];
    if(openS > closeS) {
      valid = false;
    }
    if(openS == closeS) {
      if(openHour > closeHour) {
        valid = false;
      }
      if(openHour == closeHour) {
        if(openMin == closeMin) {
          valid = false;
        }
        if(openMin > closeMin)  {
          valid = false;
        }
      }
    }
    return valid;
  }

  _showPickerDateRange() {
    Picker ps = new Picker(
      hideHeader: true,
      adapter: new DateTimePickerAdapter(type: PickerDateTimeType.kHM_AP),
      selecteds: [7,0,0],
      onConfirm: (Picker picker, List value) {
        print((picker.adapter as DateTimePickerAdapter).value);
      }
    );

    Picker pe = new Picker(
      hideHeader: true,
      adapter: new DateTimePickerAdapter(type: PickerDateTimeType.kHM_AP),
      selecteds: [9, 0, 1],
      onConfirm: (Picker picker, List value) {
        print((picker.adapter as DateTimePickerAdapter).value);
      }
    );
    List<Widget> actions = [
      FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: new Text(AppLang().cancel)),
      FlatButton(
          onPressed: () {
            final openHour = ps.selecteds[0] + 1;
            final openMin = ps.selecteds[1];
            final openS = ps.selecteds[2] == 0 ? 'AM' : 'PM';
            var openTime = openHour < 10 ? ('0' + openHour.toString()) : openHour.toString();
            openTime += ':';
            openTime += openMin < 10 ? ('0' + openMin.toString()) : openMin.toString();
            openTime += ' ' + openS;

            final closeHour = pe.selecteds[0] + 1;
            final closeMin = pe.selecteds[1];
            final closeS = pe.selecteds[2] == 0 ? 'AM' : 'PM';
            var closeTime = closeHour < 10 ? ('0' + closeHour.toString()) : closeHour.toString();
            closeTime += ':';
            closeTime += closeMin < 10 ? ('0' + closeMin.toString()) : closeMin.toString();
            closeTime += ' ' + closeS;

            final timeRange = openTime + ' - ' + closeTime;
            if(_validPicker(ps, pe)) {
              print(timeRange);
              Navigator.pop(context);
              if(widget.isDaily) {
                if(widget.onChangeValue != null) {
                  widget.onChangeValue([timeRange]);
                }
              } else {
                var newValue = widget.value ?? [];
                if(newValue.length == 1) {
                  if(newValue[0] == 'Open 24 hours' || newValue[0] == 'Closed') {
                    newValue = [];
                  }
                }
                newValue.add(timeRange);
                if(widget.onChangeValue != null) {
                  widget.onChangeValue(newValue);
                }
              }
            } else {
              Helper.showError(context, AppLang().timeRangeError);
            }
          },
          child: new Text(AppLang().confirm))
    ];

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return new AlertDialog(
          title: Text(AppLang().timeRange),
          actions: actions,
          content: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text("${AppLang().begin}:"),
                ps.makePicker(),
                Text("${AppLang().end}:"),
                pe.makePicker()
              ],
            ),
          ),
        );
      }
    );
  }
}
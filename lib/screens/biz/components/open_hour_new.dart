import 'package:flutter/material.dart';

class OpenHourWidget extends StatefulWidget {
  final String title;
  final List<String> value;
  final bool isDaily;
  final VoidCallback onChangeValue;

  OpenHourWidget({
    @required this.title, 
    this.value, 
    this.onChangeValue,
    this.isDaily = false
  }) {
    assert(title != null);
  }

  @override
  State<StatefulWidget> createState() => _OpenHourState();
}

class _OpenHourState extends State<OpenHourWidget> {
  
  @override
  Widget build(BuildContext context) {
    return null;
  }
}
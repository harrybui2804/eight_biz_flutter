import 'package:flutter/material.dart';

class OpenHourView extends StatelessWidget {
  final String dayName;
  final List<String> hours;

  OpenHourView({this.dayName, this.hours});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 3),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 120,
            alignment: Alignment.centerLeft,
            child: Text(
              dayName,
            ),
          ),
          hours != null && hours.length > 0 
          ? Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: hours.map((value) {
              return Padding(
                padding: EdgeInsets.symmetric(vertical: 3),
                child: Text(value.trim()),
              );
            }).toList(),
          ) : Container()
        ],
      ),
    );
  }
}
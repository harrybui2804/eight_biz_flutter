import 'package:flutter/material.dart';

class PagingDot extends StatelessWidget {
  final int length;
  final Decoration decoration;
  final Color color;
  final int index;
  final double size;
  final double borderWidth;
  final MainAxisAlignment mainAxisAlignment;

  PagingDot({
    this.length = 3, 
    this.decoration, 
    this.color = Colors.black, 
    this.index = 1, 
    this.size = 6,
    this.borderWidth = 1,
    this.mainAxisAlignment = MainAxisAlignment.center
  }){
    assert(length > 0);
    assert(size > 0);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> list = [];
    for(var i=0; i< length; i++) {
      list.add(
        Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            color: (index == i + 1) ? color : Colors.white,
            border: Border.all(width: borderWidth, color: color),
            borderRadius: BorderRadius.all(Radius.circular(size))
          )
        ),
      );
      if(i<length -1) {
        list.add(
          Container(width: size,)
        );
      }
    }
    return Container(
      decoration: decoration,
      child: Center(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: mainAxisAlignment,
          children: list,
        ),
      ),
    );
  }
}
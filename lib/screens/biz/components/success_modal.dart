import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:flutter/material.dart';

typedef void VoidCallback();

class SuccessCreateModal extends StatelessWidget {
  final bool isCreate;
  final VoidCallback onClose;

  SuccessCreateModal({@required this.onClose, this.isCreate = true}) {
    assert(onClose != null);
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width - 40;
    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: Container(
        width: width,
        padding: EdgeInsets.all(20),
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 20),
              child: Center(
                child: Text(
                  isCreate
                  ? AppLang().almostThere
                  : AppLang().updateBizTitle,
                  style: AppStyles.titleTextCustom(),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Container(
              child: Center(
                child: Text(
                  isCreate
                  ? AppLang().thankCreate
                  : AppLang().thankUpdate
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: Center(
                child: Text(
                  AppLang().bodyCreateUpdateReview,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Container(
              child: Center(
                child: Text(
                  AppLang().bodyCreateUpdateNotif,
                  textAlign: TextAlign.center
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: 10,
                bottom: 10
              ),
              color: Colors.white,
              alignment: Alignment.center,
              child: RaisedButton(
                onPressed: (){
                  Navigator.of(context).pop();
                  onClose();
                },
                color: AppColors.primaryOrange,
                highlightColor: AppColors.primaryOrange,
                elevation: 0,
                highlightElevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(6)),
                ),
                child: Text(
                    AppLang().close,
                    style: TextStyle(
                      color: AppColors.whiteColor,
                      fontSize: 15,
                      fontWeight: FontWeight.w500
                    ),
                  ),
              ),
            ),
          ],
        ),
      )
    );
  }
}
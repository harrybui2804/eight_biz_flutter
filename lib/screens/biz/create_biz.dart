import 'package:carousel_slider/carousel_slider.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/redux/actions/category_actions.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/utils/string_utils.dart';
import 'package:eight_biz_flutter/view_models/create_biz_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'components/ask_create_update_modal.dart';
import 'components/create_biz_basic_form.dart';
import 'components/create_biz_bottom.dart';
import 'components/create_biz_contact_form.dart';
import 'components/create_biz_media_form.dart';
import 'components/create_biz_other_form.dart';
import 'components/create_biz_signature_form.dart';
import 'components/success_modal.dart';

class CreateBizPage extends StatefulWidget {
  static const routeName = '/create_biz';

  final int pageIndex;
  final CreateBizParams params;

  CreateBizPage(this.pageIndex, this.params) {
    assert(params != null);
  }

  @override
  State<StatefulWidget> createState() => _CreateBizState();
}

class _CreateBizState extends State<CreateBizPage> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  int currentIndex = 0;
  bool isPhoneValid = true;
  bool isLoadingMedias = false; //using for edit biz
  
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: LayoutBuilder(
        builder: (context, constraints)=> Container(
          child: Container(
            color: Colors.white,
            child: StoreConnector<AppState, CreateBizViewModel> (
              onInit: (store){
                store.dispatch(RequestCategoriesAction());
              },
              converter: (store) => CreateBizViewModel.fromStore(store),
              builder: (_, viewModel) => buildPage(viewModel)
            )
          ),
        ),
      ),
    );
  }

  Widget buildPage(CreateBizViewModel viewModel) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            title: Text(
              widget.params.isNew ? AppLang().createBiz : AppLang().editBiz,
              style: AppStyles.navTitle(),
            ),
            leading: IconButton(
              icon: Icon(Icons.chevron_left),
              iconSize: 35,
              color: AppColors.primaryOrange,
              onPressed: () {
                Navigator.of(context).pop();
                if(widget.pageIndex == 0) {
                  viewModel.clearEditingBiz();
                }
              },
            ),
          ),
        ),
      ),
      body: content(viewModel),
    );
  }

  Widget content(CreateBizViewModel viewModel) {
    bool isLoading = viewModel.loading;
    return Container(
      child: Stack(
        children: <Widget>[
          _buildForm(viewModel),
          isLoading 
          ? Positioned( 
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                color: Colors.black.withOpacity(0.05),
                alignment: Alignment.center,
                child: SpinKitCircle(
                  size: 50,
                  color: AppColors.primaryOrange,
                ),
              ),
            ) : Container(),
        ],
      ),
    );
  }

  Widget _buildForm(CreateBizViewModel viewModel) {
    if(widget.params.isNew) {
      Widget content = CreateBizBasicForm(
        scaffoldKey: _scaffoldKey, 
        model: viewModel
      );
      if(widget.pageIndex == 0) {
        content = CreateBizBasicForm(
          scaffoldKey: _scaffoldKey, 
          model: viewModel
        );
      } else if(widget.pageIndex == 1) {
        content = CreateBizContactForm(
          scaffoldKey: _scaffoldKey, 
          model: viewModel, 
          onPhoneValidChange: (valid) {
            if(valid != isPhoneValid) {
              setState(() {
                isPhoneValid = valid;
              });
            }
          }
        );
      } else if(widget.pageIndex == 2) {
        content = CreateBizOtherForm(
          scaffoldKey: _scaffoldKey, 
          model: viewModel,
        );
      }
      return Column(
        children: <Widget>[
          Expanded(
            child: content,
          ),
          CreateBizBottom(
            model: viewModel,
            dotIndex: widget.pageIndex + 1,
            isNew: true,
            onPressed: _validForm(viewModel) 
              ? () {
                if(widget.pageIndex == 2) {
                  _submitCreate(viewModel);
                } else {
                  viewModel.nextPage(widget.pageIndex + 1, widget.params.isNew);
                }
              }
              : null
            ,
          )
        ],
      );
    } else {
      return Column(
        children: <Widget>[
          Expanded(
            child: CarouselSlider(
              height: MediaQuery.of(context).size.height,
              reverse: false,
              viewportFraction: 1.0,
              enableInfiniteScroll: false,
              items: [
                CreateBizBasicForm(
                  scaffoldKey: _scaffoldKey, 
                  model: viewModel
                ),
                CreateBizContactForm(
                  scaffoldKey: _scaffoldKey, 
                  model: viewModel,
                  onPhoneValidChange: (valid) {
                    if(valid != isPhoneValid) {
                      setState(() {
                        isPhoneValid = valid;
                      });
                    }
                  }
                ),
                CreateBizOtherForm(
                  scaffoldKey: _scaffoldKey, 
                  model: viewModel
                ),
                CreateBizMediaForm(
                  scaffoldKey: _scaffoldKey, 
                  model: viewModel
                ),
                CreateBizSignatureForm(
                  scaffoldKey: _scaffoldKey, 
                  model: viewModel
                )
              ],
              onPageChanged: (index) {
                setState(() {
                  currentIndex = index;
                });
                if(index == 3 && (viewModel.resMedias.results == null || viewModel.resMedias.results.length == 0)) {
                  viewModel.requestListMedias(viewModel.editing.id, null, _showError);
                }
                if(index == 4 && (viewModel.agreement == null || viewModel.agreement.length == 0)) {
                  viewModel.loadAgreement(viewModel.business.id,(_) {}, _showError);
                }
              },
            )
          ),
          CreateBizBottom(
            model: viewModel,
            dotIndex: currentIndex + 1,
            isNew: false,
            onPressed: _validForm(viewModel) 
              ? () { 
                if(!viewModel.loading) {
                  _submitEdit(viewModel);
                }
              }
              : null
            ,
          )
        ],
      );
    }
  }

  bool _validForm(CreateBizViewModel model) {
    final editing = model.editing ?? BusinessDetail();
    if(widget.params.isNew) {
      var index = widget.pageIndex;
      if(index == 0) {
        if(editing.name == null || editing.name.trim().length == 0) {
          return false;
        }
        if(editing.abn == null || editing.abn.trim().length == 0) {
          return false;
        }
        return true;
      } else if(index == 1) {
        if(!isPhoneValid) {
          return false;
        }
        if(editing.addresses == null || editing.addresses.trim().length == 0) {
          return false;
        }
        if(editing.email != null && editing.email.length > 0 && !validateEmail(editing.email)) {
          return false;
        }
        return true;
      } else if(index == 2) {
        if(editing.categories == null || editing.categories.length == 0) {
          return false;
        }
        if(editing.openHour == null || !editing.openHour.valid()) {
          return false;
        }
        return true;
      }
      return true;
    } else {
      if(editing.name == null || editing.name.trim().length == 0) {
        return false;
      }
      if(editing.abn == null || editing.abn.trim().length == 0) {
        return false;
      }
      if(!isPhoneValid) {
        return false;
      }
      if(editing.addresses == null || editing.addresses.trim().length == 0) {
        return false;
      }
      if(editing.email != null && editing.email.length > 0 && !validateEmail(editing.email)) {
        return false;
      }
      if(editing.categories == null || editing.categories.length == 0) {
        return false;
      }
      if(editing.openHour == null || !editing.openHour.valid()) {
        return false;
      }
      return true;
    }
  }

  _submitCreate(CreateBizViewModel model) {
    if(!model.loading) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return ModalAskCreateUpdate(
            isCreate: true,
            onAccept: () {
              model.submitCreateBusiness(model.editing, (businessId)=> _showSuccess(model, businessId), _showError);
            },
          );
        }
      );
    }
    
  }

  _submitEdit(CreateBizViewModel model) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return ModalAskCreateUpdate(
          isCreate: false,
          onAccept: () {
            model.submitUpdateBusiness(model.editing, (message) => _showSuccessEdit(model, message), _showError);
          },
        );
      }
    );
  }

  _showSuccessEdit(CreateBizViewModel model, String message)  {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return SuccessCreateModal(
          isCreate: false,
          onClose: () {
            Keys.navKey.currentState.pop(context);
            model.clearEditingBiz();
          },
        );
      }
    );
  }

  _showSuccess(CreateBizViewModel model, String businessId) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return SuccessCreateModal(
          isCreate: true,
          onClose: () {
            model.goToAgreement(businessId);
            //model.popToListBiz();
            model.clearEditingBiz();
          },
        );
      }
    );
  }

  _showError(String message) {
    Helper.showError(_scaffoldKey, message);
  }
}

class CreateBizParams {
  final int index;
  final bool isNew;

  CreateBizParams({@required this.index, this.isNew = true});

  factory CreateBizParams.initial() {
    return CreateBizParams(index: 0, isNew: true);
  }

  CreateBizParams copyWith({
    int index,
    bool isNew
  }) {
    return CreateBizParams(
      index: index ?? this.index,
      isNew: isNew ?? this.isNew
    );
  }
}
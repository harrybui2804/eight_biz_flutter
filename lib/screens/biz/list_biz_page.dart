import 'dart:ui';

import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/business_list.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/screens/biz/components/biz_row.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/listbiz_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ListBusinessPage extends StatefulWidget {
  static const routeName = '/list_biz';

  final ListBizPageParams params;

  ListBusinessPage(this.params);
  
  @override
  State<StatefulWidget> createState() => _ListBusinessState();
}

class _ListBusinessState extends State<ListBusinessPage> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _scrollController = new ScrollController();
  final _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  BusinessList _selectedBiz;

  //Locale myLocale;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            title: Text(
              AppLang().switchToBiz,
              style: AppStyles.navTitle(),
            ),
            leading: widget.params.canPop ? 
            InkWell(
              onTap: () => Navigator.pop(context),
              child: Icon(
                Icons.chevron_left, 
                color: AppColors.primaryOrange,
                size: 35,
              ),
            ) : Container(),
          ),
        ),
      ),
      body: LayoutBuilder(
        builder: (context, constraints)=> SafeArea(
          child: Container(
            color: Colors.white,
            child: StoreConnector<AppState, ListBizViewModel> (
              onInit: (store){
                _scrollController.addListener(() => _onScroll(store));
                final model = ListBizViewModel.fromStore(store);
                _loadListBusiness(model, reload: true);
              },
              converter: (store) => ListBizViewModel.fromStore(store),
              builder: (_, viewModel) => content(viewModel)
            )
          ),
        ),
      ),
    );
  }

  Widget content(ListBizViewModel viewModel) {
    if(viewModel.data.results.length == 0 && viewModel.loading) {
      return Center(
        child: SpinKitCircle(
          color: AppColors.primaryOrange,
          size: 50,
        )
      );
    }
    List<BusinessList> list = [];
    if(viewModel.data.results.length > 0) {
      list.add(BusinessList());
      if(widget.params.withoutBizId.isEmpty) {
        list.addAll(viewModel.data.results);
      } else {
        list.addAll(
          viewModel.data.results.where((biz) => biz.business.id != widget.params.withoutBizId ).toList()
        );
      }
      if(viewModel.loadMore) {
        list.add(BusinessList());
      }
    }

    return Stack(
      children: [
        Column(
          children: [
            Expanded(
              child: RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: () async {
                  _loadListBusiness(viewModel, reload: true);
                },
                child: list.length == 0 
                ? Center(
                    child: Text(
                      AppLang().emptyBiz,
                      style: TextStyle(
                        decorationStyle: TextDecorationStyle.wavy,
                        fontStyle: FontStyle.italic
                      ),
                    ),
                  ) 
                : ListView.builder(
                  controller: _scrollController,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  itemCount: list.length,
                  itemBuilder: (BuildContext context, int index) {
                    if(index == 0) {
                      return Container(
                        padding: EdgeInsets.only(top: 20, bottom: 20),
                        child: Center(
                          child: Text(
                            AppLang().listBizHeader,
                            style: AppStyles.normalTextCustom(),
                          ),
                        )
                      );
                    }
                    if(index == list.length - 1 && viewModel.loadMore) {
                      return Container(
                        padding: EdgeInsets.all(10),
                        child: Center(
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(AppColors.primaryOrange)
                          ),
                        ),
                      );
                    }
                    bool isSelected = _selectedBiz != null && list[index] == _selectedBiz;
                    return BusinessRow(
                      business: list[index], 
                      isSelected: isSelected,
                      bottom: 10,
                      onSelected: (biz) { 
                        if(!viewModel.loading) {
                          setState(() {
                            _selectedBiz = biz;
                          });
                          viewModel.switchBusiness(biz, _showError);
                        }
                      }
                    );
                  }
                )
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: 5,
                bottom: 20,
                left: 20,
                right: 20
              ),
              child: Column(
                children: [
                  RaisedButton(
                    onPressed: (){
                      viewModel.createBusiness();
                    },
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(6)
                      ),
                      side: BorderSide(
                        color: Colors.black
                      )
                    ),
                    child: Center(
                      child: Text(
                        AppLang().createBiz,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        viewModel.loading ? 
        Positioned( 
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.black.withOpacity(0.05),
            alignment: Alignment.center,
            child: SpinKitCircle(
              size: 50,
              color: AppColors.primaryOrange,
            ),
          ),
        ) : Container()
      ],
    );
  }

  _onScroll(Store<AppState> store) {
    double maxScroll = _scrollController.position.maxScrollExtent;
    double currentScroll = _scrollController.position.pixels;
    double delta = 50;
    if ( maxScroll - currentScroll <= delta) { 
      ListBizViewModel viewModel = ListBizViewModel.fromStore(store);
      _loadListBusiness(viewModel);
    }
  }

  _loadListBusiness(ListBizViewModel viewModel, {bool reload = false}) {
    if(!viewModel.loading && !viewModel.loadMore) {
      if(reload){
        viewModel.loadBusiness(null, _showError);
      } else if(viewModel.data.next != null) {
        viewModel.loadBusiness(viewModel.data.next, _showError);
      }
    }
  }

  _showError(String message) {
    Helper.showError(_scaffoldKey, message);
  }
}

class ListBizPageParams {
  final String withoutBizId;
  final bool canPop;

  ListBizPageParams({
    this.withoutBizId = '',
    this.canPop = false
  });
}
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/images.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/view_models/biz_detail_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/svg.dart';

import 'components/biz_galery.dart';
import 'components/biz_info.dart';

class BusinessDetailPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _BusinessDetailState();
}

class _BusinessDetailState extends State<BusinessDetailPage> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.white, // Color for Android
      statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
    ));
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            title: Text(
              AppLang().myBizTitle,
              style: AppStyles.navTitle(),
            ),
          ),
        ),
      ),
      body: LayoutBuilder(
        builder:  (context, constraints) => SafeArea(
          child: StoreConnector<AppState, BizDetailViewModel> (
            onInit: (store) async {
              //load medias
              
            },
            converter: (store) => BizDetailViewModel.fromStore(store),
            builder: (_, viewModel) => content(viewModel)
          ),
        ),
      ),
    );
  }

  Widget content(BizDetailViewModel model) {
    final biz = model.business;
    //print('logo ${biz.logoImage}');
    //print('cover ${biz.coverImage}');
    final canEdit = model.permissions.where((per) => per.role == BusinessRole.updateBiz && per.isGranted).toList().length > 0;
    return Column(
      children: [
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                BusinessGalery(business: biz),
                BusinessInfoSection(business: biz)
              ],
            ),
          ),
        ),
        canEdit ? Container(
          padding: EdgeInsets.only(
            top: 10,
            left: 20,
            right: 20,
            bottom: 20
          ),
          color: Colors.white,
          child: RaisedButton(
            onPressed: (){
              model.submitEditBizDetail(biz);
            },
            //padding: EdgeInsets.all(10),
            color: AppColors.primaryOrange,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(6))),
            child: Container(
              //height: 45,
              alignment: Alignment.center,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SvgPicture.asset(AppImages.iconEdit, height: 18),
                  Container(width: 10,),
                  Text(
                    AppLang().editDetails,
                    style: AppStyles.normalTextCustom(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      //fontSize: 20
                    ),
                  )
                ],
              ),
            ),
          ),
        ) : Container()
      ],
    );
  }
}
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BusinessDescription extends StatelessWidget {
  final BusinessDetail business;

  BusinessDescription({@required this.business});

  @override
  Widget build(BuildContext context) {
    final biz = business ?? BusinessDetail();
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                Icon(Icons.location_on, color: Colors.black),
                Container(width: 10),
                Expanded(
                  child: Text(
                    biz.address != null
                      ? biz.address.toString()
                      : '',
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                Container(
                  width: 30,
                  child: Icon(Icons.call, color: Colors.black)
                ),
                Text(
                  biz.contactNumber != null
                    ? biz.contactNumber
                    : '',
                  style: TextStyle(fontWeight: FontWeight.w500)
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                Container(
                  width: 30,
                  child: Icon(FontAwesomeIcons.mousePointer, color: Colors.black, size: 18,)
                ),
                Text(
                  biz.website != null
                    ? biz.website
                    : '',
                  style: TextStyle(fontWeight: FontWeight.w500)
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                Container(
                  width: 30,
                  child: Icon(Icons.alternate_email, color: Colors.black, size: 18,)
                ),
                Text(
                  biz.email != null
                    ? biz.email
                    : '',
                  style: TextStyle(fontWeight: FontWeight.w500)
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Text(
              biz.description != null
                ? biz.description 
                : ''
            ),
          ),
        ],
      ),
    );
  }
}
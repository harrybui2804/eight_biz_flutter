import 'package:carousel_pro/carousel_pro.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class BusinessGalery extends StatefulWidget {
  final BusinessDetail business;

  const BusinessGalery({Key key, @required this.business}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BusinessGaleryState();
}

class _BusinessGaleryState extends State<BusinessGalery> {

  int currentIndexSlider = 0;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.width;
    final biz = widget.business ?? BusinessDetail();
    return Stack( 
      children: <Widget>[
        SizedBox(
          width: width,
          height: height / 1.5,
          child: (biz.listMedia == null || biz.listMedia.isEmpty) //show cover image
              ? Stack(
                  children: <Widget>[
                    Container(
                      width: width,
                      height: height / 1.5,
                      child:  biz.coverImage != null && biz.coverImage.length > 0 
                        ?  FadeInImage.memoryNetwork(
                            image: biz.coverImage,
                            placeholder: kTransparentImage,
                            fit: BoxFit.cover,
                          )
                        : Container(),
                    ),
                    Positioned(
                      bottom: -1,
                      child: Container(
                        width: width,
                        height: height / 7,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                            colors: [
                              Colors.white.withOpacity(1),
                              Colors.white.withOpacity(0.1),
                              Colors.transparent,
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : Carousel(
                  autoplay: false,
                  images: biz.listMedia.map(
                    (item) {
                      return Container(
                        width: width,
                        child: Stack(
                          children: <Widget>[
                            Container(
                              height: height,
                              width: width,
                              child: item != null ? FadeInImage.memoryNetwork(
                                image: item,
                                placeholder: kTransparentImage,
                                fit: BoxFit.cover,
                              ) : Container()
                            ),
                            Positioned(
                              bottom: 0,
                              left: 0,
                              right: 0,
                              child: Container(
                                width: width,
                                height: height / 7,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.bottomCenter,
                                    end: Alignment.topCenter,
                                    colors: [
                                      Colors.white.withOpacity(1),
                                      Colors.white.withOpacity(0.1),
                                      Colors.transparent,
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ).toList(),
                  dotBgColor: Colors.transparent,
                  dotIncreasedColor: AppColors.primaryOrange,
                  dotColor: AppColors.primaryOrange,
                  showIndicator: false,
                  onImageChange: (int currentIndex, nextIndex) {
                    setState(
                      () {
                        currentIndexSlider = nextIndex;
                      },
                    );
                  },
                ),
        ),
        (biz.listMedia != null && biz.listMedia.isNotEmpty) 
        ? Positioned(
            bottom: 20.0,
            left: 0,
            right: 0,
            child: Wrap(
              alignment: WrapAlignment.center,
              spacing: 8.0,
              runSpacing: 5.0,
              children: biz.listMedia.map(
                (item) {
                  return Container(
                    height: 8.0,
                    width: 8.0,
                    decoration: BoxDecoration(
                      color: biz.listMedia.indexOf(item) ==
                              currentIndexSlider
                          ? AppColors.primaryOrange
                          : Colors.transparent,
                      border: Border.all(color: AppColors.primaryOrange),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                  );
                },
              ).toList(),
            ),
          )
        : Container()
      ],
    );
  }
}
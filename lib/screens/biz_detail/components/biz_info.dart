import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/screens/biz_detail/components/biz_work_time_section.dart';
import 'package:flutter/material.dart';

import 'biz_description.dart';
import 'biz_other_info_section.dart';

class BusinessInfoSection extends StatefulWidget {

  final BusinessDetail business;

  const BusinessInfoSection({Key key, @required this.business}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BusinessInfoState();
}

class _BusinessInfoState extends State<BusinessInfoSection> {
  bool isShowWorkTime = false;

  _setShowWorkTime() {
    setState(() {
      isShowWorkTime = !isShowWorkTime;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      padding:
          EdgeInsets.only(top: 30.0, bottom: 10.0, left: 20.0, right: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          BusinessOtherInfoSection(
            business: widget.business,
            setShowWorkTime: _setShowWorkTime,
            isShowWorkTime: isShowWorkTime,
          ),
          BusinessWorkTimeSection(
            business: widget.business,
            isShowWorkTime: isShowWorkTime,
          ),
          BusinessDescription(business: widget.business)
          /*
          DescriptionSection(
            myBiz: widget.myBiz,
          ),
          */
        ],
      ),
    );
  }
}
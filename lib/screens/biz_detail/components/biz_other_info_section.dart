import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:flutter/material.dart';

class BusinessOtherInfoSection extends StatefulWidget {
  final BusinessDetail business;
  final Function setShowWorkTime;
  final bool isShowWorkTime;

  BusinessOtherInfoSection({
    Key key, 
    @required this.business,
    @required this.isShowWorkTime,
    @required this.setShowWorkTime
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _OtherInfoState();
}

class _OtherInfoState extends State<BusinessOtherInfoSection> {
  
  @override
  Widget build(BuildContext context) {
    final biz = widget.business ?? BusinessDetail();
    List<String> stringCategories = [];
    if (biz.categories != null && biz.categories.isNotEmpty) {
      stringCategories = biz.categories.map((tag) {
        return tag.name;
      }).toList();
    }
    return Padding(
      padding: EdgeInsets.only(top: 0.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _mapStatusBizToString(biz.status),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Text(
                    stringCategories.reduce((value, element) => value + ', ' + element),
                    style: AppStyles.normalTextCustom(
                      color: AppColors.secondaryColor,
                    ),
                  ),
                ),
                Text(
                  biz.name,
                  style: AppStyles.bigTextCustom(fontSize: 25.0),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 2.0),
                        child: Icon(
                          Icons.star,
                          size: 17.0,
                          color: AppColors.secondaryColor,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 5.0),
                        child: Text(
                          '4.0 •',
                          style: AppStyles.normalTextCustom(),
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 1.0),
                                child: Text(
                                  '${AppLang().openToday}: ${biz.openHour.openingToday()}',
                                  style: AppStyles.smallTextCustom(),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 5.0),
                              child: InkWell(
                                onTap: widget.setShowWorkTime,
                                child: widget.isShowWorkTime
                                    ? Icon(Icons.keyboard_arrow_up)
                                    : Icon(Icons.keyboard_arrow_down),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            width: 70.0,
            height: 70.0,
            padding: EdgeInsets.all(5.0),
            child: biz.logoImage != null 
              ? CircleAvatar(
                  backgroundColor: AppColors.grey3Color,
                  backgroundImage: NetworkImage(biz.logoImage),
                  radius: 18,
                )
              : Container(),
          ),
        ],
      ),
    );
  }

  Widget _mapStatusBizToString(int status) {
    switch (status) {
      case 0:
        return Container(
          padding: EdgeInsets.all(3.0),
          decoration: BoxDecoration(
            color: AppColors.primaryOrange.withOpacity(0.9),
            borderRadius:BorderRadius.all(Radius.circular(3.0)),
          ),
          child: Text(
            AppLang().statusPending,
            style: AppStyles.smallTextCustom(color: AppColors.whiteColor, fontWeight: FontWeight.w500),
          ),
        );
        break;
      case 1:
        return Container(
          padding: EdgeInsets.all(3.0),
          decoration: BoxDecoration(
            color: AppColors.secondaryColor,
            borderRadius:BorderRadius.all(Radius.circular(3.0)),
          ),
          child: Text(
            AppLang().statusApproval,
            style: AppStyles.smallTextCustom(color: AppColors.whiteColor, fontWeight: FontWeight.w500),
          ),
        );
        break;
      case 2:
        return Container(
          padding: EdgeInsets.all(3.0),
          decoration: BoxDecoration(
            color: Colors.red,
            borderRadius:BorderRadius.all(Radius.circular(3.0)),
          ),
          child: Text(
            AppLang().statusReject,
            style: AppStyles.smallTextCustom(color: AppColors.whiteColor, fontWeight: FontWeight.w500),
          ),
        );
        break;
    }
    return Container();
  }
}
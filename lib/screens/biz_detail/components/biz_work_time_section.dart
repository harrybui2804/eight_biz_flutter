import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/models/biz_open_hour.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

class BusinessWorkTimeSection extends StatelessWidget {
  final BusinessDetail business;
  final bool isShowWorkTime;
  final controller = ExpandableController();

  BusinessWorkTimeSection({Key key, @required this.business, this.isShowWorkTime = false}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    controller.expanded = isShowWorkTime;
    final openHour = business.openHour;
    return ExpandablePanel(
      controller: controller,
      header: Container(),
      collapsed: Container(),
      expanded: Container(
        padding: EdgeInsets.only(left: 20),
        child: Column(
          children: _buildLists(openHour),
        ),
      ),
      tapHeaderToExpand: false,
      hasIcon: false,
    );
  }

  List<Widget> _buildLists(BizOpenHour openHour) {
    List<Widget> list = [];
    if(openHour != null) {
      list.add(Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 80,
            padding: EdgeInsets.only(bottom: 5),
            child: Text(AppLang().monday),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: openHour.monday != null && openHour.monday.length> 0
              ? openHour.monday.map((value) {
                var title = value;
                if(value.contains('Open 24 hours')) {
                  title = AppLang().open24hours;
                }
                if(value.contains('Closed')) {
                  title = AppLang().closed;
                }
                return Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Text(title),
                );
              }).toList()
              : [],
          ),
        ],
      ));
      list.add(Row(
        children: <Widget>[
          Container(
            width: 80,
            padding: EdgeInsets.only(bottom: 5),
            child: Text(AppLang().tuesday),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: openHour.tuesday != null && openHour.tuesday.length> 0
              ? openHour.tuesday.map((value) {
                var title = value;
                if(value.contains('Open 24 hours')) {
                  title = AppLang().open24hours;
                }
                if(value.contains('Closed')) {
                  title = AppLang().closed;
                }
                return Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Text(title),
                );
              }).toList()
              : [],
          )
        ],
      ));
      list.add(Row(
        children: <Widget>[
          Container(
            width: 80,
            padding: EdgeInsets.only(bottom: 5),
            child: Text(AppLang().wednesday),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: openHour.wednesday != null && openHour.wednesday.length> 0
              ? openHour.wednesday.map((value) {
                var title = value;
                if(value.contains('Open 24 hours')) {
                  title = AppLang().open24hours;
                }
                if(value.contains('Closed')) {
                  title = AppLang().closed;
                }
                return Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Text(title),
                );
              }).toList()
              : [],
          )
        ],
      ));
      list.add(Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 80,
            padding: EdgeInsets.only(bottom: 5),
            child: Text(AppLang().thursday),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: openHour.thursday != null && openHour.thursday.length> 0
              ? openHour.thursday.map((value) {
                var title = value;
                if(value.contains('Open 24 hours')) {
                  title = AppLang().open24hours;
                }
                if(value.contains('Closed')) {
                  title = AppLang().closed;
                }
                return Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Text(title),
                );
              }).toList()
              : [],
          )
        ],
      ));
      list.add(Row(
        children: <Widget>[
          Container(
            width: 80,
            padding: EdgeInsets.only(bottom: 5),
            child: Text(AppLang().friday),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: openHour.friday != null && openHour.friday.length> 0
              ? openHour.friday.map((value) {
                var title = value;
                if(value.contains('Open 24 hours')) {
                  title = AppLang().open24hours;
                }
                if(value.contains('Closed')) {
                  title = AppLang().closed;
                }
                return Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Text(title),
                );
              }).toList()
              : [],
          )
        ],
      ));
      list.add(Row(
        children: <Widget>[
          Container(
            width: 80,
            padding: EdgeInsets.only(bottom: 5),
            child: Text(AppLang().saturday),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: openHour.saturday != null && openHour.saturday.length> 0
              ? openHour.saturday.map((value) {
                var title = value;
                if(value.contains('Open 24 hours')) {
                  title = AppLang().open24hours;
                }
                if(value.contains('Closed')) {
                  title = AppLang().closed;
                }
                return Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Text(title),
                );
              }).toList()
              : [],
          )
        ],
      ));
      list.add(Row(
        children: <Widget>[
          Container(
            width: 80,
            padding: EdgeInsets.only(bottom: 5),
            child: Text(AppLang().sunday),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: openHour.sunday != null && openHour.sunday.length> 0
              ? openHour.sunday.map((value) {
                var title = value;
                if(value.contains('Open 24 hours')) {
                  title = AppLang().open24hours;
                }
                if(value.contains('Closed')) {
                  title = AppLang().closed;
                }
                return Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Text(title),
                );
              }).toList()
              : [],
          )
        ],
      ));
    }
    return list;
  }
}

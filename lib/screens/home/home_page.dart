import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/images.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/screens/admin/admin_tab_page.dart';
import 'package:eight_biz_flutter/screens/biz_detail/biz_detail_page.dart';
import 'package:eight_biz_flutter/screens/profile/profile_page.dart';
import 'package:eight_biz_flutter/screens/wallet/wallet_tab_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomeTabPage extends StatefulWidget {
  static const routeName = '/home';

  @override
  State<StatefulWidget> createState() => _HomeTabPage(); 
}

class _HomeTabPage extends State<HomeTabPage> {
  int _selectedIndex = 1;

  _onSelectedTab(int index) {
    if(index != _selectedIndex) {
      setState(() {
        _selectedIndex = index;
      });
    }
  }

  static List _widgetOptions = [
    WalletTabPage(),
    BusinessDetailPage(),
    AdminStackPage(),
    //PayPage(),
    ProfilePage()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        currentIndex: _selectedIndex,
        selectedItemColor: AppColors.primaryOrange,
        iconSize: 24,
        type: BottomNavigationBarType.fixed,
        //fixedColor: AppColors.primaryOrange.withOpacity(0.1),
        showSelectedLabels: true,
        showUnselectedLabels: true,
        selectedFontSize: 12,
        unselectedFontSize: 12,
        elevation: 20.0,
        onTap: _onSelectedTab,
        items: [
          BottomNavigationBarItem(
            activeIcon: SvgPicture.asset(AppImages.tabWalletActive, height: 25),
            icon: SvgPicture.asset(AppImages.tabWalletInactive, height: 25),
            title: Padding(
              padding: EdgeInsets.only(top: 8),
              child: Text(AppLang().wallet),
            )
          ),
          BottomNavigationBarItem(
            activeIcon: SvgPicture.asset(AppImages.tabBizActive, height: 20),
            icon: SvgPicture.asset(AppImages.tabBizInactive, height: 20),
            title: Padding(
              padding: EdgeInsets.only(top: 8),
              child: Text(AppLang().business),
            )
          ),
          BottomNavigationBarItem(
            activeIcon: SvgPicture.asset(AppImages.tabAdminActive, height: 25),
            icon: SvgPicture.asset(AppImages.tabAdminInactive, height: 25),
            title: Padding(
              padding: EdgeInsets.only(top: 8),
              child: Text(AppLang().admin),
            )
          ),
          /*
          BottomNavigationBarItem(
            activeIcon: Image.asset(AppImages.tabPayActive),
            icon: Image.asset(AppImages.tabPayInactive),
            title: Text('')
          ),
          */
          BottomNavigationBarItem(
            activeIcon: SvgPicture.asset(AppImages.tabProfileActive, height: 25),
            icon: SvgPicture.asset(AppImages.tabProfileInactive, height: 25),
            title: Padding(
              padding: EdgeInsets.only(top: 8),
              child: Text(AppLang().profile),
            )
          )
        ]
      ),
    );
  }
}
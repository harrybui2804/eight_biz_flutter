import 'package:collection/collection.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/notification.dart' as notif;
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/notif_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class NotificationsPage extends StatefulWidget {
  static const routeName = '/notifications';

  @override
  State<StatefulWidget> createState() => _NotificationsState();
}

class _NotificationsState extends State<NotificationsPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final scrollController = ScrollController(initialScrollOffset: 0);
  final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  bool loading = false;
  bool loadMore = false;

  _loadNotifs(NotifViewModel model, {bool loadMore = false}) {
    if(loadMore) {
      setState(() {
        loadMore = true;
      });
    } else {
      setState(() {
        loading = true;
      });
    }
    model.loadNotifs(loadMore, _onSuccess, _onError);
  }

  _onScroll(NotifViewModel model) {
    double maxScroll = scrollController.position.maxScrollExtent;
    double currentScroll = scrollController.position.pixels;
    double delta = 50;
    if ( maxScroll - currentScroll <= delta && !loadMore) {
      //check can load more
      if(model.notifications != null && model.notifications.length > 0 && model.nextNotifs != null) {
        setState(() {
          loadMore = true;
        });
        model.loadNotifs(true, _onSuccess, _onError);
      }
    }
  }

  _onSuccess(String _) {
    setState(() {
      loading = false;
      loadMore = false;
    });
  }

  _onError(String message) {
    setState(() {
      loading = false;
      loadMore = false;
    });
    Helper.showError(context, message);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            title: Text(
              AppLang().notifications,
              style: AppStyles.navTitle(),
            ),
            leading: InkWell(
              onTap: () => Navigator.pop(context),
              child: Icon(
                Icons.chevron_left, 
                color: AppColors.primaryOrange,
                size: 35,
              ),
            )
          ),
        ),
      ),
      body: LayoutBuilder(
        builder: (context, constraints)=> SafeArea(
          child: Container(
            color: AppColors.grey4Color,
            child: StoreConnector<AppState, NotifViewModel> (
              onInitialBuild: (model) {
                scrollController.addListener(() => _onScroll(model));
                if(model.notifications == null || model.notifications.length == 0) {
                  Future.delayed(const Duration(milliseconds: 10), () {
                    _loadNotifs(model, loadMore: false);
                  });
                }
              },
              converter: (store) => NotifViewModel.fromStore(store),
              builder: (_, viewModel) => _buildContent(viewModel)
            )
          ),
        ),
      ),
    );
  }


  Widget _buildContent(NotifViewModel model) {
    List<notif.Notification> lst = model.notifications ?? [];
    var newMap = groupBy(lst, (notif.Notification obj){
      var date = DateTime.parse(obj.createdAt);
      var strDate = date.year.toString();
      strDate += '-';
      strDate += date.month < 10 ? '0' + date.month.toString() : date.month.toString();
      strDate += '-';
      strDate += date.day < 10 ? '0' + date.day.toString() : date.day.toString();
      return strDate;
    });
    var keys = newMap.keys.toList();
    keys.sort((a, b) => DateTime.parse(b).compareTo(DateTime.parse(a)));
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Expanded(
              child: RefreshIndicator(
                key: refreshIndicatorKey,
                onRefresh: () async {
                  _loadNotifs(model, loadMore: false);
                },
                child: keys.length == 0 
                ? Container() 
                : ListView.builder(
                  controller: scrollController,
                  //padding: EdgeInsets.only(left: 20, right: 20),
                  itemCount: keys.length,
                  itemBuilder: (BuildContext context, int index) {
                    var key = keys[index];
                    var childs = newMap[key];
                    DateTime date = DateTime.parse(key);
                    var title = date.day < 10 ? '0' + date.day.toString() : date.day.toString();
                    title += ' ' + Helper.getMonthName(date.month);
                    return Container(
                      //padding: EdgeInsets.only(top: 20, bottom: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          //header
                          Container(
                            padding: EdgeInsets.only(
                              top: 10, 
                              bottom: 10,
                              left: 20,
                              right: 20
                            ),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(width: 1, color: AppColors.grey3Color)
                              )
                            ),
                            height: 45,
                            alignment: Alignment.bottomLeft,
                            child: Text(
                              title,
                              style: AppStyles.titleTextCustom(fontWeight: FontWeight.w400),
                            ),
                          ),
                          _buildListChilds(childs)
                        ],
                      )
                    );
                  }
                )
              ),
            ),
            loadMore ? Container(
              height: 25,
              padding: EdgeInsets.only(top: 5),
              alignment: Alignment.center,
              child: SpinKitCircle(
                size: 20,
                color: AppColors.primaryOrange,
              ),
            ): Container()
          ],
        ),
        loading ? SpinKitCircle(
          size: 50,
          color: AppColors.primaryOrange,
        ) : Container()
      ],
    );
  }

  Widget _buildListChilds(List<notif.Notification> childs) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: childs.map((notif) {
        return Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              bottom: BorderSide(width: 1, color: AppColors.grey3Color)
            )
          ),
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                notif.title,
                style: AppStyles.titleTextCustom(),
              ),
              Container(height: 8),
              Wrap(
                children: <Widget>[
                  Text(
                    notif.text,
                    textAlign: TextAlign.left
                  ),
                ],
              )
            ],
          ),
        );
      }).toList(),
    );
  }
}
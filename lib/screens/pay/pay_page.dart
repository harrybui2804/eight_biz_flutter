import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/view_models/biz_detail_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class PayPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _PayPageState();
}

class _PayPageState extends State<PayPage> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Pay',
          style: AppStyles.navTitle(),
        ),
        centerTitle: true
      ),
      body: LayoutBuilder(
        builder:  (context, constraints) => SafeArea(
          child: StoreConnector<AppState, BizDetailViewModel> (
            converter: (store) => BizDetailViewModel.fromStore(store),
            builder: (_, viewModel) => content(viewModel)
          ),
        ),
      ),
    );
  }

  Widget content(BizDetailViewModel viewModel) {
    return Stack(
      children: <Widget>[
        Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    //Content page here ...
                  ],
                ),
              ),
            )
          ],
        ),
        //Add circle loading here

      ],
    );
  }
}
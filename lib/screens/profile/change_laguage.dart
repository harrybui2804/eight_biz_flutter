import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/profile_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class _Language {
  final String name;
  final String code;
  final String flag;

  _Language({
    @required this.name,
    @required this.code,
    @required this.flag
  });
}

class ChangeLanguagePage extends StatefulWidget {
  static const routeName = '/language';

  @override
  State<StatefulWidget> createState() => _LanguageState();
}

class _LanguageState extends State<ChangeLanguagePage> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final List<_Language> languages = [
    _Language(
      code: 'en',
      name: "English",
      flag: ''
    ),
    _Language(
      code: 'zh',
      name: "中文",
      flag: ''
    ),
  ];
  bool loading = false;
  
  @override
  void initState() {
    super.initState();
  }

  _onChangeSuccess(ProfileViewModel model, String message) {
    setState(() {
      loading = false;
    });
    model.reloadBizDetail();
    print('permissions.length ${model.permissions.length}');
    if(model.permissions != null && model.permissions.length > 0) {
      model.loadPermissions();
    }
    Helper.showSuccess(scaffoldKey, message);
    Navigator.of(context).pop();
  }

  _onError(String message) {
    setState(() {
      loading = false;
    });
    Helper.showError(scaffoldKey, message);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            iconTheme: IconThemeData(color: AppColors.primaryOrange),
            title: Text(
              AppLang().changeLanguage,
              style: AppStyles.navTitle(),
            ),
            leading: InkWell(
              onTap: () => Navigator.pop(context),
              child: Icon(
                Icons.chevron_left, 
                color: AppColors.primaryOrange,
                size: 35,
              ),
            )
          ),
        ),
      ),
      body: LayoutBuilder(
        builder: (context, constraints)=> SafeArea(
          child: Container(
            color: Colors.white,
            child: StoreConnector<AppState, ProfileViewModel> (
              converter: (store) => ProfileViewModel.fromStore(store),
              builder: (_, viewModel) => content(viewModel)
            )
          ),
        ),
      ),
    );
  }

  Widget content(ProfileViewModel model) {
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  itemCount: languages.length,
                  itemBuilder: (BuildContext context, int index) {
                    bool isSelected = languages[index].code == AppConfig().language;
                    return MaterialButton(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            languages[index].name
                          ),
                          isSelected ? Icon(Icons.check, color: AppColors.primaryOrange,) : Container()
                        ],
                      ),
                      onPressed: () {
                        setState(() {
                          loading = true;
                        });
                        model.changeLanguage(languages[index].code, (String message) {
                          _onChangeSuccess(model, message);
                        }, _onError);
                      },
                    );
                  }
                ),
            )
          ],
        ),
        loading ? Positioned( 
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.black.withOpacity(0.05),
            alignment: Alignment.center,
            child: SpinKitCircle(
              size: 50,
              color: AppColors.primaryOrange,
            ),
          ),
        ) : Container(),
      ],
    );
  }
}
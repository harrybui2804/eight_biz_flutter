import 'package:eight_biz_flutter/components/close_modal.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/profile_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ChangePasswordPage extends StatefulWidget {
  static const routeName = '/change_pass';

  @override
  State<StatefulWidget> createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePasswordPage> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _currentController = new TextEditingController();
  final FocusNode _currentFocus = FocusNode();
  final TextEditingController _newController = new TextEditingController();
  final FocusNode _newFocus = FocusNode();
  final TextEditingController _confirmController = new TextEditingController();
  final _confirmFocus = FocusNode();

  String currentPass = '';
  String newPass = '';
  String confirmPass = '';

  bool _isShowPassword = false;
  bool _isShowNewPassword = false;
  bool _isShowConfirmPassword = false;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            title: Text(
              AppLang().changePassword,
              style: AppStyles.navTitle(),
            ),
            leading: InkWell(
              onTap: () => Navigator.pop(context),
              child: Icon(
                Icons.chevron_left, 
                color: AppColors.primaryOrange,
                size: 35,
              ),
            )
          ),
        ),
      ),
      body: LayoutBuilder(
        builder:  (context, constraints) => SafeArea(
          child: StoreConnector<AppState, ProfileViewModel> (
            converter: (store) => ProfileViewModel.fromStore(store),
            builder: (_, viewModel) => content(viewModel)
          ),
        ),
      ),
    );
  }

  Widget content(ProfileViewModel model) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Stack(
        children: <Widget>[
          Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                AppLang().currentPassword,
                                style: AppStyles.normalTextCustom(),
                              ),
                            ),
                            Stack(
                              alignment: Alignment.centerRight,
                              children: <Widget>[
                                TextFormField(
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.symmetric(
                                          vertical: 18.0, horizontal: 20.0),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(6.0),
                                        borderSide: BorderSide(width: 1.0),
                                      ),
                                      errorText: currentPass.length == 0
                                          ? AppLang().passRequired
                                          : null),
                                  controller: _currentController,
                                  focusNode: _currentFocus,
                                  style: AppStyles.normalTextCustom(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      currentPass = value;
                                    });
                                  },
                                  obscureText: _isShowPassword ? false : true,
                                  autocorrect: false,
                                  textInputAction: TextInputAction.done,
                                  onFieldSubmitted: (_) => _fieldFocusChange(context, _currentFocus, _newFocus),
                                ),
                                Positioned(
                                  top: 20.0,
                                  right: 15.0,
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        _isShowPassword = !_isShowPassword;
                                      });
                                    },
                                    child: _isShowPassword
                                        ? Icon(
                                            Icons.layers,
                                            size: 17.0,
                                          )
                                        : Icon(
                                            Icons.layers_clear,
                                            size: 17.0,
                                          ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                AppLang().newPassword,
                                style: AppStyles.normalTextCustom(),
                              ),
                            ),
                            Stack(
                              alignment: Alignment.centerRight,
                              children: <Widget>[
                                TextFormField(
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.symmetric(
                                          vertical: 18.0, horizontal: 20.0),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(6.0),
                                        borderSide: BorderSide(width: 1.0),
                                      ),
                                      errorText: newPass.length < 8
                                          ? AppLang().errPassLength
                                          : null),
                                  controller: _newController,
                                  focusNode: _newFocus,
                                  style: AppStyles.normalTextCustom(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      newPass = value;
                                    });
                                  },
                                  obscureText: _isShowNewPassword ? false : true,
                                  autocorrect: false,
                                  textInputAction: TextInputAction.done,
                                  onFieldSubmitted: (_) => _fieldFocusChange(context, _newFocus, _confirmFocus),
                                ),
                                Positioned(
                                  top: 20.0,
                                  right: 15.0,
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        _isShowNewPassword = !_isShowNewPassword;
                                      });
                                    },
                                    child: _isShowNewPassword
                                        ? Icon(
                                            Icons.layers,
                                            size: 17.0,
                                          )
                                        : Icon(
                                            Icons.layers_clear,
                                            size: 17.0,
                                          ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                AppLang().confirmPassword,
                                style: AppStyles.normalTextCustom(),
                              ),
                            ),
                            Stack(
                              alignment: Alignment.centerRight,
                              children: <Widget>[
                                TextFormField(
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.symmetric(
                                          vertical: 18.0, horizontal: 20.0),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(6.0),
                                        borderSide: BorderSide(width: 1.0),
                                      ),
                                      errorText: newPass != confirmPass
                                          ? AppLang().errPassMatch
                                          : null),
                                  controller: _confirmController,
                                  focusNode: _confirmFocus,
                                  style: AppStyles.normalTextCustom(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      confirmPass = value;
                                    });
                                  },
                                  obscureText: _isShowConfirmPassword ? false : true,
                                  autocorrect: false,
                                  textInputAction: TextInputAction.done,
                                  onFieldSubmitted: (_) => _fieldFocusChange(context, _newFocus, _confirmFocus),
                                ),
                                Positioned(
                                  top: 20.0,
                                  right: 15.0,
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        _isShowConfirmPassword = !_isShowConfirmPassword;
                                      });
                                    },
                                    child: _isShowConfirmPassword
                                        ? Icon(
                                            Icons.layers,
                                            size: 17.0,
                                          )
                                        : Icon(
                                            Icons.layers_clear,
                                            size: 17.0,
                                          ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(top: 40.0),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          height: 65.0,
                          child: RaisedButton(
                            onPressed: () {
                              _submitUpdate(model);
                            },
                            child: Text(
                              AppLang().save,
                              style: AppStyles.normalTextCustom(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 19),
                            ),
                            color: AppColors.primaryOrange,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(6.0),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ]
          ),
          model.loadingChangePass 
          ? Positioned( 
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.black.withOpacity(0.05),
              alignment: Alignment.center,
              child: SpinKitCircle(
                size: 50,
                color: AppColors.primaryOrange,
              ),
            ),
          ) : Container(),
        ],
      )
    );
  }

  bool _validForm() {
    if(currentPass == null || currentPass.length == 0) {
      return false;
    }
    if(newPass == null || newPass.length < 0) {
      return false;
    }
    if(confirmPass != newPass) {
      return false;
    }
    return true;
  }

  _submitUpdate(ProfileViewModel model) {
    FocusScope.of(context).requestFocus(FocusNode());
    if(!model.loadingChangePass) {
      if(!_validForm()) {
        Helper.showError(_scaffoldKey, AppLang().errFillOut);
        return; 
      } 
      model.submitChangePass(currentPass, newPass, (message) => _showSuccess(model, message), _showError);
    }
  }

  _showSuccess(ProfileViewModel model, String message) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CloseModal(
          title: AppLang().updateSuccessTitle,
          body: Center(
            child: Text(AppLang().updatePassSuccessBody)
          ),
          onClose: () => Navigator.of(context).pop(),
        );
      }
    );
  }

  _showError(String message) {
    Helper.showError(_scaffoldKey, message);
  }

  _fieldFocusChange(BuildContext context, FocusNode currentNode, FocusNode nextNode) {
    currentNode.unfocus();
    FocusScope.of(context).requestFocus(nextNode);
  }

  @override
  void dispose() {
    _currentController.dispose();
    _newController.dispose();
    _confirmController.dispose();
    super.dispose();
  }
}
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/images.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/screens/notification/notification_page.dart';
import 'package:eight_biz_flutter/screens/profile/change_laguage.dart';
import 'package:eight_biz_flutter/screens/profile/change_password_page.dart';
import 'package:eight_biz_flutter/screens/profile/edit_profile_page.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

typedef void VoidCallback();

class ProfileDrawer extends StatelessWidget {
  final String version;
  final VoidCallback onLogout;

  ProfileDrawer({this.version = '1.0', this.onLogout});

  @override
  Widget build(BuildContext context) {
    var height = AppBar().preferredSize.height + MediaQuery.of(context).padding.top;
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            height: height,
            child: DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //Icon(Icons.settings, color: AppColors.primaryOrange),
                  SvgPicture.asset(AppImages.iconSetting),
                  Container(width: 10),
                  Text(
                    AppLang().setting,
                    style: AppStyles.titleTextCustom(color: AppColors.primaryOrange, fontWeight: FontWeight.bold),
                    //style: AppStyles.normalTextCustom(fontWeight: FontWeight.bold, color: AppColors.primaryOrange),
                  )
                ],
              )
            ),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
              Keys.navKey.currentState.pushNamed(NotificationsPage.routeName);
            },
            padding: EdgeInsets.only(
              left: 20,
              top: 5,
              bottom: 5
            ),
            highlightColor: Colors.white,
            focusColor: Colors.white,
            hoverColor: Colors.white,
            child: Row(
              children: [
                SvgPicture.asset(AppImages.iconNotification),
                Container(width: 10),
                Text(
                  AppLang().notifications,
                  style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w400),
                )
              ],
            ),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
              Keys.navKey.currentState.pushNamed(ChangePasswordPage.routeName);
            },  
            padding: EdgeInsets.only(
              left: 20,
              top: 5,
              bottom: 5
            ),
            highlightColor: Colors.white,
            focusColor: Colors.white,
            hoverColor: Colors.white,
            child: Row(
              children: [
                SvgPicture.asset(AppImages.iconChangePass),
                Container(width: 10),
                Text(
                  AppLang().changePassword,
                  style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w400),
                )
              ],
            ),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
              Keys.navKey.currentState.pushNamed(EditProfilePage.routeName);
            },
            padding: EdgeInsets.only(
              left: 20,
              top: 5,
              bottom: 5
            ),
            highlightColor: Colors.white,
            focusColor: Colors.white,
            hoverColor: Colors.white,
            child: Row(
              children: [
                SvgPicture.asset(AppImages.iconEditBlack),
                Container(width: 10),
                Text(
                  AppLang().editProfile,
                  style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w400),
                )
              ],
            ),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
              Keys.navKey.currentState.pushNamed(ChangeLanguagePage.routeName);
            },
            padding: EdgeInsets.only(
              left: 20,
              top: 5,
              bottom: 5
            ),
            highlightColor: Colors.white,
            focusColor: Colors.white,
            hoverColor: Colors.white,
            child: Row(
              children: [
                Icon(FontAwesomeIcons.language, color: Colors.black),
                Container(width: 10),
                Text(
                  AppLang().changeLanguage,
                  style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w400),
                )
              ],
            ),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
              Helper.makeLaunched(context, 'https://userportal.eightapp.com/');
            },
            padding: EdgeInsets.only(
              left: 20,
              top: 5,
              bottom: 5
            ),
            highlightColor: Colors.white,
            focusColor: Colors.white,
            hoverColor: Colors.white,
            child: Row(
              children: [
                SvgPicture.asset(AppImages.tabBizInactive),
                Container(width: 10),
                Text(
                  AppLang().bizPortal,
                  style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w400),
                )
              ],
            ),
          ),
          Divider(color: Colors.grey),
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
              if(this.onLogout != null) {
                this.onLogout();
              }
            },
            padding: EdgeInsets.only(
              left: 20,
              top: 5,
              bottom: 5
            ),
            highlightColor: Colors.white,
            focusColor: Colors.white,
            hoverColor: Colors.white,
            child: Row(
              children: [
                Text(
                  AppLang().logOut,
                  style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w400),
                )
              ],
            ),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
              Helper.makeLaunched(context, 'https://eightapp.com/');
            },
            padding: EdgeInsets.only(
              left: 20,
              top: 5,
              bottom: 5
            ),
            highlightColor: Colors.white,
            focusColor: Colors.white,
            hoverColor: Colors.white,
            child: Row(
              children: [
                Text(
                  AppLang().support,
                  style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w400),
                )
              ],
            ),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
              Helper.makeLaunched(context, 'https://eightapp.com/');
            },
            padding: EdgeInsets.only(
              left: 20,
              top: 5,
              bottom: 5
            ),
            highlightColor: Colors.white,
            focusColor: Colors.white,
            hoverColor: Colors.white,
            child: Row(
              children: [
                Text(
                  AppLang().legal,
                  style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w400),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 20, top: 40),
            child: Text(
              AppLang().version + version,
              style: AppStyles.normalTextCustom(color: Colors.black),
            )
          )
        ],
      )
    );
  }
}
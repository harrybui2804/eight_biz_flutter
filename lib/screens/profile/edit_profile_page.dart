import 'package:eight_biz_flutter/components/close_modal.dart';
import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/business_address.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/profile_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_maps_webservice/places.dart';

class EditProfilePage extends StatefulWidget {
  static const routeName = '/edit_profile';

  @override
  State<StatefulWidget> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfilePage> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _firstNameController = new TextEditingController();
  final FocusNode _firstNameFocus = FocusNode();
  final TextEditingController _lastNameController = new TextEditingController();
  final FocusNode _lastNameFocus = FocusNode();
  final TextEditingController _addressController = new TextEditingController();
  final _addressFocusNode = FocusNode();

  String firstName = '';
  String lastName = '';
  String dateOfBirth = '';
  BizAddress address;


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            title: Text(
              AppLang().editProfile,
              style: AppStyles.navTitle(),
            ),
            leading: InkWell(
              onTap: () => Navigator.pop(context),
              child: Icon(
                Icons.chevron_left, 
                color: AppColors.primaryOrange,
                size: 35,
              ),
            )
          ),
        ),
      ),
      body: LayoutBuilder(
        builder:  (context, constraints) => SafeArea(
          child: StoreConnector<AppState, ProfileViewModel> (
            onInit: (store) async {
              final model = ProfileViewModel.fromStore(store);
              final _firstName = model.userProfile != null ? model.userProfile.firstName : '';
              final _lastName = model.userProfile != null ? model.userProfile.lastName : '';
              final _dateOfBirth = model.userProfile != null ? model.userProfile.dateOfBirth : '';
              final _address = model.userProfile != null ? model.userProfile.address : null;

              _firstNameController.text = _firstName;
              _lastNameController.text = _lastName;
              _addressController.text = _address != null && _address.address1 != null 
                ? _address.address1 : '';
              setState(() {
                firstName = _firstName;
                lastName = _lastName;
                dateOfBirth = _dateOfBirth;
                address = _address;
              });

            },
            converter: (store) => ProfileViewModel.fromStore(store),
            builder: (_, viewModel) => content(viewModel)
          ),
        ),
      ),
    );
  }

  Widget content(ProfileViewModel viewModel) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Stack(
        children: <Widget>[
          viewModel.loadingEdit 
          ? SpinKitCircle(
            color: AppColors.primaryOrange,
            size: 50,
          )
          : Container(),
          Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                AppLang().firstName,
                                style: AppStyles.normalTextCustom(),
                              ),
                            ),
                            TextFormField(
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0),
                                  ),
                                  errorText: firstName.trim().length == 0
                                      ? AppLang().firstNameRequired
                                      : null),
                              controller: _firstNameController,
                              focusNode: _firstNameFocus,
                              onChanged: (value) {
                                setState(() {
                                  firstName = value.trim();
                                });
                              },
                              onFieldSubmitted: (_) {
                                _fieldFocusChange(context, _firstNameFocus, _lastNameFocus);
                              },
                              autocorrect: true,
                              textInputAction: TextInputAction.next,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                AppLang().lastName,
                                style: AppStyles.normalTextCustom(),
                              ),
                            ),
                            TextFormField(
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    borderSide: BorderSide(width: 1.0),
                                  ),
                                  errorText: lastName.trim().length == 0
                                      ? AppLang().lastNameRequired
                                      : null),
                              controller: _lastNameController,
                              focusNode: _lastNameFocus,
                              onChanged: (value) {
                                setState(() {
                                  lastName = value.trim();
                                });
                              },
                              onFieldSubmitted: (_) {
                                _lastNameFocus.unfocus();
                                _showDateOfBirthPicker();
                              },
                              autocorrect: false,
                              textInputAction: TextInputAction.next,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                AppLang().dateOfBirth,
                                style: AppStyles.normalTextCustom(),
                              ),
                            ),
                            InkWell(
                              onTap: _showDateOfBirthPicker,
                              child: Container(
                                alignment: Alignment.centerLeft,
                                width: MediaQuery.of(context).size.width,
                                height: 60,
                                padding: EdgeInsets.only(left: 10),
                                decoration: BoxDecoration(
                                    color: AppColors.whiteColor,
                                    border: Border.all(width: 1, color: AppColors.grey1Color),
                                    borderRadius:
                                      BorderRadius.all(Radius.circular(6.0))),
                                child: Text(
                                  dateOfBirth ?? ''
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                AppLang().address,
                                style: AppStyles.normalTextCustom(),
                              ),
                            ),
                            TextFormField(
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(6.0),
                                  borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(6.0),
                                  borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                                ),
                                errorText: address == null || (address.address1 != null && address.address1.trim().length == 0)
                                    ? AppLang().addressRequired
                                    : null
                              ),
                              controller: _addressController,
                              focusNode: _addressFocusNode,
                              onTap: () {
                                _searchPlace();
                              },
                              onFieldSubmitted: (_) {
                                _fieldFocusChange(context, _addressFocusNode, FocusNode());
                              },
                              autocorrect: false,
                              textInputAction: TextInputAction.next,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 40.0),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          //height: 65.0,
                          child: RaisedButton(
                            onPressed: () {
                              _submitUpdate(viewModel);
                            },
                            child: Text(
                              AppLang().save,
                              style: AppStyles.normalTextCustom(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 19),
                            ),
                            color: AppColors.primaryOrange,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(6.0),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ]
          )
        ],
      )
    );
  }

  _showDateOfBirthPicker() async {
    final now = DateTime.now();
    DateTime inital = DateTime(now.year, now.month, now.day, now.hour - 1);
    final selectedDate = await showDatePicker(
      context: context,
      initialDatePickerMode: DatePickerMode.day,
      initialDate: dateOfBirth != null && dateOfBirth.length > 0 ? DateTime.parse(dateOfBirth) : inital,
      firstDate: DateTime(1970),
      lastDate: inital,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData(
            accentColor: AppColors.primaryOrange,
            primaryColor: AppColors.primaryOrange,
          ),
          child: child,
        );
      },
    );
    if(selectedDate != null) {
      var _dateOfBirth = selectedDate.year.toString();
      _dateOfBirth += '-';
      _dateOfBirth += selectedDate.month < 10 ? ('0' + selectedDate.month.toString()) : selectedDate.month.toString();
      _dateOfBirth += '-';
      _dateOfBirth += selectedDate.day < 10 ? ('0' + selectedDate.day.toString()) : selectedDate.day.toString();
      setState(() {
        dateOfBirth = _dateOfBirth;
      });
    }
  }

  _searchPlace() async {
    var kGoogleApiKey = AppConfig().googleApiKey;
    GoogleMapsPlaces _places = new GoogleMapsPlaces(apiKey: kGoogleApiKey);
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: kGoogleApiKey,
      mode: Mode.overlay, // Mode.fullscreen
      language: "en",
      components: [new Component(Component.country, "au")]
    );
    if (p != null) {
      // get detail (lat/lng)
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;
      _addressController.text = p.description;
      setState(() {
        address = BizAddress(
          latitude: lat,
          longitude: lng,
          address1: p.description
        );
      });
    }
  }

  bool _validForm() {
    if(firstName == null || firstName.trim().length == 0) {
      return false;
    }
    if(lastName == null || lastName.trim().length == 0) {
      return false;
    }
    if(dateOfBirth == null || dateOfBirth.trim().length == 0) {
      return false;
    }
    if(address == null) {
      return false;
    }
    return true;
  }

  _submitUpdate(ProfileViewModel model) {
    FocusScope.of(context).requestFocus(FocusNode());
    if(!model.loadingEdit) {
      if(!_validForm()) {
        Helper.showError(_scaffoldKey, AppLang().errFillOut);
        return; 
      } 
      model.submitUpdate(firstName, lastName, dateOfBirth, model.userProfile.phone, address, (message) => _showSuccess(model, message), _showError);
    }
  }

  _showSuccess(ProfileViewModel model, String message) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CloseModal(
          title: AppLang().updateSuccessTitle,
          body: Center(
            child: Text(AppLang().updateProfileSuccessBody)
          ),
          onClose: () => Navigator.of(context).pop(),
        );
      }
    );
  }

  _showError(String message) {
    Helper.showError(_scaffoldKey, message);
  }

  _fieldFocusChange(BuildContext context, FocusNode currentNode, FocusNode nextNode) {
    currentNode.unfocus();
    FocusScope.of(context).requestFocus(nextNode);
  }

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _addressController.dispose();
    super.dispose();
  }
}
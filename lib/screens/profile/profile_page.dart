import 'dart:io';

import 'package:collection/collection.dart';
import 'package:eight_biz_flutter/components/custom_modal.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/images.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/user_profile.dart';
import 'package:eight_biz_flutter/models/notification.dart' as notif;
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/screens/notification/notification_page.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/profile_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:package_info/package_info.dart';
import 'package:transparent_image/transparent_image.dart';

import 'components/profile_drawer.dart';

class ProfilePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  PackageInfo packageInfo;
  String version = '1.0';
  bool loading = false;

  @override
  void initState() {
    _loadVersion();
    super.initState();
  }

  _loadVersion() async {
    packageInfo = await PackageInfo.fromPlatform();
    String _version = packageInfo.version;
    print('appversion $_version');
    setState(() {
      version = _version;
    });
  }

  _doLogout(ProfileViewModel viewModel) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CustomModal(
          title: AppLang().logOut,
          body: AppLang().askLogout,
          cancelText: AppLang().cancel.toUpperCase(),
          acceptText: AppLang().accept.toUpperCase(),
          onAccept: () => viewModel.doLogout(),
        );
      }
    );
  }

  _changeAvatar(ProfileViewModel viewModel) {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
      builder: (BuildContext bc) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 20.0),
          child: Wrap(
            children: [
              ListTile(
                leading: new Icon(Icons.camera_alt, color: Colors.grey,),
                title: new Text(AppLang().takePhoto),
                onTap: () {
                  Navigator.pop(context);
                  _getImage(viewModel, ImageSource.camera);
                },
              ),
              ListTile(
                leading: new Icon(Icons.folder, color: Colors.grey),
                title: new Text(AppLang().gallery),
                onTap: () {
                  Navigator.pop(context);
                  _getImage(viewModel, ImageSource.gallery);
                },
              )
            ],
          ),
        );
      },
    );
  }

  _getImage(ProfileViewModel model, ImageSource imageSource) async {
    final File file = await ImagePicker.pickImage(source: imageSource);
    if(file != null) {
      setState(() {
        loading = true;
      });
      model.updateAvatar(file, _uploadAvatarSuccess, _uploadAvatarError);
    }
  }

  _uploadAvatarSuccess(String message) {
    setState(() {
        loading = false;
      });
    Helper.showSuccess(context, message);
  }

  _uploadAvatarError(String message) {
    setState(() {
        loading = false;
      });
    Helper.showError(context, message);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.white, // Color for Android
      statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
    ));
    return Material(
      child: LayoutBuilder(
        builder: (context, constraints)=> Container(
          child: Container(
            color: Colors.white,
            child: StoreConnector<AppState, ProfileViewModel> (
              onInitialBuild: (model) {
                model.loadNotifs();
              },
              converter: (store) => ProfileViewModel.fromStore(store),
              builder: (_, viewModel) => buildPage(viewModel)
            )
          ),
        ),
      ),
    );
  }

  Widget buildPage(ProfileViewModel viewModel) {
    final user = viewModel.userProfile ?? UserProfile();
    final hasLocal = viewModel.localAvatar != null;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      endDrawer: ProfileDrawer(
        version: version,
        onLogout: () => _doLogout(viewModel),
        // NavNavigateToNotifications
      ),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            title: Text(
              AppLang().profileTitle,
              style: AppStyles.navTitle(),
            ),
            actions: [
              IconButton(
                icon: SvgPicture.asset(AppImages.iconSetting, height: 22),
                onPressed: () => _scaffoldKey.currentState.openEndDrawer(),
              )
            ],
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          color: AppColors.grey4Color,
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.all(20),
                      child: Center(
                        child: Column(
                          children: [
                            InkWell(
                              onTap: () {
                                if(!loading) {
                                  _changeAvatar(viewModel);
                                }
                              },
                              child: Container(
                                width: 100.0,
                                height: 100.0,
                                decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: new Border.all(
                                    color: AppColors.primaryOrange,
                                    width: 2.5,
                                  ),
                                ),
                                child: ClipOval(
                                  child: hasLocal 
                                  ? Image.file(viewModel.localAvatar, fit: BoxFit.fill)
                                  : (user != null && user.imageUrl != null && user.imageUrl.length > 0) 
                                    ? FadeInImage.memoryNetwork(
                                        placeholder: kTransparentImage,
                                        image: user.imageUrl,
                                        fit: BoxFit.fill,
                                      ) : Container(),
                                ),
                              ),
                            ),
                            
                            Container(
                              child: Center(
                                child: Text(
                                  user.firstName + ' ' + user.lastName,
                                  style: AppStyles.bigTextCustom(fontSize: 20),
                                ),
                              ),
                              margin: EdgeInsets.only(bottom: 5),
                            ),
                            InkWell(
                              highlightColor: Colors.white,
                              focusColor: Colors.white,
                              hoverColor: Colors.white,
                              splashColor: Colors.white,
                              onTap: () {
                                if(!loading) {
                                  viewModel.editProfile();
                                }
                              },
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(AppImages.iconEditBlack),
                                  Container(width: 5),
                                  Text(
                                    AppLang().editProfile,
                                    style: AppStyles.normalTextCustom(
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                      fontSize: 15
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width,
                                height: 45.0,
                                child: RaisedButton(
                                  onPressed: () {
                                    if(!loading) {
                                      viewModel.switchBusiness(viewModel.business.id);
                                    }
                                  },
                                  child: Text(
                                    AppLang().switchBiz,
                                    style: AppStyles.titleTextCustom(color: Colors.white),
                                  ),
                                  color: AppColors.primaryOrange,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ),
                    _buildNotifs(viewModel),
                  ],
                ),
              ),
              loading ? SpinKitCircle(
                size: 50,
                color: AppColors.primaryOrange
              ) : Container()
            ],
          ),
        ),
      )
    );
  }

  Widget _buildNotifs(ProfileViewModel model) {
    return Container(
      color: AppColors.grey4Color,
      alignment: Alignment.topLeft,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppLang().notifications,
                  style: AppStyles.headingTextCustom(color: Colors.black),
                ),
                InkWell(
                  onTap: () {
                    if(!loading) {
                      Keys.navKey.currentState.pushNamed(NotificationsPage.routeName);
                    }
                  },
                  child: Text(
                    AppLang().seeAll,
                    style: AppStyles.titleTextCustom(color: AppColors.primaryOrange),
                  ),
                )
              ],
            ),
          ),
          _listNotifs(model)
        ],
      ),
    );
  }

  Widget _listNotifs(ProfileViewModel model) {
    List<notif.Notification> lst = model.notifications ?? [];
    var newMap = groupBy(lst, (notif.Notification obj){
      var date = DateTime.parse(obj.createdAt);
      var strDate = date.year.toString();
      strDate += '-';
      strDate += date.month < 10 ? '0' + date.month.toString() : date.month.toString();
      strDate += '-';
      strDate += date.day < 10 ? '0' + date.day.toString() : date.day.toString();
      return strDate;
    });
    var keys = newMap.keys.toList();
    keys.sort((a, b) => DateTime.parse(b).compareTo(DateTime.parse(a)));
    return Column(
      children: keys.map((key) {
        var childs = newMap[key];
        DateTime date = DateTime.parse(key);
        var title = date.day < 10 ? '0' + date.day.toString() : date.day.toString();
        title += ' ' + Helper.getMonthName(date.month);
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //header
            Container(
              padding: EdgeInsets.only(
                top: 10, 
                bottom: 10,
                left: 20,
                right: 20
              ),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1, color: AppColors.grey3Color)
                )
              ),
              height: 45,
              alignment: Alignment.bottomLeft,
              child: Text(
                title,
                style: AppStyles.titleTextCustom(fontWeight: FontWeight.w400),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: childs.map((notif) {
                return Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      bottom: BorderSide(width: 1, color: AppColors.grey3Color)
                    )
                  ),
                  padding: EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        notif.title,
                        style: AppStyles.titleTextCustom(),
                      ),
                      Container(height: 8),
                      Wrap(
                        children: [
                          Text(
                            notif.text,
                            textAlign: TextAlign.left
                          ),
                        ],
                      )
                    ],
                  ),
                );
              }).toList(),
            )
          ],
        );
      }).toList()
    );
  }
}
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/screens/biz/components/paging_dot.dart';
import 'package:flutter/material.dart';

typedef void OnPressedCallback();

class IntroBottom extends StatelessWidget {
  final int dotIndex;
  final OnPressedCallback onSkip;
  final OnPressedCallback onDone;

  IntroBottom({
    this.dotIndex,
    this.onSkip,
    this.onDone,
  }) {
    assert(dotIndex != null);
  }

  @override
  Widget build(BuildContext context) {
    if(dotIndex < 2) {
      return Container(
        height: 100,
        padding: EdgeInsets.all(20),
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                bottom: 10
              ),
              child: PagingDot(
                color: AppColors.primaryOrange, 
                index: dotIndex + 1, 
                borderWidth: 1.5,
                length: 3,
                size: 20
              ),
            ),
            Positioned(
              top: 25,
              right: 0,
              width: 40,
              height: 35,
              child: InkWell(
                onTap: () {
                  if(onSkip != null) {
                    onSkip();
                  }
                },
                child: Text(
                  'Skip',
                  style: AppStyles.normalTextCustom(color: Colors.black),
                ),
              ),
            )
          ],
        ) 
      );
    }
    return Container(
      height: 100,
      padding: EdgeInsets.only(
        top: 25,
        bottom: 25,
        left: 20,
        right: 20
      ),
      child: Container(
        height: 45,
        child: RaisedButton(
          onPressed: onDone,
          color: AppColors.primaryOrange,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(6)
            ),
            side: BorderSide(
              color: AppColors.primaryOrange
            )
          ),
          child: Center(
            child: Text(
              'Get started',
              style: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontWeight: FontWeight.w500
              ),
            ),
          ),
        ),
      ),
    ) ;
  }
}
import 'package:carousel_slider/carousel_slider.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/images.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/redux/keys.dart';
import 'package:eight_biz_flutter/screens/auth/get_started.dart';
import 'package:flutter/material.dart';

import 'intro_bottom.dart';

class WalkThroughPage extends StatefulWidget {
  static const routeName = '/walk_through';

  @override
  State<StatefulWidget> createState() => _WalkThroughState();
}

class _WalkThroughState extends State<WalkThroughPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int pageIndex = 0;

  @override
  Widget build(BuildContext context) {
    final carousel = _carouselSlider(context);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: 40,
                bottom: 20
              ),
              child: Center(
                child: Image.asset(AppImages.logoAppOrange, fit: BoxFit.contain,),
              ),
            ),
            Expanded(
              child: carousel,
            ),
            IntroBottom(
              dotIndex: pageIndex,
              onSkip: () {
                carousel.nextPage(duration: Duration(milliseconds: 300), curve: Curves.linear);
              },
              onDone: () {
                Keys.navKey.currentState.pushNamedAndRemoveUntil(GetStarterdPage.routeName, (Route<dynamic> route) => false);
              },
            )
          ],
        ),
      ),
    );
  }

  CarouselSlider _carouselSlider(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 40;
    return CarouselSlider(
      height: width + width * 0.3,
      reverse: false,
      viewportFraction: 1.0,
      enableInfiniteScroll: false,
      items: [
        _buildIntro(0),
        _buildIntro(1),
        _buildIntro(2),
      ],
      onPageChanged: (index) {
        setState(() {
          pageIndex = index;
        });
      },
    );
  }

  Widget _buildIntro(int index) {
    double width = MediaQuery.of(context).size.width - 40;
    var title = 'Locate';
    var underlineWidth = 30.0;
    var description = 'Discover businesses near you and save with the latest deals on Eight.';
    var image = Image.asset(AppImages.introLocate);
    if(index == 1) {
      title = 'Spend and shop';
      underlineWidth = 60.0;
      description = 'Pay instantly through Eight and get rewarded with Smiles :)';
      image = Image.asset(AppImages.introSpend);
    }
    if(index == 2) {
      title = 'Get cashback';
      underlineWidth = 40.0;
      description = 'Smiles get converted to cash, which you will receive every 8 days, based on your activity.';
      image = Image.asset(AppImages.introCashback);
    }
    return Container(
      width: width,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.4),
            spreadRadius: 5,
            blurRadius: 5,
            offset: Offset(0, 5), // changes position of shadow
          ),
        ],
      ),
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.only(
          top: 20
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  Text(
                    title,
                    style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600, fontSize: 20),
                  ),
                  Container(
                    width: underlineWidth,
                    height: 5,
                    color: AppColors.strongCyan,
                  ),
                  image,
                  Container(
                    //width: 280,
                    padding: EdgeInsets.only(
                      top: 00,
                      left: 40,
                      right: 40
                    ),
                    child: Center(
                      child: Text(
                        description,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              ),
            )
            
          ],
        ),
      ),
    ); 
  }

  /*
  Widget _buildIntro(int index) {
    double width = MediaQuery.of(context).size.width - 40;
    var title = 'Locate';
    var underlineWidth = 30.0;
    var description = 'Discover businesses near you and save with the latest deals on Eight.';
    if(index == 1) {
      title = 'Spend and shop';
      underlineWidth = 60.0;
      description = 'Pay instantly through Eight and get rewarded with Smiles :)';
    }
    if(index == 2) {
      title = 'Get cashback';
      underlineWidth = 40.0;
      description = 'Smiles get converted to cash, which you will receive every 8 days, based on your activity.';
    }
    return Column(
      children: <Widget>[
        Expanded(
          child: Container(
            width: width,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.4),
                  spreadRadius: 5,
                  blurRadius: 5,
                  offset: Offset(0, 5), // changes position of shadow
                ),
              ],
            ),
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.only(
                top: 40
              ),
              child: Column(
                children: <Widget>[
                  Text(
                    title,
                    style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600, fontSize: 20),
                  ),
                  Container(
                    width: underlineWidth,
                    height: 5,
                    color: AppColors.strongCyan,
                  ),
                  Image.asset(AppImages.introLocate),
                  Container(
                    width: 250,
                    padding: EdgeInsets.only(
                      top: 20,
                      left: 40,
                      right: 40
                    ),
                    child: Center(
                      child: Text(
                        description,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        )
      ],
    ); 
  }
  */
}
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_bank.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

typedef void VoidCallback(double amount, String remark);

class ConfirmTransferModal extends StatefulWidget {
  final BizBank bank;
  final double totalBalance;
  final VoidCallback onConfirm;

  ConfirmTransferModal({@required this.bank, @required this.totalBalance, this.onConfirm});
  
  @override
  State<StatefulWidget> createState() => _ModelState();
}

class _ModelState extends State<ConfirmTransferModal> {
  final ScrollController _scrollController = ScrollController();
  final TextEditingController _amountController = new TextEditingController();
  final TextEditingController _remarkController = new TextEditingController();
  final _remarkFocus = FocusNode();

  String amount = '';
  String remark = '';

  bool validForm() {
    if(widget.totalBalance <= 0) {
      return false;
    }
    if(widget.bank == null) {
      return false;
    }
    return true;
  }

  _onConfirm() {
    if(validForm()) {
      Navigator.of(context).pop();
      final amount = widget.totalBalance;
      final remark = _remarkController.text;
      if(widget.onConfirm != null) {
        widget.onConfirm(amount, remark);
      }
    }
  }

  @override
  void dispose() {
    _amountController.dispose();
    _remarkController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      //contentPadding: EdgeInsets.only(left: 20, right: 20),
      title: Center(
        child: Text(
          AppLang().getCashOut,
          style: AppStyles.headingTextCustom(),
        ),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0))
      ),
      content: _buildbody(),
    );
    /*
    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: Container(
        width: width,
        padding: EdgeInsets.all(20),
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(0),
              child: Center(
                child: Text(
                  AppLang().getCashOut,
                  style: AppStyles.headingTextCustom(),
                ),
              ),
            ),
            _buildbody()
          ],
        ),
      )
    );
    */
  }

  Widget _buildbody() {
    final bank = widget.bank;
    final totalBalance = widget.totalBalance;
    var strBalance = totalBalance != null && totalBalance > 0 ? NumberFormat('###,###.##', 'en_US').format(totalBalance) : '0.00';
    if(strBalance.indexOf('.') < 0) {
      strBalance += '.00';
    } else {
      if(strBalance.substring(strBalance.indexOf('.') + 1).length == 0) {
        strBalance += '00';
      } else if(strBalance.substring(strBalance.indexOf('.') + 1).length == 1) {
        strBalance += '0';
      }
    }
    return SingleChildScrollView(
      controller: _scrollController,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              AppLang().bankDetails,
              style: AppStyles.titleTextCustom(color: Colors.black),
            ),
          ),
          Wrap(
            children: <Widget>[
              Text(
                '${AppLang().bankName}: ',
                style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
              ),
              Text(
                bank != null && bank.bankName != null 
                ? bank.bankName : '',
                style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600),
              ),
            ],
          ),
          Container(height: 10),
          Wrap(
            children: <Widget>[
              Text(
                '${AppLang().holderName}: ',
                style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
              ),
              Text(
                bank != null && bank.accountName != null
                ? bank.accountName : '',
                style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600),
              ),
            ],
          ),
          Container(height: 10),
          Wrap(
            children: <Widget>[
              Text(
                '${AppLang().last4Number}: ',
                style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
              ),
              Text(
                bank != null && bank.last4Number != null
                ? bank.last4Number : '',
                style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600),
              ),
            ],
          ),
          Container(height: 10),
          Wrap(
            children: <Widget>[
              Text(
                '${AppLang().bsbNumber}: ',
                style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
              ),
              Text(
                bank != null && bank.bsb != null
                ? bank.bsb : '',
                style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600),
              ),
            ],
          ),
          Container(height: 10),
          Wrap(
            children: <Widget>[
              Text(
                '${AppLang().amount}: ',
                style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
              ),
              Text(
                '\$$strBalance',
                style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              AppLang().remark,
              style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
            ),
          ),
          TextFormField(
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(6.0),
                borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(6.0),
                borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
              )
            ),
            controller: _remarkController,
            focusNode: _remarkFocus,
            onTap: () {
              _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
            },
            onChanged: (value) {
              remark = value;
            },
            onFieldSubmitted: (_) {
              _onConfirm();
            },
            autocorrect: true,
            minLines: 3,
            maxLines: 3,
            textInputAction: TextInputAction.done,
          ),
          /*
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Text(
                    AppLang().bankDetails,
                    style: AppStyles.titleTextCustom(color: Colors.black),
                  ),
                ),
                Wrap(
                  children: <Widget>[
                    Text(
                      '${AppLang().bankName}: ',
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
                    ),
                    Text(
                      bank != null && bank.bankName != null 
                      ? bank.bankName : '',
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
                Container(height: 10),
                Wrap(
                  children: <Widget>[
                    Text(
                      '${AppLang().holderName}: ',
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
                    ),
                    Text(
                      bank != null && bank.accountName != null
                      ? bank.accountName : '',
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
                Container(height: 10),
                Wrap(
                  children: <Widget>[
                    Text(
                      '${AppLang().last4Number}: ',
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
                    ),
                    Text(
                      bank != null && bank.last4Number != null
                      ? bank.last4Number : '',
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
                Container(height: 10),
                Wrap(
                  children: <Widget>[
                    Text(
                      '${AppLang().bsbNumber}: ',
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
                    ),
                    Text(
                      bank != null && bank.bsb != null
                      ? bank.bsb : '',
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
                Container(height: 10),
                Wrap(
                  children: <Widget>[
                    Text(
                      '${AppLang().amount}: ',
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
                    ),
                    Text(
                      '\$$strBalance',
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Text(
                    AppLang().remark,
                    style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
                  ),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(6.0),
                      borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(6.0),
                      borderSide: BorderSide(width: 1.0, color: AppColors.strongCyan),
                    )
                  ),
                  controller: _remarkController,
                  focusNode: _remarkFocus,
                  onChanged: (value) {
                    remark = value;
                  },
                  onFieldSubmitted: (_) {
                    _onConfirm();
                  },
                  autocorrect: true,
                  minLines: 3,
                  maxLines: 3,
                  textInputAction: TextInputAction.done,
                ),
              ],
            ),
          ),
          */
          Container(
            padding: EdgeInsets.only(
              top: 10,
              bottom: 10,
              left: 20,
              right: 20
            ),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                  color: Colors.grey,
                  elevation: 0,
                  highlightElevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                    side: BorderSide.none
                  ),
                  child: Center(
                    child: Text(
                      AppLang().cancel.toUpperCase(),
                      style: TextStyle(
                        color: AppColors.whiteColor,
                        fontSize: 15,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                ),
                Container(width: 10),
                RaisedButton(
                  onPressed: validForm() ? (){
                    _onConfirm();
                  }: null,
                  color: AppColors.primaryOrange,
                  highlightColor: AppColors.primaryOrange,
                  elevation: 0,
                  highlightElevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
                  child: Center(
                    child: Text(
                      AppLang().accept.toUpperCase(),
                      style: TextStyle(
                        color: AppColors.whiteColor,
                        fontSize: 15,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/enums.dart';
import 'package:eight_biz_flutter/view_models/wallet_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';

typedef void OnAcceptCallback(String fromDate, String toDate, EarningOrderFilter ordering);
typedef void OnResetCallback();

class FilterModal extends StatefulWidget {
  final WalletViewModel model;
  final OnAcceptCallback onApply;
  final OnResetCallback onReset;

  FilterModal({
    Key key,
    @required this.model,
    @required this.onApply,
    @required this.onReset
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ModelState();
}

class _ModelState extends State<FilterModal> {
  
  String fromDate;
  String toDate;
  EarningOrderFilter orderFilter;
  //EarningStatusFilter statusFilter;
  //EarningTransactionFilter transactionFilter;

  @override
  void initState() {
    super.initState();
    var _fromDate = fromDate;
    var _toDate = toDate;
    var _orderFilter = orderFilter;
    //var _statusFilter = statusFilter;
    //var _transactionFilter = transactionFilter;

    if(widget.model.fromDate != null || widget.model.toDate != null) {
      _fromDate = widget.model.fromDate;
      _toDate = widget.model.toDate;
    }
    _orderFilter = widget.model.orderFilter ?? EarningOrderFilter.modifiedAtDsc;
    //_statusFilter = widget.model.statusFilter;
    //_transactionFilter = widget.model.transactionFilter;
    setState(() {
      fromDate = _fromDate;
      toDate = _toDate;
      orderFilter = _orderFilter;
      //statusFilter = _statusFilter;
      //transactionFilter = _transactionFilter;
    });
  }

  _onConfirm() {
    if(widget.onApply != null) {
      widget.onApply(
        fromDate, toDate, orderFilter
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width - 40;
    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: Container(
        width: width,
        padding: EdgeInsets.all(20),
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    AppLang().modalFilterTitle,
                    style: AppStyles.titleTextCustom(),
                  ),
                  IconButton(
                    icon: Icon(Icons.close, color: Colors.black),
                    onPressed: () => Navigator.pop(context),
                  )
                ],
              ),
            ),
            SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Text(
                      AppLang().timePeriod,
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w400, fontSize: 16)
                    )
                  ),
                  RaisedButton(
                    color: Colors.white,
                    highlightColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                      side: BorderSide(color: AppColors.strongCyan, width: 1)
                    ),
                    onPressed: (){
                      _showPickerDateRange();
                    },
                    child: Container(
                      width: width - 80,
                      height: 45,
                      padding: EdgeInsets.only(top:10, bottom: 10),
                      child: fromDate == null && toDate == null
                      ? Text(
                          AppLang().selectTimePeriod, 
                          style: AppStyles.normalTextCustom(fontWeight: FontWeight.w400, fontSize: 16)
                        )
                      : Text(
                          fromDate + '  -  ' + toDate, 
                          style: AppStyles.normalTextCustom(fontWeight: FontWeight.w400, fontSize: 16)
                        ),
                    )
                  ),
                  Container(
                    height: 1,
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    color: AppColors.grey2Color,
                  ),
                  Container(
                    padding: EdgeInsets.all(0),
                    child: Text(
                      AppLang().sort,
                      style: AppStyles.normalTextCustom(fontWeight: FontWeight.w400, fontSize: 16)
                    )
                  ),
                  RadioListTile(
                    title: Text("${AppLang().amount}: ${AppLang().highestFirst}"),
                    groupValue: orderFilter,
                    activeColor: AppColors.strongCyan,
                    value: EarningOrderFilter.amountDsc,
                    onChanged: (val) {
                      setState(() {
                        orderFilter = EarningOrderFilter.amountDsc;
                      });
                    },
                  ),
                  RadioListTile(
                    title: Text("${AppLang().amount}: ${AppLang().lowestFirst}"),
                    groupValue: orderFilter,
                    activeColor: AppColors.strongCyan,
                    value: EarningOrderFilter.amountAsc,
                    onChanged: (val) {
                      setState(() {
                        orderFilter = EarningOrderFilter.amountAsc;
                      });
                    },
                  ),
                  RadioListTile(
                    title: Text("${AppLang().time}: ${AppLang().oldestFirst}"),
                    groupValue: orderFilter,
                    activeColor: AppColors.strongCyan,
                    value: EarningOrderFilter.modifiedAtDsc,
                    onChanged: (val) {
                      setState(() {
                        orderFilter = EarningOrderFilter.modifiedAtDsc;
                      });
                    },
                  ),
                  RadioListTile(
                    title: Text("${AppLang().time}: ${AppLang().newestFirst}"),
                    groupValue: orderFilter,
                    activeColor: AppColors.strongCyan,
                    value: EarningOrderFilter.modifiedAtAsc,
                    onChanged: (val) {
                      setState(() {
                        orderFilter = EarningOrderFilter.modifiedAtAsc;
                      });
                    },
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: Container(
                width: 160,
                child: RaisedButton(
                  onPressed: (){
                    Navigator.of(context).pop();
                    _onConfirm();
                  },
                  color: AppColors.primaryOrange,
                  highlightColor: AppColors.primaryOrange,
                  elevation: 0,
                  highlightElevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
                  child: Center(
                    child: Text(
                      AppLang().buttonApplyFilter,
                      style: TextStyle(
                        color: AppColors.whiteColor,
                        fontSize: 15,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                ),
              )
            ),
            Container(
              alignment: Alignment.center,
              child: Container(
                width: 160,
                child: RaisedButton(
                  onPressed: (){
                    Navigator.of(context).pop();
                    if(widget.onReset != null) {
                      widget.onReset();
                    }
                  },
                  color: Colors.white,
                  elevation: 0,
                  highlightElevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                    side: BorderSide(width: 1, color: Colors.black)
                  ),
                  child: Center(
                    child: Text(
                      AppLang().buttonResetFilter,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                ),
              )
            )
          ],
        ),
      )
    );
  }

  _showPickerDateRange() {
    List<String> months = [
      AppLang().january,
      AppLang().february,
      AppLang().march,
      AppLang().april,
      AppLang().may,
      AppLang().june,
      AppLang().june,
      AppLang().july,
      AppLang().august,
      AppLang().september,
      AppLang().october,
      AppLang().november,
      AppLang().december
    ];
    Picker ps = new Picker(
      hideHeader: true,
      adapter: new DateTimePickerAdapter(
        type: PickerDateTimeType.kYMD,
        months: months,
        yearBegin: 1900,
        maxValue: DateTime.now()
      ),
      selecteds: [7,0,0]
    );

    Picker pe = new Picker(
      hideHeader: true,
      adapter: new DateTimePickerAdapter(
        type: PickerDateTimeType.kYMD,
        months: months,
        yearBegin: 1900,
        maxValue: DateTime.now()
      ),
      selecteds: [9, 0, 1],
    );
    List<Widget> actions = [
      FlatButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: new Text(AppLang().cancel)
      ),
      FlatButton(
        onPressed: () {
          final fromYear = ps.selecteds[0] + 1900;
          final fromMonth = ps.selecteds[1] + 1;
          final fromDay = ps.selecteds[2] + 1;
          var _fromDate = fromYear.toString();
          _fromDate += '-';
          _fromDate += fromMonth < 10 ? ('0' + fromMonth.toString()) : fromMonth.toString();
          _fromDate += '-';
          _fromDate += fromDay < 10 ? ('0' + fromDay.toString()) : fromDay.toString();

          final toYear = pe.selecteds[0] + 1900;
          final toMonth = pe.selecteds[1] + 1;
          final toDay = pe.selecteds[2] + 1;
          var _toDate = toYear.toString();
          _toDate += '-';
          _toDate += toMonth < 10 ? ('0' + toMonth.toString()) : toMonth.toString();
          _toDate += '-';
          _toDate += toDay < 10 ? ('0' + toDay.toString()) : toDay.toString();
          if(_validPicker(ps, pe)) {
            Navigator.pop(context);
            setState(() {
              fromDate = _fromDate;
              toDate = _toDate;
            });
          }
        },
        child: new Text(AppLang().confirm)
      )
    ];

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return new AlertDialog(
          title: Text(AppLang().selectTimePeriod),
          actions: actions,
          content: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text("${AppLang().from}:"),
                ps.makePicker(),
                Text("${AppLang().to}:"),
                pe.makePicker(),
                !_validPicker(ps, pe) ?
                Container(
                  padding: EdgeInsets.only(top:8),
                  child: Text(
                    AppLang().errDateFromTo,
                    style: TextStyle(
                      color: Colors.red,
                      fontStyle: FontStyle.italic
                    ),
                  ),
                ): Container()
              ],
            ),
          ),
        );
      }
    );
  }

  bool _validPicker(Picker ps, Picker pe) {
    final fromYear = ps.selecteds[0];
    final fromMonth = ps.selecteds[1];
    final fromDay = ps.selecteds[2];

    final toYear = pe.selecteds[0];
    final toMonth = pe.selecteds[1];
    final toDay = pe.selecteds[2];
    if(fromYear > toYear) {
      return false;
    }
    if(fromYear == toYear) {
      if(fromMonth > toMonth) {
        return false;
      }
      if(fromMonth == toMonth && fromDay > toDay) {
        return false;
      }
    }
    return true;
  }
}
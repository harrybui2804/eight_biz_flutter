import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/models/transactions_graph.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class LineChartWidget extends StatefulWidget {
  final List<TransactionsGraph> data;
  final bool canShow;
  
  LineChartWidget({@required this.data, this.canShow = true});

  @override
  State<StatefulWidget> createState() => _LineChartState();
}

class _LineChartState extends State<LineChartWidget> {
  final List<int> showIndexes = const [1, 3, 5];

  List<Color> gradientColors = [
    const Color(0xff02d39a),
    const Color(0xff02d39a),
  ];

  List<FlSpot> _buildData() {
    List<FlSpot> lst = [];
    final list = widget.data;
    if(list == null || list.length == 0) {
      for(int i=0; i<8; i++) {
        lst.add(
          FlSpot(i.toDouble(), 0),
        );
      }
    } else {
      list.sort((a, b) => a.date.compareTo(b.date));
      for(var i=0; i< list.length; i++) {
        var total = 0.0;
        total += list[i].bizEarning ?? 0;
        total += list[i].userTransfer ?? 0;
        total += list[i].withDraw ?? 0;
        lst.add(
          FlSpot(i.toDouble(), total),
        );
      }
    }
    return lst;
  }

  double _getMax() {
    if(widget.data == null) {
      return 0.0;
    }
    final list = List.from(widget.data);
    if(list == null || list.length == 0) {
      return 0;
    } else {
      list.sort((a, b) {
        var totala = 0.0;
        totala += a.bizEarning ?? 0;
        totala += a.userTransfer ?? 0;
        totala += a.withDraw ?? 0;
        var totalb = 0.0;
        totalb += b.bizEarning ?? 0;
        totalb += b.userTransfer ?? 0;
        totalb += b.withDraw ?? 0;
        return totala.compareTo(totalb);
      });
      final last = list[list.length - 1];
      var total = 0.0;
      total += last.bizEarning ?? 0;
      total += last.userTransfer ?? 0;
      total += last.withDraw ?? 0;
      return total;
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 40;
    if(!widget.canShow) {
      return SizedBox(
        width: width,
        height: width/2 - 60,
        child: Center(
          child: Icon(Icons.lock),
        ),
      );
    }
    return SizedBox(
      width: width,
      height: width/2 - 60,
      child: _getMax() > 0 
      ? LineChart(
          mainData(),
        )
      : Center(
        child: Text(
          AppLang().noEarning,
          style: TextStyle(
            fontStyle: FontStyle.italic
          ),
        ),
      ),
    );
  }

  LineChartData mainData() {
    return LineChartData(
      gridData: FlGridData(
        show: false,
        drawVerticalGrid: false
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: false,
          reservedSize: 0,
          textStyle: TextStyle(
              color: const Color(0xff68737d),
              fontWeight: FontWeight.bold,
              fontSize: 16),
          getTitles: (value) {
            switch (value.toInt()) {
              case 0:
                return AppLang().mondayShort;
              case 1:
                return AppLang().tuesdayShort;
              case 2:
                return AppLang().wednesdayShort;
              case 3:
                return AppLang().thursdayShort;
              case 4:
                return AppLang().fridayShort;
              case 5:
                return AppLang().saturdayShort;
              case 6:
                return AppLang().sundayShort;
              case 7:
                return AppLang().mondayShort;
            }
            return '';
          },
          margin: 0,
        ),
        leftTitles: SideTitles(
          showTitles: false,
          reservedSize: 0,
          margin: 0,
        ),
      ),
      borderData: FlBorderData(
          show: false,
          border: Border.all(color: const Color(0xff37434d), width: 1)),
      minY: 0,
      lineBarsData: [
        LineChartBarData(
          spots: _buildData(),
          isCurved: true,
          colors: gradientColors,
          barWidth: 1,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            colors:
                gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }

  
}
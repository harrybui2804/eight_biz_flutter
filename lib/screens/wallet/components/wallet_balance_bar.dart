import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/view_models/wallet_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';

class WalletBlanceBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(right: 20),
      child: LayoutBuilder(
        builder: (context, constraints)=> Container(
          child: Container(
            color: Colors.white,
            child: StoreConnector<AppState, WalletViewModel> (
              converter: (store) => WalletViewModel.fromStore(store),
              builder: (_, viewModel) {
                var balance = 0.0;
                if(viewModel.balance != null && viewModel.balance.totalBalance != null) {
                  balance = viewModel.balance.totalBalance;
                }
                var strBalance = balance > 0 ? NumberFormat('###,###.##', 'en_US').format(balance) : '0.00';
                if(strBalance.indexOf('.') < 0) {
                  strBalance += '.00';
                } else {
                  if(strBalance.substring(strBalance.indexOf('.') + 1).length == 0) {
                    strBalance += '00';
                  } else if(strBalance.substring(strBalance.indexOf('.') + 1).length == 1) {
                    strBalance += '0';
                  }
                }
                return Text(
                  '\$$strBalance',
                  style: AppStyles.navTitle(color: Colors.black),
                );
              }
            )
          ),
        ),
      ),
    );
  }
}


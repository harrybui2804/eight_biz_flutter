import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/screens/wallet/components/confirm_transfer_modal.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/wallet_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';

import 'components/line_chart.dart';

class DashboardPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _DashboardState();
}

class _DashboardState extends State<DashboardPage> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool loading = false;

  _loadWalletDetail(WalletViewModel model) {
    model.loadWalletDetail(_loadWalletSuccess, _onError);
  }

  _loadWalletSuccess(String _) {
    if(mounted) {
      setState(() {
        loading = false;
      });
    }
  }

  _onError(String message) {
    setState(() {
      loading = false;
    });
    Helper.showError(context, message);
  }

  _getCashout(WalletViewModel model) {
    if(loading) {
      return;
    }
    _showCashoutModal(model);
    /*
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
      builder: (BuildContext bc) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 20.0),
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: new Icon(FontAwesomeIcons.wallet),
                title: new Text(AppLang.walletModalTitle),
                onTap: () async {
                  Navigator.pop(context);
                  _showCashoutModal(model, TransferType.towallet);
                },
              ),
              ListTile(
                leading: new Icon(FontAwesomeIcons.creditCard),
                title: new Text(AppLang.withDrawModalTitle),
                onTap: () async {
                  Navigator.pop(context);
                  _showCashoutModal(model, TransferType.withdraw);
                },
              )
            ],
          ),
        );
      },
    );
    */
  }

  _showCashoutModal(WalletViewModel model) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return ConfirmTransferModal(
          bank: model.bank,
          totalBalance: model.balance != null ? model.balance.totalBalance : null,
          onConfirm: (double amount, String remark) {
            _confirmCashout(model, amount, remark);
          },
        );
      }
    );
  }

  _confirmCashout(WalletViewModel model, double amount, String remark) {
    setState(() {
      loading = true;
    });
    model.requestCashout(amount, remark, _cashoutSuccess, _onError);
  }

  _cashoutSuccess(String message) {
    setState(() {
      loading = false;
    });
    Helper.showSuccess(context, message);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: LayoutBuilder(
        builder: (context, constraints)=> SafeArea(
          child: Container(
            color: Colors.white,
            child: StoreConnector<AppState, WalletViewModel> (
              onInitialBuild: (model) {
                _loadWalletDetail(model);
                model.loadBizBank(null, null);
              },
              converter: (store) => WalletViewModel.fromStore(store),
              builder: (_, viewModel) => _buildContent(viewModel)
            )
          ),
        ),
      ),
    );
  }

  Widget _buildContent(WalletViewModel model) {
    double width = MediaQuery.of(context).size.width - 40;
    final showBalance = model.permissions.where((per) => per.role == BusinessRole.getBizWallet && per.isGranted).toList().length > 0;
    final getCash = model.permissions.where((per) => per.role == BusinessRole.getWalletTransfer && per.isGranted).toList().length > 0;
    var balance = 0.0;
    var earningData;
    if(model.balance != null && model.balance.totalBalance != null) {
      balance = model.balance.totalBalance;
    }
    if(model.balance != null && model.balance.currentWeekGraph != null) {
      earningData = model.balance.currentWeekGraph;
    }
    var strBalance = balance > 0 ? NumberFormat('###,###.##', 'en_US').format(balance) : '0.00';
    if(strBalance.indexOf('.') < 0) {
      strBalance += '.00';
    } else {
      if(strBalance.substring(strBalance.indexOf('.') + 1).length == 0) {
        strBalance += '00';
      } else if(strBalance.substring(strBalance.indexOf('.') + 1).length == 1) {
        strBalance += '0';
      }
    }
    var hideBalance = '';
    for(var i=0; i< strBalance.length; i++) {
      if(strBalance[i] == ',' || strBalance[i] == '.') {
        hideBalance += strBalance[i];
      } else {
        hideBalance += '*';
      }
    }
    return Stack(
      children: <Widget>[
        Container(
          color: AppColors.grey4Color,
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Expanded(
                child: Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(
                        bottom: 20
                      ),
                      child: Wrap(
                        children: <Widget>[
                          Text(
                            '${AppLang().currentBalance}  ',
                            style: AppStyles.headingTextCustom(),
                          ),
                          Text(
                            showBalance ? '\$$strBalance' : '\$$hideBalance',
                            style: AppStyles.headingTextCustom(color: AppColors.primaryOrange),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: width,
                      height: width/2,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        boxShadow: [
                          BoxShadow(
                            color: AppColors.grey2Color,
                            offset: Offset(2.0, 2.0),
                            blurRadius: 10
                          )
                        ],
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 60,
                            padding: EdgeInsets.all(20),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              AppLang().earnThisMonth,
                              style: AppStyles.titleTextCustom(fontWeight: FontWeight.w400),
                            ),
                          ),
                          LineChartWidget(
                            data: earningData,
                            canShow: showBalance,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              getCash
              ? Container(
                padding: EdgeInsets.only(
                  top: 20,
                ),
                child: RaisedButton(
                  onPressed: balance > 0 ? () {
                    _getCashout(model);
                  } : null,
                  child: Container(
                    height: 45,
                    child: Center(
                      child: Text(
                        AppLang().getCashOut,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  color: AppColors.primaryOrange,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                ),
              ) : Container()
            ],
          ),
        ),
        loading ? Positioned( 
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.black.withOpacity(0.05),
            alignment: Alignment.center,
            child: SpinKitCircle(
              size: 50,
              color: AppColors.primaryOrange,
            ),
          ),
        ) : Container()
      ],
    );
  }
}
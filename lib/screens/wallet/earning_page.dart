import 'package:collection/collection.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/enums.dart';
import 'package:eight_biz_flutter/models/wallet_transaction.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/screens/wallet/components/filter_modal.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/wallet_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';

class EarningPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _EarningPageState();
}

class _EarningPageState extends State<EarningPage> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final scrollController = ScrollController(initialScrollOffset: 0);

  bool loading = false;
  bool loadMore = false;

  void _decrementMonth(WalletViewModel model) {
    DateTime now = DateTime.now();
    var newMonth = model.selectedMonth > 1 ? model.selectedMonth - 1 : now.month;
    int lastday = DateTime(now.year, newMonth + 1, 0).day;
    var fromDate = now.year.toString();
    fromDate += '-';
    fromDate += newMonth < 10 ? '0' + newMonth.toString() : newMonth.toString();
    fromDate += '-01';
    var toDate = now.year.toString();
    toDate += '-';
    toDate += newMonth < 10 ? '0' + newMonth.toString() : newMonth.toString();
    toDate += '-' + lastday.toString();
    model.changeFilter(month: newMonth, fromDate: fromDate, toDate: toDate);
    setState(() {
      loading = true;
    });
    model.requestTransactions(
      false, fromDate, toDate, model.statusFilter, 
      model.transactionFilter, model.orderFilter, _onSuccess, _onError
    );
  }

  void _incrementMonth(WalletViewModel model) {
    DateTime now = DateTime.now();
    if(model.selectedMonth <= now.month) {
      var newMonth = model.selectedMonth < 12 
        ? model.selectedMonth < now.month
          ? model.selectedMonth + 1 : 1
        : 1;
      int lastday = DateTime(now.year, newMonth + 1, 0).day;
      var fromDate = now.year.toString();
      fromDate += '-';
      fromDate += newMonth < 10 ? '0' + newMonth.toString() : newMonth.toString();
      fromDate += '-01';
      var toDate = now.year.toString();
      toDate += '-';
      toDate += newMonth < 10 ? '0' + newMonth.toString() : newMonth.toString();
      toDate += '-' + lastday.toString();
      model.changeFilter(month: newMonth, fromDate: fromDate, toDate: toDate);

      setState(() {
        loading = true;
      });
      model.requestTransactions(
        false, fromDate, toDate, model.statusFilter, 
        model.transactionFilter, model.orderFilter, _onSuccess, _onError
      );
    }
  }

  _filterSearch(WalletViewModel model) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return FilterModal(
          model: model,
          onApply: (fromDate, toDate, ordering) {
            var _fromDate = fromDate;
            var _toDate = toDate;
            if(fromDate == null && toDate == null) {
              DateTime now = DateTime.now();
              var newMonth = model.selectedMonth;
              int lastday = DateTime(now.year, newMonth + 1, 0).day;
              _fromDate = now.year.toString();
              _fromDate += '-';
              _fromDate += newMonth < 10 ? '0' + newMonth.toString() : newMonth.toString();
              _fromDate += '-01';
              _toDate = now.year.toString();
              _toDate += '-';
              _toDate += newMonth < 10 ? '0' + newMonth.toString() : newMonth.toString();
              _toDate += '-' + lastday.toString();
            }
            model.changeFilter(fromDate: _fromDate, toDate: _toDate, orderFilter: ordering);
            setState(() {
              loading = true;
            });
            model.requestTransactions(
              false, _fromDate, _toDate, model.statusFilter, 
              model.transactionFilter, ordering, _onSuccess, _onError
            );
          },
          onReset: () {
            model.resetFilter();
            DateTime now = DateTime.now();
            var newMonth = model.selectedMonth > 1 ? model.selectedMonth - 1 : now.month;
            int lastday = DateTime(now.year, newMonth + 1, 0).day;
            var fromDate = now.year.toString();
            fromDate += '-';
            fromDate += newMonth < 10 ? '0' + newMonth.toString() : newMonth.toString();
            fromDate += '-01';
            var toDate = now.year.toString();
            toDate += '-';
            toDate += newMonth < 10 ? '0' + newMonth.toString() : newMonth.toString();
            toDate += '-' + lastday.toString();
            setState(() {
              loading = true;
            });
            model.requestTransactions(
              false, fromDate, toDate, null, 
              null, EarningOrderFilter.modifiedAtDsc, _onSuccess, _onError
            );

          },
        );
      }
    );
  }

  _onScroll(WalletViewModel model) {
    double maxScroll = scrollController.position.maxScrollExtent;
    double currentScroll = scrollController.position.pixels;
    double delta = 50;
    if ( maxScroll - currentScroll <= delta && !loadMore) {
      //check can load more
      if(model.transactions != null && model.transactions.length > 0 && model.nextTransactions != null) {
        setState(() {
          loadMore = true;
        });
        model.requestTransactions(
          true, model.fromDate, model.toDate, model.statusFilter, 
          model.transactionFilter, model.orderFilter, _onSuccess, _onError
        );
      }
    }
  }

  _onSuccess(String _) {
    setState(() {
      loading = false;
      loadMore = false;
    });
  }

  _onError(String message) {
    setState(() {
      loading = false;
      loadMore = false;
    });
    Helper.showError(context, message);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: LayoutBuilder(
        builder: (context, constraints) => SafeArea(
          child: Container(
            color: Colors.white,
            child: StoreConnector<AppState, WalletViewModel> (
              onInitialBuild: (model) {
                scrollController.addListener(() => _onScroll(model));
                if(model.transactions == null || model.transactions.length == 0) {
                  Future.delayed(const Duration(milliseconds: 10), () {
                    var _fromDate = model.fromDate;
                    var _toDate = model.toDate;
                    if(model.fromDate == null && model.toDate == null) {
                      DateTime now = DateTime.now();
                      var newMonth = model.selectedMonth;
                      int lastday = DateTime(now.year, newMonth + 1, 0).day;
                      _fromDate = now.year.toString();
                      _fromDate += '-';
                      _fromDate += newMonth < 10 ? '0' + newMonth.toString() : newMonth.toString();
                      _fromDate += '-01';
                      _toDate = now.year.toString();
                      _toDate += '-';
                      _toDate += newMonth < 10 ? '0' + newMonth.toString() : newMonth.toString();
                      _toDate += '-' + lastday.toString();
                    }
                    setState(() {
                      loading = true;
                    });
                    model.requestTransactions(
                      false, _fromDate, _toDate, model.statusFilter, 
                      model.transactionFilter, model.orderFilter, _onSuccess, _onError
                    );
                  });
                }
              },
              converter: (store) => WalletViewModel.fromStore(store),
              builder: (_, viewModel) => _buildContent(viewModel)
            )
          ),
        ),
      ),
    );
  }

  Widget _buildContent(WalletViewModel model) {
    return Stack(
      children: [
        Column(
          children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(width: 60,),
                  Row(
                    children: [
                      IconButton(
                        icon: Icon(Icons.chevron_left, color: Colors.black),
                        onPressed: () {
                          _decrementMonth(model);
                        },
                      ),
                      Container(
                        width: 100,
                        alignment: Alignment.center,
                        child: Text(
                          Helper.getMonthName(model.selectedMonth),
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.chevron_right, 
                          color: model.selectedMonth <= DateTime.now().month 
                            ? Colors.black : Colors.grey
                        ),
                        onPressed: () {
                          _incrementMonth(model);
                        },
                      )
                    ],
                  ),
                  Container(
                    width: 55,
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(width: 1, color: Colors.black),
                      borderRadius: BorderRadius.all(Radius.circular(5.0))
                    ),
                    child: IconButton(
                      icon: Icon(Icons.sort, color: Colors.black,),
                      onPressed: () {
                        _filterSearch(model);
                      },
                    ),
                  ),
                ],
              ),
            ),
            _buildList(model),
            loadMore ? Container(
              height: 25,
              padding: EdgeInsets.only(top: 5),
              alignment: Alignment.center,
              child: SpinKitCircle(
                size: 20,
                color: AppColors.primaryOrange,
              ),
            ): Container()
          ],
        ),
        loading ? Positioned( 
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.black.withOpacity(0.05),
            alignment: Alignment.center,
            child: SpinKitCircle(
              size: 50,
              color: AppColors.primaryOrange,
            ),
          ),
        ) : Container()
      ],
    );
  }

  Widget _buildList(WalletViewModel model) {
    final canShow = model.permissions.where((per) => per.role == BusinessRole.listTransactions && per.isGranted).toList().length > 0;
    if(!canShow) {
      return Container(
        alignment: Alignment.center,
        child: Icon(Icons.lock),
      );
    }
    List<WalletTransaction> lst = model.transactions ?? [];
    var newMap = groupBy(lst, (WalletTransaction obj){
      var date = DateTime.parse(obj.modifiedAt);
      var strDate = date.year.toString();
      strDate += '-';
      strDate += date.month < 10 ? '0' + date.month.toString() : date.month.toString();
      strDate += '-';
      strDate += date.day < 10 ? '0' + date.day.toString() : date.day.toString();
      return strDate;
    });
    var keys = newMap.keys.toList();
    keys.sort((a, b) => DateTime.parse(b).compareTo(DateTime.parse(a)));
    if(model.orderFilter == EarningOrderFilter.modifiedAtAsc) {
      keys.sort((a, b) => DateTime.parse(a).compareTo(DateTime.parse(b)));
    }
    return Expanded(
      child: lst.length > 0 
      ? ListView.builder(
          controller: scrollController,
          itemCount: keys.length,
          itemBuilder: (BuildContext context, int index) {
            var key = keys[index];
            var childs = newMap[key];
            DateTime date = DateTime.parse(key);
            var title = date.day < 10 ? '0' + date.day.toString() : date.day.toString();
            title += ' ' + Helper.getMonthName(date.month);
            return Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      top: 10, 
                      bottom: 10,
                      left: 20,
                      right: 20
                    ),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(width: 1, color: AppColors.grey3Color)
                      )
                    ),
                    height: 45,
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      title,
                      style: AppStyles.titleTextCustom(fontWeight: FontWeight.w400),
                    ),
                  ),
                  _buildListTrans(childs)
                ],
              )
            );
          }
        )
      : Container(
          alignment: Alignment.center,
          child: Text(
            !loading ? AppLang().noEarning : '',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
    );
  }

  Widget _buildListTrans(List<WalletTransaction> lst) {
    return Column(
      children: lst.map((tran) {
        var strAmount = tran.amount != null && tran.amount > 0 
          ? NumberFormat('###,###.##', 'en_US').format(tran.amount) 
          : '0.00';
        if(strAmount.indexOf('.') < 0) {
          strAmount += '.00';
        } else {
          if(strAmount.substring(strAmount.indexOf('.') + 1).length == 0) {
            strAmount += '00';
          } else if(strAmount.substring(strAmount.indexOf('.') + 1).length == 1) {
            strAmount += '0';
          }
        }
        return Container(
          height: 54,
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(width: 1, color: AppColors.grey3Color)
            )
          ),
          padding: EdgeInsets.only(
            top: 10, 
            bottom: 10,
            left: 20,
            right: 20
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                tran.transactionType,
                style: AppStyles.titleTextCustom(),
              ),
              Text(
                '\$$strAmount',
                style: AppStyles.titleTextCustom(color: AppColors.strongCyan),
              )
            ],
          ),
        );
      }).toList(),
    );
  }
}
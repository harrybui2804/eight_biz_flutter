import 'dart:io';

import 'package:eight_biz_flutter/components/custom_modal.dart';
import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:eight_biz_flutter/models/biz_bank.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/utils/helper.dart';
import 'package:eight_biz_flutter/view_models/wallet_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stripe_payment/stripe_payment.dart';

class MyBankPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _MyBankPageState();
}

class _MyBankPageState extends State<MyBankPage> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final scrollController = ScrollController(initialScrollOffset: 0);
  final TextEditingController _accNameController = new TextEditingController();
  final _accNameFocus = FocusNode();
  final TextEditingController _accNoController = new TextEditingController();
  final _accNoFocus = FocusNode();
  final TextEditingController _bsbController = new TextEditingController();
  final _bsbFocus = FocusNode();
  
  bool loading = false;
  bool isMounted = false;
  bool isAcceptTerms = false;
  String stripeStatus;
  bool isEdit = false;

  void initState() {
    super.initState();
    bool testMode = AppConfig().appEnvironment == AppEnvironment.DEV ? true : false;
    StripePayment.setOptions(
      StripeOptions(
        publishableKey: AppConfig().publicKeyStripe, 
        merchantId: testMode ? "Test" : "Live",
        androidPayMode: testMode ? 'test' : 'live'
      )
    );
    setState(() {
      isAcceptTerms = false;
      isEdit = false;
    });
  }

  _onLoadedBank(WalletViewModel model) {
    if(mounted) {
      if(_bsbController.text.length == 0) {
        _bsbController.text = model.bank?.bsb;
      }
      if(_accNoController.text.length == 0) {
        _accNoController.text = model.bank?.accountNo;
      }
      if(_accNameController.text.length == 0) {
        _accNameController.text = model.bank?.accountName;
      }
      scrollController.animateTo(
        0.0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 100),
      );
    }
  }

  bool validForm() {
    if(_accNameController.text.trim().length == 0) {
      return false;
    }
    if(_accNoController.text.trim().length == 0) {
      return false;
    }
    if(_bsbController.text.trim().length == 0) {
      return false;
    }
    return true;
  }

  _saveBankAccount(WalletViewModel model) {
    if(!loading) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return CustomModal(
            title: AppLang().askUpdateBankTitle,
            body: AppLang().askUpdateBankBody,
            cancelText: AppLang().cancel.toUpperCase(),
            acceptText: AppLang().accept.toUpperCase(),
            onAccept: () {
              _createToken(model);
            },
          );
        }
      );
    }
  }

  _createToken(WalletViewModel model) async {
    setState(() {
      loading = true;
    });
    StripePayment.createTokenWithBankAccount(BankAccount(
      accountHolderName: _accNameController.text.trim(),
      accountNumber: _accNoController.text.trim(),
      countryCode: 'AU',
      currency: 'AUD',
      routingNumber: _bsbController.text.trim()
    )).then((token) {
      print('bank token ${token.tokenId}');
      model.updateBizBank(
        token.tokenId, 
        BizBank(),
        (_) {
          setState(() {
            loading = false;
            isEdit = false;
          });
          Helper.showSuccess(context, 'SUCCESS');
          model.loadBizBank((String _) => _onLoadedBank(model), null);
          model.getStripeVerifyStatus(_onStripeStatusSuccess, _onError);
        },
        (String message) {
          setState(() {
            loading = false;
          });
          Helper.showError(context, message);
        }
      );
    }).catchError((e) {
      setState(() {
        loading = false;
      });
      var message = e.message ?? e.toString();
      Helper.showError(context, message);
    });
  }

  _onStripeStatusSuccess(String status) {
    setState(() {
      stripeStatus = status;
      loading = false;
    });
  }

  _onError(String message) {
    setState(() {
      loading = false;
    });
    Helper.showError(context, message);
  }

  _openSelectImageOption(WalletViewModel model, {bool isFront = true}) {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
      builder: (BuildContext bc) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 20.0),
          child: Wrap(
            children: [
              ListTile(
                leading: new Icon(Icons.camera_alt, color: Colors.grey,),
                title: new Text('Take a photo'),
                onTap: () {
                  Navigator.pop(context);
                  _getImage(model, ImageSource.camera, isFront);
                },
              ),
              ListTile(
                leading: new Icon(Icons.folder, color: Colors.grey),
                title: new Text('Galerry'),
                onTap: () {
                  Navigator.pop(context);
                  _getImage(model, ImageSource.gallery, isFront);
                },
              )
            ],
          ),
        );
      },
    );
  }

  _getImage(WalletViewModel model, ImageSource imageSource, bool isFront) async {
    final File file = await ImagePicker.pickImage(source: imageSource, imageQuality: 80, maxWidth: 800);
    if(file != null) {
      print(file);
      model.selectPhotoID(
        frontID: isFront ? file: null,
        backID: !isFront ? file: null,
      );
    }
  }

  bool _validVerify(WalletViewModel model) {
    if(model.bank == null) {
      return false;
    }
    if(model.frontID == null && model.backID == null) {
      return false;
    }
    if(!isAcceptTerms) {
      return false;
    }
    return true;
  }

  _verifyStripe(WalletViewModel model) {
    if(_validVerify(model) && !loading) {
      setState(() {
        loading = true;
      });
      model.submitVerifyStripe((String msg) => _verifySuccess(msg, model), _onError);
    }
  }

  _verifySuccess(String message, WalletViewModel model) {
    setState(() {
      loading = false;
    });
    Helper.showSuccess(context, message);
    model.getStripeVerifyStatus(_onStripeStatusSuccess, _onError);
  }
  
  @override
  void dispose() {
    _accNameController.dispose();
    _accNoController.dispose();
    _bsbController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: LayoutBuilder(
        builder: (context, constraints)=> SafeArea(
          child: Container(
            color: Colors.white,
            child: StoreConnector<AppState, WalletViewModel> (
              onInitialBuild: (model) {
                final canShow = model.permissions.where((per)=> per.role == BusinessRole.getBizBank && per.isGranted).toList().length > 0;
                if(canShow) {
                  model.loadBizBank((String _) => _onLoadedBank(model), null);
                  model.getStripeVerifyStatus(_onStripeStatusSuccess, _onError);
                }
              },
              converter: (store) => WalletViewModel.fromStore(store),
              builder: (_, viewModel) => _buildContent(viewModel)
            )
          ),
        ),
      ),
    );
  }

  Widget _buildContent(WalletViewModel model) {
    final canUpdate = model.permissions.where((per)=> per.role == BusinessRole.updateBizBank && per.isGranted).toList().length > 0;
    if(isEdit) {
      if(canUpdate) {
        return _buildEdit(model);
      } else {
        return Container();
      }
    }
    final strStripeStatus = stripeStatus == 'verified' 
      ? AppLang().verified
      : stripeStatus == 'pending' ? AppLang().pending : AppLang().unverified;
    return Stack(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: SingleChildScrollView(
                controller: scrollController,
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              AppLang().bankDetails,
                              style: AppStyles.headingTextCustom(),
                            ),
                            Container(width: 10),
                            stripeStatus != null
                            ? Chip(
                              label: Text(strStripeStatus.toUpperCase()),
                              labelStyle: TextStyle(
                                color: stripeStatus == 'verified' 
                                ? Colors.green 
                                : stripeStatus == 'pending' ? Colors.orange : Colors.red
                              ),
                              backgroundColor: stripeStatus == 'verified' 
                              ? Colors.green.withOpacity(0.3) 
                              : stripeStatus == 'pending' 
                                ? Colors.orange.withOpacity(0.3)
                                : Colors.red.withOpacity(0.3),
                            ): Container(),
                          ],
                        ),
                        canUpdate 
                        ? InkWell(
                          onTap: () {
                            setState(() {
                              isEdit = true;
                            });
                          },
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              AppLang().edit,
                              style: AppStyles.normalTextCustom(color: AppColors.primaryOrange),
                            ),
                          ),
                        ) : Container()
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Text(
                        AppLang().bankName,
                        style: AppStyles.normalTextCustom(color: Colors.black),
                      ),
                    ),
                    Text(
                      model.bank?.bankName ?? '',
                      style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w600),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Text(
                        AppLang().accountName,
                        style: AppStyles.normalTextCustom(color: Colors.black),
                      ),
                    ),
                    Text(
                      model.bank?.accountName ?? '',
                      style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w600),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Text(
                        AppLang().last4Number,
                        style: AppStyles.normalTextCustom(color: Colors.black),
                      ),
                    ),
                    Text(
                      model.bank?.last4Number ?? '',
                      style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w600),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Text(
                        'BSB',
                        style: AppStyles.normalTextCustom(color: Colors.black),
                      ),
                    ),
                    Text(
                      model.bank?.bsb ?? '',
                      style: AppStyles.normalTextCustom(color: Colors.black, fontWeight: FontWeight.w600),
                    ),
                    Container(height: 30),
                    canUpdate ? buildVerify(model) : Container()
                  ],
                ),
              ),
            ),
          ],
        ),
        loading ? Positioned( 
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.black.withOpacity(0.05),
            alignment: Alignment.center,
            child: SpinKitCircle(
              size: 50,
              color: AppColors.primaryOrange,
            ),
          ),
        ) : Container(),
      ],
    );
  }

  Widget _buildEdit(WalletViewModel model) {
    final canUpdate = model.permissions.where((per)=> per.role == BusinessRole.updateBizBank && per.isGranted).toList().length > 0;
    return Stack(
      children: [
        loading ? SpinKitCircle(
          size: 50,
          color: AppColors.primaryOrange,
        ) : Container(),
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    controller: scrollController,
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              AppLang().bankAccDetails,
                              style: AppStyles.headingTextCustom(),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  isEdit = false;
                                });
                              },
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  AppLang().cancel,
                                  style: AppStyles.normalTextCustom(color: AppColors.primaryOrange),
                                ),
                              ),
                            )
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.0),
                          child: Text(
                            '${AppLang().accountName} *',
                            style: AppStyles.normalTextCustom(color: Colors.black),
                          ),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                              vertical: 18.0, horizontal: 20.0
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0),
                            ),
                            errorText: _accNameController.text.trim().length == 0 && canUpdate
                              ? AppLang().accountNameRequired
                              : null
                          ),
                          controller: _accNameController,
                          focusNode: _accNameFocus,
                          enabled: canUpdate,
                          style: AppStyles.normalTextCustom(
                            fontSize: 16,
                            fontWeight: FontWeight.w400
                          ),
                          autocorrect: false,
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (_) {
                            _accNameFocus.unfocus();
                            FocusScope.of(context).requestFocus(_accNoFocus);
                          },
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.0),
                          child: Text(
                            '${AppLang().accountNumber} *',
                            style: AppStyles.normalTextCustom(color: Colors.black),
                          ),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                              vertical: 18.0, horizontal: 20.0
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0),
                            ),
                            errorText: _accNoController.text.trim().length == 0 && canUpdate
                              ? AppLang().accountNameRequired
                              : null
                          ),
                          controller: _accNoController,
                          focusNode: _accNoFocus,
                          enabled: canUpdate,
                          style: AppStyles.normalTextCustom(
                            fontSize: 16,
                            fontWeight: FontWeight.w400
                          ),
                          autocorrect: false,
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (_) {
                            _accNoFocus.unfocus();
                            FocusScope.of(context).requestFocus(_bsbFocus);
                          },
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.0),
                          child: Text(
                            'BSB *',
                            style: AppStyles.normalTextCustom(color: Colors.black),
                          ),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                              vertical: 18.0, horizontal: 20.0
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(width: 1.0),
                            ),
                            errorText: _bsbController.text.trim().length < 6 && canUpdate
                              ? _bsbController.text.trim().length == 0 ? AppLang().bsbRequired : AppLang().bsbIsShort
                              : null
                          ),
                          controller: _bsbController,
                          focusNode: _bsbFocus,
                          enabled: canUpdate,
                          inputFormatters:[
                            LengthLimitingTextInputFormatter(6),
                          ],
                          style: AppStyles.normalTextCustom(
                            fontSize: 16,
                            fontWeight: FontWeight.w400
                          ),
                          autocorrect: false,
                          textInputAction: TextInputAction.done,
                          onFieldSubmitted: (_) {
                            _bsbFocus.unfocus();
                            if(validForm() && canUpdate) {
                              _saveBankAccount(model);
                            }
                          },
                        ),
                        canUpdate ? Container(
                          padding: EdgeInsets.symmetric(vertical: 20),
                          child: RaisedButton(
                            onPressed: validForm() && canUpdate ? () {
                              _saveBankAccount(model);
                            } : null,
                            child: Container(
                              height: 45,
                              child: Center(
                                child: Text(
                                  AppLang().save,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                            color: AppColors.primaryOrange,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(6.0),
                            ),
                          ),
                        ) : Container(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget buildVerify(WalletViewModel model) {
    if(stripeStatus == null || stripeStatus != 'unverified') {
      return Container();
    }
    double width = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          height: 30,
        ),
        Text(
          AppLang().verifyStripe,
          style: AppStyles.headingTextCustom(),
        ),
        Container(height: 10),
        Container(
          width: width - 40,
          height: (width - 40) * 2 / 3,
          margin: EdgeInsets.symmetric(vertical: 10.0),
          child: InkWell(
            onTap: () => _openSelectImageOption(model, isFront: true),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.all(Radius.circular(4.0)),
                  color: AppColors.grey3Color),
              child: Container(
                child: model.frontID == null 
                ? Center(
                    child: Text(
                      AppLang().selectFrontID,
                      style: AppStyles.normalTextCustom(color: Colors.black),
                    ),
                  ) 
                : Image.file(model.frontID, fit: BoxFit.cover),
              ),
            ),
          ),
        ),
        Container(height: 10),
        Container(
          width: width - 40,
          height: (width - 40) * 2 / 3,
          margin: EdgeInsets.symmetric(vertical: 10.0),
          child: InkWell(
            onTap: () => _openSelectImageOption(model, isFront: false),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.all(Radius.circular(4.0)),
                  color: AppColors.grey3Color),
              child: Container(
                child: model.backID == null 
                ? Center(
                    child: Text(
                      AppLang().selectBackID,
                      style: AppStyles.normalTextCustom(color: Colors.black),
                    ),
                  )
                : Image.file(model.backID, fit: BoxFit.cover),
              ),
            ),
          ),
        ),
        Container(height: 10),
        Container(
          padding: EdgeInsets.only(
            top: 5,
            bottom: 5
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Checkbox(
                value: isAcceptTerms,
                activeColor: AppColors.primaryOrange,
                onChanged: (checked) {
                  setState(() {
                    isAcceptTerms = checked;
                  });
                },
              ),
              Container(width: 10),
              Expanded(
                child: Wrap(
                  alignment: WrapAlignment.start,
                  children: <Widget>[
                    Text(
                      AppLang().checkCliking,
                      style: AppStyles.normalTextCustom(),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 3.0, right: 3.0),
                      child: InkWell(
                        onTap: () {
                          Helper.makeLaunched(context, AppConfig().termsUrl);
                        },
                        child: Text(
                          AppLang().checkOurTerms,
                          style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                    Text(
                      AppLang().checkAndThe,
                      style: AppStyles.normalTextCustom(),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 3.0),
                      child: InkWell(
                        onTap: () {
                          Helper.makeLaunched(context, AppConfig().stripeUrl);
                        },
                        child: Text(
                          'Stripe Connected Account Agreement',
                          style: AppStyles.normalTextCustom(fontWeight: FontWeight.w500),
                        ),
                      ),
                    )
                  ],
                )
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: RaisedButton(
            onPressed: _validVerify(model) ? () {
              _verifyStripe(model);
            } : null,
            child: Container(
              height: 45,
              child: Center(
                child: Text(
                  AppLang().verify,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            color: AppColors.primaryOrange,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6.0),
            ),
          ),
        )
      ],
    );
  }
}
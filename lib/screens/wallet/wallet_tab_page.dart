import 'package:eight_biz_flutter/config/colors.dart';
import 'package:eight_biz_flutter/config/language.dart';
import 'package:eight_biz_flutter/config/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'components/wallet_balance_bar.dart';
import 'dashboard_page.dart';
import 'earning_page.dart';
import 'my_bank_page.dart';

class WalletTabPage extends StatefulWidget {
  static const routeName = '/wallet';

  @override
  State<StatefulWidget> createState() => _WalletTabPage(); 
}

class _WalletTabPage extends State<WalletTabPage> with SingleTickerProviderStateMixin {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(vsync: this, length: 3);
    _controller.addListener(_handleTabSelection);
  }

  _handleTabSelection() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.white, // Color for Android
      statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
    ));
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: AppColors.grey3Color,
                offset: Offset(0, 2.0),
                blurRadius: 6.0,
                spreadRadius: 1.0)
          ]),
          child: AppBar(
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 0.0,
            title: Text(
              AppLang().balance,
              style: AppStyles.navTitle(),
            ),
            actions: [
              WalletBlanceBar()
            ],
          ),
        ),
      ),
      body: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.grey4Color,
            elevation: 0,
            title: TabBar(
              controller: _controller,
              indicatorSize: TabBarIndicatorSize.tab,
              indicator: CircleTabIndicator(color: AppColors.primaryOrange, radius: 4),
              labelColor: Colors.black,
              unselectedLabelColor: Colors.grey,
              tabs: [
                Tab(text: AppLang().dashboard),
                Tab(text: AppLang().myBank),
                Tab(text: AppLang().earn),
              ],
            ),
            bottom: PreferredSize(child: Container(color: AppColors.grey3Color, height: 1.0,), preferredSize: Size.fromHeight(1.0)),
          ),
          body: TabBarView(
            controller: _controller,
            children: [
              DashboardPage(),
              MyBankPage(),
              EarningPage(),
            ],
          ),
        ),
      )
    );
  }
}

class CircleTabIndicator extends Decoration {
  final BoxPainter _painter;

  CircleTabIndicator({@required Color color, @required double radius}) : _painter = _CirclePainter(color, radius);

  @override
  BoxPainter createBoxPainter([onChanged]) => _painter;
}

class _CirclePainter extends BoxPainter {
  final Paint _paint;
  final double radius;

  _CirclePainter(Color color, this.radius)
      : _paint = Paint()
          ..color = color
          ..isAntiAlias = true;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration cfg) {
    final Offset circleOffset = offset + Offset(cfg.size.width / 2, cfg.size.height - radius);
    canvas.drawCircle(circleOffset, radius, _paint);
  }
}
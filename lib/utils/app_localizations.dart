import 'dart:async';
import 'dart:convert';

import 'package:eight_biz_flutter/config/app_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppLocalizations {
  Locale locale;
  Map<String, String> _sentences;

  static final AppLocalizations _singleton =
      new AppLocalizations._internal();

  factory AppLocalizations() {
    return _singleton;
  }

  AppLocalizations._internal();
  
  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);  
  }

  get currentLanguage => locale != null ? locale.languageCode : 'en';
  
  static Future<AppLocalizations> load(Locale locale) async {
    AppLocalizations appTranslations = AppLocalizations();
    appTranslations.locale = AppConfig().locale ?? locale;
    print('AppLocalizations load ${locale.languageCode}');
    String data = await rootBundle.loadString('lib/config/lang/${appTranslations.locale.languageCode}.json');
    Map<String, dynamic> _result = json.decode(data);

    appTranslations._sentences = new Map();
    _result.forEach((String key, dynamic value) {
      appTranslations._sentences[key] = value.toString();
    });
    print(appTranslations._sentences);
    return appTranslations;
  }
  
  String trans(String key) {
    return this._sentences[key];
  }
}
import 'dart:async';  

import 'package:flutter/material.dart';

import 'app_localizations.dart';

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  final Locale locale;

  const AppLocalizationsDelegate({this.locale});
  
  @override  
  bool isSupported(Locale locale) => ['en', 'zh'].contains(locale.languageCode);  
  
  @override  
  Future<AppLocalizations> load(Locale locale) async {
    print("Load ${locale.languageCode}"); 
    return AppLocalizations.load(locale);
  }  
  
  @override  
  bool shouldReload(AppLocalizationsDelegate old) => true;  
}
import 'dart:io';
import 'dart:typed_data';

import 'package:eight_biz_flutter/config/language.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:url_launcher/url_launcher.dart';

class Helper {
  
  /// Lauch url
  static Future<void> makeLaunched(BuildContext context, String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      showError(
          context, 'Could not launch $url');
    }
  }

  static Future<bool> canOpenUrl(String url) async {
    return await canLaunch(url);
  }

  static void showError(context, String message) {
    if(context is BuildContext) {
      try {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            backgroundColor: Colors.red,
            content: Text(message, style: TextStyle(color: Colors.white)),
          )
        );
      } catch (e) {
        print(e);
      }
    } else if(context is GlobalKey<ScaffoldState>) {
      try {
        context.currentState.showSnackBar(
          SnackBar(
            backgroundColor: Colors.red,
            content: Text(message, style: TextStyle(color: Colors.white)),
          )
        );
      } catch (e) {
        print(e);
      }
    }
  }

  static void showWarning(context, String message) {
    final snackBar = SnackBar(
      backgroundColor: Colors.yellow,
      content: Container(
        height: 50,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(Icons.warning, color: Colors.black,),
            Container(width: 8),
            Text(message, style: TextStyle(color: Colors.black))
          ],
        ),
      )
      
    );
    if(context is BuildContext) {
      Scaffold.of(context).showSnackBar(snackBar);
    } else if(context is GlobalKey<ScaffoldState>) {
      context.currentState.showSnackBar(snackBar);
    }
  }

  static void showSuccess(context, String message) {
    if(context is BuildContext) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.green,
          content: Text(message)
        )
      );
    } else if(context is GlobalKey<ScaffoldState>) {
      context.currentState.showSnackBar(
        SnackBar(
          backgroundColor: Colors.green,
          content: Text(message)
        )
      );
    }
    
  }

  static Future<Uint8List> loadImageDataFromUrl( String url ) async {
    assert(url != null);
    final File markerImageFile = await DefaultCacheManager().getSingleFile(url);
    Uint8List markerImageBytes = await markerImageFile.readAsBytes();
    return markerImageBytes;
  }

  static Iterable<E> enumerate<E, T>(
      Iterable<T> items, E Function(int index, T item) f) {
    var index = 0;
    return items.map((item) {
      final result = f(index, item);
      index = index + 1;
      return result;
    });
  }

  static String getMonthName(int month) {
    switch (month) {
      case 1:
        return AppLang().january;
        break;
      case 2:
        return AppLang().february;
        break;
      case 3:
        return AppLang().march;
        break;
      case 4:
        return AppLang().april;
        break;
      case 5:
        return AppLang().may;
        break;
      case 6:
        return AppLang().june;
        break;
      case 7:
        return AppLang().july;
        break;
      case 8:
        return AppLang().august;
        break;
      case 9:
        return AppLang().september;
        break;
      case 10:
        return AppLang().october;
        break;
      case 11:
        return AppLang().november;
        break;
      case 12:
        return AppLang().december;
        break;
      default:
        return '';
    }
  }
}
import 'package:country_pickers/country.dart';

class Utils {
  static Country getInitialSelectedCountry(
      List<Country> countries, String countryCode) {
    return countries.firstWhere((country) => country.isoCode == countryCode,
        orElse: () => countries[0]);
  }

  static Country getInitialSelectedCountryByCodeNumber(
      List<Country> countries, String countryCode) {
    return countries.firstWhere((country) => country.phoneCode == countryCode,
        orElse: () => countries[0]);
  }

  static void validatePhoneNumber(String phoneNumber) {}
}

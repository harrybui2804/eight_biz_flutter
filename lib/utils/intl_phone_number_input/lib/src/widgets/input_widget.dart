import 'package:country_pickers/country_pickers.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/countries.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl_phone_number_input/src/utils/phone_input_formatter.dart';
import 'package:intl_phone_number_input/src/utils/util.dart';
import 'package:libphonenumber/libphonenumber.dart';

typedef StringCallback(String value);

class InternationalPhoneNumberInput extends StatefulWidget {
  final ValueChanged<String> onInputChanged;
  final ValueChanged<bool> onInputValidated;
  final ValueChanged<String> onInputClear;

  final String initialCountry2LetterCode;
  final String hintText;
  final String errorMessage;

  final bool formatInput;
  final bool shouldParse;
  final bool shouldValidate;
  final bool isNormalInput;
  final bool showFlag;
  final bool showDropDownIcon;
  final TextStyle phoneCodeStyle;
  final bool anableChange;

  final InputBorder inputBorder;
  final InputDecoration inputDecoration;

  final FocusNode focusNode;

  final List<String> countries;

  final String initialSelectPhoneCode;
  final String initialSelectPhoneNumber;
  final bool isRequiredPhone;
  final TextInputAction inputAction;
  final StringCallback onSubmitted;

  const InternationalPhoneNumberInput({
    Key key,
    @required this.onInputChanged,
    this.onInputValidated,
    this.focusNode,
    this.countries,
    this.inputBorder,
    this.inputDecoration,
    this.initialCountry2LetterCode = 'NG',
    this.hintText = '(800) 000-0001 23',
    this.shouldParse = true,
    this.shouldValidate = true,
    this.formatInput = true,
    this.errorMessage = 'Invalid phone number',
    this.isNormalInput = true,
    this.initialSelectPhoneCode = '',
    this.initialSelectPhoneNumber = '',
    this.isRequiredPhone = true,
    this.showFlag = true,
    this.showDropDownIcon = true,
    this.phoneCodeStyle,
    this.anableChange = true,
    this.onInputClear,
    this.inputAction,
    this.onSubmitted
  }) : super(key: key);

  factory InternationalPhoneNumberInput.withCustomDecoration({
    @required ValueChanged<String> onInputChanged,
    ValueChanged<bool> onInputValidated,
    ValueChanged<String> onInputClear,
    FocusNode focusNode,
    List<String> countries,
    @required InputDecoration inputDecoration,
    String initialCountry2LetterCode = 'NG',
    bool formatInput = true,
    bool shouldParse = true,
    bool shouldValidate = true,
    String initialSelectPhoneCode = '',
    String initialSelectPhoneNumber = '',
    bool isRequiredPhone = true,
    bool showFlag = true,
    TextStyle phoneCodeStyle,
    bool showDropDownIcon = true,
    bool anableChange = true,
    TextInputAction inputAction,
    StringCallback onSubmitted,
  }) {
    return InternationalPhoneNumberInput(
      onInputChanged: onInputChanged,
      onInputValidated: onInputValidated,
      onInputClear: onInputClear,
      focusNode: focusNode,
      countries: countries,
      inputDecoration: inputDecoration,
      initialCountry2LetterCode: initialCountry2LetterCode,
      formatInput: formatInput,
      shouldParse: shouldParse,
      shouldValidate: shouldValidate,
      initialSelectPhoneCode: initialSelectPhoneCode,
      initialSelectPhoneNumber: initialSelectPhoneNumber,
      isRequiredPhone: isRequiredPhone,
      showFlag: showFlag,
      phoneCodeStyle: phoneCodeStyle,
      showDropDownIcon: showDropDownIcon,
      anableChange: anableChange,
      inputAction: inputAction,
      onSubmitted: onSubmitted,
    );
  }

  factory InternationalPhoneNumberInput.withCustomBorder({
    @required ValueChanged<String> onInputChanged,
    @required ValueChanged<bool> onInputValidated,
    FocusNode focusNode,
    List<String> countries,
    @required InputBorder inputBorder,
    @required String hintText,
    String initialCountry2LetterCode = 'NG',
    String errorMessage = 'Invalid phone number',
    bool formatInput = true,
    bool shouldParse = true,
    bool shouldValidate = true,
  }) {
    return InternationalPhoneNumberInput(
      onInputChanged: onInputChanged,
      onInputValidated: onInputValidated,
      focusNode: focusNode,
      countries: countries,
      inputBorder: inputBorder,
      hintText: hintText,
      initialCountry2LetterCode: initialCountry2LetterCode,
      errorMessage: errorMessage,
      formatInput: formatInput,
      shouldParse: shouldParse,
      shouldValidate: shouldValidate,
    );
  }

  @override
  State<StatefulWidget> createState() => _InternationalPhoneNumberInputState();
}

class _InternationalPhoneNumberInputState
    extends State<InternationalPhoneNumberInput> {
  final PhoneInputFormatter _kPhoneInputFormatter = PhoneInputFormatter();

  bool _isNotValid = false;

  List<Country> _countries = [];
  Country _selectedCountry;

  TextEditingController _controller;

  List<TextInputFormatter> _buildInputFormatter() {
    List<TextInputFormatter> formatter = [
      LengthLimitingTextInputFormatter(20),
      WhitelistingTextInputFormatter.digitsOnly,
    ];
    if (widget.formatInput) {
      formatter.add(_kPhoneInputFormatter);
    }

    return formatter;
  }

  _loadCountries(BuildContext context) async {
    setState(() {
      _countries = countryList;
      if (widget.initialSelectPhoneCode.isEmpty) {
        _selectedCountry = Utils.getInitialSelectedCountry(
            _countries, widget.initialCountry2LetterCode);
      } else {
        _selectedCountry = Utils.getInitialSelectedCountryByCodeNumber(
            _countries, widget.initialSelectPhoneCode);
      }
    });
  }

  void _phoneNumberControllerListener() {
    if(_controller.text.length == 0 && widget.onInputClear != null) {
      widget.onInputClear('');
    }
    _isNotValid = false;
    String parsedPhoneNumberString =
        _controller.text.replaceAll(RegExp(r'([\(\1\)\1\s\-])'), '');

    if (widget.shouldParse) {
      //print('${_selectedCountry.phoneCode}, $parsedPhoneNumberString');
      getParsedPhoneNumber(parsedPhoneNumberString, _selectedCountry.isoCode)
          .then((phoneNumber) {
        //print(phoneNumber);
        if (phoneNumber == null) {
          if (widget.onInputValidated != null) {
            if (_controller.text.isEmpty && !widget.isRequiredPhone) {
              widget.onInputValidated(true);
              widget.onInputChanged('');
            } else {
              widget.onInputValidated(false);
            }
          }
          if (widget.shouldValidate) {
            setState(() {
              _isNotValid = true;
            });
          }
        } else {
          widget.onInputChanged(phoneNumber);
          if (widget.onInputValidated != null) {
            widget.onInputValidated(true);
          }
          if (widget.shouldValidate) {
            setState(() {
              _isNotValid = false;
            });
          }
        }
      });
    } else {
      String phoneNumber =
          'phone: ${_selectedCountry.phoneCode}$parsedPhoneNumberString';
      widget.onInputChanged(phoneNumber);
    }
  }

  static Future<String> getParsedPhoneNumber(
      String phoneNumber, String iso) async {
    if (phoneNumber.isNotEmpty) {
      try {
        bool isValidPhoneNumber = await PhoneNumberUtil.isValidPhoneNumber(
            phoneNumber: phoneNumber, isoCode: iso);
        //print(isValidPhoneNumber);

        if (isValidPhoneNumber) {
          return await PhoneNumberUtil.normalizePhoneNumber(
              phoneNumber: phoneNumber, isoCode: iso);
        }
      } on Exception {
        return null;
      }
    }
    return null;
  }

  @override
  void initState() {
    _loadCountries(context);
    _controller = TextEditingController(
        text: widget.initialSelectPhoneNumber.isNotEmpty
            ? widget.initialSelectPhoneNumber
            : '');
    _controller.addListener(_phoneNumberControllerListener);
    super.initState();
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Container(
        child: Stack(
      alignment: Alignment.centerLeft,
      children: <Widget>[
        TextField(
          controller: _controller,
          focusNode: widget.focusNode,
          keyboardType: TextInputType.phone,
          textInputAction: TextInputAction.done,//widget.inputAction,
          style: TextStyle(
            color: Colors.black,
            fontSize: 16
          ),
          inputFormatters: _buildInputFormatter(),
          onChanged: (text) {
            _phoneNumberControllerListener();
          },
          onSubmitted: (value) {
            if(widget.onSubmitted != null) {
              widget.onSubmitted(value);
            }
          },
          decoration: _getInputDecoration(widget.inputDecoration),
        ),
        Positioned(
          top: 17.0,
          left: 10.0,
          child: InkWell(
            onTap: () {
              if(widget.anableChange) {
                _openCountryPickerDialog();
              }
            },
            child: _buildPhoneCode(_selectedCountry),
          ),
        )
      ],
    ));
  }

  Widget _buildPhoneCode(Country country) {
    if(widget.showDropDownIcon != null && widget.showDropDownIcon) {
      return Row(
        children: <Widget>[
          _buildItemSelected(_selectedCountry),
          Icon(Icons.arrow_drop_down, color: Colors.black,)
        ],
      );
    }
    return _buildItemSelected(_selectedCountry);
  }

  InputDecoration _getInputDecoration(InputDecoration decoration) {
    return decoration ??
        InputDecoration(
          border: widget.inputBorder ?? UnderlineInputBorder(),
          hintText: widget.hintText,
          errorText: _isNotValid ? widget.errorMessage : null,
        );
  }

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => Theme(
            data: Theme.of(context).copyWith(primaryColor: Colors.pink),
            child: CountryPickerDialog(
                titlePadding: EdgeInsets.all(8.0),
                searchCursorColor: Colors.pinkAccent,
                searchInputDecoration: InputDecoration(hintText: 'Search...'),
                isSearchable: true,
                title: Text('Select your phone code'),
                onValuePicked: (Country country) => setState(() {
                      _selectedCountry = country;
                      _phoneNumberControllerListener();
                    }),
                itemBuilder: _buildDialogItem)),
      );

  Widget _buildDialogItem(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          Text("+${country.phoneCode}"),
          SizedBox(width: 8.0),
          Flexible(child: Text(country.name))
        ],
      );

  Widget _buildItemSelected(Country country) {
    if(!widget.showFlag) {
      var phoneCode = country.phoneCode;
      return Row(
        children: <Widget>[
          Text("+$phoneCode", style: widget.phoneCodeStyle ?? TextStyle(color: Colors.black, fontSize: 16),),
          SizedBox(width: 8.0),
        ],
      );
    }
    return Row(
      children: <Widget>[
        CountryPickerUtils.getDefaultFlagImage(country),
        SizedBox(width: 8.0),
        Text("+${country.phoneCode}", style: widget.phoneCodeStyle ?? TextStyle(color: Colors.black, fontSize: 16),),
        SizedBox(width: 8.0),
      ],
    );
  }
}

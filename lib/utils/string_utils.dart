import 'package:email_validator/email_validator.dart';

bool validateEmail(String str) {
  return EmailValidator.validate(str);
}

String removeBaseUrl(String baseUrl, String arg) {
  assert(baseUrl != null && baseUrl.length > 0);
  assert(arg != null && arg.length > 0);
  var https = baseUrl;
  var http = baseUrl;
  if(baseUrl.indexOf('https') > -1) {
    http = baseUrl.replaceFirst('https', 'http');
  } else {
    https =  baseUrl.replaceFirst('http://', 'https://');
  }
  return arg.replaceAll(https, '').replaceAll(http, '');
}
import 'dart:collection';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class UserDetail {
  String _token;
  final storage = new FlutterSecureStorage();

  Future<bool> isLogined() async {
    try {
      final value = await storage.read(key: 'isLogined');
      return value != null && value == 'true';
    } catch (e) {
      return false;
    }
  }

  Future<void> setToken(String token) async {
    _token = token;
    await storage.write(key: 'token', value: token);
  }

  Future<void> clearData() async {
    await storage.deleteAll();
  }

  Future<String> getToken() async {
    String value = _token;
    if(value == null || value.length == 0) {
      value = await storage.read(key: 'token');
    }
    return value;
  }
  
  Future<void> setLogin(String phone, String password) async {
    await storage.write(key: 'isLogined', value: 'true');
    await storage.write(key: 'phone', value: phone);
    await storage.write(key: 'password', value: password);
  }

  Future<void> updateStorePass(String password) async {
    await storage.write(key: 'password', value: password);
  }

  Future<Map<String, dynamic>> getLogin() async {
    var result = HashMap<String, dynamic>();
    try {
      result['isLogined'] = await storage.read(key: 'isLogined');
      result['phone'] = await storage.read(key: 'phone');
      result['password'] = await storage.read(key: 'password');
    } catch (e) {
      
    }
    
    return result;
  }

}
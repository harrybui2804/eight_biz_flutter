import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/redux/actions/biz_actions.dart';
import 'package:equatable/equatable.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';

class BizDetailViewModel extends Equatable {
  final BusinessDetail business;
  final List<BusinessPermision> permissions;

  final Function(BusinessDetail) submitEditBizDetail;

  BizDetailViewModel({
    this.business,
    this.permissions = const [],
    this.submitEditBizDetail,
    //this.resMedias,
  });

  static BizDetailViewModel fromStore(Store<AppState> store) {
    return BizDetailViewModel(
      business: store.state.homeState.business,
      permissions: store.state.homeState.permissions ?? [],
      submitEditBizDetail: (biz) => store.dispatch(EditBizDetailAction(business: biz))
    );
  }

  @override
  List<Object> get props => [business, submitEditBizDetail];
}
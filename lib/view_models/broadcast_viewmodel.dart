import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/broadcast.dart';
import 'package:eight_biz_flutter/redux/admin/admin_actions.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';

class BroadcastViewModel extends Equatable {
  final List<BusinessPermision> permissions;
  final List<Broadcast> broadcast;
  final String nextBroadcastUrl;

  final Function(String, Function, Function) loadListBroadcast;
  final Function(String, String, String, bool, Function, Function) createBroadcast;
  final Function(String, String, String, String, bool, Function, Function) updateBroadcast;
  final Function(String, Function, Function) deleteBroadcast;

  BroadcastViewModel({
    @required this.permissions,
    @required this.broadcast,
    @required this.nextBroadcastUrl,
    @required this.loadListBroadcast,
    @required this.createBroadcast,
    @required this.updateBroadcast,
    @required this.deleteBroadcast
  });

  static BroadcastViewModel fromStore(Store<AppState> store) {
    final resBroadcasts = store.state.adminState.resBroadcasts;
    return BroadcastViewModel(
      permissions: store.state.homeState.permissions ?? [],
      broadcast: resBroadcasts != null && resBroadcasts.results  != null
        ? resBroadcasts.results : [],
      nextBroadcastUrl: resBroadcasts != null && resBroadcasts.results  != null
        ? resBroadcasts.next : null, 
      loadListBroadcast: (next, onSuccess, onError) {
        return store.dispatch(
          LoadListBroadcastAction(
            businessId: store.state.homeState.business.id,
            next: next,
            onSuccess: onSuccess,
            onError: onError
          )
        );
      },
      createBroadcast: (title, body, description, isSend, onSuccess, onError) {
        return store.dispatch(
          SaveBroadcastAction(
            businessId: store.state.homeState.business.id,
            title: title,
            body: body,
            description: description,
            status: isSend,
            onSuccess: onSuccess,
            onError: onError
          )
        );
      },
      updateBroadcast: (broadcastId, title, body, description, isSend, onSuccess, onError) {
        return store.dispatch(
          UpdateBroadcastAction(
            businessId: store.state.homeState.business.id,
            broadcastId: broadcastId,
            title: title,
            body: body,
            description: description,
            status: isSend,
            onSuccess: onSuccess,
            onError: onError
          )
        );
      },
      deleteBroadcast: (broadcastId, onSuccess, onError) {
        return store.dispatch(
          DeleteBroadcastAction(
            businessId: store.state.homeState.business.id,
            broadcastId: broadcastId,
            onSuccess: onSuccess,
            onError: onError
          )
        );
      }
    );
  }

  @override
  List<Object> get props => [
    permissions, broadcast, nextBroadcastUrl, loadListBroadcast, 
    createBroadcast, updateBroadcast, deleteBroadcast
  ];
}
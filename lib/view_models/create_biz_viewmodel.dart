import 'dart:io';

import 'package:eight_biz_flutter/models/biz_category.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/models/media.dart';
import 'package:eight_biz_flutter/models/user_profile.dart';
import 'package:eight_biz_flutter/redux/actions/category_actions.dart';
import 'package:eight_biz_flutter/redux/actions/navigation_actions.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/create_biz/create_biz_actions.dart';
import 'package:eight_biz_flutter/response/res_medias.dart';
import 'package:eight_biz_flutter/screens/biz/create_biz.dart';
import 'package:redux/redux.dart';
import 'package:signature/signature.dart';

class CreateBizViewModel {
  //Editing info
  final BusinessDetail editing;
  final File localLogo;
  final bool loadingMedias;
  final ResMedias resMedias;
  final String agreement;
  final List<Point> signature;
  //Get from home => Only use for edit, when create new it always null (not edit it)
  final BusinessDetail business;
  final List<BusinessPermision> permissions;

  //App state
  final bool loading;
  final List<BizCategory> categories;
  final UserProfile currentUser;
  final Function(int, bool) nextPage;
  final Function loadCategories;
  final Function(BusinessDetail) changeEditing;
  final Function(BusinessDetail, Function, Function) submitCreateBusiness;
  final Function(BusinessDetail, Function, Function) submitUpdateBusiness;
  final Function popToListBiz;
  final Function clearEditingBiz;
  final Function(String) goToAgreement;
  final Function(String, String, Function) requestListMedias;
  final Function(String, File, bool, Function, Function) uploadMedia;
  final Function(BusinessDetail, String, bool) updateCoverImage;
  final Function(BusinessDetail, String, File logo) uploadLogo;
  final Function(BusinessDetail) deleteLogo;
  final Function(String, Media, Function, Function) deleteMedia;
  final Function(String, Function, Function) loadAgreement;
  final Function(List<Point>, bool) changeSignature;
  final Function(String, File, Function, Function) saveSignature;

  CreateBizViewModel({
    this.business,
    this.permissions,
    this.editing,
    this.agreement,
    this.signature,
    this.localLogo,
    this.loadingMedias,
    this.loading,
    this.categories,
    this.resMedias,
    this.currentUser,
    this.nextPage,
    this.loadCategories,
    this.changeEditing,
    this.submitCreateBusiness,
    this.submitUpdateBusiness,
    this.popToListBiz,
    this.clearEditingBiz,
    this.requestListMedias,
    this.uploadMedia,
    this.updateCoverImage,
    this.uploadLogo,
    this.deleteLogo,
    this.deleteMedia,
    this.loadAgreement,
    this.changeSignature,
    this.saveSignature,
    this.goToAgreement
  });

  static CreateBizViewModel fromStore(Store<AppState> store) {
    return CreateBizViewModel(
      loading: store.state.createBizState.loading,
      loadingMedias: store.state.createBizState.loadingMedias,
      categories: store.state.categoryState.categories,
      resMedias: store.state.createBizState.resMedias,
      currentUser: store.state.profileState.userDetail,
      business: store.state.homeState.business,
      permissions: store.state.homeState.permissions,
      editing: store.state.createBizState.editing,
      agreement: store.state.createBizState.agreement,
      signature: store.state.createBizState.signature,
      localLogo: store.state.createBizState.localLogo,
      nextPage: (index, isNew) => store.dispatch(NavNavigateToCreateBiz(params: CreateBizParams(index: index, isNew: isNew))),
      loadCategories: () => store.dispatch(RequestCategoriesAction()),
      changeEditing: (editing) => store.dispatch(ChangeEditingInfoAction(editing)),
      submitCreateBusiness: (biz, onSuccess, onError) => store.dispatch(SubmitCreateBizAction(biz, onSuccess, onError)),
      submitUpdateBusiness: (biz, onSuccess, onError) => store.dispatch(SubmitEditBizAction(biz, onSuccess, onError)),
      popToListBiz: () => store.dispatch(NavPushAndResetToListBiz()),
      clearEditingBiz: () => store.dispatch(ClearEditingBizAction()),
      requestListMedias: (bizId, next, onError) => store.dispatch(RequestListMediasAction(businessId: bizId, next: next, onError: onError)),
      uploadMedia: (bizId, file, isLogo, onSuccess, onError) => store.dispatch(UploadMediaAction(businessId: bizId, file: file, isLogo: isLogo, onSuccess: onSuccess, onError: onError)),
      updateCoverImage: (biz, imageId, isSetCover) => store.dispatch(UpdateCoverImageAction(business: biz, imageId: imageId, isSetCover: isSetCover)),
      uploadLogo: (biz, bizId, file) => store.dispatch(UploadMediaAction(business: biz, businessId: bizId, file: file, isLogo: true)),
      deleteLogo: (biz) => store.dispatch(DeleteBizLogoAction(business: biz)),
      deleteMedia: (bizId, image, onSuccess, onError) {
        return store.dispatch(
          DeleteMediaAction(
            businessId: bizId,
            image: image,
            onSuccess: onSuccess,
            onError:  onError
          )
        );
      },
      loadAgreement: (businessId, onSuccess, onError) {
        return store.dispatch(LoadAgreementAction(
          businessId: businessId,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      changeSignature: (points, isClear) {
        return store.dispatch(ChangeSignatureAction(points, isClear: isClear));
      },
      saveSignature: (businessId, file, onSuccess, onError) {
        return store.dispatch(UpdateSignatureAction(
          businessId: businessId,
          image: file,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      goToAgreement: (businessId) => store.dispatch(NavNavigateToAgreement(businessId: businessId))
    );
  }
}
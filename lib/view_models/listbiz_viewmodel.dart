import 'package:eight_biz_flutter/models/business_list.dart';
import 'package:eight_biz_flutter/redux/actions/biz_actions.dart';
import 'package:eight_biz_flutter/redux/actions/navigation_actions.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/response/res_listbiz.dart';
import 'package:eight_biz_flutter/screens/biz/create_biz.dart';
import 'package:redux/redux.dart';

class ListBizViewModel {
  final bool loading;
  final bool loadMore;
  final ResListBiz data;
  final Function(String, Function) loadBusiness;
  final Function createBusiness;
  final Function(BusinessList, Function) switchBusiness;

  ListBizViewModel({
    this.loading,
    this.loadMore,
    this.data,
    this.loadBusiness,
    this.createBusiness,
    this.switchBusiness
  });

  static ListBizViewModel fromStore(Store<AppState> store) {
    return ListBizViewModel(
      loading: store.state.listBizState.loading,
      loadMore: store.state.listBizState.loadMore,
      data: store.state.listBizState.data,
      loadBusiness: (next, onError) => store.dispatch(RequestListBizAction(next: next, onError: onError)),
      createBusiness: () => store.dispatch(NavNavigateToCreateBiz(params: CreateBizParams.initial())),
      switchBusiness: (biz, onError) => store.dispatch(SwitchBizDetailAction(businessList: biz, onError: onError))
    );
  }
  
}
import 'package:eight_biz_flutter/redux/actions/navigation_actions.dart';
import 'package:equatable/equatable.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';

class LoginViewModel extends Equatable {
  final String phone;
  final bool loading;
  final Function(String, String, Function, Function) doLogin;
  final Function navigateToResetPass;
  final Function(String, Function, Function) resendVerifyEmail;

  LoginViewModel({
    this.phone,
    this.loading,
    this.doLogin,
    this.navigateToResetPass,
    this.resendVerifyEmail
  });

  static LoginViewModel fromStore(Store<AppState> store) {
    return LoginViewModel(
      phone: store.state.authState.phone,
      loading: store.state.authState.loading,
      doLogin: (phone, password, onError, onUnverifEmail) => store.dispatch(LoginRequest(phone, password, onError, onUnverifEmail)),
      navigateToResetPass: () => store.dispatch(NavNavigateToResetPass()),
      resendVerifyEmail: (email, onSuccess, onError) => store.dispatch(ResendVerifyEmailAction(
        email: email,
        onSuccess: onSuccess,
        onError: onError
      ))
    );
  }

  @override
  List<Object> get props => [phone, loading, doLogin, navigateToResetPass];
}
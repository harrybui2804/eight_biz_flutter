import 'package:eight_biz_flutter/models/biz_manager.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/biz_role.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/redux/admin/admin_actions.dart';
import 'package:eight_biz_flutter/response/res_managers.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';

class ManagerViewModel extends Equatable {
  final BusinessDetail business;
  final List<BusinessPermision> permissions;
  final ResManagers resManagers;
  final List<BizManager> managers;
  final Function(String, Function, Function) loadManagers;
  final Function(String, Function, Function) loadListRoles;
  final Function(String, BizRole, Function, Function) loadRoleDetail;
  final Function(String, bool, String, List<BusinessPermision>, Function, Function) saveManager;
  final Function(String, String, List<BusinessPermision>, Function, Function) updateManger;
  final Function(String, Function, Function) deleteManager;

  ManagerViewModel({
    @required this.business,
    @required this.permissions,
    @required this.resManagers,
    @required this.managers,
    @required this.loadManagers,
    @required this.loadListRoles,
    @required this.loadRoleDetail,
    @required this.saveManager,
    @required this.updateManger,
    @required this.deleteManager
  });

  static ManagerViewModel fromStore(Store<AppState> store) {
    return ManagerViewModel(
      business: store.state.homeState.business,
      permissions: store.state.homeState.permissions ?? [],
      resManagers: store.state.adminState.resManagers,
      managers: store.state.adminState.managers ?? [],
      loadManagers: (next, onSuccess, onError) {
        return store.dispatch(LoadListUserManager(
          businessId: store.state.homeState.business.id,
          next: next,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      loadListRoles: (bizId, onSuccess, onError) {
        return store.dispatch(LoadListRolesAction(
          businessId: bizId,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      loadRoleDetail: (bizId, role, onSuccess, onError) {
        return store.dispatch(LoadRoleDetailAction(
          businessId: bizId,
          role: role,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      saveManager: (phone, isCreate, role, permissions, onSuccess, onError) {
        return store.dispatch(SaveUserManagerAction(
          businessId: store.state.homeState.business.id,
          phone: phone, 
          createRole: isCreate,
          role: role,
          permissions: permissions,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      updateManger: (managerId, roleId, permissions, onSuccess, onError) {
        return store.dispatch(UpdateUserManagerAction(
          businessId: store.state.homeState.business.id,
          managerId: managerId, 
          roleId: roleId,
          permissions: permissions,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      deleteManager: (managerId, onSuccess, onError) {
        return store.dispatch(DeleteUserManagerAction(
          businessId: store.state.homeState.business.id,
          managerId: managerId,
          onSuccess: onSuccess,
          onError: onError
        ));
      }
    );
  }

  @override
  List<Object> get props => [business, permissions, resManagers, managers, loadManagers, loadListRoles, loadRoleDetail, saveManager, deleteManager];
}
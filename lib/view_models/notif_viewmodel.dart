import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/notification.dart';
import 'package:eight_biz_flutter/redux/notifications/notification_actions.dart';
import 'package:equatable/equatable.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';

class NotifViewModel extends Equatable {
  final List<BusinessPermision> permissions;
  final List<Notification> notifications;
  final String nextNotifs;
  final Function(bool, Function, Function) loadNotifs;

  NotifViewModel({
    this.permissions,
    this.notifications,
    this.nextNotifs,
    this.loadNotifs
  });

  static NotifViewModel fromStore(Store<AppState> store) {
    return NotifViewModel(
      permissions: store.state.homeState.permissions,
      notifications: store.state.notificationState.resNotifications != null
        ? store.state.notificationState.resNotifications.results ?? []
        : [],
      nextNotifs: store.state.notificationState.resNotifications != null
        ? store.state.notificationState.resNotifications.next ?? null
        : null,
      loadNotifs: (loadMore, onSuccess, onError) {
        return store.dispatch(RequestNotificationsAction(
          businessId: store.state.homeState.business.id,
          next: loadMore 
            ? store.state.notificationState.resNotifications != null
              ? store.state.notificationState.resNotifications.next : null
            : null,
          onSuccess: onSuccess,
          onError: onError
        ));
      }
    );
  }

  @override
  List<Object> get props => [permissions, notifications, nextNotifs];
}
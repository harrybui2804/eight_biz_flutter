import 'dart:io';

import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/business_address.dart';
import 'package:eight_biz_flutter/models/business_detail.dart';
import 'package:eight_biz_flutter/models/notification.dart';
import 'package:eight_biz_flutter/models/user_profile.dart';
import 'package:eight_biz_flutter/redux/actions/biz_actions.dart';
import 'package:eight_biz_flutter/redux/actions/navigation_actions.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';
import 'package:eight_biz_flutter/redux/profile/profile_actions.dart';
import 'package:equatable/equatable.dart';
import 'package:redux/redux.dart';

class ProfileViewModel extends Equatable {
  final UserProfile userProfile;
  final File localAvatar;
  final BusinessDetail business;
  final List<BusinessPermision> permissions;
  final List<Notification> notifications;
  final bool loading;
  final bool loadingEdit;
  final bool loadingChangePass;
  final Function doLogout;
  final Function(String) switchBusiness;
  final Function editProfile;
  final Function(String, String, String, String, BizAddress, Function, Function) submitUpdate;
  final Function(String, String, Function, Function) submitChangePass;
  final Function loadNotifs;
  final Function(File, Function, Function) updateAvatar;
  final Function(String, Function, Function) changeLanguage;
  final Function({Function onSuccess, Function onError}) reloadBizDetail;
  final Function({Function onSuccess, Function onError}) loadPermissions;

  ProfileViewModel({
    this.userProfile,
    this.localAvatar,
    this.loading,
    this.loadingEdit,
    this.loadingChangePass,
    this.doLogout,
    this.switchBusiness,
    this.business,
    this.permissions,
    this.notifications,
    this.updateAvatar,
    this.editProfile,
    this.loadNotifs,
    this.submitUpdate,
    this.submitChangePass,
    this.changeLanguage,
    this.reloadBizDetail,
    this.loadPermissions
  });

  static ProfileViewModel fromStore(Store<AppState> store) {
    return ProfileViewModel(
      userProfile: store.state.profileState.userDetail,
      localAvatar: store.state.profileState.localAvatar,
      loading: store.state.profileState.loading,
      loadingEdit: store.state.profileState.loadingEdit,
      loadingChangePass: store.state.profileState.loadingChangePass,
      business: store.state.homeState.business,
      permissions: store.state.homeState.permissions ?? [],
      notifications: store.state.profileState.notifications ?? [],
      doLogout: () => store.dispatch(LogoutAppAction()),
      switchBusiness: (bizId) => store.dispatch(SwitchToOtherBusiness(bizId)),
      editProfile: () => store.dispatch(NavNavigateToChangeProfile()),
      submitUpdate: (firstName, lastName, dateOfBirth, phoneNumber, address, onSuccess, onError) {
        return store.dispatch(SubmitUpdateProfileAction(
          firstName: firstName, 
          lastName: lastName, 
          dateOfBirth: dateOfBirth,
          phoneNumber: phoneNumber,
          address: address,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      submitChangePass: (currentPass, newPass, onSuccess, onError) {
        store.dispatch(
          SubmitChangePassAction(currentPass, newPass, onSuccess: onSuccess, onError: onError)
        );
      },
      loadNotifs: () {
        return store.dispatch(LoadProfileNotificationsActions(store.state.homeState.business.id));
      },
      updateAvatar: (file, onSuccess, onError) {
        return store.dispatch(UploadProfileAvatarAction(
          file: file,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      changeLanguage: (language, onSuccess, onError) {
        return store.dispatch(ChangeLanguageAction(
          language: language,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      reloadBizDetail: ({onSuccess, onError}) {
        return store.dispatch(LoadBizDetailAction(
          store.state.homeState.business.id,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      loadPermissions: ({onSuccess, onError}) {
        return store.dispatch(LoadUserPermissionsAction(
          store.state.homeState.business.id,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
    );
  }

  @override
  List<Object> get props => [
    userProfile, localAvatar, business, permissions, notifications, loading, loadingEdit, doLogout, 
    switchBusiness, editProfile, submitUpdate, submitChangePass, updateAvatar
  ];
}
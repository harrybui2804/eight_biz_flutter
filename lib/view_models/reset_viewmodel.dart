import 'package:equatable/equatable.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';

class ResetPassViewModel extends Equatable {
  final String phone;
  final String registerId;
  final bool loading;
  final Function(String, String, String, Function, Function) submitResetPass;
  final Function(String, Function, Function) resendCodeReset;

  ResetPassViewModel({
    this.phone,
    this.registerId,
    this.loading,
    this.submitResetPass,
    this.resendCodeReset
  });

  static ResetPassViewModel fromStore(Store<AppState> store) {
    return ResetPassViewModel(
      phone: store.state.authState.phone,
      registerId: store.state.authState.registerId,
      loading: store.state.authState.loading,
      submitResetPass: (registerId, password, code, onSuccess, onError) => store.dispatch(SubmitResetPassword(registerId, password, code, onSuccess, onError)),
      resendCodeReset: (registerId, onSuccess, onError) => store.dispatch(RequestPhoneCodeReset(registerId, onSuccess, onError))
    );
  }

  @override
  List<Object> get props => [phone, loading, submitResetPass];
}
import 'package:eight_biz_flutter/models/signup.dart';
import 'package:equatable/equatable.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';

class SignupViewModel extends Equatable {
  final String registerId;
  final String publicVerifyKey;
  final String privateVerifyKey;
  final String phone;
  final bool loading;
  final Function(String, String, Function) doLogin;
  final Function(ReqSignUp, Function, Function) submitSignup;

  SignupViewModel({
    this.registerId,
    this.publicVerifyKey,
    this.privateVerifyKey,
    this.phone,
    this.loading,
    this.doLogin,
    this.submitSignup,
  });

  static SignupViewModel fromStore(Store<AppState> store) {
    return SignupViewModel(
      registerId: store.state.authState.registerId,
      publicVerifyKey: store.state.authState.publicVerifyKey,
      privateVerifyKey: store.state.authState.privateVerifyKey,
      phone: store.state.authState.phone,
      loading: store.state.authState.loading,
      doLogin: (phone, password, onError) => store.dispatch(LoginRequest(phone, password, onError, null)),
      submitSignup: (model, onSuccess, onError) => store.dispatch(SignUpRequestAction(model, onSuccess, onError)),
    );
  }

  @override
  List<Object> get props => [registerId, publicVerifyKey, phone, loading, doLogin, submitSignup];
}
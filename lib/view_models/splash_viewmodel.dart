import 'package:eight_biz_flutter/redux/actions/navigation_actions.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';

class SplashViewModel {
  final String phone;
  final Function getStoreLogin;
  final Function gotoGetStarted;

  SplashViewModel({
    this.phone,
    this.getStoreLogin,
    this.gotoGetStarted
  });

  static SplashViewModel fromStore(Store<AppState> store) {
    return SplashViewModel(
      phone: store.state.authState.phone,
      getStoreLogin: () => store.dispatch(new GetStoreLogin()),
      gotoGetStarted: () => store.dispatch(new NavResetToGetStarted())
    );
  }
}
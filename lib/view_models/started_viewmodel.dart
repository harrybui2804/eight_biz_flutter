import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';

class GetStartedViewModel {
  final String phone;
  final String password;
  final bool loading;

  final Function(String, double, double, Function) checkExistPhone;

  GetStartedViewModel({
    this.phone,
    this.password,
    this.loading,
    this.checkExistPhone
  });

  static GetStartedViewModel fromStore(Store<AppState> store) {
    return GetStartedViewModel(
      phone: store.state.authState.phone,
      password: store.state.authState.password,
      loading: store.state.authState.loading,
      checkExistPhone: (phone, latitude, longitude, onError) => store.dispatch(
        ValidatePhoneAction(
          phone: phone,
          latitude: latitude,
          longitude: longitude,
          onError: onError
        )
      ),
    );
  }
}
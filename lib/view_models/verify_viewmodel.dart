import 'package:equatable/equatable.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';
import 'package:eight_biz_flutter/redux/auth/auth_actions.dart';

class VerifyViewModel extends Equatable {
  final String registerId;
  final bool isExist;
  final String phone;
  final bool loading;
  final String sucMessage;
  final String errMessage;

  final Function(String, String, bool, Function(String), Function(String)) submitPhoneCode;
  final Function(String, String, Function(String), Function(String)) resendVerifyCode;
  final Function hideSuccessMessage;
  final Function hideFailedMessage;

  VerifyViewModel({
    this.registerId,
    this.isExist,
    this.phone,
    this.loading,
    this.sucMessage,
    this.errMessage,
    this.submitPhoneCode,
    this.resendVerifyCode,
    this.hideSuccessMessage,
    this.hideFailedMessage
  });

  static VerifyViewModel fromStore(Store<AppState> store) {
    return VerifyViewModel(
      registerId: store.state.authState.registerId,
      isExist: store.state.authState.isExist,
      phone: store.state.authState.phone,
      loading: store.state.authState.loading,
      sucMessage: store.state.authState.sucMessage,
      errMessage: store.state.authState.errMessage,
      resendVerifyCode: (phone, registerId, onSuccess, onError) => store.dispatch(ResendPhoneCodeAction(phone, registerId, onSuccess, onError)),
      submitPhoneCode: (phone, code, isExist, onSuccess, onError) => store.dispatch(VerifyPhoneCodeAction(phone, code, isExist, onSuccess, onError)),
      hideSuccessMessage: () => store.dispatch(ClearAuthSuccess()),
      hideFailedMessage: () => store.dispatch(ClearAuthError()),
    );
  }

  @override
  List<Object> get props => [registerId, phone, loading, sucMessage, errMessage, submitPhoneCode, resendVerifyCode, hideSuccessMessage, hideFailedMessage];
}
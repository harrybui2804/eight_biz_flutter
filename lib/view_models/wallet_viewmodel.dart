import 'dart:io';

import 'package:eight_biz_flutter/models/biz_bank.dart';
import 'package:eight_biz_flutter/models/biz_permission.dart';
import 'package:eight_biz_flutter/models/enums.dart';
import 'package:eight_biz_flutter/models/wallet_balance.dart';
import 'package:eight_biz_flutter/models/wallet_transaction.dart';
import 'package:eight_biz_flutter/redux/wallet/wallet_actions.dart';
import 'package:equatable/equatable.dart';
import 'package:redux/redux.dart';
import 'package:eight_biz_flutter/redux/app_state.dart';

class WalletViewModel extends Equatable {
  final List<BusinessPermision> permissions;
  final WalletBalance balance;
  final BizBank bank;
  final List<WalletTransaction> transactions;
  final String nextTransactions;
  final File frontID;
  final File backID;
  //filter transaction
  final int selectedMonth;
  final String fromDate;
  final String toDate;
  final EarningStatusFilter statusFilter;
  final EarningTransactionFilter transactionFilter;
  final EarningOrderFilter orderFilter; 
  
  final Function(Function, Function) loadWalletDetail;
  final Function(Function, Function) loadBizBank;
  final Function(double, String, Function, Function) requestCashout;
  final Function(String, BizBank, Function, Function) updateBizBank;
  final Function({
    int month, String fromDate, String toDate, EarningStatusFilter statusFilter,
    EarningTransactionFilter transactionFilter, EarningOrderFilter orderFilter
  }) changeFilter;
  final Function resetFilter;
  final Function(
    bool,
    String, String, EarningStatusFilter, 
    EarningTransactionFilter, EarningOrderFilter,
    Function, Function
  ) requestTransactions;
  final Function(Function, Function) getStripeVerifyStatus;
  final Function({File frontID, File backID}) selectPhotoID;
  final Function(Function, Function) submitVerifyStripe;

  WalletViewModel({
    this.permissions,
    this.balance,
    this.bank,
    this.transactions,
    this.nextTransactions,
    this.selectedMonth,
    this.fromDate,
    this.toDate,
    this.statusFilter,
    this.transactionFilter,
    this.orderFilter,
    this.changeFilter,
    this.resetFilter,
    this.loadWalletDetail,
    this.requestCashout,
    this.loadBizBank,
    this.updateBizBank,
    this.requestTransactions,
    this.getStripeVerifyStatus,
    this.selectPhotoID,
    this.frontID,
    this.backID,
    this.submitVerifyStripe
  });

  static WalletViewModel fromStore(Store<AppState> store) {
    return WalletViewModel(
      permissions: store.state.homeState.permissions,
      balance: store.state.walletState.balance,
      bank: store.state.walletState.bank,
      frontID: store.state.walletState.frontID,
      backID: store.state.walletState.backID,
      transactions: store.state.walletState.resTransactions != null 
        ? store.state.walletState.resTransactions.results ?? []
        : [],
      nextTransactions: store.state.walletState.resTransactions != null 
        ? store.state.walletState.resTransactions.next ?? null
        : null,
      selectedMonth: store.state.walletState.selectedMonth,
      fromDate: store.state.walletState.fromDate,
      toDate: store.state.walletState.toDate,
      statusFilter: store.state.walletState.statusFilter,
      transactionFilter: store.state.walletState.transactionFilter,
      orderFilter: store.state.walletState.orderFilter,
      loadWalletDetail: (onSuccess, onError) {
        return store.dispatch(LoadWalletDashboardAction(
          businessId: store.state.homeState.business.id,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      loadBizBank: (onSuccess, onError) {
        return store.dispatch(GetBizBankAction(
          businessId: store.state.homeState.business.id,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      requestCashout: (amount, remark, onSuccess, onError) {
        return store.dispatch(GetCashoutAction(
          businessId: store.state.homeState.business.id,
          amount: amount,
          remark: remark,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      updateBizBank: (bankToken, bank, onSuccess, onError) {
        return store.dispatch(UpdateBizBankAction(
          businessId: store.state.homeState.business.id,
          bank: bank,
          bankToken: bankToken,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      changeFilter: ({month, fromDate, toDate, statusFilter, transactionFilter, orderFilter}) {
        return store.dispatch(ChangeFilterTransactionsAction(
          selectedMonth: month,
          fromDate: fromDate,
          toDate: toDate,
          status: statusFilter,
          transaction: transactionFilter,
          ordering: orderFilter,
          canEmptyDate: true
        ));
      },
      resetFilter: () {
        return store.dispatch(ResetFilterTransactionsAction());
      },
      requestTransactions: (isNext, fromDate, toDate, statusFilter, transactionFilter, orderFilter, onSuccess, onError) {
        return store.dispatch(LoadEarningAction(
          businessId: store.state.homeState.business.id,
          next: isNext 
            ? store.state.walletState.resTransactions != null
              ? store.state.walletState.resTransactions.next
              : null
            : null,
          fromDate: fromDate,
          toDate: toDate,
          status: statusFilter,
          transaction: transactionFilter,
          ordering: orderFilter,
          onSuccess: onSuccess,
          onError: onError
        ));
      },
      getStripeVerifyStatus: (onSuccess, onError) {
        return store.dispatch(
          GetStripeVerifyStatusAction(
            businessId: store.state.homeState.business.id,
            onSuccess: onSuccess,
            onError: onError
          )
        );
      },
      selectPhotoID: ({frontID, backID}) {
        return store.dispatch(SelectPhotoIDAction(
          backID: backID,
          frontID: frontID
        ));
      },
      submitVerifyStripe: (onSuccess, onError) {
        return store.dispatch(SubmitVerifyStripe(
          businessId: store.state.homeState.business.id,
          frontID: store.state.walletState.frontID,
          backID: store.state.walletState.backID,
          onSuccess: onSuccess,
          onError: onError
        ));
      }
    );
  }

  @override
  List<Object> get props => [permissions, balance, bank, loadWalletDetail, requestCashout, getStripeVerifyStatus];
}